<?php
//add_action( 'wp_print_scripts', 'wpcustom_inspect_scripts_and_styles' );
// Adds new sidebar widget
/**
 * Adds Foo_Widget widget.
 */
class Foo_Widget extends WP_Widget {

  /**
   * Register widget with WordPress.
   */
  function __construct() {
    parent::__construct(
      'foo_widget', // Base ID
      esc_html__( 'Variable Tag Widget', 'text_domain' ), // Name
      array( 'description' => esc_html__( 'A Widget that uses theme divs, and allows you to use whatever html element you want for the title.', 'text_domain' ), ) // Args
    );
  }

  /**
   * Front-end display of widget.
   *
   * @see WP_Widget::widget()
   *
   * @param array $args     Widget arguments.
   * @param array $instance Saved values from database.
   */
  public function widget( $args, $instance ) {
    echo $args['before_widget'];
    if ( ! empty( $instance['title'] ) ) {
      echo '<header class="entry-header"><'  . $instance['tag'] . ' class = "entry-title">' . $instance['title'] . '</' . $instance['tag'] . '></header>';
    }
    echo '<div class="textwidget">' . $instance['content'] . '</div>';
  }


  /**
   * Back-end widget form.
   *
   * @see WP_Widget::form()
   *
   * @param array $instance Previously saved values from database.
   */
  public function form( $instance ) {
    $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'New title', 'text_domain' );
    $tag = ! empty( $instance['tag'] ) ? $instance['tag'] : esc_html__( 'Html Tag', 'text_domain' );
    $content = ! empty( $instance['content'] ) ? $instance['content'] : esc_html__( 'Content', 'text_domain' );
    ?>
    <p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'text_domain' ); ?></label>
    <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
    </p>
    <p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'tag' ) ); ?>"><?php esc_attr_e( 'Html Tag:', 'text_domain' ); ?></label>
    <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'tag' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'tag' ) ); ?>" type="text" value="<?php echo esc_attr( $tag ); ?>">
    </p>
    <p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'content' ) ); ?>"><?php esc_attr_e( 'Content:', 'text_domain' ); ?></label>
    <textarea class = "widefat" id="<?php echo esc_attr( $this->get_field_id( 'content' ) ); ?>" rows= "16" cols = "20" name="<?php echo esc_attr( $this->get_field_name( 'content' ) ); ?>"><?php echo esc_attr( $content); ?></textarea>
    </p>
    <?php
  }

  /**
   * Sanitize widget form values as they are saved.
   *
   * @see WP_Widget::update()
   *
   * @param array $new_instance Values just sent to be saved.
   * @param array $old_instance Previously saved values from database.
   *
   * @return array Updated safe values to be saved.
   */
  public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    $instance['tag'] = ( ! empty( $new_instance['tag'] ) ) ? strip_tags( $new_instance['tag'] ) : '';
    $instance['content'] = ( ! empty( $new_instance['content'] ) ) ? $new_instance['content'] : '';

    return $instance;
  }

} // class Foo_Widget

// register Foo_Widget widget
function register_foo_widget() {
    register_widget( 'Foo_Widget' );
}
add_action( 'widgets_init', 'register_foo_widget' );



  if (function_exists('register_sidebar')) {
    register_sidebar(array(
      'name' => 'sydney',
      'id'   => 'home_ad_1',
      'description'   => 'Drop in widget for ad area.',
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h4>',
      'after_title'   => '</h4>'
    ));
    }

// Adds new sidebar widget
  if (function_exists('register_sidebar')) {
    register_sidebar(array(
      'name' => 'melbourne',
      'id'   => 'home_ad_2',
      'description'   => 'Drop in widget for ad area.',
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h4>',
      'after_title'   => '</h4>'
    ));
    }

// Adds new sidebar widget
  if (function_exists('register_sidebar')) {
    register_sidebar(array(
      'name' => 'brisbane',
      'id'   => 'home_ad_3',
      'description'   => 'Drop in widget for ad area.',
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h4>',
      'after_title'   => '</h4>'
    ));
    }

// Adds new sidebar widget
  if (function_exists('register_sidebar')) {
    register_sidebar(array(
      'name' => 'adelaide',
      'id'   => 'home_ad_4',
      'description'   => 'Drop in widget for ad area.',
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h4>',
      'after_title'   => '</h4>'
    ));
    }

// Adds new sidebar widget
  if (function_exists('register_sidebar')) {
    register_sidebar(array(
      'name' => 'perth',
      'id'   => 'home_ad_5',
      'description'   => 'Drop in widget for ad area.',
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h4>',
      'after_title'   => '</h4>'
    ));
    }

// Adds new sidebar widget
  if (function_exists('register_sidebar')) {
    register_sidebar(array(
      'name' => 'darwin',
      'id'   => 'home_ad_6',
      'description'   => 'Drop in widget for ad area.',
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h4>',
      'after_title'   => '</h4>'
    ));
    }

// Adds new sidebar widget
  if (function_exists('register_sidebar')) {
    register_sidebar(array(
      'name' => 'others',
      'id'   => 'home_ad_7',
      'description'   => 'Drop in widget for ad area.',
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h4>',
      'after_title'   => '</h4>'
    ));
    }



// Adds new sidebar widget
  if (function_exists('register_sidebar')) {
    register_sidebar(array(
      'name' => 'camberra',
      'id'   => 'home_ad_8',
      'description'   => 'Drop in widget for ad area.',
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h4>',
      'after_title'   => '</h4>'
    ));
    }

// Remove unwanted widgets from Dashboard
function remove_dashboard_widgets() {
  global$wp_meta_boxes;
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets');


// Add a new widget to the Dashboard
function contact_help() {
   echo 'If you have questions about working with the Brothels WordPress system, contact Gabriel at gabriel@chillidee.com.au or 1300 139 503';
}

function roux_add_dashboard_widgets() {
  wp_add_dashboard_widget(
                 'contact_help_widget',         // Widget slug.
                 'Need Help?',         // Title.
                 'contact_help' // Display function.
        );
}
add_action( 'wp_dashboard_setup', 'roux_add_dashboard_widgets' );

/*-----------------------------------------------------------------------------------*/
/*  Port Count View
/*-----------------------------------------------------------------------------------*/

// Created on: 8/8/2014
// By Kevin. For Count Tracking of Posts
// Source : http://wpsnipp.com/index.php/functions-php/track-post-views-without-a-plugin-using-post-meta/

function getPostViewsk($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '23');
        return "0 View";
    }
    return $count.' Views';
}
function setPostViewsk($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '23');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
/*-------------------Override parent themes post view function, As we now use ajax---------------
------------------------------------------------------------------------------------------------*/
  function getPostViews() {}
  function setPostViews() {}

/*-----------------Ajax call for page views----------------------------------------------------
-----------------------------------------------------------------------------------------------*/
function post_views_by_ajax() {
    $count_key = 'post_views_count';
    $postID = $_POST['id'];
    $count = get_post_meta($postID, $count_key, true);
   if(!isset( $count ) || $count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, $count);
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
}
   echo $count;
  die();
  }
add_action( 'wp_ajax_post_views_by_ajax', 'post_views_by_ajax' );
add_action( 'wp_ajax_nopriv_post_views_by_ajax', 'post_views_by_ajax' );
// Remove issues with prefetching adding extra views
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function prfx_image_enqueue() {
    global $typenow;
    if( $typenow == 'city' ) {
        wp_enqueue_media();
    }
}
add_action( 'admin_enqueue_scripts', 'prfx_image_enqueue' );

 /*-----------------Over ride parent themes comment function ----------------------------------
-----------------------------------------------------------------------------------------------*/
function wplook_comment( $comment, $args, $depth ) {
  $GLOBALS['comment'] = $comment;
  if ( $comment->comment_approved == 1 ) {
  switch ( $comment->comment_type ) :
    case '' :
  ?>

  <li <?php comment_class('vcard'); ?> id="comment-<?php comment_ID(); ?>">

    <div class="comment-gravatar fleft">
      <?php echo get_avatar( $comment, 50 ); ?>
    </div>

    <div class="entry-header-comments">
      <div class="reply fright"><?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?></div>

      <?php printf( __( '%s <span class="says">says on:</span>', 'wplook' ), sprintf( '<h3 class="fn">%s</h3>', get_comment_author_link() ) ); ?> </br />
      <div class="comment-date fleft"><i class="icon-clock"></i> <?php printf( __( '%1$s at %2$s', 'wplook' ), get_comment_date(), get_comment_time() ); ?></div>
      <div class="clear"></div>
    </div>
<div class="clear"></div>
    <div class="comment-body">
      <div class="entry-content">
        <?php if ( $comment->comment_approved == '0' ) : ?>
        <div class="comment-awaiting-moderation">
          <?php _e( 'Your comment is awaiting moderation.', 'wplook' ); ?>
        </div>
        <br />
        <?php endif; ?>
        <?php comment_text(); ?>

        <div class="clear"></div>
      </div>
    </div>
    <div class="clear"></div>

  <!-- </li># comment #-->
<?php
  break;
  case 'pingback' :
  case 'trackback' :
  ?>

  <li <?php comment_class(''); ?> id="trackback-<?php comment_ID(); ?>">
  <div class="comment-gravatar">
      <img src="http://www.gravatar.com/avatar/00000000000000000000000000000000?d=identicon&s=50">
    </div>
    <div class="comment-body">
    <div class="entry-header-comments">
    <span class="tooltip"></span>

     <span class="fn"><a><?php _e( 'Pingback/Trackback', 'wplook' ); ?></a></span>
    <span class="comment-date"><?php printf( __( '%1$s at %2$s', 'wplook' ), get_comment_date(), get_comment_time() ); ?></span>
    </div>
    <div class="entry-content">
    <?php if ( $comment->comment_approved == '0' ) : ?>
      <div class="comment-awaiting-moderation">
        <?php _e( 'Your comment is awaiting moderation.', 'wplook' ); ?>
      </div>
      <br />
      <?php endif; ?>
      <?php comment_author_link(); ?>
    </div>
    </div>
    <div class="clear"></div>
  <!-- </li># ping #-->
<?php break;   endswitch;
  }
}
?>
