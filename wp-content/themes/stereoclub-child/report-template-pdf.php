<?php

/* Template Name: report-template-pdf */


require_once("./wp-content/plugins/brothels-report/dompdf/autoload.inc.php");

use Dompdf\Dompdf;

$delaconcid = $_GET['delaconcid'];
$title = $_GET['title'];
$datefrom = $_GET['from'];
$dateto = $_GET['to'];
$position = $_GET['position'];
$referrals = $_GET['clicks'] + $_GET['images'];
$phones = $_GET['phones'];
$calls = $_GET['calls'];
$missed = $_GET['missed'];


//$cids = $_GET['id'];

/* Get delacon result */


$url = BR_delacon_API_link;
$fields = array(
    'userid' => BR_delacon_userid,
    'Auth' => BR_delacon_API_key,
    'password' => BR_delacon_password,
    'datefrom' => $datefrom,
    'dateto' => $dateto,
    'cids' => $delaconcid
);


//url-ify the data for the POST
$fields_string = '';
foreach ($fields as $key => $value) {
    $fields_string .= $key . '=' . $value . '&';
}

rtrim($fields_string, '&');
$fields_string = str_replace('"', '', $fields_string);
//open connection
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, count($fields));
curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//execute post
$result = curl_exec($ch);
curl_close($ch);

// cURL response to  xml -> json -> array 
$xml = new SimpleXMLElement($result);
$json = json_encode($xml, true);
$delacon = json_decode($json, TRUE);  //delacon details
//print_r($array);
//close connection

/* finish delacon curl */

$BR_PLUGIN_URL = BR_PLUGIN_URL;
$dompdf = new Dompdf();
$dompdf->setPaper('A4', 'portrait');
$html = "
   
<html>
    <head>
        <style>
            body{
                margin : 20px;        
            }
            #BR_report_paper{
                width: 800px;
            }
            #BR_report_paper p{
                margin: 0px;
                paddin:20px;
            }
            #BR_report_paper_header img{
                margin-left:250px;
                width:200px;
            }
            #BR_report_paper_content{
                text-align: center!important;
                margin: 30px;
                background-color: #333;
                width: 600px;
                height: 500px;    
            }
            #BR_report_paper_content img{
                width:600px;
            }
            .BR_title_font{
                margin: 0px;
                font-family: 'Bernard MT Condensed';
                font-size: 2.4em;
            }
            #BR_report_paper_subtitle{
                margin: 0px;
                font-size: 0.8em;
            }
            #BR_report_paper_footer{
                margin-top:80px;
                width:700px;
                //page-break-after: always;
            }
            #BR_report_paper_footer p{
                padding: 0px;
                font-size: 1em;
                font-weight: bold;
                font-family: 'Raleway', sans-serif;
            }
            #BR_footr_referrals{
                color: #F00;
            }
            #BR_report_paper_footer_note{  
                padding: 10px 50px;
                font-size: 0.8em;
                margin-top: 100px;
                background-color: #555;
                height: 60px;
            }
            #BR_report_paper_footer_note p{
                color: #FFF;
                font-size: 1.5em;
                padding: 0px;
                font-weight: normal;
                padding-top: 20px;
            }
            #BR_report_paper h2{
                color: #fff;
                text-align: center;
                font-size: 3em;;
                margin-top: 0px;
                font-family: 'IM Fell French Canon', serif;
                border-bottom: 1px solid #841212;
                font-weight: normal;
                font-style: italic;
                padding-top: 0px;
            }
            #BR_report_paper h3{
                padding-top: 5px;
                font-size: 1.2em;;
                font-family: 'Tahoma';
                color: #FFF;
                font-weight: normal;
            }
            #BR_report_paper h4{
                padding-top: 20px;
                font-size: 1.2em;;
                font-family: 'Raleway', sans-serif;
                color: #FFF;
                font-weight: normal;
            }
            #BR_report_paper h5{
                text-align: center;
                color: #fff;
                font-size: 1.5em;
                font-family: 'Roboto Slab', serif;
                font-weight: normal;
                margin:0px;
            }
            #perf_dates {
                color: #45DCC0!important;
                text-align: center;
                font-size: 1.5em;
                font-family: 'Raleway', sans-serif;
            }

            #BR_report_paper #BR_report_value_referrals{
                color : #FFF;
                text-align: right;
                padding: 10px 0px 10px 10px;
            }
            #BR_report_paper #BR_report_value_referrals span{
                color: #F00;
            }
            #BR_report_paper #BR_report_value_wrapper{
                font-weight: normal;
                font-family: 'Roboto Slab', serif;
                font-size: 2em;
                padding: 0px 140px;
                line-height: 2em;                        
            }
            .clear_float{
                clear: both;
            }
        </style>
    </head>
    <body id='BR_report_paper'>
        <div id='BR_report_paper_header'>
            <img src='./wp-content/plugins/brothels-report/assets/images/report-title.jpg'>
        </div>
        <div id='BR_report_paper_content'>
            <h4>Performance Report</h4>
            <h2 id='BR_report_value_title'>$title</h2>
            <h5>Brothels in Sydney, Position <span id='BR_report_value_position'>$position</span></h5>
            <span id='perf_dates'>From <span id='BR_report_value_from'>$datefrom</span> to <span id='BR_report_value_to'>$dateto</span></span>
            <div id='BR_report_value_wrapper'>
                <div id='BR_report_value_referrals'><span>$referrals</span> Website Referrals<br><span>$phones</span> Phone Views&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </div>
            </div>
            <div class='clear_float'></div>
            <h3>TOTAL CALLS-<span id='BR_report_value_calls'>$calls</span>   MISSED CALLS-<span id='BR_report_value_missed'>$missed</span></h3>
            <img src='./wp-content/plugins/brothels-report/assets/images/report-footer.jpg'>
        </div>
        <div id='BR_report_paper_footer'>
            <p><span id='BR_footr_referrals'>$referrals</span> Represents the number of customers <a href='http://brothels.com.au'>brothels.com.au</a> have sent to your site (Imagine how many have called you!)</p>
            <div id='BR_report_paper_footer_note'>
                <p>Tell: 1300 780 182 Email: ADVERTISE@ADULTPRESS.COM.AU<p>
            </div>
        </div>
    
";



$html.="</body>
</html>";

$dompdf->loadHtml($html);
$dompdf->render();
$dompdf->stream("brothels-report.pdf");


echo $html;
?>