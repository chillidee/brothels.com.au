<?php
/**
 * Template Name: Darwin
 *
 * @package WordPress
 * @subpackage StereoClub
 * @since StereoClub 1.0
 */
?>

<?php get_header(); ?>
<!-- Main -->
<div id="main" class="site-main container_12">
  
  <!-- Left column -->
  <div id="primary" class="grid_8">
    <!-- Latest Events -->
      <header class="entry-header">
        <!--<h1 class="entry-title"><?php the_title(); ?></h1>-->
        <h1 class="entry-title"><?php single_cat_title( '', true ); ?></h1>
        <!-- Kevin -->
        <?php if ( ! $paged || $paged < 2 ) { ?>
        <div id="counter"><i class="icon-eye"></i>
          <?php setPostViews(get_the_ID()); ?><?php echo getPostViews(get_the_ID()); ?></div>
        <?php } else { ?>
          <div id="counter"><i class="icon-eye"></i>
          <?php

             clean_post_cache(855);
             echo getPostViews(855) . "</div>";
          } ?>
        <div class="clear"></div>
      </header>
        
      <div class="entry-content-list">
        <?php $cat_args = array( 'post_type' => array( 'post', 'city' ), 'category_name' => 'brothels-darwin' );?>
              <?php $cat_posts = new WP_Query($cat_args); ?>
        <?php $args = array( 'post_type' => 'post', 'cat' => 'Brothels Sydney', 'post_status' => 'publish', 'paged'=> $paged); ?>
        <?php $wp_query = null;
        $wp_query = new WP_Query( $args ); ?>
        <?php if ( $cat_posts->have_posts() ) : ?>
          <?php $count = 0; ?>
          <?php while ( $cat_posts->have_posts() ) : $cat_posts->the_post();?>
          <?php
            $count++;
          ?>

          <?php if ($count == 1) : ?>  

            <article id="post-<?php the_ID(); ?>" <?php post_class('latest-item'); ?>>
              <?php if ( has_post_thumbnail() ) { ?>
                <div class="grid_4 alpha omega">
                  <figure>
                    <a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>">
                      <?php the_post_thumbnail('medium-ver-thumb'); ?>
                    </a>
                  </figure>
                </div>
              <div class="grid_4 alpha omega">
              <?php } else { ?>
                <div class="grid_7  alpha">
              <?php }?>
                <div class="entry-description">
                  <h1 class="entry-head"><a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                  <div class="short-description">
                    <p><?php echo wplook_short_excerpt('40');?></p>
                  </div>
                </div>
              </div>
              <div class="clear"></div>
              <div class="entry-footer">
              
                <time class="fleft" datetime="<?php echo get_the_date( 'c' ) ?>"><a href="<?php the_permalink(); ?>"><i class="icon-calendar"></i> <?php wplook_get_date(); ?></a></time> 
                <span class="author fleft"><a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><i class="icon-user"></i> <?php echo get_the_author(); ?></a></span>
                <!--<span class="likes fleft"><?php echo getPostLikeLink(get_the_ID()); ?></span>-->
                <!--<span class="views fleft"><i class="icon-eye"></i><?php setPostViews(get_the_ID()); ?><?php echo getPostViews(get_the_ID()); ?></span>-->
                <div id="kevin">
                  <?php 
                          
                        $count = get_post_meta('post_views_count');
                    echo $count;
                
                  ?>
                </div>

                <span class="button-readmore fright"><a href="<?php the_permalink(); ?>" title="<?php _e('read more', 'wplook'); ?>"><?php _e('read more', 'wplook'); ?></a></span>
                <div class="clear"></div>
              </div>
            </article>

          <?php else : ?>

            <article id="post-<?php the_ID(); ?>" <?php post_class('list-block-item'); ?>>
              <div class="margins">
                <?php if ( has_post_thumbnail() ) { ?>
                  <div class="entry-thumb">
                    <?php the_post_thumbnail('small-thumb'); ?>
                  </div>
                <?php } else { ?>
                  <div class="entry-date">
                    <div class="date"><?php echo get_the_date( 'j' ) ?></div>
                    <div class="month"><?php echo get_the_date( 'M' ) ?></div>
                  </div>
                <?php } ?>

                <div class="entry-description">
                  <h1 class="entry-head"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                  <div class="short-description">
                    <p><?php echo wplook_short_excerpt('40');?></p>
                    
                    
                    
                  </div>
                </div>
                <div class="clear"></div>
              </div>
            </article>

          <?php endif; ?>
              
          <?php endwhile; wp_reset_postdata(); ?>

          <div class="clear"></div>

          <!--<php wplook_content_navigation($format, $link, $in_same_cat = false, $excluded_terms = '', $taxonomy = 'Brothels Sydney' ) ?>-->

<?php
function current_paged( $var = '' ) {
    if( empty( $var ) ) {
        $wp_query2 = new WP_Query("category_name=brothels-darwin");
        if( !isset( $wp_query2->max_num_pages ) )
            return;
        $pages = $wp_query2->max_num_pages;
    }
    else {
        global $$var;
            if( !is_a( $$var, 'WP_Query' ) )
                return;
        if( !isset( $$var->max_num_pages ) || !isset( $$var ) )
            return;
        $pages = absint( $$var->max_num_pages );
    }
    if( $pages < 1 )
        return;
    $page = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
  //echo '<div style="width:100px; margin:auto; display:inline-block;">Page ' . $page . ' of ' . $pages . "</div>";
  echo '<div style="padding:40px 20px;">';
  if( $page <= $pages -1 ) { ?>
   <div class="" style="font-size:16px; line-height:16px; font-weight:bold; float:left;"><?php next_posts_link( 'Previous' ); ?></div>
   <div class="" style="font-size:16px; line-height:16px; font-weight:bold; float:right"><?php previous_posts_link( 'Next' ); ?></div>
  <?php } else{ ?>
   <div class="" style="font-size:16px; line-height:16px; font-weight:bold; float:right"><?php previous_posts_link( 'Next' ); ?></div>
  <?php
  }
  echo "</div>";
}
        current_paged(); 
?>    
        
    <?php else : ?>
      <article class="single-post">

        <div class="entry-content">
          <br>
          <div class="entry-content-post">
            <br />
            <?php _e('Sorry, no posts matched your criteria.', 'wplook'); ?>
          </div>
          <div class="clear"></div>

        </div>

      </article>

    <?php endif; ?>
    </div>
  </div> <!-- / #primary -->
  
<?php get_sidebar('darwin'); ?>
<div class="clear"></div>    
</div><!-- / #main -->
<?php get_footer(); ?>