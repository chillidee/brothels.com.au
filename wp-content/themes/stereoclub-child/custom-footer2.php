<?php
/*
 * Custome footer
 * Author: Amir Far @ chillidee.com.au
 * To customise footer with SEO standards
 * 
 * 
 */
?>
<footer id="custome-footer">
    <div class="grid_12">
        <div class="grid_4 alpha no-m-b capital">
            <div class="entry-header"><div class="entry-title"><a href="../brothels-escorts-in-nsw/" title="Top Brothels Escorts in NSW">New South Wales</a></div></div>
            <div class="textwidget">
                <ul>
                    <li><a href="./brothels-in-sydney/" title="Brothels in Sydney">Sydney</a></li>
                    <li><a href="../brothels-in-north-sydney/" title="Brothels in North Sydney">North sydney</a></li>
                    <li><a href="../brothels-in-wollongong/" title="Brothels in Wollongong">wollongong</a></li>
                    <li><a href="../brothels-in-brookvale/" title="Brothels in Brookvale">Brookvale</a></li>
                    <li><a href="../brothels-surry-hills/" title="Brothels Surry Hills">Surry Hills</a></li>
                    <li><a href="../brothels-parramatta-western-sydney/" title="Brothels in Parramatta & Western Sydney">Parramatta</a></li>
                    <li><a href="../brothels-in-newcastle-maitland/" title="Brothels Newcastle Maitland">Newcastle</a></li>
                    <li><a href="../brothels-central-coast/" title="Brothels in Central Coast">Central Coast</a></li>
                    <li><a href="../brothels-in-wagga-wagga/" title="Brothels in Wagga Wagga">Wagga Wagga</a></li>                  
                    <li><a href="../asian-brothels-sydney/" title="Asian Brothels in Sydney">Asian Brothels Sydney</a></li>                  
                </ul>       
            </div>
            <div class="entry-header"><div class="entry-title"><a href="../brothels-in-queensland/" title="Brothels in Queensland">Queensland</a></div></div>
            <div class="textwidget">
                <ul>
                    <li><a href="../brothels-in-brisbane/" title="Top Brothels in Brisbane">Brisbane</a></li>
                    <li><a href="../brothels-in-gold-coast-queensland/" title="Top Brothels in Gold Coast Queensland">Gold Coast</a></li>
                    <li><a href="../brothels-sunshine-coast/" title="Brothels in Sunshine Coast">Sunshine coast</a></li>
                    <li><a href="../brothels-in-cairns/" title="Brothels in Cairns">Cairns</a></li>
                    <li><a href="../brothels-in-townsville/" title="Brothels in Townsville">Townsville</a></li>
                    <li><a href="../brothels-in-toowoomba/" title="Brothels in Toowoomba">Toowoomba</a></li>
                    <li><a href="../brothels-in-maroochydore/" title="Brothels in Maroochydore">Maroochydore</a></li>
                    <li><a href="../asian-brothels-brisbane/" title="Asian Brothels in Brisbane">Asian Brothels Brisbane</a></li>
                </ul>
            </div>
        </div>
        <div class="grid_4 no-m-b capital">
            <div class="entry-header"><div class="entry-title"><a href="../brothels-escorts-in-victoria/" title="Brothels Escorts in Victoria">Victoria</a></div></div>
            <div class="textwidget">
                <ul>
                    <li><a href="../brothels-in-melbourne/" title="20 Top Brothels in Melbourne">Melbourne</a></li>
                    <li><a href="../brothels-in-geelong/" title="Brothels in Geelong">Geelong</a></li>
                    <li><a href="../asian-brothels-melbourne/" title="Asian Brothels in Melbourne">Asian Brothels Melbourne</a></li>
                </ul>       
            </div>
            <div class="entry-header"><div class="entry-title"><a href="../massage-parlours-escorts-in-south-australia/" title="Massage Parlours Escorts in South Australia">South Australia</a></div></div>
            <div class="textwidget">
                <ul>
                    <li><a href="../brothels-massage-parlours-escorts-in-adelaide/" title="Top Brothels Massage Parlours Escorts in Adelaide">Adelade</a></li>
                </ul>
            </div>
            <div class="entry-header"><div class="entry-title">Australian Capital Territoty</div></div>
            <div class="textwidget">
                <ul>
                    <li><a href="../brothels-massage-parlours-escorts-in-canberra/" title="Brothels Massage Parlours Escorts in Canberra">Canberra</a></li>
                </ul>       
            </div>
            <div class="entry-header"><div class="entry-title">Western Australia</div></div>
            <div class="textwidget">
                <ul>
                    <li><a href="../brothels-in-perth/" title="Top Brothels in Perth">Perth</a></li>
                    <li><a href="../brothels-in-fremantle/" title="Brothels in Fremantle">Fremantle</a></li>

                </ul>       
            </div>
            <div class="entry-header"><div class="entry-title">Connect with us</div></div>
            <div class="no-title">
                <?php dynamic_sidebar('f3-widgets'); ?>
            </div>
        </div>
        <div class="grid_4 omega no-m-b">
            <div class="entry-header"><div class="entry-title">About Us</div></div>
            <div class="no-title" id="about-us-wrapper">
                <?php dynamic_sidebar('f1-widgets'); ?>
            </div>

            <div class="entry-header"><div class="entry-title"><a href="../category/news/" title="Latest News and Posts on brothels blog page">News</a></div></div>
            <div class="no-title">
                <?php dynamic_sidebar('f2-widgets'); ?>
            </div>
        </div>
    </div>   
</footer>

