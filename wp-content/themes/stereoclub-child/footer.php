<?php
/**
 * The footer template
 *
 * @package WordPress
 * @subpackage StereoClub
 * @since StereoClub 1.0
 */
?>
<!-- Footer Widget area -->
<div class="site-footer container_12">
    <?php if (( ot_get_option('wpl_footer_widget_area') == 'on')) { ?>
        <!-- Status info -->
        <div style="display: none;" id="status-info" class="grid_12 no-p-b">

            <?php if (( ot_get_option('wpl_status') == 'on')) { ?>
                <?php
                $args = array(
                    'post_type' => 'post',
                    'ignore_sticky_posts' => 1,
                    'post_status' => 'publish',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'post_format',
                            'field' => 'slug',
                            'terms' => array('post-format-status')
                        )
                    )
                );
                ?>

                <?php $wp_query = null; ?>
                <?php $wp_query = new WP_Query($args); ?>
                <?php if ($wp_query->have_posts()) : ?>
                    <div class="status-title">
                        <h1><?php _e('Status:', 'wplook'); ?></h1>
                    </div>
                    <div id="slider-status" class="flexslider">
                        <ul class="slides">

                            <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                                <li><a href="<?php the_permalink() ?>"><?php the_title() ?> - <?php wplook_get_date(); ?></a></li>
                            <?php endwhile;
                            wp_reset_postdata(); ?>

                        </ul>
                    </div>
                    <div class="clear"></div>
                <?php endif; ?>
                <div class="clear"></div>
            <?php } ?>


            <?php if (is_active_sidebar('f1-widgets') || is_active_sidebar('f2-widgets') || is_active_sidebar('f3-widgets')) { ?>

                <?php if (is_active_sidebar('f1-widgets')) : ?>          
                    <!-- First Widget Area -->
                    <div class="grid_4 alpha no-m-b">
                        <?php dynamic_sidebar('f1-widgets'); ?>
                    </div>
                <?php endif; ?>

                <?php if (is_active_sidebar('f2-widgets')) : ?>          
                    <!-- Second Widget Area -->
                    <div class="grid_4 no-m-b">
                        <?php dynamic_sidebar('f2-widgets'); ?>
                    </div>
                <?php endif; ?>

                <?php if (is_active_sidebar('f3-widgets')) : ?>          
                    <!-- Third Widget Area -->
                    <div class="grid_4 omega no-m-b">
                        <?php dynamic_sidebar('f3-widgets'); ?>
                    </div>
                <?php endif; ?>

            <?php } ?>
        </div>
    <?php } ?>

<?PHP
// new footer added for SEO
require_once 'custom-footer.php';
?>


    <!-- Site info -->
    <div id="site-info" class="grid_12"><div class="margins">
            <?php if (ot_get_option('wpl_copyright')) {
                echo ot_get_option('wpl_copyright');
            } ?>  </div></div>

</div>
</div>
</div>















<?php
if (ot_get_option('wpl_google_analytics_tracking_code')) {
    // Google Analytics Tracking Code
    echo ot_get_option('wpl_google_analytics_tracking_code');
}
?>

<?php wp_footer(); ?>




<script>
    jQuery(document).ready(function ($) {
        $(".menu-item a").on('click', function () {
            var html = $(this).html();
            ga('send', 'event', 'Main Menu', 'Click', html);
        });
        $("#text-2 table a").on('click', function () {
            var html = $(this).html();
            ga('send', 'event', 'City by City Home Page Table', 'Click', html);
        });
        $(".entry-content-list .type-post .entry-head a").on('click', function () {
            var title = $("h1.entry-title").html();
            var html = $(this).html();
            ga('send', 'event', title, 'Click', html);
        });
        jQuery('#resident-dj-13').on('click', function () {
            ga('send', 'event', 'Daily Cravings widget Home Page', 'Click', 'Daily Crave');
        });
<?php /*
  //jQuery('img[alt="bluebirds_carmel"]').css({'width' : '150px' , 'height' : '150px' , 'margin-left' : '15px' , 'margin-top' : '15px'});
  //jQuery('img[alt="secret_liasions"]').css({'width' : '150px' , 'height' : '150px' , 'margin-left' : '15px' , 'margin-top' : '15px'});
  //jQuery('img[alt="aus_maid"]').css({'width' : '150px' , 'height' : '150px' , 'margin-left' : '15px' , 'margin-top' : '15px'});
  jQuery('.home').on('click',function(){ga('send','event','Main Menu','Click','Home');});
  jQuery('.sydney').on('click',function(){ga('send','event','Main Menu','Click','Sydney');});
  jQuery('.melbourne').on('click',function(){ga('send','event','Main Menu','Click','Melbourne');});
  jQuery('.brisbane').on('click',function(){ga('send','event','Main Menu','Click','Brisbane');});
  jQuery('.adelaide').on('click',function(){ga('send','event','Main Menu','Click','Adelaide');});
  jQuery('.perth').on('click',function(){ga('send','event','Main Menu','Click','Perth');});
  jQuery('.canberra').on('click',function(){ga('send','event','Main Menu','Click','Canberra');});
  jQuery('.darwin').on('click',function(){ga('send','event','Main Menu','Click','Darwin');});
  jQuery('.contact').on('click',function(){ga('send','event','Main Menu','Click','contact');});
  jQuery('.news').on('click',function(){ga('send','event','Main Menu','Click','News');});
  jQuery('#city-by-city-brothels-sydney').on('click',function(){ga('send','event','City by City Home Page Table','Click','Brothels Sydney');});
  jQuery('#city-by-city-escorts-sydney').on('click',function(){ga('send','event','City by City Home Page Table','Click','Escorts Sydney');});
  jQuery('#city-by-city-brothels-melbourne').on('click',function(){ga('send','event','City by City Home Page Table','Click','Brothels Melbourne');});
  jQuery('#city-by-city-escorts-melbourne').on('click',function(){ga('send','event','City by City Home Page Table','Click','Escorts Melbourne');});
  jQuery('#city-by-city-brothels-brisbane').on('click',function(){ga('send','event','City by City Home Page Table','Click','Brothels Brisbane');});
  jQuery('#city-by-city-escorts-brisbane').on('click',function(){ga('send','event','City by City Home Page Table','Click','Escorts Brisbane');});
  jQuery('#city-by-city-brothels-adelaide').on('click',function(){ga('send','event','City by City Home Page Table','Click','Brothels Adelaide');});
  jQuery('#city-by-city-escorts-adelaide').on('click',function(){ga('send','event','City by City Home Page Table','Click','Escorts Adelide');});
  jQuery('#city-by-city-brothels-canberra').on('click',function(){ga('send','event','City by City Home Page Table','Click','Brothels Canberra');});
  jQuery('#city-by-city-escorts-canberra').on('click',function(){ga('send','event','City by City Home Page Table','Click','Escorts Canberra');});
  jQuery('#city-by-city-brothels-perth').on('click',function(){ga('send','event','City by City Home Page Table','Click','Brothels Perth');});
  jQuery('#city-by-city-escorts-perth').on('click',function(){ga('send','event','City by City Home Page Table','Click','Escorts Perth');});
  jQuery('#city-by-city-brothels-darwin').on('click',function(){ga('send','event','City by City Home Page Table','Click','Brothels Darwin');});
  jQuery('#city-by-city-escorts-darwin').on('click',function(){ga('send','event','City by City Home Page Table','Click','Escorts Darwin');});
  jQuery('h1').find('a:contains("Asian Brothels in Sydney")').on('click',function(){ga('send','event','Home page Industry Telegram','Click','Asian Brothels in Sydney');});
  jQuery('h1').find('a:contains("Brothels in Maroochydore")').on('click',function(){ga('send','event','Home page Industry Telegram','Click','Brothels in Maroochydore');});
  jQuery('h1').find('a:contains("Brothels in Toowoomba")').on('click',function(){ga('send','event','Home page Industry Telegram','Click','Brothels in Toowoomba');});
  jQuery('h1').find('a:contains("Brothels in Wagga Wagga")').on('click',function(){ga('send','event','Home page Industry Telegram','Click','Brothels in Wagga Wagga');});
  jQuery('#post-1193').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Brothels Surry Hills');});
  jQuery('#post-845').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Cougar Town');});
  jQuery('#post-831').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Club 121');});
  jQuery('#post-825').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Claudias Penthouse');});
  jQuery('#post-817').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Centrefolds North Shore');});
  jQuery('#post-813').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Centrefolds Central Coast');});
  jQuery('#post-809').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Centrefolds of Albury');});
  jQuery('#post-804').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Centrefold Escorts');});
  jQuery('#post-795').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Casino Royale Escorts');});
  jQuery('#post-782').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Bondi Bikini Babes');});
  jQuery('#post-777').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Bollywood Escorts');});
  jQuery('#post-766').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Boardroom Escorts');});
  jQuery('#post-758').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Bliss Erotic Massage');});
  jQuery('#post-749').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Black cat Parlour');});
  jQuery('#post-745').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Billionaire Escorts');});
  jQuery('#post-741').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Babylonia');});
  jQuery('#post-733').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Room Service Escorts');});
  jQuery('#post-728').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Michelles');});
  jQuery('#post-724').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Angel Escorts');});
  jQuery('#post-717').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Amore');});
  jQuery('#post-706').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Adorable Asian Escorts');});
  jQuery('#post-692').on('click',function(){ga('send','event','Brothels Sydney Page','Click','A1 Temptations');});
  jQuery('#post-683').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Kyoto 206');});
  jQuery('#post-457').on('click',function(){ga('send','event','Brothels Sydney Page','Click','30 Cronulla');});
  jQuery('#post-453').on('click',function(){ga('send','event','Brothels Sydney Page','Click','12 Cottan');});
  jQuery('#post-449').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Breathless');});
  jQuery('#post-396').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Victorias Secrets');});
  jQuery('#post-392').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Centrefolds');});
  jQuery('#post-388').on('click',function(){ga('send','event','Brothels Sydney Page','Click','The Gateway Club');});
  jQuery('#post-383').on('click',function(){ga('send','event','Brothels Sydney Page','Click','City Touch');});
  jQuery('#post-379').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Infinity Bordelo');});
  jQuery('#post-364').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Xclusive');});
  jQuery('#post-401').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Ma Belle Cheri');});
  jQuery('#post-374').on('click',function(){ga('send','event','Brothels Sydney Page','Click','Tiffanys');});
  jQuery('#post-850').on('click',function(){ga('send','event','Brothels Melbourne Page','Click','Daily Planet');});
  jQuery('#post-841').on('click',function(){ga('send','event','Brothels Melbourne Page','Click','Club 859');});
  jQuery('#post-790').on('click',function(){ga('send','event','Brothels Melbourne Page','Click','California Club');});
  jQuery('#post-786').on('click',function(){ga('send','event','Brothels Melbourne Page','Click','Buterflys of Blackburn');});
  jQuery('#post-773').on('click',function(){ga('send','event','Brothels Melbourne Page','Click','Bollywood Escorts');});
  jQuery('#post-710').on('click',function(){ga('send','event','Brothels Melbourne Page','Click','Affordable Escorts');});
  jQuery('#post-677').on('click',function(){ga('send','event','Brothels Melbourne Page','Click','69 Relaxation');});
  jQuery('#post-499').on('click',function(){ga('send','event','Brothels Melbourne Page','Click','Show me Fire');});
  jQuery('#post-498').on('click',function(){ga('send','event','Brothels Melbourne Page','Click','Gotham City - House of Sin');});
  jQuery('#post-837').on('click',function(){ga('send','event','Brothels Brisbane Page','Click','Club 7');});
  jQuery('#post-762').on('click',function(){ga('send','event','Brothels Brisbane Page','Click','Bluebirds on Carmel');});
  jQuery('#post-753').on('click',function(){ga('send','event','Brothels Brisbane Page','Click','Secret Liasions');});
  jQuery('#post-737').on('click',function(){ga('send','event','Brothels Brisbane Page','Click','Australian Maid');});
  jQuery('#post-413').on('click',function(){ga('send','event','Brothels Brisbane Page','Click','Sensations on Spine');});
  jQuery('#post-409').on('click',function(){ga('send','event','Brothels Brisbane Page','Click','88 on Logan');});
  jQuery('#post-405').on('click',function(){ga('send','event','Brothels Brisbane Page','Click','The Viper Room');});
  jQuery('#post-436').on('click',function(){ga('send','event','Brothels Brisbane Page','Click','Yimi 476');});
  jQuery('#post-432').on('click',function(){ga('send','event','Brothels Brisbane Page','Click','Club 26');});
  jQuery('#post-428').on('click',function(){ga('send','event','Brothels Brisbane Page','Click','Happy Place 8');});
  jQuery('#post-423').on('click',function(){ga('send','event','Brothels Brisbane Page','Click','Montecito');});
  jQuery('#post-419').on('click',function(){ga('send','event','Brothels Brisbane Page','Click','Cleos');});
  jQuery('#post-799').on('click',function(){ga('send','event','Brothels Adelaide Page','Click','CCs');});
  jQuery('#post-485').on('click',function(){ga('send','event','Brothels Adelaide Page','Click','House of Ann');});
  jQuery('#post-481').on('click',function(){ga('send','event','Brothels Adelaide Page','Click','Studio 444');});
  jQuery('#post-477').on('click',function(){ga('send','event','Brothels Adelaide Page','Click','Studio 207');});
  jQuery('#post-472').on('click',function(){ga('send','event','Brothels Adelaide Page','Click','Regency 215');});
  jQuery('#post-468').on('click',function(){ga('send','event','Brothels Adelaide Page','Click','Adelaide City Penthouse');});
  jQuery('#post-701').on('click',function(){ga('send','event','Brothels Perth Page','Click','Ada Rose Gentlemens Club');});
  jQuery('#post-519').on('click',function(){ga('send','event','Brothels Perth Page','Click','La Cherie');});
  jQuery('#post-516').on('click',function(){ga('send','event','Brothels Perth Page','Click','City West Massage');});
  jQuery('#post-510').on('click',function(){ga('send','event','Brothels Perth Page','Click','Club 316');});
  jQuery('#post-506').on('click',function(){ga('send','event','Brothels Perth Page','Click','Caseys Massage');});
  jQuery('#post-821').on('click',function(){ga('send','event','Brothels Canberra Page','Click','Chrisindys');});
  jQuery('#post-697').on('click',function(){ga('send','event','Brothels Canberra Page','Click','ACT Pretty Girls');});
  jQuery('#post-629').on('click',function(){ga('send','event','Brothels Canberra Page','Click','Tiffanys Place');});
  jQuery('#post-440').on('click',function(){ga('send','event','Brothels Canberra Page','Click','Lollipop Lounge');});
  jQuery('#post-885').on('click',function(){ga('send','event','Brothels Darwin Page','Click','Darwin Escorts');}); */
?>
        jQuery('#gate-way-url').on('click', function () {
            ga('send', 'event', 'Gateway Brothels Listing', 'Click', 'URL Click');
        });
        jQuery('#gate-way-image').on('click', function () {
            ga('send', 'event', 'Gateway Brothels Listing', 'Click', 'Image Click');
        });
        jQuery('#gate-way-number').on('click', function () {
            ga('send', 'event', 'Gateway Brothels Listing', 'Click', 'Phone Number Click');
        });
        jQuery('#gate-way-sidebar-image').on('click', function () {
            ga('send', 'event', 'Gateway Brothels Listing', 'Click', 'Sidebar Image Listing');
        });
        jQuery('#hornsby-mobile-image').on('click', function () {
            ga('send', 'event', 'Hornsby142 Brothels Listing', 'Click', 'Image Listing');
        });
        jQuery('#hornsby142-view-site-link').on('click', function () {
            ga('send', 'event', 'Hornsby142 Brothels Listing', 'Click', 'View Site Link');
        });
    });
</script>

</body>

</html>