- Getting started
  - [Installing the plugin](installation.md)
  - [Updating the plugin](updates.md)
  - [Building a form](build.md)
  - [Import & Export](import-export.md)

- Elements
  - Layout Elements
    - [Columns / Grid](columns.md)
    - [Multi-parts / Multi-steps](multi-parts.md)

  - Form Elements
    - [Text](text.md)
    - [Dropdown](dropdown.md)
    - [Checkbox](checkbox.md)
  
  - HTML Elements
    - [Heading](heading.md)
    - [Spacer](spacer.md)
    - [Google Map](google-map.md)

- Field Validations
  - [Email validation](email-validation.md)
  - [Phone validation](phone-validation.md)

- Features 
  - [Conditional Logic](conditional-logic.md)
  - [Variable fields](variable-fields.md)

- Theme Styles & Colors
  - [Field Size](field-size.md)
  - [Field Colors](field-colors.md)

- Actions & Filters (for developers)
  - Actions
    - [action1](super_before_action_hook.md)
    
  - Filters
    - [filter1](super_string_filter_hook.md)

- Add-ons / Extensions
  - [Calculator](calculator-add-on.md)
  - [WooCommerce](woocommerce-add-on.md)
  - [PayPal](paypal-add-on.md)
  - [Zapier](zapier-add-on.md)
  - [CSV Attachment](csv-attachment-add-on.md)

- [Changelog](changelog.md)

- [Help & Support](support.md)



