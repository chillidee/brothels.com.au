<?php
/*
Plugin Name: Brothels Dashboard
Description: A functional dashboard for managaing advertising and brothels
Author: Che Jansen- Chillidee- The Digital Marketing Group
Version: 1.65
Author URI: http://chillidee.com.au
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

define( 'BD_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'BD_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

/* ---------------------Load Required Files------------------------------------------
----------------------------------------------------------------------------*/
require_once( BD_PLUGIN_PATH . 'inc/init.php' );
require_once( BD_PLUGIN_PATH . 'inc/brothel-class.php' );
require_once( BD_PLUGIN_PATH . 'inc/city-class.php' );
require_once( BD_PLUGIN_PATH . 'inc/ajax.php' );
require_once( BD_PLUGIN_PATH . 'inc/widget.php' );
require_once( BD_PLUGIN_PATH . 'inc/dashboard.php' );
require_once( BD_PLUGIN_PATH . 'inc/custom_login_screen.php' );
require_once( BD_PLUGIN_PATH . 'inc/helper.php' );
require_once( BD_PLUGIN_PATH . 'inc/update-view-meta.php' );


/* ---------------------Enqueue CSS and JS for admin-----------------------------------------
----------------------------------------------------------------------------*/
function load_custom_wp_admin_style() {
  global $post_type;
  // CSS and JS for the brothel post type
  if( 'brothel' == $post_type ){
    wp_register_style( 'brothels-dash-css', BD_PLUGIN_URL . 'assets/css/style.css', false );
    wp_enqueue_style( 'brothels-dash-css' );
    wp_enqueue_script( 'datepicker', BD_PLUGIN_URL .  'assets/js/jquery-ui.min.js', array('jquery'));
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_style( 'datepicker' );
    wp_register_script( 'brothels-dash-js', BD_PLUGIN_URL . 'assets/js/bd.js', array('jquery', 'jquery-ui-datepicker'));
    wp_enqueue_script( 'brothels-dash-js' );
  }
  // CSS and JS for the city post type
  if( 'city' == $post_type ){
    wp_register_style( 'city-dash-css', BD_PLUGIN_URL . 'assets/css/city-style.css', false );
    wp_enqueue_style( 'city-dash-css' );
    wp_register_script( 'data-table', BD_PLUGIN_URL .  'assets/js/data-tables.js', array('jquery'));
    wp_register_script( 'table-dnd', BD_PLUGIN_URL .  'assets/js/jquery.tablednd.0.8.min.js', array('jquery'));
    wp_register_script( 'city-bd-js', BD_PLUGIN_URL .  'assets/js/city-bd.js', array('jquery', 'jquery-ui-core','jquery-ui-datepicker', 'data-table', 'table-dnd'));
    wp_enqueue_script( 'city-bd-js' );
  }
  // CSS and JS for all admin pages
  else {
    wp_register_style( 'bd-generic', BD_PLUGIN_URL . 'assets/css/generic.css', false );
    wp_enqueue_style( 'bd-generic' );
  }
  wp_register_script( 'bd-genericjs', BD_PLUGIN_URL . 'assets/js/generic.js', array('jquery') );
  wp_enqueue_script( 'bd-genericjs' );
}

function bd_enqueue_reporting_scripts($hook) {
    if ('dashboard_page_brothels-reporting' != $hook)
        return;
        wp_enqueue_style('select-datatables', BD_PLUGIN_URL . 'assets/css/select.dataTables.min.css');
    wp_enqueue_style('reporting', BD_PLUGIN_URL . 'assets/css/reporting.css');
    //wp_enqueue_style('BR_datatable_style', '//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css');

    wp_enqueue_script('BR_pluginscript', BD_PLUGIN_URL . 'assets/js/reporting.js', array('jquery', 'jquery-ui', 'jquery-ui-datepicker'));
    wp_enqueue_script('data-tables', '//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js');
    wp_enqueue_script('reload-datables', '//cdn.datatables.net/plug-ins/1.10.12/api/fnReloadAjax.js');
    wp_enqueue_script('datatables-select', BD_PLUGIN_URL . 'assets/js/dataTables.select.min.js', array('data-tables'));
    wp_localize_script('ajax-script', 'my_ajax_object', array('ajax_url' => admin_url('admin-ajax.php')));
}
  /* ---------------------Enqueue CSS and JS for front-end-----------------------------------------
  ----------------------------------------------------------------------------*/
  function bd_load_custom_css_frontend() {
   if( get_post_type() == ( 'city' )) {
     wp_enqueue_style( 'bd-city-css',BD_PLUGIN_URL . 'assets/css/bd-city.css', false, NULL, 'all' );
     wp_register_script( 'bd_front', BD_PLUGIN_URL .  'assets/js/bd_front.js', array('jquery'));
     wp_enqueue_script( 'bd_front' );
   }
 }
/* --------------------- Load our custom template for the city single page-------------------------
----------------------------------------------------------------------------*/
add_filter('single_template', 'bd_cities_custom_template');

function bd_cities_custom_template($single) {
  global $wp_query, $post;

  /* Checks for single template by post type */
  if (isset( $post ) && $post->post_type == "city"){
    if(file_exists(BD_PLUGIN_PATH. 'templates/cities.php'))
      return BD_PLUGIN_PATH . 'templates/cities.php';
  }
  return $single;
}
/* --------------------- Prevent Brothels from being saved if they are in a city--------------------
----------------------------------------------------------------------------*/
function my_admin_error_notice() {
  global $wp_query;
  if (isset($wp_query->query_vars['bd_delete'])&& get_option('bd_error_msg')== '1'){
    $class = "update-nag";
    $message = "You must remove this brothel from all cities before deleting.";
    echo"<div class=\"$class\"> <p>$message</p></div>";
    update_option('bd_error_msg', '0' );
  }
}
/* --------------------- Javascript for the city and brothel post type list pages--------------------
----------------------------------------------------------------------------*/
add_action( 'admin_notices', 'my_admin_error_notice' );
function bd_print_inline_js() {
  echo "<script>
  function bd_get_stats(el){
    var form_data = {
     'action':'bd_get_brothels_stats',
     'post_id': jQuery(el).data('id')
   };
   jQuery.post(ajaxurl, form_data, function(response) {
     jQuery( el ).parent().html( response );
   });
 }
 function bd_get_city_clicks(el) {
   var form_data = {
     'action':'bd_get_city_clicks',
     'post_id': jQuery(el).data('id')
   };
   jQuery.post(ajaxurl, form_data, function(response) {
     jQuery( el ).parent().html( response );
   });
 }
 function bd_get_city_revenue(el) {
   var form_data = {
     'action':'bd_get_city_revenue',
     'post_id': jQuery(el).data('id')
   };
   jQuery.post(ajaxurl, form_data, function(response) {
     jQuery( el ).parent().html( response );
   });
 }
 var url = window.location.href.replace( 'bd_delete=false', '');
 window.history.pushState('Brothels', 'Brothels', url);
</script>";
}
/* --------------------- Create need database files on plugin activation--------------------
----------------------------------------------------------------------------*/
register_activation_hook( __FILE__, 'bd_create_our_tables' );
//Actions
add_filter('query_vars', 'parameter_queryvars' );
function parameter_queryvars( $qvars )
{
  $qvars[] = 'bd_delete';
  return $qvars;
}
  /* ---------------------Hard over-ride for strange lockout bug---------------------------
  ----------------------------------------------------------------------------*/
  function wpse_120179()
  {
    if( 'brothel' === get_current_screen()->post_type )
      add_filter( 'wp_check_post_lock_window', '__return_zero' );

  }
  add_action( 'load-edit.php', 'wpse_120179' );
  add_action( 'load-post.php', 'wpse_120179' );

  function wpse_220179()
  {
    if( 'city' === get_current_screen()->post_type )
      add_filter( 'wp_check_post_lock_window', '__return_zero' );

  }

  /*----------------------Custom Google fonts for dashboard
  -----------------------------------------------------------------------------*/
  function add_google_font() {
    wp_enqueue_style( 'my-google-font', '//fonts.googleapis.com/css?family=Montserrat|Droid+Sans|Material+Icons' );
  }

  /* ---------------------Actions and Filters----------------------------------------------
  ----------------------------------------------------------------------------*/
  add_action( 'load-edit.php', 'wpse_220179' );
  add_action( 'load-post.php', 'wpse_220179' );
  add_action( 'wp_trash_post', array('bd_brothel_cpt','my_wp_trash_post'));
  add_action( 'init', array( 'bd_init', 'bd_register_transaction_table'), 1 );
  add_action( 'switch_blog', array( 'bd_init', 'bd_register_transaction_table' ));
  add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );
  add_action( 'wp_enqueue_scripts', 'bd_load_custom_css_frontend' );
  add_action( 'init', array ('bd_brothel_cpt','create_brothels' ) );
  add_action( 'init', array('bd_city_cpt','create_cities' ) );
  add_action( 'pre_get_posts', array( 'bd_city_cpt','custom_parse_request_tricksy') );
  add_action( 'admin_init', array( 'bd_city_cpt','brothels_meta_1') );
  add_action( 'admin_init', array( 'bd_brothel_cpt','brothels_meta_1') );
  add_action( 'template_redirect', array( 'bd_brothel_cpt','remove_brothels_single_page' ));
  add_action( 'widgets_init', array('bd_widgets', 'bd_widgets_init' ));
  add_action( 'save_post', array( 'bd_city_cpt','bd_save_city_content'), 10, 2 );
  add_action( 'admin_print_scripts', 'bd_print_inline_js' );
  add_action( 'manage_posts_custom_column' , array( 'bd_brothel_cpt', 'display_brothels_columns'), 10, 2 );
  add_action( 'manage_posts_custom_column' , array( 'bd_city_cpt', 'display_cities_columns'), 10, 2 );
  add_action('admin_menu', array( 'bd_dashboard', 'bd_register_menu') );
  add_action( 'admin_menu', array( 'bd_login_screen','bd_register_menu' ));
  add_action( 'admin_enqueue_scripts', 'add_google_font' );
  add_action( 'admin_enqueue_scripts', 'bd_enqueue_reporting_scripts');

//Filters
  add_filter('admin_footer',array( 'bd_city_cpt','bd_add_brothel_form'));
  add_filter('admin_footer',array( 'bd_brothel_cpt','bd_add_note_form'));
  add_filter('admin_footer',array( 'bd_brothel_cpt','bd_vc_form' ));
  add_filter('admin_footer',array( 'bd_brothel_cpt','bd_add_contact_form'));
  add_filter('admin_footer',array( 'bd_brothel_cpt','bd_edit_contact_form'));
  add_filter('admin_footer',array( 'bd_brothel_cpt','bd_add_extra_descriptions'));
  add_filter('admin_footer',array( 'bd_city_cpt','bd_choose_descriptions'));
  add_filter('admin_footer',array( 'bd_city_cpt','bd_add_brothels_form'));
  add_filter('admin_footer',array( 'bd_city_cpt','bd_really_remove_brothel'));
  add_filter('admin_footer',array( 'bd_city_cpt','bd_assign_top_spot'));
  add_filter('admin_footer',array( 'bd_city_cpt','confirm_assign_top_spot' ));
  add_filter('admin_footer',array( 'bd_city_cpt','transaction_details' ));
  add_filter('admin_footer',array( 'bd_city_cpt','bd_remove_top_6' ));
  //add_filter('admin_footer',array( 'bd_city_cpt','bd_city_statistics' ));
  add_filter('admin_footer',array( 'bd_helper','create_menu' ));
  add_filter( 'post_type_link', array( 'bd_city_cpt','custom_remove_cpt_slug'), 10, 3 );
  add_filter( 'manage_brothel_posts_columns' , array( 'bd_brothel_cpt', 'add_sticky_column' ));
  add_filter( 'manage_city_posts_columns' , array( 'bd_city_cpt', 'bd_custom_city_columns' ));
  add_filter( 'login_redirect', array( 'bd_login_screen', 'my_login_redirect'), 10, 3 );

  ?>
