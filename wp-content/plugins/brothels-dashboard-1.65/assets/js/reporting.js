/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global ajaxurl */

var DateFormat = 'dd-mm-yy';
var BR_delacon_API_key = '660_8w2PZ6VhpWExPlozKg5faV1nT9gCbegGe6/IKUkyc1BmoQJTcfoJkSyizOn0/Ps7';
var BR_delacon_userid = 'Angelica.Brenton@adultpress.com.au';
var BR_delacon_password = 'ABadp!2';
var BR_delacon_API_link = 'http://vxml5.delacon.com.au/site/report/report.jsp';


jQuery(document).ready(function ($) {

    var $select = $('#bd_report_categories').selectize({
        maxItems: 100,
        plugins: ['remove_button'],
        items: []
    });
    var selectedCities = $select[0].selectize

    selectedCities.on('change', function () {
        var test = selectedCities.getValue();
    })
    $('#edit_templates').on('click', function () {
        $('#template_edit_section').show();
    })
    $('#create_template').on('click', function () {
        $('#new_template_name').val('');
        $('#create_template_overlay').show();
        $('#loading_overlay').show();
    })
    $('#cancel_delete_template').on('click', function () {
        $('.template_overlay').hide();
        $('#loading_overlay').hide();
    })
    $('#loading_overlay').on('click', function () {
        $('.template_overlay').hide();
        $(this).hide();
    })
    $('#delete_template').on('click', function () {
        $('#deleting_template_text').hide();
        $('#deleting_template').show();
        data = {
            action: 'bd_delete_template',
            id: $('#delete_template_id').val()
        }
        $.post(ajaxurl, data, function (response) {
            $('#deleting_template_text').show();
            $('#deleting_template').hide();
            $('.template_overlay').hide();
            $('#loading_overlay').hide();
            $('#template-button-' + $('#delete_template_id').val()).hide();
            $('#delete-button-' + $('#delete_template_id').val()).hide();
        })
    })
    $('body').on('click', '.template_button', function () {
        $('#current_template').html($(this).data('name'));
        $('#template_wp_edior').html('');
        $('#bd_current_template_id').val($(this).data('id'))
        data = {
            action: 'bd_load_email_template_editor',
            id: $(this).data('id')
        }
        $.post(ajaxurl, data, function (response) {
            $('#template_wp_edior').html(response)
        })
    })
    $('body').on('click', '.delete_template', function () {
        $('#delete_template_overlay').show();
        $('#loading_overlay').show();
        $('#delete_template_id').val($(this).data('id'))
        $('#delete_template_name').html($(this).data('name'))
    })

    $('#close_template_overlay').on('click', function () {
        $('#template_edit_section').hide();
        $('#bd_load_report').click();
    })

    $('#save_template').on('click', function () {
        $('#save_template_text').hide();
        $('#saving_template').show();
        const html = (tinymce.activeEditor ? tinymce.activeEditor.getContent() : $('#bd_template_editor').val())
        data = {
            action: 'bd_edit_email_template',
            id: $('#bd_current_template_id').val(),
            html: html
        }
        $.post(ajaxurl, data, function (response) {
            $('#save_template_text').show();
            $('#saving_template').hide();
        })
    })

    $('#bd_load_report').on('click', function () {
        reportingTable.ajax.reload();
    })

    $('#bd_report_from').datepicker({dateFormat: DateFormat});
    $('#bd_report_from').datepicker("setDate", "-7");
    $('#bd_report_to').datepicker({dateFormat: DateFormat});
    $('#bd_report_to').datepicker("setDate", "0");

    function buildSearchData() {
        var data = {
            "action": 'bd_test',
            "cities": selectedCities.getValue(),
            "date_from": $('#bd_report_from').val(),
            "date_to": $('#bd_report_to').val()
        }
        return data;
    }

    var reportingTable = $('#bd_reporting_table').DataTable({
        searching: true,
        bServerSide: false,
        paging: false,
        ordering: true,
        "ajax": {
            "url": ajaxurl,
            "type": "POST",
            "data": buildSearchData,
        },
        rowId: 'brothel_id',
        "serverSide": false,

        language: {
            select: {
                rows: {
                    _: "<span id = 'sending_reports'>Send Report to %d brothels</span><div class='loader white' id ='sending_reports_spinner'><svg class='circular' viewBox='25 25 50 50'><circle class='white' cx='50' cy='50' r='20' fill='none' stroke-width='2' stroke-miterlimit='10'/></svg></div>",
                    0: ""
                }
            },
            processing: "<div class='showbox'><div class='loader'><svg class='circular' viewBox='25 25 50 50'><circle class='path' cx='50' cy='50' r='20' fill='none' stroke-width='2' stroke-miterlimit='10'/></svg></div></div>"
        },
        processing: true,
        dom: 'Bfrtip',
        select: true,
        buttons: [
            {
                text: 'Select all',
                action: function () {
                    reportingTables.rows().select();
                }
            },
            {
                text: 'Select none',
                action: function () {
                    table.rows().deselect();
                }
            }
        ],
        select: {
            style: 'multi',
            selector: 'td:first-child'
        },
        "paging": false,
        "serverSide": true,
        "searching": true,
        "processing": true,
        "columns": [
            {
                "className": 'select-checkbox',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            {"data": "name",
                "className": "open_inner_table"},
            {"data": "position",
                "className": "open_inner_table position_header"},
            {"data": "website_clicks",
                "className": "open_inner_table"},
            {"data": "image_clicks",
                "className": "open_inner_table"},
            {"data": "phone_clicks",
                "className": "open_inner_table"},
            {"data": "total_calls",
                "className": "open_inner_table"},
            {"data": "missed_calls",
                "className": "open_inner_table"},
            {"data": null,
                "render": function (data, type, row) {
                    var select = '<select class = "bd_template_select" data-id = "' + data.brothel_id + '">'
                    $.each(data.templates, function (index, value) {
                        select += '<option value = "' + value.id + '"' + (value.id == data.template ? ' selected' : '') + '>' + value.name + '</option>'
                    });
                    select += '</select>'
                    return select;
                }
            },
            {
                "targets": -1,
                "data": null,
                "render": function (data, type, row) {
                    return (data.csv
                            ? '<a href="data:text/csv;charset=utf-8,' + escape(data.csv) + '" download="' + data.name + '.csv"><i class="material-icons">&#xE2C4;</i></a>'
                            : "")
                },
            }
        ],
        "order": [[2, 'asc']],
        "createdRow": function (row, data, index) {
            if (!data.contact[0].email)
                $(row).addClass('disable_row');
        }
    });

    $('#bd_help').on('click', function () {
        $('#help_section').toggle();
    })

    $('#bd_reporting_settings').on('click', function () {
        $('#setting_section').show()
    })
    $('#close_settings_overlay').on('click', function () {
        $('#setting_section').hide()
    })
    $('#save_settings').on('click', function () {
        $('#save_settings_text').hide();
        $('#saving_settings').show();
        var data = $('#bd_reporting_settings_form').serialize()
        $.post(ajaxurl, data, function (response) {
            $('#save_settings_text').show();
            $('#saving_settings').hide();
            var settings = JSON.parse(response);
            console.log(settings)
            console.log(settings['test_mode'])
            if (settings['test_mode'] === 'on')
                $('#test_mode_on').show();
            else
                $('#test_mode_on').hide();
        })
    })

    $('body').on('change', '.bd_template_select', function (e) {
        $('.dataTables_processing').show();
        data = {
            action: 'bd_assign_brothel_template',
            brothel_id: $(this).data('id'),
            template_id: $(this).val()
        }
        $.post(ajaxurl, data, function (response) {
            set = JSON.parse(response)
            console.log(set)
            $('.dataTables_processing').hide();
            data = reportingTable.row('#' + set.brothel_id).data();
            data.template = set.template_id
            reportingTable.row('#' + set.brothel_id).data(data)
            console.log(reportingTable.row('#' + set.brothel_id).data())
        })
    })

    $('body').on('click', '.select-info', function () {
        var selected = reportingTable.rows({selected: true}).data();
        var collector = []
        $('#sending_reports').hide();
        $('#sending_reports_spinner').show();
        $.each(selected, function (index, value) {
            collector.push(value);
        });
        data = {
            action: 'bd_send_reports',
            data: collector,
            date_range: [$('#bd_report_from').val(), $('#bd_report_to').val()]
        }
        console.log(data);
        $.post(ajaxurl, data, function (response) {
            console.log(response);
            $('#sending_reports').show();
            $('#sending_reports_spinner').hide();
            console.log(response)
        })
    })

    $('#save_new_template').on('click', function () {
        if (!$('#new_template_name').val()) {
            alert('Please enter a name for this template!')
            return false;
        }
        data = {
            action: 'bd_create_template',
            name: $('#new_template_name').val()
        }
        $.post(ajaxurl, data, function (response) {
            $('#template_button_holder').append(response)
            $('#create_template_overlay').hide();
            $('#loading_overlay').hide();
        })
    })


    $('.select-item').on('click', function () {
        var bdRows = reportingTable.rows({selected: true})
    })

    $('#rt_checkall').on('click', function () {
        if ($(this).prop('checked')) {
            var bdRows = reportingTable.rows().data();
            $.each(bdRows, function (index, value) {
                if (value.contact[0].email) {
                    $('#' + value.brothel_id + ' .select-checkbox').click();
                    $('.inner_table').hide();
                }
            });
        } else {
            reportingTable.rows().deselect();
        }
    })

    $('#bd_reporting_table').on('click', '.edit_contact', function () {
        $('.inner_table input').prop('disabled', false);
        $(this).html('&#xE161');
        $('.edit_contact').off();
        $(this).addClass('save_contact').removeClass('edit_contact');
    });

    $('#bd_reporting_table').on('click', '.save_contact', function () {
        email = $("input[name='email']").val()
        console.log(email)
        var re = /\S+@\S+\.\S+/;
        if (email && !re.test(email)) {
            alert('Please enter a valid email address')
            return false;
        }
        $(this).html('&#xE254;');
        $('.save_contact').off();
        $(this).addClass('edit_contact').removeClass('save_contact');
        data = $(this).closest('.edit_contact_form').serialize();
        $.post(ajaxurl, data, function (response) {
            $('.inner_table input').prop('disabled', true);
            if (response) {
                $('#' + response).removeClass('disable_row')
            }
        })
    })


    function format(d) {
        // `d` is the original data object for the row
        return '<form class = "edit_contact_form">' +
                '<div><input type ="hidden" name = "brothel_id" value ="' + d.brothel_id + '">' +
                '<div><input type ="hidden" name = "action" value ="bd_edit_reporting_contact">' +
                '<div><label>Contact Name:</label>' +
                '<input name = "Name" type ="text" value = "' + (d.contact[0].Name ? d.contact[0].Name : '') + '" disabled></div>' +
                '<div><label>Contact Phone:</label>' +
                '<input name = "Phone" type ="text" value = "' + (d.contact[0].Phone ? d.contact[0].Phone : '') + '" disabled></div>' +
                '<div><label>Mobile:</label>' +
                '<input name = "Mobile" type ="text" value = "' + (d.contact[0].Mobile ? d.contact[0].Mobile : '') + '" disabled></div>' +
                '<div><label>Email:</label>' +
                '<input name = "email" type ="text" value = "' + (d.contact[0].email ? d.contact[0].email : '') + '" disabled></div>' +
                '<div class ="reporting_info"><label>Phone:</label>' +
                '<span>' + (d.phone ? d.phone : '') + '</span></div>' +
                '<div class ="reporting_info"><label>Address:</label>' +
                '<span>' + (d.address ? d.address : '') + '</span></div>' +
                '<i class="material-icons edit_contact">&#xE254;</i>' +
                '</form>' +
                '<div id = "reporting_stats_holder"></div>';
    }


    $('#bd_reporting_table').on('click', '.open_inner_table', function () {
        $('.inner_table').hide();
        var tr = $(this).parent();
        var row = reportingTable.row(tr);

        if (row.child.isShown()) {
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child(format(row.data())).show();
            tr.next().addClass('inner_table');
            tr.addClass('shown');
            thisData = reportingTable.row('#' + tr.attr('id')).data()
            console.log(thisData)
            data = {
                action: 'bd_get_reporting_stats',
                brothel_id: thisData.brothel_id,
                category_id: thisData.category_id
            }
            $.post(ajaxurl, data, function (response) {
                stats = JSON.parse(response);
                console.log(stats);
                table = '<table class = "stats_table">' +
                        '<thead>' +
                        '<tr><th>Report Date</th><th><Sent by</th><th>Page</th><th>Website referrals</th><th>Total Calls</th><th>Missed Calls</th><th>Email</th><th>Date Range</th></tr></thead><tbody>';
                $.each(stats, function (index, value) {
                    table += '<tr><td>' + value.date + '</td><td>' + value.user_id + '</td><td>' + value.page + '</td><td>' + value.website_referrals + '</td><td>' + value.total_calls + '</td><td>' + value.missed_calls + '</td><td>' + value.email + '</td><td>' + value.date_range + '</tr>'
                });
                table += '<tbody></table>'
                $('#reporting_stats_holder').html(table)
            })
        }
    });

    $('#bd_reporting_table').on('click', '.disable_row .select-checkbox', function () {
        var tr = $(this);
        var row = reportingTable.row(tr);

        row.deselect();
        alert('You must add an email address to select this brothel');
        return false;
    });

});
