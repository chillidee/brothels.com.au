jQuery.noConflict();
jQuery( document ).ready(function($) {
    var mult = false,
    prev = 0;

$('input').keypress(function(e) {
    if (!mult) {
        mult = true;
        prev = e.which;
        setTimeout(function() {
            mult = false;
        }, 2000)
    }
    else if (prev != e.which) {
        mult = false;
    }
    else {
        return false;
    }
});
  $('textarea').keypress(function(e) {
    if (!mult) {
        mult = true;
        prev = e.which;
        setTimeout(function() {
            mult = false;
        }, 2000)
    }
    else if (prev != e.which) {
        mult = false;
    }
    else {
        return false;
    }
})
  $( '.datepicker' ).datepicker({
                    dateFormat: "dd-mm-yy"
              });
  $( '#bd_back_to_wordpress' ).on( 'click', function() {
         window.history.back();
    });
    $('#bd_add_brothels_datatable').DataTable();
    $( 'input[name="page_id"]' ).val( post_id );
    $( '#close_add_brothels_form img' ).on( 'click',function() {
       $( '#bd_add_brothels_form' ).hide();
       $( '#underlay' ).hide();
    });  
  $( '#open_add_brothel_form' ).on( 'click',function(e) {
    e.preventDefault();
    if ( $( '#publish' ).val() == 'Publish' ) {
          alert( 'You must save this city before adding brothels.' ); 
    }
    else {
       $("html, body").animate({ scrollTop: 0 }, "slow");
       $( '#bd_add_brothels_form' ).show();
       $( '#underlay' ).show();
    }
  });
  $( '#bd_add_new_brothels_button' ).on('click',function(e) {
       e.preventDefault();
       var form_data = $( '#bd_add_brothels_to_page' ).serialize();
       $.post(ajaxurl, form_data, function(response) {
             $( '#brothel_add_contact_div' ).hide();
             $( '#underlay' ).hide();
       });
  });
  $( 'body' ).on('change', '.bd_add_brothels_checkbox', function() {
        var ischecked= $(this).is(':checked'),
        id = $( this ).next().val();
           if(ischecked){
                $( '.bd_status[data-id="' + id + '"]' ).show();
           }
           else {
                $( '.bd_status[data-id="' + id + '"]' ).hide();
           }
  });  
  $( '#bd_add_new_brothels_ajax' ).on('click',function(e) {
       e.preventDefault();
       var status = $( '.bd_status' );
       errors = 0;
       $.each( status, function( key, value ) {
         if ( $(this).is(":visible") && $(this).hasClass( 'bd_alert' ) || $(this).is(":visible") && $(this).hasClass( 'bd_error' ) ) {
               errors++;
         }
        });
       if ( errors > 0 ) {
              alert( 'Please fix the errors' );
       }
    else { 
      $( '#white_underlay' ).show();
      $( '.generic_loader' ).show();
      form_data = $( '#bd_add_brothels_to_page' ).serialize();
      $.post(ajaxurl, form_data, function(response) {
          location.reload();
       });
    }
  });
  $( '.bd_alert' ).on( 'click',function() {
       $("html, body").animate({ scrollTop: 0 }, "slow");
       $( '#bd_choose_descriptions_output' ).html('');
       post_id = $(this).data( "id" ),
       data = {
          'action': 'populate_description_selects',
          'post_id': post_id
       };
       $.post(ajaxurl, data, function(response) {
          $( '#underlay2' ).show();
          $( '#bd_choose_descriptions_form' ).show();
          $( '#choose_descriptions_post_id' ).val( post_id );
          $( '#bd_choose_descriptions_output' ).html(response);
       });
  });
  $( '#close_choose_description_popup' ).on( 'click',function() {
       $( '#underlay2' ).hide();
       $( '#bd_choose_descriptions_form' ).hide();
  });
  $( '#bd_complete_description_choice' ).on( 'click',function(e) {
       e.preventDefault();
       var post_id = $( '#choose_descriptions_post_id' ).val();
          $( '#underlay2' ).hide();
          $( '#bd_choose_descriptions_form' ).hide();
          $( '.bd_description_choice_holder[data-id="' + post_id + '"]' ).val( $( 'input[name=description_number]:checked' ).val() );
          $( '.bd_status[data-id="' + post_id + '"]' ).removeClass( 'bd_alert' ).addClass( 'bd_ok' );
   });
  $( '.bd_remove_brothel_from_page' ).on( 'click', function(e) {
        e.preventDefault();
        var remove_id = $( this ).data( 'id' ),
            obj = $( '.post_id_holder' ),
            error = 0;
        $.each( obj, function() {
            if ( $( this ).val() == remove_id ) {
                  error++;
             }
        });
             if ( error > 0 ) {
                  alert( 'You cannot remove a brothel that is currently being advertised' );
             }
          else {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        $( '#bd_really_remove_brothel_popup' ).show();
        $( '#underlay' ).show();
        $( '#bd_really_remove_brothel_id' ).val( $( this ).data( 'id' ));
  }
  });
  $( '.close_really_remove_popup' ).on( 'click', function() {
        $( '#bd_really_remove_brothel_popup' ).hide();
        $( '#underlay' ).hide();
  });
  $( '#bd_remove_brothel_from_page' ).on( 'click', function(e) {
        e.preventDefault();
        data = {
             'action': 'bd_remove_brothel_from_page',
             'brothel_id': $( '#bd_really_remove_brothel_id' ).val(),
             'page_id': post_id
        };
    $( '#white_underlay' ).show();
    $( '.generic_loader' ).show();
    $.post(ajaxurl, data, function(response) {
          location.reload();
      //alert( response );
       });
  }); 
  adv_count = $( '#bd_adv_count' ).val();
  $("#table-dnd1").tableDnD({
     onDrop: function(table, row) {
         order = {},
         rows = $( '.city-drag-table' );
         $.each(rows, function( index ) {
              order[index] = $( this ).data( 'id' )
         });  
        var form_data = {
                 action:'bd_rearrange_brothels',
                 page_id: post_id,
                 order:JSON.stringify(order)
           };
         bottom_Rows = $( '.position_td' ); 
         $.each(bottom_Rows, function( this_index ) {
              console.log( this_index );
              new_pos =  +adv_count + +this_index +1;
            //  console.log( new_pos );
              $( this ).html(new_pos );
         });
      $.post(ajaxurl, form_data, function(response) {
       });  
      }
  });
  $( 'body' ).on('click', '.enter_desc', function(e) {
      $("html, body").animate({ scrollTop: 0 }, "slow");
      e.preventDefault();
      $( '#brothel_add_extra_description_form' ).show();
      $( '#underlay2' ).show();
      $( '#bd_add_desc_id' ).val( $( this ).data( 'id' ) );
   });
  $( '#close_extra_description' ).on('click',function() {
      $( '#brothel_add_extra_description_form' ).hide();
      $( '#underlay2' ).hide();
   });
         $( '#bd_add_extra_description_button' ).on('click',function(e) {
           e.preventDefault();
           form_data = $( '#abd_add_extra_description' ).serialize();
         if ( $( '#bd_add_desc_textarea' ).val() == "" ) {
               alert( 'You must enter a description' );
         }
         else {
           $.post(ajaxurl, form_data, function(response) {
             $( '#brothel_add_extra_description_form' ).hide();
             $( '#underlay2' ).hide();
             console.log( response );
             $( '.bd_status[data-id="' + response + '"]' ).removeClass( 'bd_error' ).addClass( 'bd_ok' );
       });
         }
       });
  $( '.top_3' ).on( 'click',function() {
       $( '#assign_top_spots' ).show();
       $( '#underlay' ).show();
       $("html, body").animate({ scrollTop: 0 }, "slow");
       var position = $( this ).data( 'id' ),
       form_data = {
             action: 'populate_assign_top_spots',
             post_id: post_id,
             pos: position,
             top_6_action: 'current',
             security: $( '#bd_populate_eligible_brothels' ).val()
    };
        $.post(ajaxurl, form_data, function(response) {
        $( '#bd_assign_content_holder' ).html( response );
            $('input[name="chosen_brothel"]').on( 'change', function() {
      if ( $( this ).parents( '.in_image_spot' ).length ) {
               alert( 'This brothel is already being advertised' );
      }
    });
       });
    }); 
  $( '#close_assign_top_spots img' ).on( 'click',function() {
       $( '#assign_top_spots' ).hide();
       $( '#underlay' ).show();
       $( '#underlay2' ).hide();
    });
     $( '.top_6' ).on( 'click',function() {
       $("html, body").animate({ scrollTop: 0 }, "slow");
       var position = $( this ).data( 'id' );
       $( '#assign_top_spots' ).show();
       $( '#underlay' ).show();
    var form_data = {
             action: 'populate_assign_top_6',
             post_id: post_id,
             pos: position,
             top_6_action: 'current',
             security: $( '#bd_populate_eligible_brothels' ).val()
    };
        $.post(ajaxurl, form_data, function(response) {
        $( '#bd_assign_content_holder' ).html( response );
              $('input[name="chosen_brothel"]').on( 'change', function() {
      if ( $( this ).parents( '.in_image_spot' ).length ) {
               alert( 'This brothel is already being advertised' );
      }
    });
       });
    });
  $( '#close_assign_top_spots img' ).on( 'click',function() {
       $( '#assign_top_spots' ).hide();
       $( '#underlay' ).hide();
    });
  $( '#open_confirm_assign_popup' ).on( 'click',function() {
       $("html, body").animate({ scrollTop: 0 }, "slow");
       $( '#confirm_assign_top_spot' ).show();
       $( '#underlay2' ).show();
     var date_picked = 0;
     to_min_date = 0,
     to_max_date = '';
     if ( $('#bd_available_days').val()!= undefined ) { to_max_date = $('#bd_available_days').val() };
     if ( $( '#bd_available_days_min' ).val() != undefined ) { to_min_date = $('#bd_available_days_min').val() };
    $( "#to_datepicker").datepicker("destroy");
    $( "#from_datepicker" ).datepicker("destroy");
    $( "#to_datepicker").val('');
    $( "#from_datepicker" ).val('');
    $("#to_datepicker").prop('disabled', true);
    $( "#from_datepicker" ).datepicker({
        minDate: to_min_date,
        maxDate: to_min_date,
        dateFormat: "dd-mm-yy",
        onSelect: function() {
            $("#to_datepicker").prop('disabled', false);
            date_picked = 1;
        },
      onClose: function() {
          if ( date_picked == 1 ) {
            res = $( '#from_datepicker' ).val().split("-");
            var format = res[1] + '-' + res[0] + '-' + res[2];
              $( "#to_datepicker" ).datepicker({
                    minDate: new Date( format ),
                    dateFormat: "dd-mm-yy",
                    maxDate: to_max_date,
                    minDate: to_min_date
              });
              $( "#to_datepicker" ).datepicker( "show" );
          }
      }
    });
    $( '#confirm_assign_top_spot_id' ).val( $( "input[name=chosen_brothel]:checked" ).val());
       $( '#confirm_assign_pos_chosen' ).val( $( '#pre_confirm_assign_pos_chosen' ).val());
       $( '#confirm_post_id' ).val( $( '#pre_confirm_post_id' ).val());
       $( '#confirm_choose_or' ).val( $( '#pre_confirm_choose_or' ).val());
    });
    $( '#bd_confirm_assign_brothel_button' ).on( 'click',function(e) {
       e.preventDefault();
       $( '.error' ).removeClass( 'error' );
       $( '#confirm_assign_errors' ).html( '');
       form_data = $( '#confirm_assign_top_form' ).serialize();
       errors = 0;
       error_msg = '',
       dollar_reg = /^\$?[0-9]+\.?[0-9]*$/,
       trans_reg = /^\d{9,}$/;
      if ( $('input[name=payment_amount]' ).val() != 'PROMOTION' && !$('input[name=payment_amount]' ).val().match( dollar_reg ) ) {
         error_msg += 'Please enter a valid payment amount<br>';
         errors++;
        $('input[name=payment_amount]' ).addClass( 'error');
       }
      if ( $( '#to_datepicker' ).val() == '' ) {
          error_msg += 'Please select a to date<br>';
          errors++;
          $( '#to_datepicker' ).addClass( 'error');
       }
      if ( $( '#from_datepicker' ).val() == '' ) {
          error_msg += 'Please select a from date<br>';
          errors++;
          $( '#from_datepicker' ).addClass( 'error');
       }
      if ( errors > 0 ) {
          $( '#confirm_assign_errors' ).html( error_msg );
      }
      else {    
       $( '#white_underlay' ).show();
       $( '.generic_loader' ).show();

       $.post(ajaxurl, form_data, function(response) {
             location.reload();
            // alert( response );
      }); 
      }
    });
    $( '#close_confirm_assign_top' ).on( 'click',function() {
       $( '#confirm_assign_top_spot' ).hide();
       $( '#underlay2' ).hide();
    });
       var meta_image_frame;
    $( '.load_image' ).on( 'click',function() {
        var this_post_id = $(this).data( 'id' );
        // If the frame already exists, re-open it.
        if ( meta_image_frame ) {
             meta_image_frame.open();
             return;
        }
        // Sets up the media library frame
        meta_image_frame = wp.media.frames.meta_image_frame = wp.media({
            title: 'Upload image',
            button: { text:  'Upload image' },
            library: { type: 'image' }
        }); 
        // Runs when an image is selected.
        meta_image_frame.on('select', function(){
            // Grabs the attachment selection and creates a JSON representation of the model.
            var media_attachment = meta_image_frame.state().get('selection').first().toJSON(),
            form_data = {
                action: 'add_image_city_screen',
                post_id: this_post_id,
                image: media_attachment.url
            };
                  $.post(ajaxurl, form_data, function(response) {
             $( '.load_image[data-id="' + this_post_id + '"]' ).removeClass( 'bd_error' ).addClass( 'bd_ok' );
      });
        // Opens the media library frame.
      });
              meta_image_frame.open();
   });
  $( '.taken_top_spot' ).on( 'click',function() {
     $("html, body").animate({ scrollTop: 0 }, "slow");
     $( '#underlay' ).show();
     $( '#transaction_details_div' ).show();
     $( '#transactions_details_content_holder' ).html( loader );
     var brothel_id = $( this ).data( 'brothel_id' ),
     trans_id = $( this ).data( 'trans_id' ),
     pos= $( this ).data( 'id' ),
     queued_brothel_id = $( this ).data( 'queued_brothel_id' ),
     queued_trans_id = $( this ).data( 'queued_trans_id' ),
     pos= $( this ).data( 'id' ),
           form_data = {
                   action: 'output_transaction_details',
                   pos: pos,
                   page_id: page_id,
                   security: $( '#bd_populate_eligible_brothels' ).val()
            };
           $.post(ajaxurl, form_data, function(response) {
                   $( '#transactions_details_content_holder' ).html( response );
                   $( '#bd_queue_brothel' ).on( 'click',function(e) {
                         $( '#bd_assign_content_holder' ).html(loader);
                         e.preventDefault();
                         $("html, body").animate({ scrollTop: 0 }, "slow");
                         var position = $( this ).data( 'id' );
                         $( '#assign_top_spots' ).show();
                         $( '#underlay2' ).show();
                         var form_data = {
                             action: 'populate_assign_top_spots',
                             post_id: post_id,
                             pos: $( '#bd_trans_details_pos_number' ).html(),
                             top_6_action: 'queued',
                             min_days: $( '#trans_details_date_to' ).html(),
                             security: $( '#bd_populate_eligible_brothels' ).val()
                          };
                          console.log( form_data );
                          $.post(ajaxurl, form_data, function(response) {
                              $( '#bd_assign_content_holder' ).html( response );
                              $('input[name="chosen_brothel"]').on( 'change', function() {
                          if ( $( this ).parents( '.in_image_spot' ).length ) {
                              alert( 'This brothel is already being advertised' );
                          }
                       });

                    });
               });
                $( '#bd_trans_details_pos_number' ).html( pos );  
                $( '#remove_top_post' ).on( 'click',function() {
                      $( '#bd_remove_pos_number' ).html( $( '#bd_trans_details_pos_number' ).html() );
                      $( '#bd_remove_brothel_name' ).html( $( '#bd_trans_details_brothel_name' ).html() );
                      $( '#transaction_details_brothel_id' ).val( brothel_id );
                      $( '#bd_remove_top_brothel_id').val( brothel_id );
                      $( '#trans_details_trans_id' ).val( trans_id );  
                      $( '#trans_pos_no' ).val( pos );  
                      $( 'input[name=bd_current_or_queued]' ).val( 'current' );
                      $( '#underlay2' ).show();
                      $( '#bd_remove_top_6_div' ).show();
                      $( '#bd_remove_brothel_top_button' ).on( 'click',function(e) {
                            e.preventDefault();
                        if ( $( 'input[name=bd_removal_reason]' ).val() == '' ) {
                              alert( 'You must enter a reason for this removal' );
                        }
                        else {
                          $( '#white_underlay' ).show();
                          $( '.generic_loader' ).show();
                          form_data = $( '#bd_remove_brothels_from_top' ).serialize();
                          $.post(ajaxurl, form_data, function(response) {
                                console.log( response );
                                location.reload();
                          });
                        }
                          
                    });
                });
                $( '#remove_queued_top_post' ).on( 'click',function() {
                      $( '#bd_remove_pos_number' ).html( $( '#bd_trans_details_pos_number' ).html() );
                      $( '#bd_remove_brothel_name' ).html( $( '#bd_trans_details_brothel_name' ).html() );
                      $( '#transaction_details_brothel_id' ).val( queued_brothel_id );
                      $( '#bd_remove_top_brothel_id').val( queued_brothel_id );
                      $( '#trans_details_trans_id' ).val( queued_trans_id );  
                      $( '#trans_pos_no' ).val( pos );  
                      $( 'input[name=bd_current_or_queued]' ).val( 'queued' );
                      $( '#underlay2' ).show();
                      $( '#bd_remove_top_6_div' ).show();
                      $( '#bd_remove_brothel_top_button' ).on( 'click',function(e) {
                            e.preventDefault();
                        if ( $( 'input[name=bd_removal_reason]' ).val() == '' ) {
                              alert( 'You must enter a reason for this removal' );
                        }
                       else {
                          $( '#white_underlay' ).show();
                          $( '.generic_loader' ).show();
                          form_data = $( '#bd_remove_brothels_from_top' ).serialize();
                          $.post(ajaxurl, form_data, function(response) {
                                console.log( response );
                                location.reload();
                          });
                        }    
                    });
                   $( '#close_remove_top_6' ).on( 'click',function() {
                      $( '#underlay2' ).hide();
                      $( '#bd_remove_top_6_div' ).hide();
                    });
                 });
             });    
   });
  $( '#close_transaction_details' ).on( 'click',function() {
     $( '#underlay' ).hide();
     $( '#transaction_details_div' ).hide();
  });
  $( '#set_advertiser_count' ).on( 'click',function(e) {
                          e.preventDefault();
                          $( '#white_underlay' ).show();
                          $( '.generic_loader' ).show();
                          var form_data = {
                          security: $( '#set_advertiser_count_secrurity' ).val(),
                          action: 'set_advertiser_count',
                          post_id: post_id,
                          count: $( '#advertiser_count_select' ).val()
                    };
        $.post(ajaxurl, form_data, function(response) {
         console.log( response );
         location.reload();
       });
  });
  $( '.available_until' ).on( 'click',function() {
    $( '#assign_top_spots' ).show();
       $( '#underlay' ).show();
       $("html, body").animate({ scrollTop: 0 }, "slow");
       var days = $( this ).data( 'days' ),
       position = $( this ).data( 'id' ),
       form_data = {
             action: 'populate_assign_top_spots',
             post_id: post_id,
             pos: position,
             top_6_action: 'current',
             days: days,
             security: $( '#bd_populate_eligible_brothels' ).val()
       };
        $.post(ajaxurl, form_data, function(response) {
        $( '#bd_assign_content_holder' ).html( response );
          $('input[name="chosen_brothel"]').on( 'change', function() {
      if ( $( this ).parents( '.in_image_spot' ).length ) {
               alert( 'This brothel is already being advertised' );
      }
    });

       });
  });
  $( '#close_remove_top_6' ).on( 'click',function() {
       $( '#underlay2' ).hide();
       $( '#bd_remove_top_6_div' ).hide();
  });
  $( '#save_bottom_content' ).on( 'click',function(e) {
       e.preventDefault();
       form_data = {
             action: 'save_bottom_content',
             post_id: post_id,
             content: $( '#city_bottom_editor' ).val()
       };
        $.post(ajaxurl, form_data, function(response) {
                alert( 'Content saved' );
        });
    });
    $( '#save_mobile_banner' ).on( 'click',function(e) {
       e.preventDefault();
       form_data = {
             action: 'save_mobile_banner',
             post_id: post_id,
             content: $( '#mobile_banner_content' ).val()
       };
        $.post(ajaxurl, form_data, function(response) {
               alert( 'Content saved' );
        });
    });
  form_data = {
          action: 'bd_city_populate_statistics',
          page_id: post_id,
          from: $( '#city_stats_top_from' ).val(),
          to: $( '#city_stats_top_to' ).val(),
          stat_count: $( '#advertiser_count_select' ).val()
       };
        $.post(ajaxurl, form_data, function(response) {
                 events = JSON.parse(response)
                 $.each(events, function( index, value ) {
                  $( '#stat_top_' + index ).children( '.event_holder' ).html( 'Events last 7 days: ' + value );
                });
        });
  $( '#statistics_date_changer' ).on( 'click',function() {
    form_data = {
          action: 'bd_city_populate_statistics',
          page_id: post_id,
          from: $( '#city_stats_top_from' ).val(),
          to: $( '#city_stats_top_to' ).val(),
          stat_count: $( '#advertiser_count_select' ).val()
       };
    $( '.event_holder' ).html( loader );
                 $.post(ajaxurl, form_data, function(response) {
                 events = JSON.parse(response)
                $.each(events, function( index, value ) {
                  $( '#stat_top_' + index ).children( '.event_holder' ).html( 'Events : ' + value );
                });
        });
  });
  var screen = 0;
  $( '#top_spot_changer' ).on( 'click',function() {
    if ( screen == 0 ) {
          $( '#bd_city_normal_view' ).removeClass( 'active_screen' );
          $( '#bd_city_statistics_view' ).addClass( 'active_screen' );
          $( '#top_spot_changer' ).html( 'Show Brothels' );
          $( '#advertiser_count_label' ).hide();
          $( '#advertiser_count_select' ).hide();
          $( '#set_advertiser_count' ).hide();
          $( '.date_range_picker_stats' ).show();
          screen = 1;
    }
    else { $( '#bd_city_normal_view' ).addClass( 'active_screen' );
          $( '#bd_city_statistics_view' ).removeClass( 'active_screen' );
          $( '#top_spot_changer' ).html( 'Show Statistics' );
          $( '#advertiser_count_label' ).show();
          $( '#advertiser_count_select' ).show();
          $( '#set_advertiser_count' ).show();
          $( '.date_range_picker_stats' ).hide();
          screen = 0;
    }
  });
  function populate_stats(position) {
    brothels = $( '#bd_stats_brothel_choice_select option:selected' ).val();
    date_from = $( '#from_datepicker_stats' ).val();
     if ( date_from == '' ) {
         date_from = 'default';
      }
     date_to = $( '#to_datepicker_stats' ).val();
     if ( date_to == '' ) {
         date_to = 'default';
     }
     form_data = {
             action: 'bd_city_populate_detailed_statistics',
             position: position,
             page_id: page_id,
             date_from: date_from,
             date_to: date_to,
             brothels: brothels,
             unit_rev: $( '#bd_stats_unit-rev' ).val(),
             conv: $( '#bd_stats_conv-perc' ).val(),
             ads_charge: $( '#bd_stats_ads-charge' ).val()
       };
       $( '#statistics_holder' ).html( loader );
        $.post(ajaxurl, form_data, function(response) {
              $( '#statistics_holder' ).html( response );
              $( '#bd_stats_header' ).html( 'Statistics- Position ' + position );
        });
  }
  $( 'body' ).on('change', 'select[name="bd_stats_city_select"]', function() {
        populate_stats( $('select[name="bd_stats_city_select"] option:selected').val());
  });
    $( 'body' ).on('change', 'select#bd_stats_brothel_choice_select', function() {
        populate_stats( $('select[name="bd_stats_city_select"] option:selected').val());
  });
  $( '#bd_close_page_stats' ).on( 'click',function() {
        $( '#bd_city_page_statistics' ).hide();
        $( '#hide_brothel_names' ).hide();
        $( '#create_perf_report' ).hide();
         $( '#underlay' ).hide();
  });
  $( '.stat_top_spot' ).on( 'click',function() {
        $( '#hide_brothel_names' ).show();
        $( '#create_perf_report' ).show();
        $( '#underlay' ).show();
        $( '#bd_city_page_statistics' ).show();
        $( '#statistics_holder' ).html( loader );
        populate_stats( $(this ).data('position'));
  });
  $( '#hide_brothel_names' ).on( 'click',function() {
        $( '#bd_stats_brothel_name' ).hide();
  });
  $( '#create_perf_report' ).on( 'click', function() {
       $( this ).hide();
       $( '#back_to_stats' ).show();
       var websiteClicks = ( $( '#bd_inv_website' ).html() ? parseInt($( '#bd_inv_website' ).html() ) : 0 );
       var imageClicks = ( $( '#bd_inv_image' ).html() ? parseInt($( '#bd_inv_image' ).html() ) : 0 );
       var websiteReferrals = websiteClicks + imageClicks;

    if ( $( '#bd_stats_brothel_name' ).html() != 'undefined' ) {
           $( '#perf_brothel' ).html(  $( '#bd_stats_brothel_name' ).html().substring(23) );
    }
       $( '#perf_pagePos' ).html( $( '#title' ).val() + ', Position ' + $( 'select[name="bd_stats_city_select"] option:selected' ).val() );
       $( '#perf_web_ref' ).html( websiteReferrals );
       $( '#perf_phone_click' ).html( $( '#bd_inv_phone' ).html() );
    if ( !$('#bd_inv_phone' ).length ){$( '#perf_phone_click' ).hide().next().hide(); }else {$( '#perf_phone_click' ).show().next().show(); }
    if ( $( '#from_datepicker_stats' ).val() != 'undefined' && $( '#to_datepicker_stats' ).val() != 'undefined' ) {
          $( '#perf_dates' ).html( 'From ' + $( '#from_datepicker_stats' ).val() + ' to ' + $( '#to_datepicker_stats' ).val() );
      }
       $( '#bd_perf_report' ).show();
       $( '#bd_city_page_statistics' ).hide();
    });
    $( '#back_to_stats' ).on( 'click', function() {
       $( this ).hide();
       $( '#create_perf_report' ).show();
       $( '#bd_perf_report' ).hide();
       $( '#bd_city_page_statistics' ).show();
    });
     
     
   $( "#from_datepicker_stats" ).datepicker({
        dateFormat: "dd-mm-yy",
        maxDate: 0,
        onSelect: function() {
            $("#to_datepicker_stats").prop('disabled', false);
        },
      onClose: function() {
            res = $( '#from_datepicker_stats' ).val().split("-");
            var format = res[1] + '-' + res[0] + '-' + res[2];
            $( "#to_datepicker_stats").datepicker("destroy");
              $( "#to_datepicker_stats" ).datepicker({
                    minDate: new Date( format ),
                    dateFormat: "dd-mm-yy",
                    maxDate: 0,
              });
              $( "#to_datepicker_stats" ).datepicker( "show" );
      }
     });
  $( '#bd_save_ga_category' ).on( 'click', function(e) {
     e.preventDefault();
     form_data = {
             action: 'bd_update_ga_category',
             value: $( '#bd_ga_category_value' ).val(),
             post_id: post_id
       };
       $( '#statistics_holder' ).html( loader );
        $.post(ajaxurl, form_data, function(response) {
              alert( 'Category updated' );
        });
  });
    $( '#bd_save_seo_hack' ).on( 'click', function(e) {
     e.preventDefault();
     form_data = {
             action: 'bd_save_seo_hack',
             description: $( '#bd_yoast_description' ).val(),
             title:$( '#bd_yoast_title' ).val(),
             post_id: post_id
       };
       $( '#statistics_holder' ).html( loader );
        $.post(ajaxurl, form_data, function(response) {
              alert( response );
        });
  });
  $( '#statistics_holder' ).on( 'click', '#bd_set_stats_button', function(e) {
    e.preventDefault();
    populate_stats( $('select[name="bd_stats_city_select"] option:selected').val());
  });
  $( '#bd_set_dates' ).on( 'click',function() {
    populate_stats( $('select[name="bd_stats_city_select"] option:selected').val());
  });
       $( '#publish' ).addClass( 'special_publish' );
       $( '#submitdiv' ).addClass( 'hide_publish');
       $( '#submitdiv' ).show();
       $( '#postbox-container-2' ).addClass( 'bd_large_column');
       $( '#bd_show_hidden_seo' ).show();
       $( '#city_page_ga_category' ).hide();
       $( '#bd_show_hidden_seo' ).on( 'click',function(e) {
       e.preventDefault();
    if ( $( this ).html() == 'Show SEO' ) {
       $( '#cas-content-sidebars' ).show();
       $( '#brothels_meta_box_seo').show();
       $( '#brothels_meta_box' ).width( '47.5%' );
       $( '#bd_show_hidden_seo' ).html('Hide SEO');
       $( '#submitdiv' ).removeClass( 'hide_publish');
       $( '#postbox-container-2' ).removeClass( 'bd_large_column');
       $( '.postbox' ).show();
       $( '#city_page_ga_category' ).show();
       $( '#publish' ).addClass( 'special_publish_right' );
  }
   else {
       $( '#cas-content-sidebars' ).hide();
       $( '#brothels_meta_box_seo').hide();
       $( '#brothels_meta_box' ).width( '64.5%');
       $( '#bd_show_hidden_seo' ).html('Show SEO');
       $( '#submitdiv' ).addClass( 'hide_publish');
       $( '#postbox-container-2' ).addClass( 'bd_large_column');
       $( '#publish' ).removeClass( 'special_publish_right' );
       $( '#categorydiv' ).hide();
       $( '#tagsdiv-post_tag' ).hide();
       $( '#city_page_ga_category' ).hide();
       $( '#postimagediv' ).hide();
       $( '#brothels_meta_box_content' ).hide();
       $( '#brothels_meta_box_bottom_content' ).hide();
       $( '#brothels_meta_box_mobile_banner' ).hide();
       $( '#post-views-meta' ).hide();
       $( '#wpseo_meta' ).hide();
       $( '#commentsdiv' ).hide();
       $( '#commentstatusdiv' ).hide();
  }
  });
});