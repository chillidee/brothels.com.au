jQuery( document ).ready(function($) {
  var mult = false,
    prev = 0;

$('input').keypress(function(e) {
    if (!mult) {
        mult = true;
        prev = e.which;
        setTimeout(function() {
            mult = false;
        }, 2000)
    }
    else if (prev != e.which) {
        mult = false;
    }
    else {
        return false;
    }
});
  $('textarea').keypress(function(e) {
    if (!mult) {
        mult = true;
        prev = e.which;
        setTimeout(function() {
            mult = false;
        }, 2000)
    }
    else if (prev != e.which) {
        mult = false;
    }
    else {
        return false;
    }
})
    adv_show = 0;
  $( '#show_advanced' ).on( 'click',function() {
    if ( adv_show == 0 ) {
       $( '.advanced' ).show();
       adv_show = 1;
    }
    else {
      $( '.advanced' ).hide();
       adv_show = 0;
    }
      
  });
  $( '.notice-success' ).on( 'click',function() {
        window.location.href = "/wp-admin/edit.php?post_type=city";
  });
  if ( $( '#publish' ).val() =='Publish' ) {
    $( '.wrap:after' ).hide();
    function publish_check() {
      if( title == 1 && address == 1 ) {
          $( '#publish' ).val( 'Create Brothel' );
          $( '#publish' ).show();
          $( '#second_publish' ).html( 'Create Brothel' );
          $( '#second_publish' ).show();
      }
    }
       var title = 0,
           address =0;
    $( '#title' ).on( "change", function() {
      if ( $( this ).val() != '' ) {
           title = 1;
           publish_check();
      }
    });
    $( 'input[name="bd_address"]' ).on( "change", function() {
      if ( $( this ).val() != '' ) {
           address = 1;
           publish_check();
      }
    });
    
  }
  else {
      $( '#publish' ).val( 'Update brothel' );
      $( '#publish' ).show();
}
   $( '#bd_back_to_wordpress' ).on( 'click', function() {
         window.history.back();
    });
  var form_data = {
                 'action':'bd_populate_statistics',
                 'city':'all',
                 'post_id': post_id,
                 'date_from': 'default',
                 'date_to': 'default'
           };
             $.post(ajaxurl, form_data, function(response) {
                 $( '#statistics_holder' ).html( response );
           });

   $( '#bd_set_stats_button' ).on( 'click', function() {
     $( '#statistics_holder' ).html( loader );
     var date_from,
         date_to;
     if ( $( '#from_datepicker' ).val() == '' ) {
             date_from = 'default';
     } else { date_from = $( '#from_datepicker' ).val() };
     if ( $( '#to_datepicker' ).val() == '' ) {
             date_to = 'default';
     } else { date_to = $( '#to_datepicker' ).val() };
           var form_data = {
                 'action': 'bd_set_statistics',
                 'unit_revenue': $( '#bd_stats_unit-rev' ).val(),
                 'post_id': post_id,
                 'conv_percent': $( '#bd_stats_conv-perc' ).val(),
                 'date_from': date_from,
                 'ads_charge': $( '#bd_stats_ads-charge' ).val(),
                 'date_to': date_to,
                 'city': $( 'select[name="bd_stats_city_select"]' ).val()
           };
     console.log( form_data );
             $.post(ajaxurl, form_data, function(response) {
                 $( '#statistics_holder' ).html( response );
           });
   });
  $( 'select[name="bd_stats_city_select"]' ).on( 'change',function() {
    var date_from,
         date_to;
     if ( $( '#from_datepicker' ).val() == '' ) {
             date_from = 'default';
     } else { date_from = $( '#from_datepicker' ).val() };
     if ( $( '#to_datepicker' ).val() == '' ) {
             date_to = 'default';
     } else { date_to = $( '#to_datepicker' ).val() };
    $( '#statistics_holder' ).html( loader );
     var form_data = {
                 'action':'bd_populate_statistics',
                 'post_id': post_id,
                 'date_from': date_from,
                 'date_to': date_to,
                 'city': $( 'select[name="bd_stats_city_select"]' ).val()
           };
             $.post(ajaxurl, form_data, function(response) {
                 $( '#statistics_holder' ).html( response );
                 $( '#statistics_date_range' ).html( 'Results from ' + $( '#from_datepicker' ).val() + ' to ' + $( '#to_datepicker').val() );
           });
  });
    
  $( '#bd_set_dates' ).on( 'click', function() {
    $( '#statistics_holder' ).html( loader );
     var form_data = {
                 'action':'bd_populate_statistics',
                 'post_id': post_id,
                 'date_from': $( '#from_datepicker' ).val(),
                 'date_to': $( '#to_datepicker' ).val(),
                 'city': $( 'select[name="bd_stats_city_select"]' ).val()
           };
             $.post(ajaxurl, form_data, function(response) {
                 $( '#statistics_holder' ).html( response );
                 $( '#statistics_date_range' ).html( 'Results from ' + $( '#from_datepicker' ).val() + ' to ' + $( '#to_datepicker').val() );
           });
  });
   $( "#from_datepicker" ).datepicker({
        dateFormat: "dd-mm-yy",
        maxDate: 0,
        onSelect: function() {
            $("#to_datepicker").prop('disabled', false);
        },
      onClose: function() {
            res = $( '#from_datepicker' ).val().split("-");
            var format = res[1] + '-' + res[0] + '-' + res[2];
              $( "#to_datepicker" ).datepicker({
                    minDate: new Date( format ),
                    dateFormat: "dd-mm-yy",
                    maxDate: 0,
              });
              $( "#to_datepicker" ).datepicker( "show" );
      }
     });
      $( '#close_contact' ).on( 'click', function(e) {
           $( '#brothel_add_contact_div' ).hide();
           $( '#underlay' ).hide();
       });
      $( '.bd_prev_contact_versions' ).on( 'click', function() {
           $( '.high-z' ).removeClass( 'high-z' );
           $( this ).next().addClass( 'high-z' );
      });
      $( '#close_contact_edit' ).on( 'click', function(e) {
           $( '#brothel_edit_contact_div' ).hide();
           $( '#underlay' ).hide();
       });
      $( '.edit_contact' ).on( 'click', function(e) {
           e.preventDefault();
           $( '#brothel_edit_contact_div' ).show();
           $( '#underlay' ).show();
           var meta_id = $( this ).data('id');
           $('select[name="bdc_edit_position"]').val($( '.' + meta_id + '_position' ).html() );
           $( 'input[name="bdc_edit_name"]').val($( '.' + meta_id + '_name' ).html() );
           $( 'input[name="bdc_edit_phone"]').val($( '.' + meta_id + '_phone' ).html() );
           $( 'input[name="bdc_edit_mobile"]').val($( '.' + meta_id + '_mobile' ).html() );
           $( 'input[name="bdc_edit_email1"]').val($( '.' + meta_id + '_email1' ).html() );
           $( 'input[name="bdc_edit_email2"]').val($( '.' + meta_id + '_email2' ).html() );
           $( 'input[name="bdc_edit_address"]').val($( '.' + meta_id + '_address' ).html() );
           $( 'textarea[name="bdc_edit_comments"]').val($( '.' + meta_id + '_comments' ).html() );
           $( 'input[name="meta_id"]' ).val( meta_id );
           $( 'input[name="previous_value"]' ).val( $( this ).prev('input[name="contact_array"]').val() );
      });
       $( '#add_contact' ).on( 'click', function(e) {
           e.preventDefault();
           $( '#brothel_add_contact_div' ).show();
           $( '#underlay' ).show();
       });
       $( '#add_note' ).on( 'click', function(e) {
           e.preventDefault();
           $( '#brothel_add_note_div' ).show();
           $( '#underlay' ).show();
       });
       $( '#close_notes' ).on( 'click', function() {
           $( '#brothel_add_note_div' ).hide();
           $( '#underlay' ).hide();
       });
       $( '#new_brothel' ).on( 'click', function(e) {
           e.preventDefault();
           $( '#brothels_overlay' ).show();
           $( '#underlay' ).show();
       });
       $( '#bd_close' ).on( 'click', function() {
           $( '#brothels_overlay' ).hide();
           $( '#underlay' ).hide();
       });
       $( '#bd_add_contact_button' ).on('click',function(e) {
           e.preventDefault();
           var form_data = $( '#abd_add_new_contact' ).serialize();
           $.post(ajaxurl, form_data, function(response) {
             //$( '#brothels_meta_notes .inside' ).prepend( response );
             $( '#brothels_contact .inside .clear_helper' ).prepend( response );
             $( '#brothel_add_contact_div' ).hide();
             $( '#underlay' ).hide();
           });
       });
       $( '#bd_edit_contact_button' ).on('click',function(e) {
           e.preventDefault();
           var form_data = $( '#abd_edit_contact' ).serialize();
           $.post(ajaxurl, form_data, function(response) {
              console.log( response );
              location.reload();
            });
        });        
       $( '#bd_add_note_button' ).on('click',function(e) {
           e.preventDefault();
           var form_data = $( '#abd_add_new_note' ).serialize(),
           errors = 0,
           error_msg = '';
           $( '#error_holder_notes' ).html('');
           $( '.error' ).removeClass( 'error' );
         if ( $( 'input[name=bd_author]' ).val() > 3 ){
          errors ++;
          $( 'input[name=bd_author]' ).addClass( 'error' ).focus();
          error_msg = 'Please provide an author<br>' + error_msg;
      }
      if ( $( 'input[name=bd_note]' ).val() < 3 ) {
          errors ++;
          $( 'input[name=bd_note]' ).addClass( 'error' ).focus();
          error_msg = 'Please add your note<br>' + error_msg;
      }
      if ( errors > 0 ) {
          $( '#error_holder_notes' ).html( error_msg );
      }
      else {
      $.post(ajaxurl, form_data, function(response) {
           $( '#brothels_meta_notes .inside' ).prepend( response );
           $( '#brothel_add_note_div' ).hide();
           $( '#underlay' ).hide();
      });
     }  
   });
       $( '#bd_add_description' ).on('click',function(e) {
           window.post_id;
           e.preventDefault();
           $( '#brothel_add_extra_description_form' ).show();
           $( '#underlay' ).show();
           $( '#bd_add_desc_id' ).val( post_id );
       });
       $( '#bd_add_extra_description_button' ).on('click',function(e) {
           e.preventDefault();
           form_data = $( '#abd_add_extra_description' ).serialize();
         if ( $( '#bd_add_desc_textarea' ).val() == "" ) {
               alert( 'You must enter a description' );
         }
         else {
           $.post(ajaxurl, form_data, function(response) {
           location.reload();
       });
         }
       });
       $( '#close_extra_description' ).on('click',function(e) {
           e.preventDefault();
           $( '#brothel_add_extra_description_form' ).hide();
           $( '#underlay' ).hide();
       });
       $( '#second_publish' ).on('click',function(e) {
           $("html, body").animate({ scrollTop: 0 }, "slow");
           var form_data = $( '#bd_add_new_brothel' ).serialize(),
           errors = 0,
           error_msg = '',
           description = {},
           description_objects = $( '.bd_description_input' );
           $.each( description_objects, function( key, value ) {
                description;
                name = $(this).attr("name");
                var start_pos = name.indexOf('"') + 1;
                var end_pos = name.indexOf('"',start_pos);
                var array_key = name.substring(start_pos,end_pos);
             if(!description[array_key]) {
                     description[array_key] = {};
                }
                description[array_key][key] = $( this ).val();
           });
           var form_data = {
                 action:'bd_add_brothel',
                 bd_post_id: $('input[name=post_id]').val(),
                 security:$( 'input[name=security]' ).val(),
                 bd_description:JSON.stringify(description), 
                 bd_address:$( 'input[name=bd_address]' ).val(), 
                 bd_phone:$( 'input[name=bd_phone]' ).val(), 
                 bd_track_phone:$( 'input[name=bd_track_phone]' ).val(),
                 bd_name:$( '#title' ).val(), 
                 bd_website_readable: $( 'input[name=bd_website_readable]').val(),
                 bd_website:$( 'input[name=bd_website]' ).val(),
                 bd_landing_page:$( 'input[name=bd_landing_page]' ).val(), 
                 bd_ga_name:$( 'input[name=ga_name]' ).val(),
                 bd_image:$( 'input[name=bd_image]' ).val(),
                 bd_inpage:$( 'input[name=in_page]' ).val()
           };
           $( '#error_holder' ).html('');
      $( '.error' ).removeClass( 'error' );
           //Validation!!
      var phone_num = '^(?=.*[0-9])[- +()0-9]+$';
     /* if ( form_data.bd_description > 100  ) {
          errors ++;
          $( 'textarea[name=bd_description]' ).addClass( 'error' ).focus();
          error_msg = 'Description must not be more than 100 characters<br>' + error_msg;
      }
       if ( form_data.bd_description < 50  ) {
          errors ++;
          $( 'textarea[name=bd_description]' ).addClass( 'error' ).focus();
          error_msg = 'Description must be at least 50 characters long<br>' + error_msg;
      }
      if ( form_data.bd_address < 3 ) {
          errors ++;
          $( 'input[name=bd_address]' ).addClass( 'error' ).focus();
          error_msg = 'Please enter a valid Address<br>' + error_msg;
      }*/
      if ( errors > 0 ) {
          alert( 'Please fill in the missing information' );
          $( '#error_holder' ).html( error_msg );
                     e.preventDefault();
      }
      else {
      $.post(ajaxurl, form_data, function(response) {
           //alert( response );
       });
      }
   });
// $( '.bd_statistics_label').on('click', function (e){
//   $("html, body").animate({ scrollTop: 0 }, "slow");
//            var form_data = $( '#bd_add_new_brothel' ).serialize(),
//            errors = 0,
//            error_msg = '',
//            description = {},
//            description_objects = $( '.bd_description_input' );
//            $.each( description_objects, function( key, value ) {
//                 description;
//                 console.log( description, '>>>>>>>>>>>>>>>.DES')
//                 name = $(this).attr("name");
//                 console.log( name, '>>>>>>>>>>>>>>>>name')
//                 var start_pos = name.indexOf('"') + 1;
//                 var end_pos = name.indexOf('"',start_pos);
//                 var array_key = name.substring(start_pos,end_pos);
//                 console.log( array_key, '>>>......aRRAYKEY')
//              if(!description[array_key]) {
//                      description[array_key] = {};
//                 }
//                 description[array_key][key] = $( this ).val();
//            });
//            var form_data = {
//                  action:'bd_add_brothel',
//                  bd_post_id: $('input[name=post_id]').val(),
//                  security:$( 'input[name=security]' ).val(),
//                  bd_description:JSON.stringify(description), 
//                  bd_address:$( 'input[name=bd_address]' ).val(), 
//                  bd_phone:$( 'input[name=bd_phone]' ).val(),
//                  bd_track_phone:$( 'input[name=bd_track_phone]' ).val(),
//                  bd_name:$( '#title' ).val(), 
//                  bd_website:$( 'input[name=bd_website]' ).val(), 
//                  bd_website_readable: $( 'input[name=bd_website_readable]').val(),
//                  bd_landing_page:$( 'input[name=bd_landing_page]' ).val(),
//                  bd_extra_ga_name:$( 'input[name=extra_ga_name]' ).val(),
//                  bd_extra_ga_name2:$( 'input[name=extra_ga_name2]' ).val(),
//                  bd_ga_name:$( 'input[name=ga_name]' ).val(),
//                  bd_image:$( 'input[name=bd_image]' ).val(),
//                  bd_inpage:$( 'input[name=in_page]' ).val()
//            };
//            console.log( form_data )
//            $( '#error_holder' ).html('');
//       $( '.error' ).removeClass( 'error' );
//            //Validation!!
//       var phone_num = '^(?=.*[0-9])[- +()0-9]+$';
//    /*   if ( form_data.bd_description > 100  ) {
//           errors ++;
//           $( 'textarea[name=bd_description]' ).addClass( 'error' ).focus();
//           error_msg = 'Description must not be more than 100 characters<br>' + error_msg;
//       }
//        if ( form_data.bd_description < 50  ) {
//           errors ++;
//           $( 'textarea[name=bd_description]' ).addClass( 'error' ).focus();
//           error_msg = 'Description must be at least 50 characters long<br>' + error_msg;
//       }*/
//     /*  if ( form_data.bd_address < 3 ) {
//           errors ++;
//           $( 'input[name=bd_address]' ).addClass( 'error' ).focus();
//           error_msg = 'Please enter a valid Address<br>' + error_msg;
//       }*/
//       if ( errors > 0 ) {
//           alert( 'Please fill in the missing information' );
//           $( '#error_holder' ).html( error_msg );
//                      e.preventDefault();
//       }
//       else {
//       $( '#white_underlay' ).show();
//       $( '.generic_loader' ).show(); 
//       $.post(ajaxurl, form_data, function(response) {
//            console.log( response )
//            return false;
//        });
//       }

// })
       $( '#publish' ).on('click',function(e) {
           $("html, body").animate({ scrollTop: 0 }, "slow");
           var form_data = $( '#bd_add_new_brothel' ).serialize(),
           errors = 0,
           error_msg = '',
           description = {},
           description_objects = $( '.bd_description_input' );
           $.each( description_objects, function( key, value ) {
                description;
                name = $(this).attr("name");
                var start_pos = name.indexOf('"') + 1;
                var end_pos = name.indexOf('"',start_pos);
                var array_key = name.substring(start_pos,end_pos);
             if(!description[array_key]) {
                     description[array_key] = {};
                }
                description[array_key][key] = $( this ).val();
           });
           var form_data = {
                 action:'bd_add_brothel',
                 bd_post_id: $('input[name=post_id]').val(),
                 security:$( 'input[name=security]' ).val(),
                 bd_description:JSON.stringify(description), 
                 bd_address:$( 'input[name=bd_address]' ).val(), 
                 bd_phone:$( 'input[name=bd_phone]' ).val(),
                 bd_track_phone:$( 'input[name=bd_track_phone]' ).val(),
                 bd_name:$( '#title' ).val(), 
                 bd_website:$( 'input[name=bd_website]' ).val(), 
                 bd_website_readable: $( 'input[name=bd_website_readable]').val(),
                 bd_landing_page:$( 'input[name=bd_landing_page]' ).val(),
                 bd_extra_ga_name:$( 'input[name=extra_ga_name]' ).val(),
                 bd_extra_ga_name2:$( 'input[name=extra_ga_name2]' ).val(),
                 bd_ga_name:$( 'input[name=ga_name]' ).val(),
                 bd_image:$( 'input[name=bd_image]' ).val(),
                 bd_inpage:$( 'input[name=in_page]' ).val()
           };
           console.log( form_data )
           $( '#error_holder' ).html('');
      $( '.error' ).removeClass( 'error' );
           //Validation!!
      var phone_num = '^(?=.*[0-9])[- +()0-9]+$';
   /*   if ( form_data.bd_description > 100  ) {
          errors ++;
          $( 'textarea[name=bd_description]' ).addClass( 'error' ).focus();
          error_msg = 'Description must not be more than 100 characters<br>' + error_msg;
      }
       if ( form_data.bd_description < 50  ) {
          errors ++;
          $( 'textarea[name=bd_description]' ).addClass( 'error' ).focus();
          error_msg = 'Description must be at least 50 characters long<br>' + error_msg;
      }*/
    /*  if ( form_data.bd_address < 3 ) {
          errors ++;
          $( 'input[name=bd_address]' ).addClass( 'error' ).focus();
          error_msg = 'Please enter a valid Address<br>' + error_msg;
      }*/
      if ( errors > 0 ) {
          alert( 'Please fill in the missing information' );
          $( '#error_holder' ).html( error_msg );
                     e.preventDefault();
      }
      else {
      $( '#white_underlay' ).show();
      $( '.generic_loader' ).show(); 
      $.post(ajaxurl, form_data, function(response) {
           console.log( response )
           return false;
       });
      }
   });
       $( '#bd_add_brothel_button' ).on('click',function(e) {
           e.preventDefault();
           var form_data = $( '#bd_add_new_brothel' ).serialize(),
           errors = 0,
           error_msg = '',
           description = {},
           description_objects = $( '.bd_description_input' );
           $.each( description_objects, function( key, value ) {
                description;
                name = $(this).attr("name");
                var start_pos = name.indexOf('"') + 1;
                var end_pos = name.indexOf('"',start_pos);
                var array_key = name.substring(start_pos,end_pos);
             if(!description[array_key]) {
                     description[array_key] = {};
                }
                description[array_key][key] = $( this ).val();
           });
           var form_data = {
                 action:'bd_add_brothel',
                 bd_post_id: $('input[name=post_id]').val(),
                 security:$( 'input[name=security]' ).val(),
                 bd_description:JSON.stringify(description), 
                 bd_address:$( 'input[name=bd_address]' ).val(), 
                 bd_phone:$( 'input[name=bd_phone]' ).val(),
                 bd_track_phone:$( 'input[name=bd_track_phone]' ).val(),
                 bd_name:$( '#title' ).val(), 
                 bd_website:$( 'input[name=bd_website]' ).val(), 
                 bd_landing_page:$( 'input[name=bd_landing_page]' ).val(),
                 bd_extra_ga_name:$( 'input[name=extra_ga_name]' ).val(),
                 bd_ga_name:$( 'input[name=ga_name]' ).val(),
                 bd_image:$( 'input[name=bd_image]' ).val(),
                 bd_inpage:$( 'input[name=in_page]' ).val()
           };
           $( '#error_holder' ).html('');
      $( '.error' ).removeClass( 'error' );
           //Validation!!
      var phone_num = '^(?=.*[0-9])[- +()0-9]+$';
      if ( form_data.bd_description > 100  ) {
          errors ++;
          $( 'textarea[name=bd_description]' ).addClass( 'error' ).focus();
          error_msg += 'Description must not be more than 100 characters<br>';
      }
       if ( form_data.bd_description < 50  ) {
          errors ++;
          $( 'textarea[name=bd_description]' ).addClass( 'error' ).focus();
          error_msg += 'Description must be at least 50 characters long<br>';
      }
      if ( form_data.bd_address < 3 ) {
          errors ++;
          $( 'input[name=bd_address]' ).addClass( 'error' ).focus();
          error_msg += 'Please enter a valid Address<br>';
      }
      if ( errors > 0 ) {
          alert( 'Please fix the errors' );
          $( '#error_holder' ).html( error_msg );
      }
      else {
      $.post(ajaxurl, form_data, function(response) {
           //alert( response );
       });
      }
   });
     var meta_image_frame;
    $( '#upload_new_brothel_image' ).on( 'click',function(e) {
    e.preventDefault();
        // If the frame already exists, re-open it.
        if ( meta_image_frame ) {
             meta_image_frame.open();
             return;
        }
        // Sets up the media library frame
        meta_image_frame = wp.media.frames.meta_image_frame = wp.media({
            title: 'Upload image',
            button: { text:  'Upload image' },
            library: { type: 'image' }
        }); 
        // Runs when an image is selected.
        meta_image_frame.on('select', function(){
            // Grabs the attachment selection and creates a JSON representation of the model.
            var media_attachment = meta_image_frame.state().get('selection').first().toJSON();
 
            // Sends the attachment URL to our custom image input field.
            $('#bd_image').val(media_attachment.url);
            $('#bd_image_display').attr( 'src', media_attachment.url);
            $('#bd_image_display').show();
            $( '#upload_new_brothel_image' ).hide();
        });
 
        // Opens the media library frame.
        meta_image_frame.open();
  });
      $( '#bd_image_holder' ).on( 'click',function() {
            $('#bd_image').val('');
            $('#bd_image_display').attr( 'src', '');
            $('#bd_image_display').hide();
            $( '#upload_new_brothel_image' ).show();
      });
  $( '#close_vc' ).on( 'click',function() {
            $( '#bd_vc_popup' ).hide();
            $( '#underlay_light' ).hide();
     });
  $( '#old_versions' ).on( 'click',function() {
            $( '#bd_vc_popup' ).show();
            $( '#underlay_light' ).show();
     });
  $( '#bd_vc_next' ).on( 'click',function() {
    if ( $( '.bd_vc_current' ).next().hasClass( 'bd_version' ) ) {
           $( '.bd_vc_current' ).removeClass( 'bd_vc_current' ).next().addClass( 'bd_vc_current' );
           $( '#bc_vd_restore_button' ).attr("data-vc_id", $( '.bd_vc_current' ).data( 'vc_id' ));
    }
    else {
           $( '.bd_vc_current' ).removeClass( 'bd_vc_current' );
           $( '.bd_version:first' ).addClass( 'bd_vc_current' );
           $( '#bc_vd_restore_button' ).attr("data-vc_id", $( '.bd_vc_current' ).data( 'vc_id' ));
    }
     });
   $( '#bc_vd_restore_button' ).on('click',function(e) {
           parent_form = '#' + 'bd_revert_vc_form' + $( this ).data( 'vc_id' );
           description = {},
           description_objects = $( parent_form + ' .bd_vc_description' );
           $.each( description_objects, function( key, value ) {
                description;
                name = $(this).attr("name");
                var start_pos = name.indexOf('"') + 1;
                var end_pos = name.indexOf('"',start_pos);
                var array_key = name.substring(start_pos,end_pos);
             if(!description[array_key]) {
                     description[array_key] = {};
                }
                description[array_key][key] = $( this ).val();
           });
           var form_data = {
                 action:'bd_add_brothel',
                 bd_post_id: $('input[name=post_id]').val(),
                 security:$( 'input[name=security]' ).val(),
                 bd_description:JSON.stringify(description), 
                 bd_address:$( parent_form + ' input[name=bd_address]' ).val(), 
                 bd_phone:$( parent_form + ' input[name=bd_phone]' ).val(),
                 bd_track_phone:$( 'input[name=bd_track_phone]' ).val(),
                 bd_name:$( '#title' ).val(), 
                 bd_website:$( parent_form + ' input[name=bd_website]' ).val(), 
                 bd_landing_page:$( parent_form + ' input[name=bd_landing_page]' ).val(),
                 bd_ga_name:$( 'input[name=ga_name]' ).val(),
                 bd_image:$( parent_form + ' input[name=bd_image]' ).val(),
                 bd_inpage:$( 'input[name=in_page]' ).val()
           };  
      $( '#white_underlay' ).show();
      $( '.generic_loader' ).show(); 
      $.post(ajaxurl, form_data, function(response) {
             location.reload();
       });
   });
  $( '.bd_remove_description' ).on( 'click',function() {
        $( this ).next().remove();
        $( this ).prev().prev().remove();
        $( this ).remove();
     });
 });