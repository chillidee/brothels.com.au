<?php 
  /**
   * Custom box to update the views for all pages and post types
   **
   * Should be commented out after development phase
  */
  function add_views_meta() { 
    add_meta_box('post-views-meta', 'Update post views', 'post_views_meta', 'page', 'side');
    add_meta_box('post-views-meta', 'Update post views', 'post_views_meta', 'city', 'side');
    add_meta_box('post-views-meta', 'Update post views', 'post_views_meta', 'brothel', 'side');
    add_meta_box('post-views-meta', 'Update post views', 'post_views_meta', 'post_events', 'side');
    add_meta_box('post-views-meta', 'Update post views', 'post_views_meta', 'post_sponsor','side');
    add_meta_box('post-views-meta', 'Update post views', 'post_views_meta', 'post_video','side');
    add_meta_box('post-views-meta', 'Update post views', 'post_views_meta', 'post_gallery','side');
  } 
function post_views_meta($post) {
  $views = get_post_meta($post->ID, 'post_views_count', true);
   echo "<input type ='text' value ='" . $views . "' name ='post_views_count'>
         <button data-post-id = '" . $post->ID . "' class ='bd_update_post_views'>Update views</button>";
}
add_action( 'add_meta_boxes', 'add_views_meta' );
    //Save the post views
function save_post_views( $post_id ) {
   update_post_meta($_POST['post_ID'], 'post_views_count', $_POST['post_views_count']);
}
 add_action( 'edit_page_form', 'save_post_views', 1,3 );  
 add_action( 'pre_post_update', 'save_post_views',1,3 );  
?>