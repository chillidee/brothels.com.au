<?php /**
 * Register Widget Area.
 *
 */
class bd_widgets {
public static function bd_widgets_init() {
 
  register_sidebar( array(
    'name' => 'In city sidebar',
    'id' => 'in_city_sidebar',
    'before_widget' => '<aside class = "widget">',
    'after_widget' => '</aside>',
    'before_title' => '<header class ="entry-header"><h1 class ="entry-title">',
    'after_title' => '</h1></header>',
  ) );
}
}
  ?>