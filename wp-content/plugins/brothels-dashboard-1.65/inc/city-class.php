<?php
/**
 * Class bd_city_cpt
 * This class creates our cities custom post type, including meta boxes and custom slug implentation
 * Contains static functions that can be called by actions and filters
 * @ Author- Che Jansen- Chillidee Marketing Group
 * Version 1.1.0
 */

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

// Add og:published_time for cities
add_filter( 'wpseo_opengraph_show_publish_date', 'return_city_type');  
function return_city_type() {
    if ( ! is_singular( 'city' ) ) {
         return false;
    }
    return true;
  }
//Remove the quick edit link from the city post type
$supports = array(
    ''
);
function remove_quick_edit2($actions)
{
    global $post;
    if ($post->post_type == 'city') {
        unset($actions['inline hide-if-no-js']);
    }
    return $actions;
}

if (is_admin()) {
    add_filter('post_row_actions', 'remove_quick_edit2', 10, 2);
}

class bd_city_cpt
{
    //Create custom post type for our cities
    public static function create_cities()
    {
        register_post_type('city', array(
            'labels' => array(
                'name' => 'Cities',
                'singular_name' => 'City',
                'add_new' => 'Add a New City',
                'add_new_item' => 'Add New City',
                'edit' => 'Edit',
                'edit_item' => 'Edit City',
                'new_item' => 'New Cities',
                'view' => 'View',
                'view_item' => 'View City',
                'search_items' => 'Search Ciies',
                'not_found' => 'No ',
                'not_found_in_trash' => 'No Cities found in Trash',
                'parent' => 'Parent City'
            ),
            'public' => true,
            'menu_position' => 14.236,
            'supports' => array(
                'title',
                'thumbnail',
                'comments'
            ),
            'taxonomies' => array(
                'post_tag',
                'category'
            ),
            'menu_icon' => BD_PLUGIN_URL . 'assets/images/icon.png',
            'has_archive' => true,
            'rewrite' => array(
                'slug' => '',
                'with_front' => false
            )
        ));
    }
    /**
     * Remove the slug from published post permalinks.
     * Allows us to have clean, custom urls for this post type
     */
    public static function custom_remove_cpt_slug($post_link, $post, $leavename)
    { 
        if ('city' != $post->post_type || 'publish' != $post->post_status) {
            return $post_link;
        }
        $post_link = str_replace('/' . $post->post_type . '/', '/', $post_link);
        return $post_link;
    }

    /**
     * Some hackery to have WordPress match postname to any of our public post types
     * All of our public post types can have /post-name/ as the slug, so they better be unique across all posts
     * Typically core only accounts for posts and pages where the slug is /post-name/
     */
    public static function custom_parse_request_tricksy($query)
    {
        
        // Only noop the main query
         if (!$query->is_main_query())
            return;
        
        // Only noop our very specific rewrite rule match
           if (2 != count($query->query) || !isset($query->query['page'])) {
            return;
        }
        
        // 'name' will be set if post permalinks are just post_name, otherwise the page rule will match
          if (!empty($query->query['name'])) {
            $query->set('post_type', array(
                'post',
                'city',
                'page'
            ));
        }
        $query->reset_postdata();
    }
      
    /**
     * Register the meta boxes for our cities custom post types
     */
    public static function brothels_meta_1()
    {
        global $_wp_post_type_features;
        add_meta_box('brothels_meta_box_top6', 'Paid listings', array(
            'bd_city_cpt',
            'output_brothels_top_6_meta_box'
        ), 'city', 'normal', 'high');
        add_meta_box('brothels_meta_box', 'Listings', array(
            'bd_city_cpt',
            'output_brothels_meta_box'
        ), 'city', 'normal', 'high');
        add_meta_box('brothels_meta_box_content', 'Page Content', array(
            'bd_city_cpt',
            'city_page_editor'
        ), 'city', 'normal', 'high');
        add_meta_box('brothels_meta_box_bottom_content', 'Bottom Content', array(
            'bd_city_cpt',
            'city_bottom_editor'
        ), 'city', 'normal', 'high');
      add_meta_box('brothels_meta_box_mobile_banner', 'Mobile Banner', array(
            'bd_city_cpt',
            'mobile_banner'
        ), 'city', 'normal', 'high');
      add_meta_box('brothels_meta_box_seo', 'Seo Hack', array(
            'bd_city_cpt',
            'meta_box_seo'
        ), 'city', 'normal', 'high');
      add_meta_box('city_page_ga_category', 'Google Analytics Category', array(
            'bd_city_cpt',
            'city_ga_editor'
        ), 'city', 'side', 'default');
    }
    /**
     * Function city_page_editor
     * Adds an editor for content to our post type, under our meta boxes
     */
    public static function city_page_editor($post)
    {
        wp_editor($post->post_content, 'bd_city_editor');
    }
  public static function city_bottom_editor($post)
    {
        $content = get_post_meta ( $post->ID,  'bd_bottom_content', true);
        wp_editor( $content, 'city_bottom_editor' );
      echo "<button class ='bd_button nice_blue_button' id ='save_bottom_content'>Save bottom content</button>";
    }
    public static function mobile_banner($post)
    {
        $content = get_post_meta ( $post->ID,  'brothels_meta_box_mobile_banner', true);
        wp_editor( $content, 'mobile_banner_content' );
      echo "<button class ='bd_button nice_blue_button' id ='save_mobile_banner'>Save Mobile banner</button>";
    }
    public static function city_ga_editor($post)
    {
        $content = get_post_meta ( $post->ID,  'bd_ga_category', true);
       echo "<input type = 'text' value = '" . $content . "' id ='bd_ga_category_value'>
         <button id ='bd_save_ga_category' class ='bd_button'>Update</button>
       ";
    }
   public static function meta_box_seo($post)
    {
        $title = get_post_meta ( $post->ID,  '_yoast_wpseo_title', true);
        $description = get_post_meta ( $post->ID,  '_yoast_wpseo_metadesc', true);
        echo "
          <input type = 'text' value = '" . htmlentities ( $title, ENT_QUOTES) . "' id ='bd_yoast_title'>
          <textarea id ='bd_yoast_description'>" . htmlentities ( $description, ENT_QUOTES) . "</textarea>
          <button id ='bd_save_seo_hack' class ='bd_button'>Update</button>
          <style>#bd_yoast_description {width: 100%;float: none;margin-top: 20px;height: 100px;}</style>
       ";
    }
    /**
     * Function bd_add_brothel_form
     * Adds the div brothels_overlay to be populated by ajax
     */
    public static function bd_add_brothel_form()
    {
        global $post;
        echo "
           <div id ='underlay'></div>
           <div id ='brothels_overlay'>
              
</div>";
    }
    /**
     * Function output_brothels_top_6_meta_box
     * Output for the advertisers meta box
     * Checks for any advertisers current or queued, and outputs accordingly
     */
    public static function output_brothels_top_6_meta_box()
    {
      $weekAgo = date('d-m-Y', strtotime( date('d-m-Y') ) - ( 60*60*24*7 ) );
      $today  = date('d-m-Y');
      echo "<script>
      var page_id = '" . get_the_id() . "';
</script>
          <style>
          #bd_city_by_link {
          display:inline-block!important;
          </style>";
        global $post;
        date_default_timezone_set('Australia/Sydney');
        $adv = get_post_meta(get_the_id(), 'top_6', true );
        $l = 1;
        $days = get_option( 'notification_times', '3' ) + 1;
        $notifications = get_option( 'bd_notifications', array() );
        $j = get_post_meta(get_the_id(), 'advertiser_count', true );
        $helper = new bd_helper();
        $result = $helper->calculate_advertisers( $j, $adv, $notifications, $days );
        $top_6 = $result['top_6'];
        $notifications = $result['notifications'];
        update_option( 'bd_notifications', $notifications );
        update_post_meta(get_the_id(), 'top_6', $top_6);
        $brothels_in = get_post_meta( get_the_id(), 'brothels_in_page', true);
        foreach( $brothels_in as $brothel ){
             $update = get_post_meta($brothel, 'brothels', true );
             if( !isset( $update['description'][get_the_title()] ))
                 continue;
             $update['description'][get_the_ID()] = $update['description'][get_the_title()];
             unset( $update['description'][get_the_title()] );
             update_post_meta($brothel, 'brothels', $update );
        }
        foreach( $brothels_in as $brothel ){
             $update = get_post_meta($brothel, 'brothels', true );
             if( !isset( $update['description'][ htmlspecialchars_decode ( get_the_title() )] )){
               continue;
               
               }
             $update['description'][get_the_ID()] = $update['description'][htmlspecialchars_decode( get_the_title() )];
             unset( $update['description'][htmlspecialchars_decode ( get_the_title() )] );
             update_post_meta($brothel, 'brothels', $update );
        }
        echo "<div id = 'top_spot_changer'>Show Statistics</div>
  <div class ='date_range_picker_stats'>
          <input id ='city_stats_top_from' class ='datepicker' value ='" . $weekAgo . "' placeholder ='Date from'><input value ='" . $today . "' id ='city_stats_top_to' class ='datepicker' placeholder ='Date To'><div id ='statistics_date_changer'></div>
  </div>
  <label id ='advertiser_count_label'>Change image spots</label>
  <select id ='advertiser_count_select'>";
        for ($l = 0; $l < 16; $l++) {
            echo "<option value ='" . $l . "'";
            if ($j == $l) {
                echo " selected";
            }
            echo ">" . $l . "</option>";
        }
        echo "
  </select>
    <button id ='set_advertiser_count' class ='bd_button'>Update</button>
    <input type ='hidden' name ='security' value ='" . wp_create_nonce('set_advertiser_count_nonce') . "' id ='set_advertiser_count_secrurity'>
    <div id ='bd_city_normal_view' class ='active_screen'>
      <div class ='spacer'></div>";
        if ($j == '') {
            $j = 0;
        }
        for ($k = 1; $k < $j + 1; $k++) {
            $class = 'top_3';
            if (isset($top_6[$k]['current']) && count($top_6[$k]['current'] > 0)) {
                $count_down       = round((strtotime($top_6[$k]['current']['date_to']) - strtotime(date('d-m-Y'))) / 60 / 60 / 24);
                $queued           = '';
                $current_trans_id = 0;
                if (isset($top_6[$k]['current']['trans_id'])) {
                    $current_trans_id = $top_6[$k]['current']['trans_id'];
                }
                ;
                $queued_trans_id = 0;
                if (isset($top_6[$k]['queued']['trans_id'])) {
                    $queued_trans_id = $top_6[$k]['queued']['trans_id'];
                }
                ;
                if (isset($top_6[$k]['queued']['brothel_id'])) {
                    $queued = "data-queued_brothel_id = '" . $top_6[$k]['queued']['brothel_id'] . "' data-queued_trans_id = '" . $current_trans_id . "' ";
                }
                echo "
       <input type ='hidden' value = '" . $top_6[$k]['current']['brothel_id'] . "' class ='post_id_holder'>
                    <div class ='top_spot taken_top_spot' id = 'top_" . $k . "' " . $queued . "data-id = '" . $k . "' data-brothel_id = '" . $top_6[$k]['current']['brothel_id'] . "' data-trans_id = '" . $queued_trans_id . "'>
            <div data-id = '" . $k . "' class ='taken_position " . $class . "'></div><span class = 'top_6_title'>
                    " . get_the_title($top_6[$k]['current']['brothel_id']) . "</span>
                    <span class ='top_6_countdown'>" . $count_down . " days</span>";
                if (isset($top_6[$k]['queued']) && isset($top_6[$k]['queued']['brothel_id'])) {
                    echo "<input type ='hidden' value = '" . $top_6[$k]['queued']['brothel_id'] . "' class ='post_id_holder'>
                           <div class ='queued queued_taken'><div class ='vertical_text'>" . get_the_title($top_6[$k]['queued']['brothel_id']) . "</div></div>";
                } else {
                    echo "<div class ='queued'><div class ='vertical_text'>Available</div></div>";
                }
                echo "
       </div>";
            } else if (isset($top_6[$k]['queued']) && count($top_6[$k]['queued'] > 0)) {
                echo "
       <input type ='hidden' value = '" . $top_6[$k]['queued']['brothel_id'] . "' class ='post_id_holder'>
       <div class ='top_spot queued_top_spot' id = 'top_" . $k . "' data-id = '" . $k . "'>
            <div data-id = '" . $k . "' class ='taken_position " . $class . "' data-trans_id = '" . $top_6[$k]['queued']['trans_id'] . "'></div><span class = 'top_6_title'>
                    <span class ='available_until' data-id = '" . $k . "' data-days = '" . (strtotime($top_6[$k]['queued']['date_from']) - strtotime(date('d-m-Y'))) / 60 / 60 / 24 . "'>Available Until<div class ='bd_date'>" . $top_6[$k]['queued']['date_from'] . "</div></span>
                      <div class ='queued queued_taken'><div class ='vertical_text'>" . get_the_title($top_6[$k]['queued']['brothel_id']) . "</div></div>
       </div>";
            } else {
                echo "
       <input type ='hidden' value = '' class ='post_id_holder'>
       <div class ='top_spot' id = 'top_" . $k . "'>
            <div data-id = '" . $k . "' class ='main " . $class . "'>Position " . $k . "
                        <br><span class ='available'>Available</span></div>
               <div class ='queued'></div>
       </div>
         <style>
         #bd_city_by_link {
         display:inline-block!important;
         }</style>";
            }
        }
          echo "</div>
                     <div id ='back_to_stats' class ='hidden'>Back to Statistics</div>
                     <div id ='create_perf_report' class ='hidden'>Create Performance Report</div>
                     <div id ='hide_brothel_names' class ='hidden'>Hide Brothel names</div>
         <div id ='bd_city_statistics_view'>";
        for ($k = 1; $k < $j + 1; $k++) {
       echo "
         <div class ='top_spot stat_top_spot' id = 'stat_top_" . $k . "' data-position = '" . $k . "'>
            <div class ='position_holder'>Position " . $k . "</div>
            <div class ='stat-details'>See details</div>
            <div class ='event_holder'></div>
         </div>";
       }
          echo '</div>';
             $stats_data = get_post_meta(get_the_id(), 'bd_stat_settings', true);
        echo "
    <div id ='bd_perf_report'>
            <h4>Performance report</h4>
            <h2 id ='perf_brothel'></h2>
            <h5 id ='perf_pagePos'></h5>
            <span id ='perf_dates'></span>
            <table>
              <tr>
                  <td id ='perf_web_ref'></td>
                  <td>Website Referrals</td>
              </tr>
              <tr>
                  <td id ='perf_phone_click'></td>
                  <td>Phone Views</td>
              </tr>
            </table>
          <div id ='perf_logo'></div>
      </div>
    <div id ='bd_city_page_statistics'>
          <div id ='bd_close_page_stats'></div>
    <h2 id = 'bd_stats_header'>Statistics</h2>
    <label class ='bd_statistics_label' id ='bd_city_dates_label'>Select dates</label>
              <input class ='date_picker' id ='from_datepicker_stats' placeholder ='From' name ='bd_statistics_from'>
              <input class ='date_picker' id ='to_datepicker_stats' placeholder ='To' name ='bd_statistics_to'>
              <div id ='bd_set_dates'></div>
    <span id ='statistics_date_range'></span>";
        $in_page = get_post_meta($post->ID, 'brothels', true);
                echo "<label class ='bd_statistics_label' id ='bd_city_position_label'>Position</label>
                      <select name ='bd_stats_city_select'>";
                $j = get_post_meta(get_the_id(), 'advertiser_count', true );
                $j =  intval ( $j );
                for ($x = 1; $x <= $j; $x++) {
                    echo "<option value = '" . $x . "'>" . $x . "</option>";
                }
                echo "</select>";
        echo "
    <div id ='statistics_holder'><img src = '" . BD_PLUGIN_URL . 'assets/images/reload.gif' . "' class = 'loader'></div>
</div>
    <script>
        var loader = \"<img src = '" . BD_PLUGIN_URL . "assets/images/reload.gif'  class = 'in-block-loader'></div>\";
      </script>";
    }
    /**
     * Function output_brothels_meta_box
     * Output for Brothels in page meta box
     * Gets all the brothels added to the page, and outputs them in a drag and drop table
     */
    public static function output_brothels_meta_box()
    {   echo "          <button id ='bd_show_hidden_seo'>Show SEO</div>";
                echo "
       <button class ='bd_button centred_button' id ='open_add_brothel_form'>Add Brothel</button>
       ";
        global $post;
        $brothels    = array();
        $brothels    = get_post_meta($post->ID, 'brothels');
        $brothels_in = get_post_meta(get_the_id(), 'brothels_in_page', true);
        $j = get_post_meta(get_the_id(), 'advertiser_count', true);
        $i           = -1;
        echo "       <input type ='hidden' value ='" . $j . "' id ='bd_adv_count'>";
        if (count($brothels_in) == 0 || $brothels_in == '') {
            echo "
       <div class ='notification'>There are currently no brothels added</div>
              ";
        } else {
            echo "
          <table class ='bd_table drag_table' id = 'table-dnd1'>
                    <tr class = 'nodrag nodrop'>
                      <th>Name</th>
                      <th>Has image</th>
                      <th>Position</th>
                      <th>Currently in</th>
                      <th>Actions</th>
                    </tr>";
          $top_6 = get_post_meta(get_the_id(), 'top_6', true);
          $adv_array = array();
          if ( is_array( $top_6  )){
          foreach ( $top_6 as $key => $adv ) {
            if ( isset( $adv['current'] ) ) {
                  $adv_array[$key] = $adv['current']['brothel_id'];
            }
          }
        }
              foreach ($brothels_in as $id) {
                  $j++;
                  $pos = $j;
                  $i++;
                  $class = '';
                if ( in_array ($id , $adv_array )) {
                     $class = ' in_image_spot';
                     $pos = array_search ( $id , $adv_array );
                     $j--;
                }
                  $brothel_data = get_post_meta($id, 'brothels');
                  echo "<tr data-position = '" . $pos . "' data-id = '" . $id . "' id = '" . $i . "' class = 'city-drag-table" . $class . "'>
                     <td class ='bd_first_cell'><input type ='hidden' value ='" . $id . "'>" . $brothel_data[0]['name'] . "</td>
                      <td>";
                  if (!isset($brothel_data[0]['image']) || $brothel_data[0]['image'] == '') {
                    // echo '<div data-id = "' . $id . '" class = "bd_error load_image"></div>';
                    echo '<div data-id = "' . $id . '" class = "bd_error load_image no-image-error">Add image</div>';
                   } else {
                      echo '<div class = "bd_ok"></div>';
                   }
                   echo "</td><td";
                       if ( $class != ' in_image_spot' ) {
                             echo " class ='position_td'";
                               }
                               echo ">" . $pos . "</td>
         <td>";
                if (isset($brothel_data[0]['in_page'])) {
                    foreach (array_unique($brothel_data[0]['in_page']) as $page) {
                        echo get_the_title($page) . '<br>';
                    }
                }
                else {
                  print_r ( $brothel_data );
                  }
                echo "</td>
                 <td class ='bd_last_cell'><a href ='#' class = 'bd_remove_brothel_from_page' data-id = '" . $id . "'>Remove</a></td>
                  </tr>";
            }
            echo "</table>";
        }
    }
    /**
     * Function bd_save_city_content
     * Called by the save post hook
     * @parameter- $post_id
     * Adds the editor content to the post array, and saves it
     */
    public static function bd_save_city_content($post_id)
    {
        if (!wp_is_post_revision($post_id)) {
            
            // unhook this function so it doesn't loop infinitely
            remove_action('save_post', array(
                'bd_city_cpt',
                'bd_save_city_content'
            ), 10, 2);
            
            // Get the post type object.
            $post_type = get_post_type($post_id);
            
            if ($post_type != 'city')
                return $post_id;
            // Check if the current user has permission to edit the post.
            if (!current_user_can('delete_posts'))
                return $post_id;
            
            // Get the posted data and sanitize it for use as an HTML class.
            if (isset($_POST['bd_city_editor'])) {
                $my_post = array(
                    'ID' => $post_id,
                    'post_content' => $_POST['bd_city_editor']
                );
                // Update the post into the database
                wp_update_post($my_post);
            }
        }
    }
    /**
     * Function bd_add_brothels_form
     * Called by the footer filter
     * Outputs the add brothel form with all brothels not in page added
     */
    public static function bd_add_brothels_form()
    {
        $post_type = get_post_type();
        if ($post_type == 'city' || $post_type == 'brothel') {
            echo "<div id ='bd_add_brothels_form'>
            <div id='close_add_brothels_form'>
                <span>Add Brothels to page</span>
                   <img src = '" . BD_PLUGIN_URL . 'assets/images/close.png' . "'>
            </div>";
            $brothels_in = get_post_meta(get_the_id(), 'brothels_in_page', true);
            $type        = 'brothel';
            $args        = array(
                'post_type' => $type,
                'posts_per_page' => -1
            );
            $my_query    = null;
            $my_query    = new WP_Query($args);
            if ($my_query->have_posts()) {
                echo "
            <form id ='bd_add_brothels_to_page'>
            <input type ='hidden' value ='add_brothels_to_page' name = 'action'>
             <input type ='hidden' name ='security' value ='" . wp_create_nonce('bd_add_brothel_nonce') . "' id ='set_advertiser_count_secrurity'>
            <input type ='hidden' value ='' name = 'page_id'>
               <table class ='bd_table' id ='bd_add_brothels_datatable'>
                 <thead>
                    <tr>
                      <th>Select</th>
                      <th>Name</th>
                      <th>Has image</th>
                      <th>Has Website</th>
                      <th>Currently in</th>
                      <th>Description</th>
                    </tr>
                 </thead>
                 <tfoot>
                    <tr>
                      <th>Select</th>
                      <th>Name</th>
                      <th>Has image</th>
                      <th>Has Website</th>
                      <th>Currently in</th>
                      <th>Free Description</th>
                    </tr>
            </tfoot>
            <tbody>";
                while ($my_query->have_posts()):
                    $my_query->the_post();
                    $id = get_the_id();
                    if (!is_array($brothels_in)) {
                        $brothels_in = array();
                    }
                    if (!in_array($id, $brothels_in)) {
                        $meta_values = get_post_meta($id, 'brothels', true);
                        if (isset($meta_values['name'])) {
                            echo "<tr>
              <td><input type ='checkbox' name = 'id[]' value = '" . $id . "' class ='bd_add_brothels_checkbox'>
                  <input type ='hidden' value ='" . $id . "'></td>
              <td>" . get_the_title() . "</td>
              <td>" . (!isset($meta_values['image']) ? '<div data-id = "' . $id . '" class = "bd_error load_image"></div>' : '<div class = "bd_ok"></div>') . "</td>
              <td>" . (!isset($meta_values['website']) ? '<div class = "bd_error enter_website"></div>' : '<div class = "bd_ok"></div>') . "</td>
              <td></td>
              <td>";
                            if (isset($meta_values['description']) && array_key_exists('unassigned', $meta_values['description'])) {
                                if (count($meta_values['description']['unassigned']) == 1) {
                                    echo "<div class ='bd_ok bd_status' data-id = '" . $id . "'></div>";
                                } else {
                                    echo "<div class ='bd_alert bd_status' data-id = '" . $id . "'>
                        <input type ='hidden' value = '' class = 'bd_description_choice_holder' data-id = '" . $id . "' name = 'description_choice[\"" . $id . "\"]'></div>";
                                }
                            } else {
                                echo "<div class ='bd_error enter_desc bd_status' data-id = '" . $id . "'>";
                            }
                            echo "</td>
              </tr>";
                        }
                    }
                endwhile;
                echo "</tbody></table>
        <button class='bd_button nice_blue_button' id='bd_add_new_brothels_ajax'>Add brothels</button>
        </form>
        </div>";
                $my_query->reset_postdata(); // Restore global post data stomped by the_post().
            }
        }
        
    }
    /**
     * Function bd_remove_top_6
     * Creates the remove top_6 form
     * Called by the footer filter
     */
    public static function bd_remove_top_6()
    {
        $post_type = get_post_type();
        $post_type = get_post_type();
        if ($post_type == 'city' || $post_type == 'brothel') {
            echo "<div id ='bd_remove_top_6_div' class ='popup'>
            <div id='close_remove_top_6' class ='close_popup'>
                   <span>Remove <span id ='bd_remove_brothel_name'></span> from position <span id ='bd_remove_pos_number'></span></span>
                   <img src = '" . BD_PLUGIN_URL . 'assets/images/close.png' . "'>
            </div>
            <form id ='bd_remove_brothels_from_top'>
               <input type ='hidden' value ='bd_remove_brothels_from_top' name = 'action'>
               <input type ='hidden' value ='' name = 'page_id'>
               <input type ='hidden' value ='' name ='brothel_id' id ='bd_remove_top_brothel_id'>
               <input type ='hidden' value ='' name ='bd_current_or_queued'>
               <input type ='hidden' name ='trans_id' id ='trans_details_trans_id'>
               <input type ='hidden' name ='pos_no' id ='trans_pos_no'>
               <table class ='bd_table' id ='remove_top_6_table'>
                  <tr>
                     <td><label>Reason for removing</label></td>
                     <td><input type ='text' value ='' name ='bd_removal_reason'></td>
                  </tr>
                  <tr>
                     <td><label>Refund offered?</label></td>
                     <td><input type ='text' value ='' name ='bd_refund_offered'></td>
                  </tr>
                  <tr>
                     <td><label>Other notes</label></td>
            <td><textarea value ='' name ='bd_removal_notes'></textarea></td>
                  </tr>
               </table>
               <button class='bd_button next-button' id='bd_remove_brothel_top_button'>Done</button>
            </form>
        </div>";
        }
    }
    /**
     * Function bd_choose_descriptions
     * Creates the choose description box
     * Called by the footer filter
     */
    public static function bd_choose_descriptions()
    {
        echo "
     <div id = 'underlay2'></div>
   <form id ='bd_choose_descriptions_form'>
         <div id ='bd_choose_description_popup'>
               <span>Choose description</span>
               <div id='close_choose_description_popup'>
                   <img src = '" . BD_PLUGIN_URL . 'assets/images/close.png' . "'>
         </div>
         <input type ='hidden' value ='bd_choose_descriptions' name = 'action'>
         <input type ='hidden' value = '' name ='post_id' id ='choose_descriptions_post_id'>
         <div id = 'bd_choose_descriptions_output'></div> 
         <button class='bd_button next-button' id='bd_complete_description_choice'>Done</button>
         </div>
         </form>";
    }
    /**
     * Function bd_really_remove_brothel
     * Creates the Really remove brothel popup
     * Called by the footer filter
     */
    public static function bd_really_remove_brothel()
    {
        $post_type = get_post_type();
        if ($post_type == 'city' || $post_type == 'brothel') {
            echo "
         <div id ='bd_really_remove_brothel_popup' class ='popup'>
             <span>Are you sure you want to remove this brothel from the page?</span>
             <div class = 'close_really_remove_popup close_popup'>
                 <img src = '" . BD_PLUGIN_URL . 'assets/images/close.png' . "'>
             </div>
             <input type ='hidden' value ='' id = 'bd_really_remove_brothel_id'>
             <button class ='bd_button close_really_remove_popup'>Cancel</button>
             <button class ='bd_button' id ='bd_remove_brothel_from_page'>Remove</button>
        </div>";
        }
    }
    /**
     * Function bd_assign_top_spot
     * Creates assign top spot popup, to choose which brothel you are promoting
     * Called by the footer filter
     */
    public static function bd_assign_top_spot()
    {
        echo "<img src = '" . BD_PLUGIN_URL . 'assets/images/reload.gif' . "' class = 'generic_loader'>
          <div id ='white_underlay'></div>
      <div id = 'assign_top_spots' class ='popup'>
      <input type ='hidden' name ='security' value ='" . wp_create_nonce('bd_populate_eligible_brothels') . "' id ='bd_populate_eligible_brothels'>
            <div id='close_assign_top_spots' class ='close_popup'>
                <span>Add Brothel to image spot</span>
                   <img src = '" . BD_PLUGIN_URL . 'assets/images/close.png' . "'>
            </div>
      <div id ='bd_assign_content_holder'><img src = '" . BD_PLUGIN_URL . 'assets/images/reload.gif' . "' class = 'loader'></div>
      <button class='bd_button nice_blue_button' id='open_confirm_assign_popup'>Next</button>
      </div>";
    }
    /**
     * Function confirm_assign_top_spot
     * Creates assign top spot popup, to confirm your selection, and enter transaction details
     * Called by the footer filter
     */
    public static function confirm_assign_top_spot()
    {
        $post_type = get_post_type();
        if ($post_type == 'brothel' || $post_type == 'city') {
            date_default_timezone_set('Australia/Sydney');
            $date = date('d-m-Y');
            echo " <div id = 'confirm_assign_top_spot' class ='popup'>
            <form id ='confirm_assign_top_form'>
            <input type ='hidden' name ='action' value = 'confirm_assign_top_spot'>
            <input type ='hidden' name ='page_id' value =''>
            <input type ='hidden' name = 'user_id' value = '" . get_current_user_id() . "'>
            <input type ='hidden' name = 'date_processed' value = '" . $date . "'>
            <input type ='hidden' name = 'brothel_chosen' value ='' id ='confirm_assign_top_spot_id'>
            <input type ='hidden' name ='position_chosen' value ='' id ='confirm_assign_pos_chosen'>
            <input type ='hidden' name = 'choose_or_queue' value ='' id ='confirm_choose_or'>
            <div id='close_confirm_assign_top' class ='close_popup'>
            <span>Place Brothel in top spot</span>
                   <img src = '" . BD_PLUGIN_URL . 'assets/images/close.png' . "'>
            </div>
      <div id ='confirm_assign_errors'></div>
          <table class ='bd_table' id ='confirm_assign_table'>
      <tr><td></td></tr>
          <tr>
             <td class = 'smaller_td'><label>Dates</label</td>
             <td><input class ='date_picker' name ='date_from' placeholder= 'Date From' id ='from_datepicker'>
                 <input class ='date_picker' name ='date_to' placeholder= 'Date To' id ='to_datepicker'></td>
          </tr>
          <tr>
             <td class = 'smaller_td'><label>Payment Amount</label</td>
             <td><input type ='text' name ='payment_amount'></td>
          </tr>
          <tr>
             <td class = 'smaller_td'><label>Payment Type</label</td>
             <td><select name ='payment_type'>
                   <option value ='visa' selected>Visa</option>
                   <option value ='mastercard'>Mastercard</option>
                   <option value ='amex'>Amex</option>
                   <option value ='EFT'>EFTPOS</option>
                   <option value ='Bank Transfer'>Bank Transfer</option>
                   <option value ='promotion'>PROMOTION</option>
              </select></td>
          </tr>
          <tr>
             <td class = 'smaller_td'><label>Transaction number</label</td>
             <td><input type ='text' name ='trans_no'></td>
          </tr>
          <tr><td></td></tr>
      </table>
</form>
      <button class='bd_button nice_blue_button' id='bd_confirm_assign_brothel_button'>Update Position</button>
      </div>";
        }
    }
    /**
     * Function transaction_details
     * Creates the holder div for the transaction details popup
     * Data is added by ajax
     * Called by the footer filter
     */
    public static function transaction_details()
    {
        date_default_timezone_set('Australia/Sydney');
        $date = date('d-m-Y');
        echo " <div id = 'transaction_details_div' class ='popup'>
            <form id ='transaction_details_form'>
            <input type ='hidden' name ='page_id' value =''>
            <input type ='hidden' name = 'user_id' value = '" . get_current_user_id() . "'>
            <input type ='hidden' name = 'date_processed' value = '" . $date . "'>
            <input type ='hidden' name = 'bd_trans_brothel_chosen' value ='' id ='transaction_details_brothel_id'>
            <input type ='hidden' name = 'queued_bd_trans_brothel_chosen' value ='' id ='queued_transaction_details_brothel_id'>
            <input type ='hidden' name ='position_chosen' value ='' id ='transaction_details_position_chosen'>
            <input type ='hidden' name ='position_chosen' value ='' id ='queued_transaction_details_position_chosen'>
            <input type ='hidden' name = 'choose_or_queue' value ='queue' id ='confirm_choose_or'>
            <div id='close_transaction_details' class ='close_popup'>
      <span>Details- Position <span id = 'bd_trans_details_pos_number'></span></span>
                   <img src = '" . BD_PLUGIN_URL . 'assets/images/close.png' . "'>
            </div>
      <div id ='transactions_details_content_holder'><img src = '" . BD_PLUGIN_URL . 'assets/images/reload.gif' . "' class = 'in-block-loader'></div></div>
</form>
      <script>
        var loader = \"<img src = '" . BD_PLUGIN_URL . "assets/images/reload.gif'  class = 'in-block-loader'></div>\";
      </script>";
    }
    /**
     * Function bd_custom_city_columns
     * Customises the columns for the city post type
     * @parameters- $columns
     */
    public static function bd_custom_city_columns($columns)
    {
        $columns = array(
            'title' => __('Title'),
            'events' => __('Clicks last 7 days'),
            'revenue' => __('Revenue'),
            'date' => __('Date')
        );
        return $columns;
    }
    /**
     * Function display_cities_columns
     * Customises the columns for the city post type
     * @paramaters- $column, $post_id
     */
    public static function display_cities_columns($column, $post_id)
    {
        if ($column == 'events') {
            echo "<div class = 'bd_ajax_holder'><img src = '" . BD_PLUGIN_URL . "assets/images/reload.gif' class ='bd_event_stats' data-id = '" . $post_id . "' onload='bd_get_city_clicks(jQuery(this))'></div>";
        }
        if ($column == 'revenue') {
            echo "<div class = 'bd_ajax_holder'><img src = '" . BD_PLUGIN_URL . "assets/images/reload.gif' class ='bd_event_stats' data-id = '" . $post_id . "' onload='bd_get_city_revenue(jQuery(this))'></div>";
        }
    }
}
?>