<?php 
  /**
 * Redirect user after successful login.
 *
 * @param string $redirect_to URL to redirect to.
 * @param string $request URL the user is coming from.
 * @param object $user Logged user's data.
 * @return string
 */
class bd_login_screen {
public static function my_login_redirect( $redirect_to, $request, $user ) {
  //is there a user to check?
  global $user;
  if ( isset( $user->roles ) && is_array( $user->roles ) ) {
    //check for admins
      if ( in_array( 'administrator', $user->roles ) ) {
          // redirect them to the default place
        return 'wp-admin/admin.php?page=custom_screen';
      } else {
          return home_url();
      }
      } else {
        return  home_url();
      }
   }
public static function bd_register_menu() {
    $bd_custom_page = add_menu_page( 'Main Dash', 'Main Dash', 'manage_options', 'custom_screen', array( 'bd_login_screen', 'output_custom_screen'), '', 12.221 );
    add_action( 'load-' . $bd_custom_page, array('bd_login_screen','load_bd_custom_page_scripts_action'));
}
  public static function load_bd_custom_page_scripts_action(){
        // Unfortunately we can't just enqueue our scripts here - it's too early. So register against the proper action hook to do it
        add_action( 'admin_enqueue_scripts', array ( 'bd_login_screen', 'enqueue_bd_custom_screen_scripts'));
    }

    public static function enqueue_bd_custom_screen_scripts(){
        // Isn't it nice to use dependencies and the already registered core js files?
        wp_enqueue_script( 'bd-custom-js', BD_PLUGIN_URL . 'assets/js/bd-custom.js', array('jquery'));
        wp_enqueue_style( 'bd-custom-css', BD_PLUGIN_URL . 'assets/css/bd-custom.css', '');
    }

 public static function output_custom_screen() {
require_once( ABSPATH . 'wp-load.php' );
require_once( ABSPATH . 'wp-admin/admin.php' );
require_once( ABSPATH . 'wp-admin/admin-header.php' );
?>
<h1>Welcome to Brothels Dashboard</h1>
   <a href = '/wp-admin/'>
       <div id ='bd_wordpress'>
     Content Management</div>
   </a>
   <a href = '/wp-admin/index.php?page=brothels-dashboard'>
       <div id ='bd_advertise'>
     Advertising</div>
   </a>

  </div>
<?php }
}
  ?>
