<?php

require_once(SSXT_PATH.'core/grid.php');

smart_sitemap_xml()->init_default_maps();
smart_sitemap_xml()->init_custom_maps();

?>
<form method="post" action="">
    <?php settings_fields('smart-sitemapxml-tools-content'); ?>

    <div class="sct-cleanup-left">
        <div id="scs-scroll-sidebar">
            <p>
                <?php _e("This panel contains settings for the sitemap content.", "smart-sitemapxml-tools"); ?>
            </p>
            <input class="button-primary" type="submit" value="<?php _e("Save Settings", "smart-sitemapxml-tools"); ?>" />
        </div>
    </div>
    <div class="sct-cleanup-right">
        <h3 style="margin-top: 0px"><?php _e("Core Sitemaps", "smart-sitemapxml-tools"); ?></h3>

        <?php

        $_grid = new ssxt_grid_rules();
        $_grid->sitemap_type = 'core';
        $_grid->items = smart_sitemap_xml()->default_maps['core'];
        $_grid->priorities = smart_sitemap_xml()->default_priorities['core'];
        $_grid->prepare_items();
        $_grid->display();

        ?>

        <h3><?php _e("Post Types Archives Sitemaps", "smart-sitemapxml-tools"); ?></h3>

        <?php

        $_grid = new ssxt_grid_rules();
        $_grid->sitemap_type = 'cpts';
        $_grid->items = smart_sitemap_xml()->default_maps['cpts'];
        $_grid->priorities = smart_sitemap_xml()->default_priorities['cpts'];
        $_grid->prepare_items();
        $_grid->display();

        ?>

        <h3><?php _e("Taxonomies Archives Sitemaps", "smart-sitemapxml-tools"); ?></h3>

        <?php

        $_grid = new ssxt_grid_rules();
        $_grid->sitemap_type = 'taxs';
        $_grid->items = smart_sitemap_xml()->default_maps['taxs'];
        $_grid->priorities = smart_sitemap_xml()->default_priorities['taxs'];
        $_grid->prepare_items();
        $_grid->display();

        ?>

        <h3><?php _e("Date Based Archives Sitemaps", "smart-sitemapxml-tools"); ?></h3>

        <?php

        $_grid = new ssxt_grid_rules();
        $_grid->sitemap_type = 'date';
        $_grid->items = smart_sitemap_xml()->default_maps['date'];
        $_grid->priorities = smart_sitemap_xml()->default_priorities['date'];
        $_grid->prepare_items();
        $_grid->display();

        ?>

        <h3><?php _e("User Based Archives Sitemaps", "smart-sitemapxml-tools"); ?></h3>

        <?php

        $_grid = new ssxt_grid_rules();
        $_grid->sitemap_type = 'user';
        $_grid->items = smart_sitemap_xml()->default_maps['user'];
        $_grid->priorities = smart_sitemap_xml()->default_priorities['user'];
        $_grid->prepare_items();
        $_grid->display();

        ?>

        <?php if (!empty(smart_sitemap_xml()->custom_maps)) { ?>
        <h3><?php _e("Custom Archives Sitemaps", "smart-sitemapxml-tools"); ?></h3>

        <?php

        $_grid = new ssxt_grid_rules();
        $_grid->sitemap_type = 'cstm';
        $_grid->items = smart_sitemap_xml()->custom_maps;
        $_grid->priorities = array();
        $_grid->custom_type = true;
        $_grid->prepare_items();
        $_grid->display();

        ?>
        <?php } ?>
    </div>
</form>