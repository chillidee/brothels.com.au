<?php

require_once(SSXT_PATH.'core/links.php');

?>
<form method="post" action="">
    <?php settings_fields('smart-sitemapxml-tools-links'); ?>

    <div class="sct-cleanup-left">
        <div id="scs-scroll-sidebar">
            <p>
                <?php _e("You can add extra links into sitemap. Links must be part of this website, if not, search engines will ignore them and issue warnings. URL is the only required element. Last modify date is not required.", "smart-sitemapxml-tools"); ?>
            </p>
            <input class="button-primary" type="submit" value="<?php _e("Save Links", "smart-sitemapxml-tools"); ?>" />
            <p class="sct-left-info">
                <?php _e("If available, Last Modifiy Date must be in one of the supported formats", "smart-sitemapxml-tools"); ?>: <br/>
                <strong>YYYY-MM-DD</strong> (1997-07-16) <?php _e("or", "smart-sitemapxml-tools"); ?> <br/><strong>YYYY-MM-DDThh:mm:ss.sTZD</strong> (1997-07-16T19:20:30.45+01:00).
            </p>
        </div>
    </div>
    <div class="sct-cleanup-right">
        <?php

        $items = array_merge(array(array("id" => 0, "url" => "", "lastmod" => "", "priority" => "", "frequency" => "")), smart_mapxml_core()->get('custom_links'));

        $_grid = new ssxt_grid_links();
        $_grid->sitemap_type = 'cstm';
        $_grid->items = $items;
        $_grid->prepare_items();
        $_grid->display();

        ?>

        <input style="margin-top: 15px;" id="ssxt-new-link" class="button-secondary" type="button" value="<?php _e("Add New Link", "smart-sitemapxml-tools"); ?>" />
    </div>
</form>

<script type="text/javascript">
    jQuery(document).ready(function() {
        ssxt_admin.links(<?php echo smart_mapxml_core()->get('custom_link_id'); ?>);
    });
</script>