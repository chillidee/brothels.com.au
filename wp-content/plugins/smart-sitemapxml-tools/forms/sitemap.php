<?php

$sitemap_file = ssxt_get_sitemap_index_file_name();
$sitemap_url = ssxt_get_sitemap_index_url();
$sitemap_enabled = false;

?>
<div class="sct-cleanup-left">
    <div id="scs-scroll-sidebar">
        <p>
            <?php _e("Here you can see status of the sitemaps, most potential problems and solutions, link to the sitemap index. Other tabs contain options to setup the plugin.", "smart-sitemapxml-tools"); ?>
        </p>
        <p class="sct-left-promo">
            <?php _e("If you need more SEO related tools, check out this plugin", "smart-sitemapxml-tools"); ?>:
        </p>
        <div class="sct-side-banner">
            <a target="_blank" href="http://d4p.me/ccsrtt" title="Smart Robots.txt Tools"><img src="https://s3.amazonaws.com/smartplugins/banners/250x125/smart-robotstxt.tools.png" alt="Smart Robots.txt Tools" /></a>
        </div>
    </div>
</div>
<div class="sct-cleanup-right">
    <h3 style="margin-top: 0;"><?php _e("Basic Information", "smart-sitemapxml-tools"); ?></h3>
    <table class="form-table">
        <tbody>
            <tr valign="top">
                <th scope="row"><?php _e("Important", "smart-sitemapxml-tools"); ?></th>
                <td>
                    <?php

                    _e("This plugin doesn't create sitemap files, it generates virtual sitemaps files. Plugin will register new rewrite rules for sitemap files and serve sitemaps content that will be generated when requested.", "smart-sitemapxml-tools");
                    echo ' ';
                    _e("This will allow for more flexibility, easier changes, better sitemap splitting and conflicts.", "smart-sitemapxml-tools");

                    ?>
                </td>
            </tr>
        </tbody>
    </table>

    <h3><?php _e("Sitemap.xml File Status", "smart-sitemapxml-tools"); ?></h3>
    <table class="form-table">
        <tbody>
            <tr valign="top">
                <th scope="row"><?php _e("Real File", "smart-sitemapxml-tools"); ?></th>
                <td>
                    <?php

                    if (ssxt_has_real_sitemap_xml()) {
                        echo sprintf(__("You currently have a real '%s' file in your website root directory.", "smart-sitemapxml-tools"), $sitemap_file);
                        echo '<br/><strong>'.__("This plugin can't be used as long as the real file is present. If you want to keep the real file, rename the sitemap base file this plugin uses through plugin Settings panel.", "smart-sitemapxml-tools").'</strong>';
                    } else {
                        echo sprintf(__("You currently don't have '%s' file in your website root directory.", "smart-sitemapxml-tools"), $sitemap_file);
                        echo '<br/><strong>'.__("This plugin can be used.", "smart-sitemapxml-tools").'</strong>';
                    }

                    ?>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><?php _e("Virtual File", "smart-sitemapxml-tools"); ?></th>
                <td>
                    <?php

                    if (ssxt_permalinks_enabled()) {
                        if (ssxt_has_real_sitemap_xml()) {
                            echo '<strong>'.sprintf(__("Virtual '%s' file can't be used.", "smart-sitemapxml-tools"), $sitemap_file).'</strong>';
                        } else {
                            echo sprintf(__("Virtual '%s' file can be used.", "smart-sitemapxml-tools"), $sitemap_file);

                            $sitemap_enabled = true;
                        }
                    } else {
                        echo '<strong>'.sprintf(__("Virtual '%s' file can't be used because permalinks in WordPress are not enabled", "smart-sitemapxml-tools"), $sitemap_file).'</strong>';
                    }

                    ?>
                </td>
            </tr>
        </tbody>
    </table>

    <?php if ($sitemap_enabled) { ?>
    <h3><?php _e("Sitemap.xml", "smart-sitemapxml-tools"); ?></h3>
    <table class="form-table">
        <tbody>
            <tr valign="top">
                <th scope="row"><?php _e("Link to Sitemap", "smart-sitemapxml-tools"); ?></th>
                <td>
                    <?php

                    _e("Sitemap file is an index file linking to other sitemaps. You only need link to the index file to submit to the search engines.", "smart-sitemapxml-tools");

                    echo '<br/><br/><a target="_blank" href="'.$sitemap_url.'"><strong>'.$sitemap_url.'</strong></a>';
                    
                    ?>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="form-table">
        <tbody>
            <tr valign="top">
                <th scope="row"><?php _e("Sitemap Status", "smart-sitemapxml-tools"); ?></th>
                <td>
                    <?php

                    echo __("Last map index built date", "smart-sitemapxml-tools").': <strong>';
                        $dt = smart_mapxml_core()->get('last_sitemap_build');
                        if ($dt == 0) { _e("Never", "smart-sitemapxml-tools"); } else { echo date('r', $dt); }
                    echo '</strong><br/>';
                    echo __("Last Google notification date", "smart-sitemapxml-tools").': <strong>';
                        $dt = smart_mapxml_core()->get('last_notify_google');
                        if ($dt == 0) { _e("Never", "smart-sitemapxml-tools"); } else { echo date('r', $dt); }
                    echo '</strong><br/>';
                    echo __("Last Bing notification date", "smart-sitemapxml-tools").': <strong>';
                        $dt = smart_mapxml_core()->get('last_notify_bing');
                        if ($dt == 0) { _e("Never", "smart-sitemapxml-tools"); } else { echo date('r', $dt); }
                    echo '</strong>';
                    
                    ?>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="form-table">
        <tbody>
            <tr valign="top">
                <th scope="row"><?php _e("Force Map Rebuild", "smart-sitemapxml-tools"); ?></th>
                <td>
                    <?php

                    _e("Sitemap files will be automatically rebuilt every time you publish new post (page or custom post type post). In the same time, search engines will be notified about the new maps (if you enabled this in the plugin settings). But, if you want, you can force map rebuild and search engines notifications from here, by pressing the button bellow.", "smart-sitemapxml-tools");

                    echo '<br/><br/>';

                    if (wp_next_scheduled('ssxt_rebuild_sitemap') === false) {
                        echo '<a class="button-primary" href="'.$this->admin_page_url.'?page=smart-sitemapxml-tools&tab=sitemap&forced='.wp_create_nonce('smart-sitemapxml-tools').'"><strong>'.__("Force Sitemap Rebuild", "smart-sitemapxml-tools").'</strong></a>';
                    } else {
                        _e("Map rebuild is already scheduled.", "smart-sitemapxml-tools");
                    }

                    ?>
                </td>
            </tr>
        </tbody>
    </table>
    <?php } else { ?>
    <h3><?php _e("Problems with Sitemap", "smart-sitemapxml-tools"); ?></h3>
    <table class="form-table">
        <tbody>
            <tr valign="top">
                <th scope="row"><?php _e("Plugin", "smart-sitemapxml-tools"); ?></th>
                <td>
                    <?php

                    _e("You need to fix listed problems before this plugin can be used.", "smart-sitemapxml-tools");

                    ?>
                </td>
            </tr>
        </tbody>
    </table>
    <?php } ?>
</div>