<?php

$current = isset($_GET['tab']) ? $_GET['tab'] : 'sitemap';

$tabs = array(
    'sitemap' => array(__("Sitemap.xml", "smart-sitemapxml-tools"), '__inner__'),
    'settings' => array(__("Settings", "smart-sitemapxml-tools"), '__inner__'),
    'content' => array(__("Content", "smart-sitemapxml-tools"), '__inner__'),
    'links' => array(__("Extra Links", "smart-sitemapxml-tools"), '__inner__'),
    'impexp' => array(__("Export / Import", "smart-sitemapxml-tools"), '__inner__'),
    'reset' => array(__("Reset", "smart-sitemapxml-tools"), '__inner__'),
    'about' => array(__("About", "smart-sitemapxml-tools"), '__inner__')
);

?>

<div class="wrap sct-wordpress-<?php echo SSXT_WP_VERSION; ?>">
    <h2><?php _e("Smart Sitemap.xml Tools", "smart-sitemapxml-tools"); ?>
        <span class="sct-version"> | <?php echo smart_mapxml_core()->get('__version__'); ?></span></h2>
     <?php if (isset($_GET['settings-updated'])) { ?>
        <div id="message" class="updated"><p><strong><?php _e("Settings saved.", "smart-sitemapxml-tools"); ?></strong></p></div>
    <?php } if (isset($_GET['settings-reset'])) { ?>
        <div id="message" class="error"><p><strong><?php _e("All plugin settings reseted.", "smart-sitemapxml-tools"); ?></strong></p></div>
    <?php } if (isset($_GET['import-failed'])) { ?>
        <div id="message" class="error"><p><strong><?php _e("File import failed.", "smart-sitemapxml-tools"); ?></strong></p></div>
    <?php } if (isset($_GET['import-nothing'])) { ?>
        <div id="message" class="error"><p><strong><?php _e("Nothing imported.", "smart-sitemapxml-tools"); ?></strong></p></div>
    <?php } ?>
    <div id="icon-themes" class="icon32"><br></div>
    <h2 class="nav-tab-wrapper">
    <?php

    foreach ($tabs as $tab => $data) {
        $class = $tab == $current ? ' nav-tab-active' : '';
        echo '<a class="nav-tab'.$class.'" href="'.$this->admin_page_url.'?page=smart-sitemapxml-tools&tab='.$tab.'">'.$data[0].'</a>';
    }

    ?>
    </h2>
    <div id="ddw-panel" class="ddw-panel-<?php echo $current; ?>">
        <?php

        $location = $tabs[$current][1];

        if ($location == '__inner__') {
            include(SSXT_PATH.'forms/'.$current.'.php');
        } else {
            include($location);
        }

        ?>
    </div>  
</div>