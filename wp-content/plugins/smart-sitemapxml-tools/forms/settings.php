<form method="post" action="">
    <?php settings_fields('smart-sitemapxml-tools-settings'); ?>

    <div class="sct-cleanup-left">
        <div id="scs-scroll-sidebar">
            <p>
                <?php _e("This panel contains general plugin settings. Modifying any of the plugin settings will cause sitemap cache to be removed and rebuilt.", "smart-sitemapxml-tools"); ?>
            </p>
            <input class="button-primary" type="submit" value="<?php _e("Save Settings", "smart-sitemapxml-tools"); ?>" />
            <p class="sct-left-info">
                <?php _e("Make sure you read the documentation to better understand how some of the settings will impact the sitemap processing.", "smart-sitemapxml-tools"); ?>
            </p>
        </div>
    </div>
    <div class="sct-cleanup-right">
        <h3 style="margin-top: 0;"><?php _e("Sitemap", "smart-sitemapxml-tools"); ?></h3>

        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row"><?php _e("Limit URL's in file", "smart-sitemapxml-tools"); ?></th>
                    <td>
                        <input id="ssxt_links_limit" type="text" class="widefat sct-input-numeric" name="ssxt[links_limit]" value="<?php echo $settings['links_limit']; ?>" style="width: 100px;" />

                        <em>
                            <?php _e("This option will limit number of URL's in single sitemap file. Setting this value too high will have negative impact to the sitemap building performance. Maximum value allowed is 50000. But, value that is recommended should be under 10000. Depending on this value, plugin will split links into smaller sitemaps.", "smart-sitemapxml-tools"); ?> 
                        </em>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e("Base File Name", "smart-sitemapxml-tools"); ?></th>
                    <td>
                        <input id="ssxt_sitemap_base" type="text" class="widefat sct-input-alpha" name="ssxt[sitemap_base]" value="<?php echo $settings['sitemap_base']; ?>" style="width: 200px;" />

                        <em>
                            <?php _e("Only lowecase letters are allowed. It is not recommended that you change this once you start using the plugin, each time you make the change here, old sitemap URL's will no longer work, and you will need to resubmit maps to search engines. Decide on the sitemap name, and don't change it!", "smart-sitemapxml-tools"); ?> 
                        </em>
                    </td>
                </tr>
            </tbody>
        </table>

        <h3><?php _e("Search Engines Notifications", "smart-sitemapxml-tools"); ?></h3>

        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row"><?php _e("Google", "smart-sitemapxml-tools"); ?></th>
                    <td>
                        <label for="ssxt_notify_google">
                            <input<?php echo $settings['notify_google'] ? ' checked="checked"' : ''; ?> type="checkbox" value="1" name="ssxt[notify_google]" id="ssxt_notify_google" />
                            <?php _e("Enabled", "smart-sitemapxml-tools"); ?></label>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e("Bing", "smart-sitemapxml-tools"); ?></th>
                    <td>
                        <label for="ssxt_notify_bing">
                            <input<?php echo $settings['notify_bing'] ? ' checked="checked"' : ''; ?> type="checkbox" value="1" name="ssxt[notify_bing]" id="ssxt_notify_bing" />
                            <?php _e("Enabled", "smart-sitemapxml-tools"); ?></label>
                    </td>
                </tr>
            </tbody>
        </table>

        <h3><?php _e("Including Images", "smart-sitemapxml-tools"); ?></h3>

        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row"><?php _e("Images in Sitemap", "smart-sitemapxml-tools"); ?></th>
                    <td>
                        <label for="ssxt_include_images">
                            <input<?php echo $settings['include_images'] ? ' checked="checked"' : ''; ?> type="checkbox" value="1" name="ssxt[include_images]" id="ssxt_include_images" />
                            <?php _e("Attached images for posts", "smart-sitemapxml-tools"); ?></label>

                        <em>
                            <?php _e("If active, plugin will take images attached to posts and add them into post types sitemaps.", "smart-sitemapxml-tools"); ?> 
                        </em>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e("Limit Images", "smart-sitemapxml-tools"); ?></th>
                    <td>
                        <input id="ssxt_images_limit" type="text" class="widefat sct-input-numeric" name="ssxt[images_limit]" value="<?php echo $settings['images_limit']; ?>" style="width: 100px;" />

                        <em>
                            <?php _e("You can limit how many images for each post will be added into the sitemap.", "smart-sitemapxml-tools"); ?> 
                        </em>
                    </td>
                </tr>
            </tbody>
        </table>

        <h3><?php _e("Miscellaneous", "smart-sitemapxml-tools"); ?></h3>

        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row"><?php _e("Robots.txt", "smart-sitemapxml-tools"); ?></th>
                    <td>
                        <label for="ssxt_robots_txt">
                            <input<?php echo $settings['robots_txt'] ? ' checked="checked"' : ''; ?> type="checkbox" value="1" name="ssxt[robots_txt]" id="ssxt_robots_txt" />
                            <?php _e("Add entry for sitemap", "smart-sitemapxml-tools"); ?></label>

                        <em>
                            <?php _e("If you are adding sitemap URL into robots.txt in some other way, disable this option. This will work only if you are using WordPress controlled virtual robots.txt file. If you have real robots file in your website directory, you need to add sitemap entry manually.", "smart-sitemapxml-tools"); ?> 
                        </em>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php _e("XSLT Transformation", "smart-sitemapxml-tools"); ?></th>
                    <td>
                        <label for="ssxt_xslt_file">
                            <input<?php echo $settings['xslt_file'] ? ' checked="checked"' : ''; ?> type="checkbox" value="1" name="ssxt[xslt_file]" id="ssxt_xslt_file" />
                            <?php _e("Link to transformation file", "smart-sitemapxml-tools"); ?></label>

                        <em>
                            <?php _e("This file will allow you to see formatted file when viewing sitemap file in browser.", "smart-sitemapxml-tools"); ?> 
                        </em>
                    </td>
                </tr>
            </tbody>
        </table>

        <h3><?php _e("Advanced Options", "smart-sitemapxml-tools"); ?></h3>

        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row"><?php _e("Cache", "smart-sitemapxml-tools"); ?></th>
                    <td>
                        <label for="ssxt_cache_maps">
                            <input<?php echo $settings['cache_maps'] ? ' checked="checked"' : ''; ?> type="checkbox" value="1" name="ssxt[cache_maps]" id="ssxt_cache_maps" />
                            <?php _e("Enabled", "smart-sitemapxml-tools"); ?></label>

                            <em>
                                <?php _e("It is highly recommended that Cache remains enabled. Without cache, depending on number of requests, it can cause performance issues when downloading sitemaps with large number of links.", "smart-sitemapxml-tools"); ?> 
                            </em>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</form>