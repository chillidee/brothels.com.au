<form method="post" action="">
    <?php settings_fields('smart-sitemapxml-tools-reset'); ?>

    <div class="sct-cleanup-left">
        <div id="scs-scroll-sidebar">
            <p>
                <?php _e("Here you can reset all plugin settings to defaults. Also, you can remove custom rules for posts, users and terms.", "smart-sitemapxml-tools"); ?> 
                <strong>This operation is not reversible!</strong>
            </p>
            <input class="button-primary" type="submit" value="<?php _e("Save Settings", "smart-sitemapxml-tools"); ?>" />
        </div>
    </div>
    <div class="sct-cleanup-right">
        <h3><?php _e("Confirm Settings Removal", "smart-sitemapxml-tools"); ?></h3>
        <table class="form-table" style="width: 640px">
            <tbody>
                <tr valign="top">
                    <th scope="row"><?php _e("Confirmation", "smart-sitemapxml-tools"); ?></th>
                    <td>
                        <label for="ssxt_reset_confirm_settings">
                            <input type="checkbox" value="settings" name="ssxt_reset_confirm[]" id="ssxt_reset_confirm_settings" />
                            <?php _e("Reset all plugin settings", "smart-sitemapxml-tools"); ?></label><br/>
                        <label for="ssxt_reset_confirm_content">
                            <input type="checkbox" value="content" name="ssxt_reset_confirm[]" id="ssxt_reset_confirm_content" />
                            <?php _e("Reset all Content rules", "smart-sitemapxml-tools"); ?></label><br/>
                        <label for="ssxt_reset_confirm_exclude">
                            <input type="checkbox" value="custom" name="ssxt_reset_confirm[]" id="ssxt_reset_confirm_custom_links" />
                            <?php _e("Remove all Custom links", "smart-sitemapxml-tools"); ?></label>
                    </td>
                </tr>
            </tbody>
        </table>

        <h3><?php _e("Confirm Individual Rules Removal", "smart-sitemapxml-tools"); ?></h3>
        <table class="form-table" style="width: 640px">
            <tbody>
                <tr valign="top">
                    <th scope="row"><?php _e("Confirmation", "smart-sitemapxml-tools"); ?></th>
                    <td>
                        <label for="ssxt_rules_confirm_posts">
                            <input type="checkbox" value="posts" name="ssxt_rules_confirm[]" id="ssxt_rules_confirm_posts" />
                            <?php _e("Remove rules for individual posts", "smart-sitemapxml-tools"); ?></label><br/>
                        <label for="ssxt_rules_confirm_terms">
                            <input type="checkbox" value="terms" name="ssxt_rules_confirm[]" id="ssxt_rules_confirm_terms" />
                            <?php _e("Remove rules for individual terms", "smart-sitemapxml-tools"); ?></label><br/>
                        <label for="ssxt_rules_confirm_users">
                            <input type="checkbox" value="users" name="ssxt_rules_confirm[]" id="ssxt_rules_confirm_users" />
                            <?php _e("Remove rules for individual users", "smart-sitemapxml-tools"); ?></label>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</form>