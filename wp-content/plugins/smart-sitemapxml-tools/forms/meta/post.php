<?php

$frequencies = array(
    '' => __("Default", "smart-sitemapxml-tools"),
    'always' => __("Always", "smart-sitemapxml-tools"),
    'hourly' => __("Hourly", "smart-sitemapxml-tools"),
    'daily' => __("Daily", "smart-sitemapxml-tools"),
    'weekly' => __("Weekly", "smart-sitemapxml-tools"),
    'monthly' => __("Monthly", "smart-sitemapxml-tools"),
    'yearly' => __("Yearly", "smart-sitemapxml-tools"),
    'never' => __("Never", "smart-sitemapxml-tools")
);

$priorities = array(
    '' => __("Default", "smart-sitemapxml-tools"),
    '1.0' => '1.0',
    '0.9' => '0.9',
    '0.8' => '0.8',
    '0.7' => '0.7',
    '0.6' => '0.6',
    '0.5' => '0.5',
    '0.4' => '0.4',
    '0.3' => '0.3',
    '0.2' => '0.2',
    '0.1' => '0.1'
);

?>
<input type="hidden" name="gdsxt[metabox]" value="edit" />
<p>
    <label for="gdsxt_excluded">
        <input class="" type="checkbox" <?php if ($excluded == 1) { echo ' checked="checked"'; } ?> name="gdsxt[excluded]" id="gdsxt_excluded" />
        <?php _e("Exclude from Sitemap", "smart-sitemapxml-tools"); ?>
    </label>
</p>
<hr/>
<p>
    <label for="gdsxt_excluded"><?php _e("Priority", "smart-sitemapxml-tools"); ?>:</label>
    <?php echo ssxt_render_select($priorities, array('selected' => $priority, 'name' => 'gdsxt[priority]')); ?>
</p>
<p>
    <label for="gdsxt_excluded"><?php _e("Update Frequency", "smart-sitemapxml-tools"); ?>:</label>
    <?php echo ssxt_render_select($frequencies, array('selected' => $frequency, 'name' => 'gdsxt[frequency]')); ?>
</p>