<?php

$frequencies = array(
    '' => __("Default", "smart-sitemapxml-tools"),
    'always' => __("Always", "smart-sitemapxml-tools"),
    'hourly' => __("Hourly", "smart-sitemapxml-tools"),
    'daily' => __("Daily", "smart-sitemapxml-tools"),
    'weekly' => __("Weekly", "smart-sitemapxml-tools"),
    'monthly' => __("Monthly", "smart-sitemapxml-tools"),
    'yearly' => __("Yearly", "smart-sitemapxml-tools"),
    'never' => __("Never", "smart-sitemapxml-tools")
);

$priorities = array(
    '' => __("Default", "smart-sitemapxml-tools"),
    '1.0' => '1.0',
    '0.9' => '0.9',
    '0.8' => '0.8',
    '0.7' => '0.7',
    '0.6' => '0.6',
    '0.5' => '0.5',
    '0.4' => '0.4',
    '0.3' => '0.3',
    '0.2' => '0.2',
    '0.1' => '0.1'
);

?>
</table>
<h2><?php _e("Sitemap Settings", "smart-sitemapxml-tools"); ?></h2>
<table class="form-table">
<tr class="form-field">
    <th scope="row" valign="top"><?php _e("Exclusion", "smart-sitemapxml-tools"); ?></th>
    <td>
        <input type="hidden" name="gdsxt[metabox]" value="edit" />
        <label for="gdsxt_excluded">
            <input class="" type="checkbox" <?php if ($settings['excluded'] == 1) { echo ' checked="checked"'; } ?> name="gdsxt[excluded]" id="gdsxt_excluded" />
            <?php _e("Exclude this term from Sitemap", "smart-sitemapxml-tools"); ?>
        </label><br/>

        <label for="gdsxt_exclude_posts">
            <input class="" type="checkbox" <?php if ($settings['exclude_posts'] == 1) { echo ' checked="checked"'; } ?> name="gdsxt[exclude_posts]" id="gdsxt_exclude_posts" />
            <?php _e("Exclude posts with this term from Sitemap", "smart-sitemapxml-tools"); ?>
        </label>
    </td>
</tr>
<tr class="form-field">
    <th scope="row" valign="top"><?php _e("Priority", "smart-sitemapxml-tools"); ?></th>
    <td>
        <?php echo ssxt_render_select($priorities, array('selected' => $settings['priority'], 'name' => 'gdsxt[priority]', 'style' => 'width: 200px;')); ?><br/>
    </td>
</tr>
<tr class="form-field">
    <th scope="row" valign="top"><?php _e("Update Frequency", "smart-sitemapxml-tools"); ?></th>
    <td>
        <?php echo ssxt_render_select($frequencies, array('selected' => $settings['frequency'], 'name' => 'gdsxt[frequency]', 'style' => 'width: 200px;')); ?>
    </td>
</tr>
