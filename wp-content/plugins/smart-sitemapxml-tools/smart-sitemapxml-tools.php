<?php

/*
Plugin Name: Smart Sitemap.xml Tools
Plugin URI: http://www.smartplugins.info/plugin/wordpress/smart-sitemapxml-tools/
Description: Generate sitemap.xml index and sitemap files used by search engines (Google, Bing...) to help with indexing process.
Version: 1.2
Author: Milan Petrovic
Author URI: http://www.dev4press.com/

== Copyright ==
Copyright 2008 - 2015 Milan Petrovic (email: milan@gdragon.info)
*/

class ssxt_loader {
    public $settings = array();
    public $generator = null;

    function __construct() {
        global $wp_version;

        define('SSXT_DEBUG', defined('WP_DEBUG') && WP_DEBUG);

        define('SSXT_WP_VERSION', intval(substr(str_replace('.', '', $wp_version), 0, 2)));
        define('SSXT_WP_VERSION_MAJOR', substr($wp_version, 0, 3));

        if (SSXT_WP_VERSION < 35) exit;

        $_dirname = trailingslashit(dirname(__FILE__));
        $_urlname = plugin_dir_url(__FILE__);

        define('SSXT_PATH', $_dirname);
        define('SSXT_URL', $_urlname);

        if (!defined('SSXT_EOL')) {
            define('SSXT_EOL', "\r\n");
        }

        if (!defined('SSXT_TAB')) {
            define('SSXT_TAB', "\t");
        }

        require_once(SSXT_PATH.'core/defaults.php');
        require_once(SSXT_PATH.'core/public.php');

        if (is_admin()) {
            require_once(SSXT_PATH.'core/functions.php');
            require_once(SSXT_PATH.'core/admin.php');
        }

        require_once(SSXT_PATH.'sitemap/data.php');
        require_once(SSXT_PATH.'sitemap/loader.php');

        add_action('plugins_loaded', array(&$this, 'plugins_loaded'));
    }

    public function plugins_loaded() {
        $this->init_plugin_settings();
        $this->init_plugin_translation();
    }

    public function init_plugin_translation() {
        $this->l = get_locale();

        if(!empty($this->l)) {
            load_plugin_textdomain('smart-sitemapxml-tools', false, 'smart-sitemapxml-tools/languages');
        }
    }

    public function init_plugin_settings() {
        $_d = new ssxt_defaults();

        $this->settings = get_option('smart-sitemapxml-tools');

        if (!is_array($this->settings)) {
            $this->settings = $_d->settings;

            update_option('smart-sitemapxml-tools', $this->settings);

            add_action('init', array(&$this, 'flush_rewrite_rules'));
        } else if ($this->settings['__build__'] != $_d->settings['__build__']) {
            $this->settings = $_d->upgrade($this->settings);

            update_option('smart-sitemapxml-tools', $this->settings);

            add_action('init', array(&$this, 'flush_rewrite_rules'));
        }

        define('SMART_SITEMAPXML_TOOLS', $this->settings['__version__']);

        do_action('ssxt_init_plugin_settings');
    }

    public function flush_rewrite_rules() {
        ssxt_flush_rewrite_rules();

        smart_sitemap_xml()->invalidate_cache();
    }

    public function active_post_types() {
        $post_types = array();

        foreach ($this->settings['content_rules']['cpts'] as $cpt => $obj) {
            if ($obj['active']) {
                $post_types[] = $cpt;
            }
        }

        return $post_types;
    }

    public function active_taxonomies() {
        $taxonomies = array();

        foreach ($this->settings['content_rules']['taxs'] as $tax => $obj) {
            if ($obj['active']) {
                $taxonomies[] = $tax;
            }
        }

        return $taxonomies;
    }

    public function get($name) {
        return $this->settings[$name];
    }

    public function get_maps($type) {
        return isset($this->settings['content_rules'][$type]) ? $this->settings['content_rules'][$type] : array();
    }

    public function get_map($type, $code) {
        if (isset($this->settings['content_rules'][$type][$code])) {
            return $this->settings['content_rules'][$type][$code];
        } else {
            return null;
        }
    }

    public function set($name, $value) {
        $this->settings[$name] = $value;
    }

    public function save() {
        update_option('smart-sitemapxml-tools', $this->settings);
    }
}

global $ssxt_core_loader, $ssxt_sitemap;
$ssxt_core_loader = new ssxt_loader();
$ssxt_sitemap = new ssxt_sitemap();

function smart_mapxml_core() {
    global $ssxt_core_loader;
    return $ssxt_core_loader;
}

function smart_sitemap_xml() {
    global $ssxt_sitemap;
    return $ssxt_sitemap;
}

?>