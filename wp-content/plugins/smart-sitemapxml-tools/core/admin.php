<?php

if (!defined('ABSPATH')) exit;

class ssxt_admin {
    public $page_ids = array();
    public $admin_page_url = 'options-general.php';

    function __construct() {
        add_action('admin_init', array(&$this, 'save_settings'));
        add_action('admin_init', array(&$this, 'admin_meta'));
        add_action('admin_menu', array(&$this, 'admin_menu'));
        add_action('save_post', array(&$this, 'save_post'));

        add_action('show_user_profile', array(&$this, 'user_edit_form_fields'));
        add_action('edit_user_profile', array(&$this, 'user_edit_form_fields'));
        add_action('personal_options_update', array(&$this, 'user_edit_save_fields'));
        add_action('edit_user_profile_update', array(&$this, 'user_edit_save_fields'));

        add_filter('plugin_row_meta', array(&$this, 'plugin_row_meta'), 10, 2);
        add_filter('plugin_action_links_smart-sitemapxml-tools/smart-sitemapxml-tools.php', array(&$this, 'plugin_action_links'));

        add_action('admin_enqueue_scripts', array(&$this, 'admin_enqueue_scripts'));
    }

    public function admin_meta() {
        if (current_user_can('edit_posts')) {
            $post_types = smart_mapxml_core()->active_post_types();
            $taxonomies = smart_mapxml_core()->active_taxonomies();

            foreach ($post_types as $post_type) {
                add_meta_box('ssxt-meta-sitemap', __("Sitemap.xml", "smart-sitemapxml-tools"), array(&$this, 'metabox_sitemap'), $post_type, 'side', 'high');
            }

            foreach ($taxonomies as $taxonomy) {
                add_action($taxonomy.'_edit_form_fields', array(&$this, 'tax_edit_form_fields'), 10, 2);
                add_action('edited_'.$taxonomy, array(&$this, 'tax_edit_save_fields'), 10);
            }
        }
    }

    public function user_edit_form_fields($user) {
        if (current_user_can('edit_user', $user->ID)) {
            $excluded = get_user_meta($user->ID, '_gdsxt_excluded', true);
            $exclude_posts = get_user_meta($user->ID, '_gdsxt_exclude_posts', true);
            $priority = get_user_meta($user->ID, '_gdsxt_priority', true);
            $frequency = get_user_meta($user->ID, '_gdsxt_frequency', true);

            include(SSXT_PATH.'forms/meta/user.php');
        }
    }

    public function user_edit_save_fields($user_id) {
        if (isset($_POST['gdsxt']) && current_user_can('edit_user', $user_id)) {
            $data = $_POST['gdsxt'];

            if (isset($data['excluded'])) {
                update_user_meta($user_id, '_gdsxt_excluded', 1);
            } else {
                delete_user_meta($user_id, '_gdsxt_excluded');
            }

            if (isset($data['exclude_posts'])) {
                update_user_meta($user_id, '_gdsxt_exclude_posts', 1);
            } else {
                delete_user_meta($user_id, '_gdsxt_exclude_posts');
            }

            if (isset($data['priority']) && $data['priority'] != '') {
                update_user_meta($user_id, '_gdsxt_priority', $data['priority']);
            } else {
                delete_user_meta($user_id, '_gdsxt_priority');
            }

            if (isset($data['frequency']) && $data['frequency'] != '') {
                update_user_meta($user_id, '_gdsxt_frequency', $data['frequency']);
            } else {
                delete_user_meta($user_id, '_gdsxt_frequency');
            }

            smart_sitemap_xml()->invalidate_cache();
        }
    }

    public function tax_edit_save_fields($term_id) {
        if (isset($_POST['gdsxt'])) {
            $data = $_POST['gdsxt'];

            $defaults = array('excluded' => 0, 'exclude_posts' => 0, 'priority' => '', 'frequency' => '');
            $settings = array(
                'excluded' => isset($data['excluded']) ? 1 : 0,
                'exclude_posts' => isset($data['exclude_posts']) ? 1 : 0,
                'priority' => $data['priority'],
                'frequency' => $data['frequency']
            );

            delete_option('gdssxt_term_meta_'.$term_id);
            if (serialize($defaults) != serialize($settings)) {
                add_option('gdssxt_term_meta_'.$term_id, $settings, '', 'no');
            }

            smart_sitemap_xml()->invalidate_cache();
        }
    }

    public function tax_edit_form_fields($term, $taxonomy) {
        $settings = get_option('gdssxt_term_meta_'.$term->term_id);

        if (!is_array($settings)) {
            $settings = array('excluded' => 0, 'exclude_posts' => 0, 'priority' => '', 'frequency' => '');
        }

        include(SSXT_PATH.'forms/meta/term.php');
    }

    public function save_post($post_id) {
        if (isset($_POST['gdsxt']) && current_user_can('edit_post', $post_id)) {
            $data = $_POST['gdsxt'];

            if (isset($data['excluded'])) {
                update_post_meta($post_id, '_gdsxt_excluded', 1);
            } else {
                delete_post_meta($post_id, '_gdsxt_excluded');
            }

            if (isset($data['priority']) && $data['priority'] != '') {
                update_post_meta($post_id, '_gdsxt_priority', $data['priority']);
            } else {
                delete_post_meta($post_id, '_gdsxt_priority');
            }

            if (isset($data['frequency']) && $data['frequency'] != '') {
                update_post_meta($post_id, '_gdsxt_frequency', $data['frequency']);
            } else {
                delete_post_meta($post_id, '_gdsxt_frequency');
            }

            smart_sitemap_xml()->invalidate_cache();
        }
    }

    public function metabox_sitemap() {
        global $post_ID;

        if (current_user_can('edit_post', $post_ID)) {
            $excluded = get_post_meta($post_ID, '_gdsxt_excluded', true);
            $priority = get_post_meta($post_ID, '_gdsxt_priority', true);
            $frequency = get_post_meta($post_ID, '_gdsxt_frequency', true);

            include(SSXT_PATH.'forms/meta/post.php');
        }
    }

    public function plugin_action_links($links) {
        $links[] = '<a href="'.$this->admin_page_url.'?page=smart-sitemapxml-tools">'.__("Settings", "smart-sitemapxml-tools").'</a>';

	return $links;
    }

    public function plugin_row_meta($links, $plugin_file) {
        if ($plugin_file == 'smart-sitemapxml-tools/smart-sitemapxml-tools.php') {
            $links[] = 'SMART Plugins: <a href="http://www.smartplugins.info/" target="_blank">Website</a>';
            $links[] = '<a href="http://codecanyon.net/user/GDragoN/portfolio?ref=GDragoN" target="_blank">On CodeCanyon</a>';
        }

        return $links;
    }

    public function admin_enqueue_scripts($hook) {
        if ($hook == 'settings_page_smart-sitemapxml-tools' || $hook == 'smart-plugins_page_smart-sitemapxml-tools') {
            wp_enqueue_script('jquery');

            $depend = array('jquery');

            wp_enqueue_script('ssxt-admin', (defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ? SSXT_URL.'js/admin.js' : SSXT_URL.'js/admin.min.js'), $depend, smart_mapxml_core()->get('__version__'), true);
            wp_enqueue_style('ssxt-admin', SSXT_URL.'css/admin.css', array(), smart_mapxml_core()->get('__version__'));

            do_action('ssxt_admin_enqueue_scripts');
        }
    }

    public function save_settings() {
        if (defined('SMART_PLUGINS_CENTRAL')) {
            $this->admin_page_url = 'admin.php';
        }

        do_action('ssxt_admin_save_settings');

        if (isset($_POST['option_page']) && $_POST['option_page'] == 'smart-sitemapxml-tools-import') {
            check_admin_referer('smart-sitemapxml-tools-import-options');

            if (is_uploaded_file($_FILES['import_file']['tmp_name'])) {
                $data = file_get_contents($_FILES['import_file']['tmp_name']);
                $data = maybe_unserialize($data);

                if (is_object($data)) {
                    $import_done = false;

                    $settings = isset($_POST['import_settings']) && isset($data->{'smart-sitemapxml-tools'});

                    if ($settings) {
                        $import_done = true;

                        smart_mapxml_core()->settings = $data->{'smart-sitemapxml-tools'};
                        smart_mapxml_core()->save();
                        smart_sitemap_xml()->invalidate_cache();

                        ssxt_flush_rewrite_rules();
                    }

                    if ($import_done) {
                        wp_redirect($this->admin_page_url.'?page=smart-sitemapxml-tools&tab=impexp&settings-updated=true');
                    } else {
                        wp_redirect($this->admin_page_url.'?page=smart-sitemapxml-tools&tab=impexp&import-nothing=true');
                    }
                    exit;
                }
            }

            wp_redirect($this->admin_page_url.'?page=smart-sitemapxml-tools&tab=impexp&import-failed=true');
            exit;
        }

        if (isset($_POST['option_page']) && $_POST['option_page'] == 'smart-sitemapxml-tools-reset') {
            check_admin_referer('smart-sitemapxml-tools-reset-options');

            $done = false;
            
            if (isset($_POST['ssxt_reset_confirm']) && !empty($_POST['ssxt_reset_confirm'])) {
                $reset = (array)$_POST['ssxt_reset_confirm'];

                $_d = new ssxt_defaults();

                foreach ($reset as $r) {
                    switch ($r) {
                        case 'settings':
                            $remove = array('sitemap_base', 'notify_google', 'notify_bing', 'robots_txt', 'xslt_file', 'cache_maps', 'include_images', 'images_limit', 'links_limit');
                            break;
                        case 'content':
                            $remove = array('content_rules');
                            break;
                        case 'custom':
                            $remove = array('custom_links', 'custom_link_id', 'custom_links_lastmod');
                            break;
                    }

                    foreach ($remove as $key) {
                        smart_mapxml_core()->set($key, $_d->settings[$key]);
                    }
                }

                smart_mapxml_core()->save();

                ssxt_flush_rewrite_rules();

                $done = true;
            }

            if (isset($_POST['ssxt_rules_confirm']) && !empty($_POST['ssxt_rules_confirm'])) {
                $remove = (array)$_POST['ssxt_rules_confirm'];

                foreach ($remove as $r) {
                    switch ($r) {
                        case 'posts':
                            ssxt_data::clear_posts_meta();
                            break;
                        case 'terms':
                            ssxt_data::clear_terms_meta();
                            break;
                        case 'users':
                            ssxt_data::clear_users_meta();
                            break;
                    }
                }

                $done = true;
            }

            if ($done) {
                smart_sitemap_xml()->invalidate_cache();

                wp_redirect($this->admin_page_url.'?page=smart-sitemapxml-tools&tab=reset&settings-reset=true');
            } else {
                wp_redirect($this->admin_page_url.'?page=smart-sitemapxml-tools&tab=reset');
            }

            exit;
        }

        if (isset($_POST['option_page']) && $_POST['option_page'] == 'smart-sitemapxml-tools-content') {
            check_admin_referer('smart-sitemapxml-tools-content-options');

            $ssxt = $_POST['ssxt'];
            $content = array();

            foreach ($ssxt as $type => $data) {
                foreach ($data as $name => $obj) {
                    $obj['active'] = isset($obj['active']) && $obj['active'] == 'active';

                    $content[$type][$name] = $obj;
                }
            }

            smart_mapxml_core()->set('content_rules', $content);

            smart_mapxml_core()->save();
            smart_sitemap_xml()->invalidate_cache();

            wp_redirect($this->admin_page_url.'?page=smart-sitemapxml-tools&settings-updated=true&tab=content');
            exit;
        }

        if (isset($_POST['option_page']) && $_POST['option_page'] == 'smart-sitemapxml-tools-links') {
            check_admin_referer('smart-sitemapxml-tools-links-options');

            $ssxt = $_POST['ssxt'];

            $links = array();
            $i = 1;
            foreach ($ssxt['link'] as $id => $obj) {
                if ($id > 0) {
                    $link = array_map('trim', $obj);

                    if ($link['url'] != '') {
                        $link['id'] = $i;

                        $links[] = $link;

                        $i++;
                    }
                }
            }

            smart_mapxml_core()->set('custom_links', $links);
            smart_mapxml_core()->set('custom_link_id', $i);
            smart_mapxml_core()->set('custom_links_lastmod', time());

            smart_mapxml_core()->save();
            smart_sitemap_xml()->invalidate_cache();

            wp_redirect($this->admin_page_url.'?page=smart-sitemapxml-tools&settings-updated=true&tab=links');
            exit;
        }

        if (isset($_POST['option_page']) && $_POST['option_page'] == 'smart-sitemapxml-tools-settings') {
            check_admin_referer('smart-sitemapxml-tools-settings-options');

            $ssxt = $_POST['ssxt'];

            smart_mapxml_core()->set('sitemap_base', sanitize_file_name($ssxt['sitemap_base']));
            smart_mapxml_core()->set('notify_google', isset($ssxt['notify_google']));
            smart_mapxml_core()->set('notify_bing', isset($ssxt['notify_bing']));
            smart_mapxml_core()->set('robots_txt', isset($ssxt['robots_txt']));
            smart_mapxml_core()->set('xslt_file', isset($ssxt['xslt_file']));
            smart_mapxml_core()->set('cache_maps', isset($ssxt['cache_maps']));

            $images_limit = intval($ssxt['images_limit']);
            $links_limit = intval($ssxt['links_limit']);

            if ($images_limit < 1) {
                $images_limit = 0;
            }

            if ($links_limit < 100) {
                $links_limit = 100;
            } else if ($links_limit > 50000) {
                $links_limit = 50000;
            }

            smart_mapxml_core()->set('images_limit', $images_limit);
            smart_mapxml_core()->set('links_limit', $links_limit);

            smart_mapxml_core()->save();
            smart_sitemap_xml()->invalidate_cache();

            ssxt_flush_rewrite_rules();

            wp_redirect($this->admin_page_url.'?page=smart-sitemapxml-tools&settings-updated=true');
            exit;
        }
    }

    public function admin_menu() {
        if (defined('SMART_PLUGINS_CENTRAL')) {
            $this->page_ids[] = add_submenu_page('smart-plugins-central', __("Smart Sitemap.xml Tools", "smart-sitemapxml-tools"), __("Sitemap.xml Tools", "smart-sitemapxml-tools"), 'activate_plugins', 'smart-sitemapxml-tools', array(&$this, 'tools_menu'));
        } else {
            $this->page_ids[] = add_options_page(__("Smart Sitemap.xml Tools", "smart-sitemapxml-tools"), __("Smart Sitemap.xml Tools", "smart-sitemapxml-tools"), 'activate_plugins', 'smart-sitemapxml-tools', array(&$this, 'tools_menu'));
        }

        foreach ($this->page_ids as $id) {
            add_action('load-'.$id, array(&$this, 'load_admin_page_shared'));
        }
    }

    public function load_admin_page_shared() {
        $screen = get_current_screen();

        $screen->set_help_sidebar('
            <p><strong>SMART Plugins:</strong></p>
            <p><a target="_blank" href="http://www.smartplugins.info/">'.__("Website", "smart-sitemapxml-tools").'</a></p>
            <p><a target="_blank" href="http://codecanyon.net/user/GDragoN/portfolio?ref=GDragoN">'.__("On CodeCanyon", "smart-sitemapxml-tools").'</a></p>
            <p><a target="_blank" href="http://twitter.com/millanrs">'.__("On Twitter", "smart-sitemapxml-tools").'</a></p>
            <p><a target="_blank" href="http://facebook.com/smartplugins">'.__("On Facebook", "smart-sitemapxml-tools").'</a></p>');

        $screen->add_help_tab(array(
            'id' => 'scs-screenhelp-info',
            'title' => __("Information", "smart-sitemapxml-tools"),
            'content' => '<p>'.__("Set of tools for managing robots.txt file used for instructing search engines about search rules and other SEO related stuff.", "smart-sitemapxml-tools").'</p>
                <h5>'.__("Useful Links", "smart-sitemapxml-tools").'</h5>
                <p><a target="_blank" href="http://www.smartplugins.info/plugin/wordpress/smart-sitemapxml-tools/">'.__("Plugin Homepage", "smart-sitemapxml-tools").'</a></p>
                <p><a target="_blank" href="http://d4p.me/ccssxt">'.__("Plugin On CodeCanyon", "smart-sitemapxml-tools").'</a></p>'
        ));

        $screen->add_help_tab(array(
            'id' => 'scs-screenhelp-support',
            'title' => __("Support", "smart-sitemapxml-tools"),
            'content' => '<h5>'.__("Support Reources", "smart-sitemapxml-tools").'</h5>
                <p><a target="_blank" href="http://forum.smartplugins.info/forums/forum/smart/smart-sitemapxml-tools/">'.__("Official Support Forum", "smart-sitemapxml-tools").'</a></p>'
        ));
    }

    public function tools_menu() {
        if (isset($_GET['tab']) && $_GET['tab'] == 'about') {
            $about = smart_mapxml_core()->settings;
        } else {
            $settings = smart_mapxml_core()->settings;

            if (isset($_GET['forced']) && $_GET['forced'] != '') {
                $nonce = $_GET['forced'];

                if (wp_verify_nonce($nonce, 'smart-sitemapxml-tools')) {
                    smart_sitemap_xml()->schedule_rebuild_maps();
                }
            }
        }

        include(SSXT_PATH.'forms/index.php');
    }
}

global $ssxt_core_admin;
$ssxt_core_admin = new ssxt_admin();

?>