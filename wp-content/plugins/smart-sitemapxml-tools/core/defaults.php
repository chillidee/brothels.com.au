<?php

if (!defined('ABSPATH')) exit;

final class ssxt_defaults {
    public $settings = array(
        '__version__' => '1.2',
        '__date__' => '2015.03.24.',
        '__build__' => 3656,
        '__status__' => 'stable',
        '__product_id__' => 'smart-sitemapxml-tools',
        'last_sitemap_build' => 0,
        'last_notify_google' => 0,
        'last_notify_bing' => 0,
        'sitemap_base' => 'sitemap',
        'notify_google' => true,
        'notify_bing' => true,
        'robots_txt' => true,
        'xslt_file' => true,
        'cache_maps' => true,
        'include_images' => true,
        'images_limit' => 5,
        'links_limit' => 2000,
        'custom_links' => array(),
        'custom_link_id' => 1,
        'custom_links_lastmod' => 0,
        'content_rules' => array(
            'core' => array(
                'home' => array('active' => true, 'priority' => '1.0', 'frequency' => 'daily'),
                'extra' => array('active' => false, 'priority' => '0.4', 'frequency' => 'monthly')
            ),
            'cpts' => array(
                'post' => array('active' => true, 'priority' => 'date', 'frequency' => 'monthly'),
                'page' => array('active' => true, 'priority' => '0.7', 'frequency' => 'weekly'),
                'attachment' => array('active' => false, 'priority' => '0.5', 'frequency' => 'yearly')
            ),
            'taxs' => array(
                'category' => array('active' => true, 'priority' => 'posts_total', 'frequency' => 'weekly'),
                'post_tag' => array('active' => true, 'priority' => 'posts_total', 'frequency' => 'weekly')
            ),
            'date' => array(
                'year' => array('active' => true, 'priority' => 'latest', 'frequency' => 'monthly'),
                'month' => array('active' => true, 'priority' => 'latest', 'frequency' => 'monthly'),
                'week' => array('active' => false, 'priority' => 'latest', 'frequency' => 'monthly'),
                'day' => array('active' => true, 'priority' => 'latest', 'frequency' => 'monthly')
            ),
            'user' => array(
                'author' => array('active' => true, 'priority' => 'posts_total', 'frequency' => 'weekly')
            ),
            'cstm' => array()
        )
    );

    function __construct() { }

    public function upgrade($old) {
        foreach ($this->settings as $key => $value) {
            if (!isset($old[$key])) {
                $old[$key] = $value;
            }
        }

        $unset = array();
        foreach ($old as $key => $value) {
            if (!isset($this->settings[$key])) {
                $unset[] = $key;
            }
        }

        if (!empty($unset)) {
            foreach ($unset as $key) {
                unset($old[$key]);
            }
        }

        foreach ($this->settings as $key => $value) {
            if (substr($key, 0, 2) == '__') {
                $old[$key] = $value;
            }
        }

        return $old;
    }
}

?>