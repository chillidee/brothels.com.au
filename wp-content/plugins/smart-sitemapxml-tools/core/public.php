<?php

if (!defined('ABSPATH')) exit;

function ssxt_has_real_sitemap_xml() {
    return file_exists(ABSPATH.ssxt_get_sitemap_index_file_name());
}

function ssxt_permalinks_enabled() {
    return get_option('permalink_structure');
}

function ssxt_flush_rewrite_rules() {
    global $wp_rewrite;

    $wp_rewrite->flush_rules();
}

function ssxt_get_sitemap_index_xslt_url() {
    return smart_sitemap_xml()->url_index_xslt();
}

function ssxt_get_sitemap_content_xslt_url($type, $name, $page) {
    return smart_sitemap_xml()->url_content_xslt($type, $name, $page);
}

function ssxt_get_sitemap_index_url($gzip = false) {
    return smart_sitemap_xml()->url_index($gzip);
}

function ssxt_get_sitemap_content_url($type, $name, $page, $gzip = false) {
    return smart_sitemap_xml()->url_content($type, $name, $page, $gzip);
}

function ssxt_get_sitemap_index_file_name($gzip = false) {
    return smart_sitemap_xml()->sitemap_file($gzip);
}

function ssxt_add_custom_sitemap($name, $label, $active = true, $priority = .7, $frequency = 'monthly') {
    smart_sitemap_xml()->custom_maps[$name] = array('name' => $name, 'label' => $label, 'active' => $active, 'priority' => $priority, 'frequency' => $frequency);
}

?>