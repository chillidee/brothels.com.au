<?php

if (!defined('ABSPATH')) exit;

class ssxt_grid_rules extends WP_List_Table {
    public $frequencies;
    public $priority_values;

    public $sitemap_type = '';
    public $custom_type = false;
    public $priorities = array();

    function __construct($args = array()) {
        $this->frequencies = array(
            'always' => __("Always", "smart-sitemapxml-tools"),
            'hourly' => __("Hourly", "smart-sitemapxml-tools"),
            'daily' => __("Daily", "smart-sitemapxml-tools"),
            'weekly' => __("Weekly", "smart-sitemapxml-tools"),
            'monthly' => __("Monthly", "smart-sitemapxml-tools"),
            'yearly' => __("Yearly", "smart-sitemapxml-tools"),
            'never' => __("Never", "smart-sitemapxml-tools")
        );
        $this->priority_values = array(
            '1.0' => '1.0',
            '0.9' => '0.9',
            '0.8' => '0.8',
            '0.7' => '0.7',
            '0.6' => '0.6',
            '0.5' => '0.5',
            '0.4' => '0.4',
            '0.3' => '0.3',
            '0.2' => '0.2',
            '0.1' => '0.1'
        );

        parent::__construct(array(
            'singular'=> 'rule',
            'plural' => 'rules',
            'ajax' => false
        ));
    }

    public function extra_tablenav($which) { }
    public function search_box($text, $input_id) { }
    public function get_sortable_columns() { return array(); }

    protected function bulk_actions($which = '') { }
    protected function pagination($which) { }
    protected function display_tablenav($which) { }

    public function get_columns() {
        return array(
            'active' => __("Active", "smart-sitemapxml-tools"),
            'label' => __("Label", "smart-sitemapxml-tools"),
            'name' => __("Type and Name", "smart-sitemapxml-tools"),
            'priority' => __("Priority", "smart-sitemapxml-tools"),
            'frequency' => __("Frequency", "smart-sitemapxml-tools")
	);
    }

    public function column_active($item) {
        $checked = $item['active'] ? ' checked="checked"' : '';

        return sprintf('<input type="checkbox" name="ssxt[%s][%s][active]" value="active"%s />', $this->sitemap_type, $item['name'], $checked);
    }

    public function column_name($item) {
        return $this->sitemap_type.' :: '.$item['name'];
    }

    public function column_label($item) {
        return '<strong>'.$item['label'].'</strong>';
    }

    public function column_priority($item) {
        $priorities = apply_filters('ssxt_grid_priorities', $this->priorities, $this->sitemap_type, $item['name']);

        if (empty($priorities)) {
            $priorities = $this->priority_values;
        } else {
            $priorities = array_merge($priorities, $this->priority_values);
        }

        return ssxt_render_select($priorities, array('selected' => $item['priority'], 'name' => sprintf('ssxt[%s][%s][priority]', $this->sitemap_type, $item['name'])));
    }

    public function column_frequency($item) {
        return ssxt_render_select($this->frequencies, array('selected' => $item['frequency'], 'name' => sprintf('ssxt[%s][%s][frequency]', $this->sitemap_type, $item['name'])));
    }

    function prepare_items() {
        $this->_column_headers = array($this->get_columns(), array(), $this->get_sortable_columns());
    }
}

?>