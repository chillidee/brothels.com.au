<?php

if (!defined('ABSPATH')) exit;

function ssxt_html_id_from_name($name, $id = '') {
    if ($id == '') {
        $id = str_replace(']', '', $name);
        $id = str_replace('[', '_', $id);
    } else if ($id == '_') {
        $id = '';
    }

    return $id;
}

function ssxt_render_select_grouped($values, $args = array()) {
    $defaults = array(
        'selected' => '', 'name' => '', 'id' => '', 'class' => 'widefat', 
        'style' => '', 'multi' => false, 'echo' => true);
    $args = wp_parse_args($args, $defaults);
    extract($args);

    $render = '';
    $selected = (array)$selected;
    $id = ssxt_html_id_from_name($name, $id);

    if ($class != '') {
        $class = ' class="'.$class.'"';
    }

    if ($style != '') {
        $style = ' style="'.$style.'"';
    }

    $multi = $multi ? ' multiple' : '';
    $name = $multi ? $name."[]" : $name;

    $render.= '<select name="'.$name.'" id="'.$id.'"'.$class.$style.$multi.'>';
    foreach ($values as $group) {
        $render.= '<optgroup label="'.$group['title'].'">';
        foreach ($group['values'] as $value => $display) {
            $sel = in_array($value, $selected) ? ' selected="selected"' : '';
            $render.= '<option value="'.esc_attr($value).'"'.$sel.'>'.$display.'</option>';
        }
        $render.= '</optgroup>';
    }
    $render.= '</select>';

    if ($echo) {
        echo $render;
    } else {
        return $render;
    }
}

function ssxt_render_select($values, $args = array()) {
    $defaults = array(
        'selected' => '', 'name' => '', 'id' => '', 'class' => 'widefat', 
        'style' => '', 'multi' => false, 'echo' => true);
    $args = wp_parse_args($args, $defaults);
    extract($args);

    $render = '';
    $selected = (array)$selected;
    $id = ssxt_html_id_from_name($name, $id);

    if ($class != '') {
        $class = ' class="'.$class.'"';
    }

    if ($style != '') {
        $style = ' style="'.$style.'"';
    }

    $multi = $multi ? ' multiple' : '';
    $name = $multi ? $name.'[]' : $name;

    $render.= '<select name="'.$name.'" id="'.$id.'"'.$class.$style.$multi.'>';
    foreach ($values as $value => $display) {
        $sel = in_array($value, $selected) ? ' selected="selected"' : '';
        $render.= '<option value="'.$value.'"'.$sel.'>'.$display.'</option>';
    }
    $render.= '</select>';

    if ($echo) {
        echo $render;
    } else {
        return $render;
    }
}

?>