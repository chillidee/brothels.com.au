<?php

if (!defined('ABSPATH')) exit;

class ssxt_grid_links extends WP_List_Table {
    public $frequencies;
    public $priorities;

    public $sitemap_type = '';

    function __construct($args = array()) {
        $this->frequencies = array(
            '' => __("Default", "smart-sitemapxml-tools"),
            'always' => __("Always", "smart-sitemapxml-tools"),
            'hourly' => __("Hourly", "smart-sitemapxml-tools"),
            'daily' => __("Daily", "smart-sitemapxml-tools"),
            'weekly' => __("Weekly", "smart-sitemapxml-tools"),
            'monthly' => __("Monthly", "smart-sitemapxml-tools"),
            'yearly' => __("Yearly", "smart-sitemapxml-tools"),
            'never' => __("Never", "smart-sitemapxml-tools")
        );
        $this->priorities = array(
            '' => __("Default", "smart-sitemapxml-tools"),
            '1.0' => '1.0',
            '0.9' => '0.9',
            '0.8' => '0.8',
            '0.7' => '0.7',
            '0.6' => '0.6',
            '0.5' => '0.5',
            '0.4' => '0.4',
            '0.3' => '0.3',
            '0.2' => '0.2',
            '0.1' => '0.1'
        );

        parent::__construct(array(
            'singular'=> 'link',
            'plural' => 'links',
            'ajax' => false
        ));
    }

    public function extra_tablenav($which) { }
    public function search_box($text, $input_id) { }
    public function get_sortable_columns() { return array(); }

    protected function bulk_actions($which = '') { }
    protected function pagination($which) { }
    protected function display_tablenav($which) { }

    public function get_columns() {
        return array(
            'remove' => '',
            'url' => __("Full URL", "smart-sitemapxml-tools"),
            'lastmod' => __("Last Modified Date", "smart-sitemapxml-tools"),
            'priority' => __("Priority", "smart-sitemapxml-tools"),
            'frequency' => __("Frequency", "smart-sitemapxml-tools")
	);
    }

    public function column_remove($item) {
        return '<a href="#">'.__("remove", "smart-sitemapxml-tools").'</a>';
    }

    public function column_url($item) {
        return '<input type="text" name="ssxt[link]['.$item['id'].'][url]" value="'.$item['url'].'" />';
    }

    public function column_lastmod($item) {
        return '<input type="text" name="ssxt[link]['.$item['id'].'][lastmod]" value="'.$item['lastmod'].'" />';
    }

    public function column_priority($item) {
        return ssxt_render_select($this->priorities, array('id' => '_', 'selected' => $item['priority'], 'name' => sprintf('ssxt[link][%s][priority]', $item['id'])));
    }

    public function column_frequency($item) {
        return ssxt_render_select($this->frequencies, array('id' => '_', 'selected' => $item['frequency'], 'name' => sprintf('ssxt[link][%s][frequency]', $item['id'])));
    }

    function prepare_items() {
        $this->_column_headers = array($this->get_columns(), array(), $this->get_sortable_columns());
    }
}

?>