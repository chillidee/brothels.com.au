<?php

$validxpr = array('settings');
$settings = array('smart-sitemapxml-tools');

define('SMART_PLUGINS_WPLOAD', '');

function smart_server_to_local_timestamp($server) {
    $local = $server + get_option('gmt_offset') * 3600;
    return $local;
}

function smart_export_settings($settings, $export, $scope = 'site') {
    $data = new stdClass();

    if (in_array('settings', $export)) {
        foreach ($settings as $option) {
            $raw = $scope == 'site' ? get_option($option) : get_site_option($option);

            $data->$option = $raw;
        }
    }

    return serialize($data);
}

function smart_is_current_user_role($role = 'administrator') {
    global $current_user;

    if (is_array($current_user->roles)) {
        return in_array($role, $current_user->roles);
    } else {
        return false;
    }
}

function smart_get_wpload_path() {
    if (SMART_PLUGINS_WPLOAD == '') {
        $d = 0;

        while (!file_exists(str_repeat('../', $d).'wp-load.php'))
            if (++$d > 16) exit;
        return str_repeat('../', $d).'wp-load.php';
    } else {
        return SMART_PLUGINS_WPLOAD;
    }
}

$wpload = smart_get_wpload_path();
require($wpload);

@ini_set('memory_limit', '128M');
@set_time_limit(360);

check_ajax_referer('ssxt-settings-export');

if (!smart_is_current_user_role()) {
    wp_die(__("Only administrators can use export features.", "smart-sitemapxml-tools"));
}

$export_code = 'site';
$export_date = date('Y-m-d');

$export = array_values(array_intersect($validxpr, explode(',', $_GET['export'])));

if (empty($export)) {
    wp_die(__("Nothing is selected for export.", "smart-sitemapxml-tools"));
}

$export_name = 'smart_robotstxt_tools_'.$export_code.'_'.join('-', $export).'_'.$export_date;

header('Content-type: application/force-download');
header('Content-Disposition: attachment; filename="'.$export_name.'.ssxt"');

echo smart_export_settings($settings, $export, $export_code);

?>