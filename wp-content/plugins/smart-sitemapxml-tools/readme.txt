=== Smart Sitemap.xml Tools ===
Version: 1.2
Requires at least: 3.5
Tested up to: 4.2

Generate sitemap.xml index and sitemap files used by search engines (Google, Bing...) to help with indexing process.

== Installation ==
= Requirements =
* PHP: 5.2.4 or newer
* WordPress: 3.5 or newer

= Basic Installation =
* Plugin folder in the WordPress plugins folder must be `smart-sitemapxml-tools`.
* Upload folder `smart-sitemapxml-tools` to the `/wp-content/plugins/` directory
* Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==
= Does plugin works with WordPress MultiSite installations? =
Yes. Each website in network can activate and use plugin on it's on.

= Can I translate plugin to my language? =
Yes. POT file is provided as a base for translation. Translation files should go into Languages directory.

== Changelog ==
= 1.2 / 2015.03.24 =
* Edit: Changes in the loading order for main Sitemap object
* Fix: Sitemap is not working if sitemap name is changed

= 1.1 / 2015.03.02 =
* New: Date based sitemaps calculate priority by latest date
* Edit: Date based archives now include only published posts
* Fix: Some data functions have wrong filters names
* Fix: Date based archives including invalid dates
* Fix: Date days based archives count functions missing filters

= 1.0 / 2015.02.21 =
* First release
