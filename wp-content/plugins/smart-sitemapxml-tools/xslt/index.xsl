<xsl:stylesheet version="2.0"
                xmlns:html="http://www.w3.org/TR/REC-html40"
                xmlns:sitemap="http://www.sitemaps.org/schemas/sitemap/0.9"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <title>%WEBSITE_TITLE% - XML Sitemap: %SITEMAP_TITLE%</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style type="text/css">
            body {
                font-family: "Lucida Grande", "Lucida Sans Unicode", Tahoma, Verdana;
                font-size: 13px;
            }

            a { color: black; }

            #header, #top {
                background-color: #CFEBF7;
                border-top: 2px #c00 solid;
                border-bottom: 2px #c00 solid;
                padding: 0;
                width: 100%;
                margin: 10px 0 10px 2px;
            }

            #content { width: 100%; }

            #content table { width: 100%; }

            #header p, #top p {
                line-height: 18px;
                margin: 10px 15px;
                padding: 0;
            }

            thead tr th {
                background-color: #2191C0;
                color: #fefefe;
                border-bottom: 2px solid #c00;
            }

            thead tr th.lastmod {
                width: 25%;
            }

            tfoot tr th {
                background-color: #2191C0;
                color: white;
                border-top: 2px solid #c00;
            }

            td { font-size: 11px; }

            th {
                text-align: left;
                padding-right: 30px;
                font-size: 11px;
            }

            tr.high { background-color: #fefefe; }

            #footer {
                padding: 2px;
                margin: 10px 0;
                font-size: 8pt;
                color: gray;
            }

            #footer a { color:gray; }
	</style>
      </head>

      <body>
	<h1>%WEBSITE_TITLE% - XML Sitemap: %SITEMAP_TITLE%</h1>
	<div id="header">
            <p>
                XML Sitemap is supported by search engines like <a href="http://www.google.com">Google</a> and <a href="http://www.bing.com">BING</a>.
                <br/>
                This sitemap is generated for <a href="http://wordpress.org/">WordPress</a> powered website using <a href="http://d4p.me/ccssxt" title="Smart Sitemap.xml Tools">Smart Sitemap.xml Tools</a> plugin.
            </p>
        </div>

        <div id="top">
            <p>Current Sitemap URL: %SITEMAP_URL_CURRENT%</p>
        </div>

        <div id="content">
            <table cellpadding="5">
                <thead>
                    <tr>
                        <th class="url">URL</th>
                        <th class="lastmod">Last Change</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Sitemap URL</th>
                        <th>Last Change</th>
                    </tr>
                </tfoot>
                <tbody>
                    <xsl:apply-templates select="sitemap:sitemapindex/sitemap:sitemap">
                        <xsl:sort select="sitemap:lastmod" order="descending"/>
                    </xsl:apply-templates>
                </tbody>
            </table>
        </div>

	<div id="footer">
            Generated with <a href="http://d4p.me/ccssxt" title="Smart Sitemap.xml Tools for WordPress">Smart Sitemap.xml Tools for WordPress</a> by <a href="http://www.dev4press.com/">Milan Petrovic</a>.
	</div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="sitemap:sitemap">
    <tr>
      <td>
        <xsl:variable name="sitemapURL"><xsl:value-of select="sitemap:loc"/></xsl:variable>
        <a href="{$sitemapURL}"><xsl:value-of select="$sitemapURL"></xsl:value-of></a>
      </td>
      <td><xsl:value-of select="sitemap:lastmod"/></td>
    </tr>
  </xsl:template>

</xsl:stylesheet>
