<?php

if (!defined('ABSPATH')) exit;

class ssxt_sitemap {
    private $_page = 0;
    private $_type = 0;
    private $_name = 0;

    public $cache = false;
    public $generator = false;

    public $sitemap_request = false;
    public $request_type = 'xml';
    public $request_call = '';
    public $request_gzip = false;

    public $sitemap_slug = 'sitemap';
    public $default_maps = array();
    public $custom_maps = array();
    public $type_labels = array(
        'core' => 'Core',
        'cpts' => 'Post Type',
        'taxs' => 'Taxonomy',
        'date' => 'Date',
        'user' => 'User',
        'cstm' => 'Custom'
    );
    public $default_priorities = array();

    function __construct() {
        $this->cache = new ssxt_cache();

        add_action('ssxt_init_plugin_settings', array($this, 'init_settings'));
    }

    public function init_settings() {
        $this->sitemap_slug = smart_mapxml_core()->get('sitemap_base');

        $this->run();
    }
    
    public function run() {
        if (ssxt_permalinks_enabled() && !ssxt_has_real_sitemap_xml()) {
            add_action('ssxt_rebuild_sitemap', array($this, 'rebuild_sitemap'));

            add_action('transition_post_status', array($this, 'transition_post_status'));

            add_action('after_setup_theme', array(&$this, 'after_setup_theme'));

            add_filter('query_vars', array(&$this, 'query_vars'));
            add_action('parse_request', array(&$this, 'parse_request'), 1);
            add_action('generate_rewrite_rules', array(&$this, 'generate_rewrite_rules'), 9);

            add_action('wp', array(&$this, 'wp'), 1000);

            add_filter('template_redirect', array(&$this, 'template_redirect'), 1);
        }
    }

    public function rebuild_sitemap() {
        if (smart_mapxml_core()->get('cache_maps')) {
            require_once(SSXT_PATH.'sitemap/generator.php');

            $this->generator = new ssxt_generator();

            $this->prepare_generator();

            $index = $this->generator->prepare_index();

            foreach ($index as $map) {
                $this->generator->prepare_content($map['type'], $map['name'], $map['page']);
            }
        }

        if (smart_mapxml_core()->get('notify_google')) {
            $this->ping_google();
        }

        if (smart_mapxml_core()->get('notify_bing')) {
            $this->ping_bing();
        }
    }

    public function transition_post_status($new_status) {
        if ($new_status == 'publish') {
            $this->schedule_rebuild_maps();
        }
    }

    public function schedule_rebuild_maps() {
        $this->invalidate_cache();

        wp_schedule_single_event(time() + 30, 'ssxt_rebuild_sitemap');
    }

    public function after_setup_theme() {
        if (smart_mapxml_core()->get('robots_txt')) {
            add_action('do_robots', array(&$this, 'do_robots'), 1000);
        }

        do_action('ssxt_custom_maps_registration');
    }

    public function do_robots() {
        echo SSXT_EOL.SSXT_EOL.'Sitemap: '.ssxt_get_sitemap_index_url().SSXT_EOL;
    }

    public function query_vars($qv) {
        $qv[] = 'ssxt_map';
        $qv[] = 'ssxt_type';

        return $qv;
    }

    public function parse_request($wp) {
        if (isset($wp->query_vars['ssxt_map']) && $wp->query_vars['ssxt_map'] != '') {
            $this->sitemap_request = true;
            $this->request_call = $wp->query_vars['ssxt_map'];

            if (isset($wp->query_vars['ssxt_type']) && $wp->query_vars['ssxt_type'] != '') {
                $type = $wp->query_vars['ssxt_type'];

                if ($type == 'xmlgz') {
                    $this->request_gzip = true;
                } else {
                    $this->request_type = $type;
                }
            }

            add_filter('posts_where', array($this, 'posts_where'));
            add_filter('wp_headers', array($this, 'wp_headers'));

            require_once(SSXT_PATH.'sitemap/generator.php');

            $this->generator = new ssxt_generator();
        }
    }

    public function generate_rewrite_rules($wp_rewrite) {
        $slug = $this->sitemap_slug;

        $qvar = 'ssxt_map';
        $qtyp = 'ssxt_type';

        $rules = array();

        $rules[$slug."\.xml$"] = "index.php?".$qvar."=index&".$qtyp."=xml";
        $rules[$slug."\.xsl$"] = "index.php?".$qvar."=index&".$qtyp."=xsl";
        $rules[$slug."\.xml\.gz$"] = "index.php?".$qvar."=index&".$qtyp."=xmlgz";
        $rules[$slug."-([^/]+)\.xml$"] = "index.php?".$qvar."=".$wp_rewrite->preg_index(1)."&".$qtyp."=xml";
        $rules[$slug."-([^/]+)\.xsl$"] = "index.php?".$qvar."=".$wp_rewrite->preg_index(1)."&".$qtyp."=xsl";
        $rules[$slug."-([^/]+)\.xml\.gz$"] = "index.php?".$qvar."=".$wp_rewrite->preg_index(1)."&".$qtyp."=xmlgz";

        $wp_rewrite->rules = $rules + $wp_rewrite->rules;
    }

    public function wp() {
        global $wp_query;

        if (get_query_var('ssxt_map')) {
            $wp_query->is_sitemap_xml = true;

            $wp_query->is_404 = false;
            $wp_query->is_home = false;
            $wp_query->is_feed = true;
        } else {
            $wp_query->is_sitemap_xml = false;
        }
    }

    public function posts_where($where) {
        return ' AND 0=1 '.$where;
    }

    public function wp_headers($headers) {
        if (isset($headers['X-Pingback'])) {
            unset($headers['X-Pingback']);
        }

        $headers['X-Robots-Tag'] = 'noindex, follow';
        $headers['Content-Type'] = 'text/xml';

        if ($this->request_type == 'xsl') {
            $expires = WEEK_IN_SECONDS;

            $headers['Pragma'] = 'public';
            $headers['Expires'] = gmdate('D, d M Y H:i:s', time() + $expires).' GMT';
            $headers['Cache-Control'] = 'maxage='.$expires;
        }

        return $headers;
    }

    public function prepare_generator() {
        @ini_set('memory_limit', '256M');
        @set_time_limit(0);

        $this->init_default_maps();
        $this->init_custom_maps();

        $this->generator->maps = $this->default_maps;

        if (empty($this->custom_maps)) {
            $this->generator->maps['cstm'] = $this->custom_maps;
        }
    }

    public function template_redirect() {
        global $wp_query; 

        if ($wp_query->is_sitemap_xml) {
            $this->prepare_generator();

            do_action('ssxt_serve_sitemap_before', $this->request_type, $this->_type, $this->_name, $this->_page);
            
            if ($this->request_type == 'xsl') {
                if ($this->request_call == 'index') {
                    $this->build_style_index();
                } else {
                    $this->build_style_content();
                }
            } else {
                if ($this->request_call == 'index') {
                    $this->build_index();
                } else {
                    $this->build_content();
                }
            }

            do_action('ssxt_serve_sitemap_after', $this->request_type, $this->_type, $this->_name, $this->_page);

            exit;
        }
    }

    public function build_index() {
        $this->_type = 'index';

        $this->generator->index();
    }

    public function process_request() {
        $request = explode('-smapidx-', $this->request_call, 2);

        $main = $request[0];
        $this->_page = count($request) == 2 ? intval($request[1]) : 0;
        $call = explode('-', $main, 2);

        $this->_type = $call[0];
        $this->_name = $call[1];
    }

    public function build_content() {
        $this->process_request();

        $this->generator->content($this->_type, $this->_name, $this->_page);
    }

    public function build_style_index() {
        $this->_type = 'index';

        $this->output_style_file('index.xsl');
    }

    public function build_style_content() {
        $this->process_request();

        $this->output_style_file('content.xsl');
    }

    public function output_style_file($file) {
        $file = file_get_contents(SSXT_PATH.'xslt/'.$file, 'rb');

        $current_url = '';
        $current_title = '';
        $request = $this->request_call;

        if ($request == 'index') {
            $current_url = $this->sitemap_slug.'.xml';
            $current_title = 'Index';
        } else {
            $current_url = $this->sitemap_slug.'-'.$request.'.xml';
            
            $current_title = $this->type_labels[$this->_type].' / ';

            if ($this->_type == 'cstm') {
                $current_title.= $this->custom_maps[$this->_name]['label'];
            } else {
                $current_title.= $this->default_maps[$this->_type][$this->_name]['label'];
            }

            if ($this->_page > 0) {
                $current_title.= ' / Page '.($this->_page + 1);
            }
        }
        
        $file = str_replace('%WEBSITE_TITLE%', get_option('blogname'), $file);
        $file = str_replace('%SITEMAP_TITLE%', $current_title, $file);
        $file = str_replace('%SITEMAP_URL%', ssxt_get_sitemap_index_url(), $file);
        $file = str_replace('%SITEMAP_URL_CURRENT%', site_url($current_url), $file);
        
        echo $file;
        exit;
    }

    public function url_xslt($type = 'index') {
        $path = $this->sitemap_slug;

        if ($type == 'content') {
            $path.= '-content';
        }

        $path.= '.xsl';

        return site_url($path);
    }

    public function sitemap_file($gzip = false) {
        $path = $this->sitemap_slug.'.xml';

        if ($gzip) {
            $path.= '.gz';
        }

        return $path;
    }

    public function url_index_xslt() {
        return site_url($this->sitemap_slug.'.xsl');
    }

    public function url_content_xslt($type, $name, $page) {
        $path = $this->sitemap_slug.'-'.$type.'-'.$name;

        if ($page > 0) {
            $path.= '-smapidx-'.$page;
        }

        $path.= '.xsl';

        return site_url($path);
    }

    public function url_index($gzip = false) {
        return site_url($this->sitemap_file($gzip));
    }

    public function url_content($type, $name, $page, $gzip = false) {
        $path = $this->sitemap_slug.'-'.$type.'-'.$name;

        if ($page > 0) {
            $path.= '-smapidx-'.$page;
        }

        $path.= '.xml';

        if ($gzip) {
            $path.= '.gz';
        }

        return site_url($path);
    }

    public function ping_google() {
        $url = $this->url_index();

        wp_remote_get('http://www.google.com/webmasters/tools/ping?sitemap='.$url);

        smart_mapxml_core()->set('last_notify_google', time());
        smart_mapxml_core()->save();
    }

    public function ping_bing() {
        $url = $this->url_index();

        wp_remote_get('http://www.bing.com/ping?sitemap='.$url);

        smart_mapxml_core()->set('last_notify_bing', time());
        smart_mapxml_core()->save();
    }

    public function init_default_maps() {
        $this->default_priorities = array(
            'core' => array(),
            'cstm' => array(),
            'cpts' => array(
                'date' => __("Publish Date", "smart-sitemapxml-tools"),
                'comments_total' => __("Total Comments", "smart-sitemapxml-tools"),
                'comments_average' => __("Average Comments", "smart-sitemapxml-tools")
            ),
            'taxs' => array(
                'posts_total' => __("Total Posts", "smart-sitemapxml-tools")
            ),
            'date' => array(
                'latest' => __("Latest", "smart-sitemapxml-tools"),
                'posts_total' => __("Total Posts", "smart-sitemapxml-tools")
            ),
            'user' => array(
                'posts_total' => __("Total Posts", "smart-sitemapxml-tools")
            )
        );

        $this->default_maps['core'] = array(
            'home' => array('name' => 'home', 'label' => __("Home", "smart-sitemapxml-tools"), 'active' => true, 'priority' => '1.0', 'frequency' => 'daily'),
            'extra' => array('name' => 'extra', 'label' => __("Extra", "smart-sitemapxml-tools"), 'active' => true, 'priority' => '0.5', 'frequency' => 'monthly')
        );

        $this->default_maps['cpts'] = array();

        $post_types = get_post_types(array('public' => true), 'objects');

        foreach ($post_types as $cpt => $obj) {
            $this->default_maps['cpts'][$cpt] = array('name' => $cpt, 'label' => $obj->label, 'active' => true, 'priority' => '0.8', 'frequency' => 'weekly');
        }

        $this->default_maps['taxs'] = array();

        $taxonomies = get_taxonomies(array('public' => true), 'objects');

        foreach ($taxonomies as $tax => $obj) {
            $this->default_maps['taxs'][$tax] = array('name' => $tax, 'label' => $obj->label, 'active' => true, 'priority' => 'posts_total', 'frequency' => 'monthly');
        }

        $this->default_maps['date'] = array(
            'year' => array('name' => 'year', 'label' => __("Yearly", "smart-sitemapxml-tools"), 'active' => true, 'priority' => 'latest', 'frequency' => 'monthly'),
            'month' => array('name' => 'month', 'label' => __("Monthly", "smart-sitemapxml-tools"), 'active' => true, 'priority' => 'latest', 'frequency' => 'monthly'),
            'week' => array('name' => 'week', 'label' => __("Weekly", "smart-sitemapxml-tools"), 'active' => true, 'priority' => 'latest', 'frequency' => 'monthly'),
            'day' => array('name' => 'day', 'label' => __("Daily", "smart-sitemapxml-tools"), 'active' => true, 'priority' => 'latest', 'frequency' => 'monthly')
        );

        $this->default_maps['user'] = array(
            'author' => array('name' => 'author', 'label' => __("Authors", "smart-sitemapxml-tools"), 'active' => true, 'priority' => 'posts_total', 'frequency' => 'weekly')
        );

        foreach ($this->default_maps as $type => $maps) {
            $current = smart_mapxml_core()->get_maps($type);

            foreach (array_keys($maps) as $code) {
                if (isset($current[$code])) {
                    foreach ($current[$code] as $idx => $val) {
                        $this->default_maps[$type][$code][$idx] = $val;
                    }
                }
            }
        }
    }

    public function init_custom_maps() {
        $type = 'cstm';
        $maps = $this->custom_maps;

        $current = smart_mapxml_core()->get_maps($type);

        foreach (array_keys($maps) as $code) {
            if (isset($current[$code])) {
                foreach ($current[$code] as $idx => $val) {
                    $this->custom_maps[$code][$idx] = $val;
                }
            }
        }
    }

    public function invalidate_cache() {
        $this->cache = new ssxt_cache();
        $this->cache->invalidate();
    }
}

class ssxt_cache {
    public $base;
    public $expire;

    function __construct() {
        $this->base = 'ssxt-';
        $this->expire = 2 * WEEK_IN_SECONDS;
    }

    public function invalidate() {
        global $wpdb;

        $key_base = $this->base.'%';

        $sql = sprintf("delete from %s where option_name like '_transient_%s' or option_name like '_transient_timeout_%s'", $wpdb->options, $key_base, $key_base);
        $wpdb->query($sql);
    }

    public function get_index() {
        return get_transient($this->base.'index');
    }

    public function set_index($cache) {
        set_transient($this->base.'index', $cache, $this->expire);
    }

    public function get_content($type, $name, $page) {
        return get_transient($this->base.$type.'-'.$name.'-'.$page);
    }

    public function set_content($type, $name, $page, $cache) {
        set_transient($this->base.$type.'-'.$name.'-'.$page, $cache, $this->expire);
    }

    public function get_exclude($name) {
        return get_transient($this->base.'exclude-'.$name);
    }

    public function set_exclude($name, $cache) {
        set_transient($this->base.'exclude-'.$name, $cache, $this->expire);
    }
}

?>