<?php

if (!defined('ABSPATH')) exit;

class ssxt_data {
    public static function clear_posts_meta() {
        global $wpdb;

        $keys = array('_gdsxt_excluded', '_gdsxt_priority', '_gdsxt_frequency');

        $sql = sprintf("delete %s where meta_key in ('%s')", $wpdb->postmeta, join("', '", $keys));
        $wpdb->query($sql);
    }

    public static function clear_terms_meta() {
        global $wpdb;

        $key_base = 'gdssxt_term_meta_%';

        $sql = sprintf("delete from %s where option_name like '%s'", $wpdb->options, $key_base);
        $wpdb->query($sql);
    }

    public static function clear_users_meta() {
        global $wpdb;

        $keys = array('_gdsxt_excluded', '_gdsxt_priority', '_gdsxt_frequency', '_gdsxt_exclude_posts');

        $sql = sprintf("delete from %s where meta_key in ('%s')", $wpdb->usermeta, join("', '", $keys));
        $wpdb->query($sql);
    }

    public static function get_excluded_posts() {
        $items = smart_sitemap_xml()->cache->get_exclude('posts');

        if ($items === false) {
            global $wpdb;

            $sql = sprintf("select post_id from %s where meta_key = '_gdsxt_excluded' and meta_value = '1'", $wpdb->postmeta);
            $raw = $wpdb->get_results($sql);

            $items = wp_list_pluck($raw, 'post_id');

            $terms = ssxt_data::get_excluded_terms_to_posts();
            $users = ssxt_data::get_excluded_users_to_posts();

            if (!empty($terms)) {
                $sql = sprintf("select tr.object_id from %s tt inner join %s tr on tr.term_taxonomy_id = tt.term_taxonomy_id where tt.term_id in (%s)", $wpdb->term_taxonomy, $wpdb->term_relationships, join(',', $terms));
                $raw = $wpdb->get_results($sql);

                $items = array_merge($items, wp_list_pluck($raw, 'object_id'));
                $items = array_unique($items);
                $items = array_filter($items);
            }

            if (!empty($users)) {
                $sql = sprintf("select ID from %s where post_author in (%s)", $wpdb->posts, join(',', $users));
                $raw = $wpdb->get_results($sql);

                $items = array_merge($items, wp_list_pluck($raw, 'ID'));
                $items = array_unique($items);
                $items = array_filter($items);
            }

            smart_sitemap_xml()->cache->set_exclude('posts', $items);
        }

        return $items;
    }

    public static function get_excluded_terms_to_posts() {
        $items = smart_sitemap_xml()->cache->get_exclude('terms_posts');

        if ($items === false) {
            global $wpdb;

            $sql = sprintf("select option_name, option_value from %s where option_name like '%s'", $wpdb->options, 'gdssxt_term_meta_%');
            $raw = $wpdb->get_results($sql);

            $items = array();
            foreach ($raw as $row) {
                $settings = maybe_unserialize($row->option_value);

                if (is_array($settings) && isset($settings['exclude_posts']) && $settings['exclude_posts'] == 1) {
                    $term_id = intval(substr($row->option_name, 17));
                    $items[] = $term_id;
                }
            }

            smart_sitemap_xml()->cache->set_exclude('terms_posts', $items);
        }

        return $items;
    }

    public static function get_excluded_terms() {
        $items = smart_sitemap_xml()->cache->get_exclude('terms');

        if ($items === false) {
            global $wpdb;

            $sql = sprintf("select option_name, option_value from %s where option_name like '%s'", $wpdb->options, 'gdssxt_term_meta_%');
            $raw = $wpdb->get_results($sql);

            $items = array();
            foreach ($raw as $row) {
                $settings = maybe_unserialize($row->option_value);

                if (is_array($settings) && isset($settings['excluded']) && $settings['excluded'] == 1) {
                    $term_id = intval(substr($row->option_name, 17));
                    $items[] = $term_id;
                }
            }

            smart_sitemap_xml()->cache->set_exclude('terms', $items);
        }

        return $items;
    }

    public static function get_excluded_users_to_posts() {
        $items = smart_sitemap_xml()->cache->get_exclude('users');

        if ($items === false) {
            global $wpdb;

            $sql = sprintf("select user_id from %s where meta_key = '_gdsxt_exclude_posts' and meta_value = '1'", $wpdb->usermeta);
            $raw = $wpdb->get_results($sql);

            $items = wp_list_pluck($raw, 'user_id');

            smart_sitemap_xml()->cache->set_exclude('users', $items);
        }

        return $items;
    }

    public static function get_excluded_users() {
        $items = smart_sitemap_xml()->cache->get_exclude('users');

        if ($items === false) {
            global $wpdb;

            $sql = sprintf("select user_id from %s where meta_key = '_gdsxt_excluded' and meta_value = '1'", $wpdb->usermeta);
            $raw = $wpdb->get_results($sql);

            $items = wp_list_pluck($raw, 'user_id');

            smart_sitemap_xml()->cache->set_exclude('users', $items);
        }

        return $items;
    }

    public static function get_authors($offset, $limit) {
        global $wpdb;

        $exclude = apply_filters('ssxt_data_get_authors_exclude', ssxt_data::get_excluded_users());
        $post_status = apply_filters('ssxt_data_get_authors_post_status', array('publish', 'inherit'));
        $post_types = apply_filters('ssxt_data_get_authors_post_type', array('post'));

        $where = sprintf("p.post_status in ('%s') and p.post_type in ('%s')", join("', '", $post_status), join("', '", $post_types));
        if (!empty($exclude)) {
            $where.= " and p.post_author not in (".join(', ', $exclude).")";
        }

        $sql = sprintf("select u.ID, u.display_name, count(*) as count, max(post_modified_gmt) as lastmod 
                        from %s p inner join %s u on u.ID = p.post_author
                        where %s group by u.ID order by count desc limit %s, %s", 
                    $wpdb->posts, $wpdb->users, $where, $offset, $limit);
        return $wpdb->get_results($sql);
    }

    public static function get_authors_count() {
        global $wpdb;

        $exclude = apply_filters('ssxt_data_get_authors_exclude', ssxt_data::get_excluded_users());
        $post_status = apply_filters('ssxt_data_get_authors_post_status', array('publish', 'inherit'));
        $post_types = apply_filters('ssxt_data_get_authors_post_type', array('post'));

        $where = sprintf("p.post_status in ('%s') and p.post_type in ('%s')", join("', '", $post_status), join("', '", $post_types));
        if (!empty($exclude)) {
            $where.= " and p.post_author not in (".join(', ', $exclude).")";
        }

        $sql = sprintf("select count(distinct p.post_author) from %s p where %s", $wpdb->posts, $where);
        return intval($wpdb->get_var($sql));
    }

    public static function get_lastmod_global() {
        global $wpdb;

        $post_status = array('publish', 'inherit');
        $sql = sprintf("select post_modified_gmt from %s where post_status in ('%s') order by post_modified_gmt desc limit 0, 1", $wpdb->posts, join("', '", $post_status));

        return mysql2date('c', $wpdb->get_var($sql));
    }

    public static function get_posts_lastmod($post_type) {
        global $wpdb;

        $post_status = $post_type == 'attachment' ? array('inherit') : array('publish');
        $sql = sprintf("select post_modified_gmt from %s where post_type = '%s' and post_status in ('%s') order by post_modified_gmt desc limit 0, 1", $wpdb->posts, $post_type, join("', '", $post_status));

        return mysql2date('c', $wpdb->get_var($sql));
    }

    public static function get_terms_lastmod($taxonomy) {
        global $wpdb;

        $post_status = array('publish');
        $sql = sprintf("select p.post_modified_gmt from %s p inner join %s tr on tr.object_id = p.ID inner join %s tt on tt.term_taxonomy_id = tr.term_taxonomy_id where p.post_status in ('%s') and tt.taxonomy = '%s' order by p.post_modified_gmt desc limit 0, 1", $wpdb->posts, $wpdb->term_relationships, $wpdb->term_taxonomy, join("', '", $post_status), $taxonomy);

        return mysql2date('c', $wpdb->get_var($sql));
    }

    public static function get_posts($post_type, $offset, $limit) {
        global $wpdb;

        $exclude = apply_filters('ssxt_data_get_posts_exclude', ssxt_data::get_excluded_posts(), $post_type);
        $post_status = apply_filters('ssxt_data_get_posts_post_status', array('publish', 'inherit'), $post_type);
        $post_types = apply_filters('ssxt_data_get_posts_post_type', array($post_type), $post_type);

        $where = sprintf("p.post_status in ('%s') and p.post_type in ('%s')", join("', '", $post_status), join("', '", $post_types));
        if (!empty($exclude)) {
            $where.= " and p.ID not in (".join(', ', $exclude).")";
        }

        $sql = sprintf("select ID, post_title, post_modified_gmt from %s p where %s order by post_modified_gmt desc limit %s, %s", $wpdb->posts, $where, $offset, $limit);
        return $wpdb->get_results($sql);
    }

    public static function get_posts_count($post_type) {
        global $wpdb;

        $exclude = apply_filters('ssxt_data_get_posts_exclude', ssxt_data::get_excluded_posts(), $post_type);
        $post_status = apply_filters('ssxt_data_get_posts_post_status', array('publish', 'inherit'), $post_type);
        $post_types = apply_filters('ssxt_data_get_posts_post_type', array($post_type), $post_type);

        $where = sprintf("p.post_status in ('%s') and p.post_type in ('%s')", join("', '", $post_status), join("', '", $post_types));
        if (!empty($exclude)) {
            $where.= " and p.ID not in (".join(', ', $exclude).")";
        }

        $sql = sprintf("select count(*) from %s p where %s", $wpdb->posts, $where);
        return intval($wpdb->get_var($sql));
    }

    public static function get_terms_count($taxonomy) {
        $exclude = apply_filters('ssxt_data_get_terms_exclude', ssxt_data::get_excluded_terms(), $taxonomy);

        return wp_count_terms($taxonomy, array('hide_empty' => true, 'exclude' => $exclude));
    }

    public static function get_terms($taxonomy, $offset, $limit) {
        $exclude = apply_filters('ssxt_data_get_terms_exclude', ssxt_data::get_excluded_terms(), $taxonomy);

        return get_terms($taxonomy, array('hide_empty' => true, 'exclude' => $exclude, 'offset' => $offset, 'number' => $limit));
    }

    public static function get_terms_lastmod_list($taxonomy, $terms) {
        global $wpdb;

        $sql = sprintf("select tt.term_id, max(p.post_modified_gmt) AS lastmod from %s p 
                        inner join %s tr on tr.object_id = p.ID 
                        inner join %s tt on tt.term_taxonomy_id = tr.term_taxonomy_id 
                        where p.post_status in ('publish', 'inherit') and tt.taxonomy = '%s' 
                        and tt.term_id in (%s) group by tt.term_id", $wpdb->posts, $wpdb->term_relationships, 
                        $wpdb->term_taxonomy, $taxonomy, join(',', $terms)
                );
        $raw = $wpdb->get_results($sql);

        $lastmods = array();

        foreach ($raw as $r) {
            $lastmods[$r->term_id] = mysql2date('c', $r->lastmod);
        }

        return $lastmods;
    }

    public static function get_year($offset, $limit) {
        global $wpdb;

        $post_status = apply_filters('ssxt_data_get_year_post_status', array('publish'));
        $post_type = apply_filters('ssxt_data_get_year_post_type', array('post'));

        $where = array(
            sprintf("p.post_status in ('%s')", join("', '", $post_status)),
            sprintf("p.post_type in ('%s')", join("', '", $post_type))
        );

        $sql = sprintf("select year(p.post_date) as year, count(*) as count, max(p.post_modified_gmt) as lastmod 
                        from %s p where %s group by year order by year desc limit %s, %s", 
                    $wpdb->posts, join(" and ", $where), $offset, $limit);
        return $wpdb->get_results($sql);
    }

    public static function get_year_count() {
        global $wpdb;

        $post_status = apply_filters('ssxt_data_get_year_post_status', array('publish'));
        $post_type = apply_filters('ssxt_data_get_year_post_type', array('post'));

        $where = array(
            sprintf("p.post_status in ('%s')", join("', '", $post_status)),
            sprintf("p.post_type in ('%s')", join("', '", $post_type))
        );

        $sql = sprintf("select count(distinct year(post_date)) from %s p where %s", $wpdb->posts, join(" and ", $where));

        return intval($wpdb->get_var($sql));
    }

    public static function get_month($offset, $limit) {
        global $wpdb;

        $post_status = apply_filters('ssxt_data_get_month_post_status', array('publish'));
        $post_type = apply_filters('ssxt_data_get_month_post_type', array('post'));

        $where = array(
            sprintf("p.post_status in ('%s')", join("', '", $post_status)),
            sprintf("p.post_type in ('%s')", join("', '", $post_type))
        );

        $sql = sprintf("select year(p.post_date) as year, month(p.post_date) as month, count(*) as count, max(p.post_modified_gmt) as lastmod 
                        from %s p where %s group by year, month order by p.post_modified_gmt desc limit %s, %s", 
                    $wpdb->posts, join(" and ", $where), $offset, $limit);
        return $wpdb->get_results($sql);
    }

    public static function get_month_count() {
        global $wpdb;

        $post_status = apply_filters('ssxt_data_get_month_post_status', array('publish'));
        $post_type = apply_filters('ssxt_data_get_month_post_type', array('post'));

        $where = array(
            sprintf("p.post_status in ('%s')", join("', '", $post_status)),
            sprintf("p.post_type in ('%s')", join("', '", $post_type))
        );

        $sql = sprintf("select count(distinct concat(year(post_date), '-', month(post_date))) from %s p where %s", $wpdb->posts, join(" and ", $where));

        return intval($wpdb->get_var($sql));
    }

    public static function get_week($offset, $limit) {
        global $wpdb;

        $post_status = apply_filters('ssxt_data_get_week_post_status', array('publish'));
        $post_type = apply_filters('ssxt_data_get_week_post_type', array('post'));

        $where = array(
            sprintf("p.post_status in ('%s')", join("', '", $post_status)),
            sprintf("p.post_type in ('%s')", join("', '", $post_type))
        );

        $week = _wp_mysql_week('post_modified_gmt');
        $sql = sprintf("select %s as week, year(p.post_date) as year, date_format(post_date, '%s') AS yyyymmdd, count(*) as count, max(p.post_modified_gmt) as lastmod 
                        from %s p where %s group by week, year order by p.post_modified_gmt desc limit %s, %s", 
                    $week, '%Y-%m-%d', $wpdb->posts, join(" and ", $where), $offset, $limit);
        return $wpdb->get_results($sql);
    }

    public static function get_week_count() {
        global $wpdb;

        $post_status = apply_filters('ssxt_data_get_week_post_status', array('publish'));
        $post_type = apply_filters('ssxt_data_get_week_post_type', array('post'));

        $where = array(
            sprintf("p.post_status in ('%s')", join("', '", $post_status)),
            sprintf("p.post_type in ('%s')", join("', '", $post_type))
        );

        $week = _wp_mysql_week('post_modified_gmt');
        $sql = sprintf("select count(distinct concat(year(post_date), '-', %s)) from %s p where %s", $week, $wpdb->posts, join(" and ", $where));

        return intval($wpdb->get_var($sql));
    }

    public static function get_day($offset, $limit) {
        global $wpdb;

        $post_status = apply_filters('ssxt_data_get_day_post_status', array('publish'));
        $post_type = apply_filters('ssxt_data_get_day_post_type', array('post'));

        $where = array(
            sprintf("p.post_status in ('%s')", join("', '", $post_status)),
            sprintf("p.post_type in ('%s')", join("', '", $post_type))
        );

        $sql = sprintf("select year(p.post_date) as year, month(p.post_date) as month, day(p.post_date) as day, count(*) as count, max(p.post_modified_gmt) as lastmod 
                        from %s p where %s group by year, month, day order by p.post_modified_gmt desc limit %s, %s", 
                    $wpdb->posts, join(" and ", $where), $offset, $limit);
        return $wpdb->get_results($sql);
    }

    public static function get_day_count() {
        global $wpdb;

        $post_status = apply_filters('ssxt_data_get_day_post_status', array('publish'));
        $post_type = apply_filters('ssxt_data_get_day_post_type', array('post'));

        $where = array(
            sprintf("p.post_status in ('%s')", join("', '", $post_status)),
            sprintf("p.post_type in ('%s')", join("', '", $post_type))
        );

        $sql = sprintf("select count(distinct concat(year(post_date), '-', month(post_date), '-', day(post_date))) from %s p where %s", $wpdb->posts, join(" and ", $where));

        return intval($wpdb->get_var($sql));
    }
}

?>