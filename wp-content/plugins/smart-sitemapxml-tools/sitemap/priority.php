<?php

if (!defined('ABSPATH')) exit;

class ssxt_priority_calc {
    private $posts = 0;
    private $comments = 0;

    private $priority = '0.7';
    private $object = false;

    function __construct($type, $priority, $posts = 0, $comments = 0) {
        $this->posts = $posts;
        $this->comments = $comments;

        if (is_numeric($priority)) {
            $this->priority = $priority;
        } else {
            $cls = 'ssxt_priority_'.$type.'_'.$priority;

            if (class_exists($cls)) {
                $this->object = new $cls($this->posts, $this->comments);
            }
        }

        
    }

    public function priority($obj = null) {
        if ($this->object === false) {
            return $this->priority;
        } else {
            return $this->object->priority($obj);
        }
    }
}

class ssxt_priority {
    public $posts = 0;
    public $comments = 0;

    function __construct($posts = 0, $comments = 0) {
        $this->posts = $posts;
        $this->comments = $comments;
    }

    public function normalize($priority) {
        if ($priority > 1) {
            return '1';
        }

        if ($priority < 0.1) {
            return '0.1';
        }

        return number_format(floatval($priority), 1).'';
    }

    public function priority($obj = null) {
        return 0;
    }
}

class ssxt_priority_posts_total extends ssxt_priority {
    public function priority($obj = null) {
        $priority = 0;

        $posts = $obj->count;
        if ($this->posts > 0 && $posts > 0) {
            $priority = 2 * ((double)$posts / $this->posts);
        }

        return $this->normalize($priority);
    }
}

class ssxt_priority_cpts_date extends ssxt_priority {
    public function priority($obj = null) {
        $priority = 0.4;

        if ($obj->order_id < 15) {
            $priority = 0.8;
        } else if ($obj->order_id < 500) {
            $priority = 0.6;
        }

        return $this->normalize($priority);
    }
}

class ssxt_priority_cpts_comments_total extends ssxt_priority {
    public function priority($obj = null) {
        $priority = 0;

        $comments = $obj->comment_count;
        if ($this->comments > 0 && $comments > 0) {
            $priority = (double)$comments / $this->comments;
        }

        return $this->normalize($priority);
    }
}

class ssxt_priority_cpts_comments_average extends ssxt_priority {
    public $average = 0;

    function __construct($posts = 0, $comments = 0) {
        parent::__construct($posts, $comments);

        if ($this->comments > 0 && $this->posts > 0) {
            $this->average = (double)$this->comments / $this->posts;
        }
    }

    public function priority($obj = null) {
        $priority = 0;

        $comments = $obj->comment_count;
        if ($this->average == 0) {
            if ($comments > 0) {
                $priority = 1;
            }
        } else {
            $priority = (double)$comments / $this->average;
        }

        return $this->normalize($priority);
    }
}

class ssxt_priority_date_latest extends ssxt_priority {
    public function priority($obj = null) {
        $priority = 0.6;

        switch ($obj->type) {
            case 'year':
                if ($obj->id == 0) $priority = 0.8;
                else if ($obj->id < 4) $priority = 0.5;
                else $priority = 0.2;
                break;
            case 'month':
                if ($obj->id < 1) $priority = 0.8;
                else if ($obj->id < 8) $priority = 0.6;
                else if ($obj->id < 24) $priority = 0.4;
                else $priority = 0.2;
                break;
            case 'week':
                if ($obj->id < 3) $priority = 0.8;
                else if ($obj->id < 15) $priority = 0.6;
                else if ($obj->id < 48) $priority = 0.4;
                else $priority = 0.2;
                break;
            case 'day':
                if ($obj->id < 7) $priority = 0.8;
                else if ($obj->id < 60) $priority = 0.6;
                else if ($obj->id < 360) $priority = 0.4;
                else $priority = 0.2;
                break;
        }

        return $this->normalize($priority);
    }
}

class ssxt_priority_user_posts_total extends ssxt_priority_posts_total { }

class ssxt_priority_taxs_posts_total extends ssxt_priority_posts_total { }

class ssxt_priority_date_posts_total extends ssxt_priority_posts_total { }

?>