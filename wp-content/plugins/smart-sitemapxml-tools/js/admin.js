/********************************************************************
* Limit the characters that may be entered in a text field
* Common options: alphanumeric, alphabetic or numeric
* John Antoni Griffiths
* https://github.com/johnantoni/jquery.alphanumeric/
*********************************************************************/
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('(3($){$.h.k=3(p){6 b=$(8),l="n",2=$.m({f:\'!@#$%^&*()+=[]\\\\\\\';,/{}|":<>?~`.- O\',4:\'\',c:\'\'},p),s=2.c.N(\'\'),i=0,9,d;t(i;i<s.w;i++){5(2.f.g(s[i])!=-1){s[i]=\'\\\\\'+s[i]}}5(2.L){2.4+=l.q()}5(2.K){2.4+=l}2.c=s.J(\'|\');d=y D(2.c,\'A\');9=(2.f+2.4).I(d,\'\');b.z(3(e){6 a=B.C(!e.v?e.E:e.v);5(9.g(a)!=-1&&!e.F){e.G()}});b.H(3(){6 a=b.u(),j=0;t(j;j<a.w;j++){5(9.g(a[j])!=-1){b.u(\'\');7 o}}7 o});7 b};$.h.M=3(p){6 a=\'n\',x=a.q();7 8.r(3(){$(8).k($.m({4:a+x},p))})};$.h.P=3(p){6 a=\'Q\';7 8.r(3(){$(8).k($.m({4:a},p))})}})(R);',54,54,'||options|function|nchars|if|var|return|this|ch|||allow|regex||ichars|indexOf|fn|||alphanumeric|az|extend|abcdefghijklmnopqrstuvwxyz|false||toUpperCase|each||for|val|charCode|length|aZ|new|keypress|gi|String|fromCharCode|RegExp|which|ctrlKey|preventDefault|blur|replace|join|allcaps|nocaps|numeric|split|_|alpha|1234567890|jQuery'.split('|'),0,{}));

/*jslint regexp: true, nomen: true, sloppy: true, eqeq: true, vars: true, white: true, plusplus: true, maxerr: 50, indent: 4 */
var ssxt_admin = {
    i: 0,
    id: 0,
    init: function() {
        jQuery(".sct-input-numeric").numeric();
        jQuery(".sct-input-alpha").alpha();

        jQuery("#run-export").click(function(e){
            e.preventDefault();

            var url = jQuery(this).data("url"), exp = [];

            jQuery("[name^=export_]").each(function(){
                if (jQuery(this).is(":checked")) {
                    exp.push(jQuery(this).attr("id").substr(7));
                }
            });

            url+= "&export=" + exp.join(",");
            window.location = url;
        });
    },
    links: function(id) {
        ssxt_admin.id = id;

        jQuery("#ssxt-new-link").click(function(){
            var row = jQuery(".wp-list-table.links tbody tr:first-child").clone(),
                new_id = ssxt_admin.id;

            jQuery("input, select", row).each(function(){
                var name = jQuery(this).attr("name").replace("[0]", "[" + new_id + "]");

                jQuery(this).attr("name", name);
            });

            ssxt_admin.id++;

            jQuery(".wp-list-table.links tbody").append(row);
        });

        jQuery(document).on("click", ".wp-list-table.links td.column-remove a", function(e){
            e.preventDefault();

            if (jQuery(".wp-list-table.links tbody tr").length === 2) {
                jQuery("#ssxt-new-link").click();
            }

            jQuery(this).closest("tr").remove();
        });

        jQuery("#ssxt-new-link").click();
    },
    scroller: function() {
        var jQuerysidebar = jQuery("#scs-scroll-sidebar"), 
            jQuerywindow = jQuery(window);

        if (jQuerysidebar.length > 0) {
            var offset = jQuerysidebar.offset(),
                topPadding = 40;

            jQuerywindow.scroll(function() {
                if (jQuerywindow.scrollTop() > offset.top) {
                    jQuerysidebar.stop().animate({
                        marginTop: jQuerywindow.scrollTop() - offset.top + topPadding
                    });
                } else {
                    jQuerysidebar.stop().animate({
                        marginTop: 0
                    });
                }
            });
        }
    }
};

jQuery(document).ready(function() {
    ssxt_admin.init();
    ssxt_admin.scroller();
});
