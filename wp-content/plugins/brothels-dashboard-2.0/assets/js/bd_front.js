jQuery(document).ready(function ($) {

    window.functions = {

        triggerEvent: function( category, action, label, category_id, label_id, position ) {
        ga('send', 'event', category, action, label);
        var form_data = {
            action: 'bd_link_action',
            el_category: category_id || 'none',
            el_action: action,
            el_label: label_id,
            position: position || 0
        };
        //Record statistics for everyone except burwood!
        if (ip_addr != '203.27.178.78') {
            $.post(ajaxurl, form_data, function (response) {
                console.log(response);
            });
        }
    },

        tacking_code: function () {

            if ( window.pageVisit ) {
                window.functions.triggerEvent(window.pageDetails.category, 'Page Visit', window.pageDetails.label, window.pageDetails.category_id, window.pageDetails.label_id, 0);
            }

            $('.bd_action_link').on('click', function (e) {
                el = $(this);
                window.functions.triggerEvent(el.data('category'), el.data('action'), el.data('label'), el.data('category_id'), el.data('label_id'), el.data('position'));
            });
            $('.empty_phone_link').on('click', function () {
                el = $(this);
                ga('send', 'event', el.data('category'), 'Phone', 'Empty Listing');
            });
            $('.empty_email_link').on('click', function () {
                el = $(this);
                ga('send', 'event', el.data('category'), 'Email', 'Empty Listing');
            });

            var mid = 660;
            var cids = get_cid_numbers();
            var refStr = escape(document.referrer);
            var dd = document, ll = dd.createElement("script"), ss = dd.getElementsByTagName("script")[0];
            ll.type = "text/javascript";
            ll.async = true;
            ll.src = ("https:" == document.location.protocol ? "https://" : "http://") + "vxml4.plavxml.com/sited/ref/phonenum.jsp?m_id=" + mid + "&cids=" + cids + "&ref=" + refStr;
            ss.parentNode.insertBefore(ll, ss);
            function makePhoneCall(cid, defNum) {
                var numDivVar = document.getElementById("numdiv_" + cid + "_0");
                if (numDivVar) {
                    var telno = numDivVar.innerHTML;
                    var tsArray = telno.split(" ");
                    telno = tsArray.join("");
                    location.href = "tel:" + telno;
                } else {
                    location.href = "tel:" + defNum;
                }
            }


            function get_cid_numbers() {

                var cids = [];

                $('.cid_number').each(function () {
                    if ($(this).val()) {
                        cids.push($(this).val());
                    }
                });
                return cids.join(',');
            }
        }
    }

    window.functions.tacking_code();
});
