jQuery(document).ready( function($) {
  $( '.bd_update_post_views' ).click(function(e) {
       e.preventDefault();
       form_data = {
             action: 'update_post_views',
             views: $( 'input[name=post_views_count]' ).val(),
             post_id: $( this ).data( 'post-id' )
       };
        $.post(ajaxurl, form_data, function(response) {
              alert( 'Views updated' );
        });
  });
  jQuery('h3 .span').on( 'click',function(e) {
       e.stopPropagation();
  });
});
             