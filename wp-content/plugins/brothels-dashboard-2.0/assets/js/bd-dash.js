  jQuery.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        var min = parseInt( jQuery('#bd_date_from').val(), 10 );
        var max = parseInt( jQuery('#bd_date_to').val(), 10 );
        var date = parseFloat( data[0] ) || 0; // use data for the age column
 
        if ( ( isNaN( min ) && isNaN( max ) ) ||
             ( isNaN( min ) && date <= max ) ||
             ( min <= date   && isNaN( max ) ) ||
             ( min <= date   && date <= max ) )
        {
            return true;
        }
        return false;
    }
)
    jQuery( document ).ready(function($) {
    $( '#bd_back_to_wordpress' ).on( 'click', function() {
         window.history.back();
    });
    $( '#bd_date_from' ).datepicker({
        maxDate: 0,
        dateFormat: "dd-mm-yy",
        onSelect: function() {
            $("#bd_date_to").prop('disabled', false);
            date_picked = 1;
        },
      onClose: function() {
          if ( date_picked == 1 ) {
              res = $( '#bd_date_from' ).val().split("-");
              var format = res[1] + '-' + res[0] + '-' + res[2];
              $( "#bd_date_to" ).datepicker({
                    minDate: new Date( format ),
                    dateFormat: "dd-mm-yy",
                    maxDate: 0,
                    minDate: $( '#bd_date_from' ).val(),
                    onClose: function() {
                         table.draw();
                    }
              });
           }
       }
  });  
 var table = $('#bd_trans_dash_table').DataTable({
    "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
        }
      });;
    $( '.bd-nav' ).on('click',function() {
        $( '.tab' ).hide();
        var tab = '#' + $( this ).data( 'tab' );
        $( tab ).show();
        $( '.nav-tab' ).removeClass( 'nav-tab-active' );   
        $( this ).addClass( 'nav-tab-active' );      
   });
  $( '#bd_set_notifications' ).on( 'click',function() {
       data = {
             'action': 'bd_update_notification_settings',
             'notification_time': $( '#bd_notification_times' ).val()
        };
    $.post(ajaxurl, data, function(response) {
          alert( 'Notification time has been updated' );
       });
    });
  $( '#bd_set_notification_email' ).on( 'click',function() {
       data = {
             'action': 'bd_email_settings',
             'email': $( '#bd_notification_email' ).val()
        };
    $.post(ajaxurl, data, function(response) {
          alert( 'Email address has been updated' );
       });
    });
      $( '#bd_ga_submit' ).on('click', function(e) {
              e.preventDefault();
               errors = 0,
               error_msg = '';
               $( '#bd_ga_error_section' ).html('');
               $( '.error' ).removeClass( 'error' );
              if ( $( 'textarea[name=bd_ga_csv]' ).val().length < 6 ){
                    errors ++;
                    $( 'textarea[name=bd_ga_csv]' ).addClass( 'error' ).focus();
                    error_msg = 'Please enter a CSV<br>' + error_msg;
              }     if ( $( 'select[name=bd_ga_city_id] option:selected' ).text() == 'Please Select' ){
                    errors ++;
                    $( 'select[name=bd_ga_city_id]' ).addClass( 'error' ).focus();
                    error_msg = 'Please choose a City<br>' + error_msg;
              }
                /*       if ( $( 'select[name=bd_ga_brothel_id] option:selected' ).text() == 'Please Select' ){
                    errors ++;
                    $( 'select[name=bd_ga_brothel_id]' ).addClass( 'error' ).focus();
                    error_msg = 'Please choose a brothel<br>' + error_msg;
              }
                      if ( $( '#ga_position:selected' ).val() == 'Please Select' ){
                    errors ++;
                    $( '#ga_position' ).addClass( 'error' ).focus();
                    error_msg = 'Please select a Position<br>' + error_msg;
              }
              if ( $( '#ga_event option:selected' ).val() == 'Please Select' ){
                    errors ++;
                    $( '#ga_event' ).addClass( 'error' ).focus();
                    error_msg = 'Please select an event type<br>' + error_msg;
              }*/
        if ( errors > 0 ) {
                   $( '#bd_ga_error_section' ).html( error_msg );
        }
      else {
              form_data = $( '#bd_import_ga_form' ).serialize();
              $( '#white_underlay' ).show();
              $( '.generic_loader' ).show(); 
              $.post(ajaxurl, form_data, function(response) {
                 //alert( response );
                alert( response );
              /*  if ( response == 1 ) {
                     alert( 'Import sucessful' );
                    // $( 'select[name=bd_ga_city_id]' ).val('Please Select');
                    // $( 'select[name=bd_ga_brothel_id]' ).val('Please Select');
                   //  $( '#ga_event' ).val('Please Select');
                   //  $( 'textarea[name=bd_ga_csv]' ).val('');
                } 
                else {
                      alert( 'Import failed.  Please check your csv for errors.' );
                }*/
                   $( '#white_underlay' ).hide();
                   $( '.generic_loader' ).hide(); 
               });
      }
      });
      
        
  });