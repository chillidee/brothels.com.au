<?php
/**
 * The Template for the "brothels in" pages
 *
 * @package WordPress
 * @subpackage Brothels Dashboard Plugin
 * @author Che Jansen- Chillidee Marketing Group
 */
?>

<?php get_header(); ?>

<div id="main" class="site-main container_12">
    <div id="primary" class="grid_8">

        <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class('single-post'); ?>>
                    <header class="entry-header">
                        <h1 class="entry-title"><?php echo get_the_title(); ?></h1>
                        <?php $share_buttons = get_post_meta(get_the_ID(), 'wpl_share_buttons_blog', true); ?>
                        <?php
                        if ($share_buttons != 'off') {
                            wplook_get_share_buttons();
                        }
                        ?>
                        <div class="clear"></div>
                    </header>
                    <div class="entry-meta">
                        <time datetime="2010-03-20" class="fleft"><?php wplook_get_date(); ?></time>
                        <?php if (wplook_custom_taxonomies_terms_links()) { ?>
                            <span class="category-selected fleft"><?php echo wplook_custom_taxonomies_terms_links(); ?></span>
        <?php } ?>

                        <span class="likes fright"><?php echo getPostLikeLink(get_the_ID()); ?></span>
                        <span class="views fright"><i class="icon-eye"></i> <?php setPostViews(get_the_ID()); ?> <?php echo getPostViews(get_the_ID()); ?></span>
                        <div class="clear"></div>
                    </div>
                    <div class ='entry-content-list'>
                        <figure>
                            <?php
                            $img_id = get_post_thumbnail_id($post->ID); // This gets just the ID of the img
                            $alt_text = get_post_meta($img_id, '_wp_attachment_image_alt', true);
                            $attr = array('alt' => trim(strip_tags($alt_text)),);
                            the_post_thumbnail('full', $attr);
                            ?>
                        </figure>
                        <div class="clear"></div>
                            <?php echo get_post_meta(get_the_id(), 'brothels_meta_box_mobile_banner', true); ?>
                        <div class ="entry-content-post">
                            <br>
                            <p><?php echo get_the_content(); ?></p>
                            <?php
                            date_default_timezone_set('Australia/Sydney');
                            $today = strtotime(date('d-m-Y'));
                            ?>
                            <?php
                            $brothels_in = get_post_meta(get_the_id(), 'brothels_in_page', true);
                            $adv = get_post_meta(get_the_id(), 'top_6', true);
                            $l = 1;
                            $days = get_option('notification_times', '3') + 1;
                            $notifications = get_option('bd_notifications', array());
                            $j = get_post_meta(get_the_id(), 'advertiser_count', true);
                            $helper = new bd_helper();
                            $result = $helper->calculate_advertisers($j, $adv, $notifications, $days);
                            $top_6 = $result['top_6'];
                            $notifications = $result['notifications'];
                            update_option('bd_notifications', $notifications);
                            update_post_meta(get_the_id(), 'top_6', $top_6);
                            $category = $content = get_post_meta(get_the_id(), 'bd_ga_category', true);
                            if ($category == '') {
                                $category = get_the_title();
                            };
                            $category = str_replace("'", "", $category);
                            $category = str_replace('&#8217;', '', $category);
                            for ($k = 1; $k < $j + 1; $k++) {
                                if (!isset($adv[$k]['current']) || count($adv[$k]['current']) < 2) {
                                    echo "
              <div class='bd_header'>
               <span class='bd_header_span'>SPACE AVAILABLE</span>
             </div>
             <div class = 'empty_spot'>
              <div class ='empty_brothel_image'></div>
              <h4>See your brothel listing here</h4>
              If your Establishment provides exceptional service and you believe that our customers will get a world class experience at YOUR venue, then enquire now to secure your place in this space!
              <div class ='clear'></div>
              <a href ='tel:1300780182' class ='empty_spot_action empty_phone_link' data-category= '" . $category . "'>CALL NOW<br>1300 780 182</a>
              <a style='padding: 19px 0;' href ='mailto:contact@brothels.com.au' class ='empty_spot_action empty_email_link' data-category= '" . $category . "'>EMAIL US</a>
            </div>
            ";
                                } else {
                                    //Get the data for each brothel
                                    $brothel_data = get_post_meta($adv[$k]['current']['brothel_id'], 'brothels', true);
                                    $name = str_replace("'", "", $brothel_data['name']);
                                    $name = str_replace('&#8217;', '', $name);
                                    //Remove this brothel from the lower content
                                    $key = array_search($adv[$k]['current']['brothel_id'], $brothels_in);
                                    unset($brothels_in[$key]);
                                    $pretty_url = str_replace('http://', '', $brothel_data['website']);
                                    $pretty_url = stripslashes(str_replace('https://', '', $pretty_url));
                                    if (substr($pretty_url, -1) == '/') {
                                        $pretty_url = substr($pretty_url, 0, -1);
                                    }
                                    if (isset($brothel_data['website_readable']) && $brothel_data['website_readable'] != '') {
                                        $pretty_url = $brothel_data['website_readable'];
                                    }
                                    global $wpdb;
                                    $media = $wpdb->get_row("SELECT * FROM $wpdb->posts WHERE guid = '{$brothel_data['image']}'");
                                    if (isset($media)) {
                                        $meta = get_post_meta($media->ID);
                                        if (array_key_exists('_wp_attachment_image_alt', $meta)) {
                                            $alt = reset($meta['_wp_attachment_image_alt']);
                                            if (!$alt)
                                                $alt = $brothel_data['name'] . ' - ' . $category;
                                        }
                                    }
                                    else {
                                        $alt = $brothel_data['name'] . ' - ' . $category;
                                    }
                                    if (!isset($alt)) {
                                        $alt = $brothel_data['name'] . ' - ' . $category;
                                    }
                                    $phone = strtolower(preg_replace("/[^0-9]+/", "", $brothel_data['phone']));
                                    if (!isset($brothel_data['ga_name']) || $brothel_data['ga_name'] == '') {
                                        $ga_name = $k . "." . $name;
                                    } else {
                                        $ga_name = $k . "." . $brothel_data['ga_name'];
                                    }
                                    if (isset($brothel_data['tracking_number'])):
                                        $tracking = $brothel_data['tracking_number'];
                                    else:
                                        $tracking = '';
                                    endif;
                                    echo "
        <div class ='bd_header'>
         <span class ='bd_header_span'>" . $k . ". " . strtoupper($brothel_data['name']) . "</span></div>";
                                    if (isset($brothel_data['website']) && $brothel_data['website'] != '') {
                                        echo "<a class ='bd_action_link' href='" . (isset($brothel_data['landing_page']) && $brothel_data['landing_page'] != '' ? $brothel_data['landing_page'] : $brothel_data['website']) . "' target='_blank' rel='nofollow' data-category_id = '" . get_the_id() . "' data-position = '" . $k . "' data-category= '" . $category . "'data-action= 'Image Link' data-label_id = '" . $adv[$k]['current']['brothel_id'] . "' data-label = '" . $ga_name . "'>";
                                    }

                                    echo "<br><img class='alignleft size-thumbnail listing_image' src='" . $brothel_data['image'] . "' alt='" . $alt . "' data-lazy-loaded='true'>
         <noscript><img class='alignleft size-thumbnail listing_image' src='" . $brothel_data['image'] . " ' alt='" . $alt . "'></noscript>";
                                    if (isset($brothel_data['website']) && $brothel_data['website'] != '') {
                                        echo "</a>";
                                    }
                                    echo $brothel_data['address'] . "<br>";
                                    if (isset($brothel_data['website']) && $brothel_data['website'] != '') {
                                        echo "  <a class ='bd_action_link' href='" . $brothel_data['website'] . "' rel='nofollow' data-position = '" . $k . "' data-category_id = '" . get_the_id() . "' data-category= '" . $category . "' data-action= 'Website Link' data-label_id = '" . $adv[$k]['current']['brothel_id'] . "'data-label = '" . $ga_name . "' target ='_blank'>" . $pretty_url . "</a><br>";
                                    }
                                    if (isset($brothel_data['phone']) && $brothel_data['phone'] != ''): {
                                            echo "<a class ='bd_action_link click_to_show brothel-phone' href ='tel:" . $phone . "' rel='nofollow' data-category_id = '" . get_the_id() . "' data-category= '" . $category . "' data-position = '" . $k . "' data-action = 'Phone' data-label_id = '" . $adv[$k]['current']['brothel_id'] . "'data-label = '" . $ga_name . "' data-number = '" . htmlentities($brothel_data['phone']) . "'id='{$tracking}_parent'>Click to show phone number</a>";
                                        }
                                        ?>
                                        <div class="featured-phone-number" id="<?php echo $brothel_data['tracking_number'] ?>_featured-phone-number">
                                        <?php if (isset($brothel_data['tracking_number']) && !empty($brothel_data['tracking_number'])): ?>
                                                <input type="hidden" class="cid_number" value="<?php echo $brothel_data['tracking_number'] ?>"></input>
                                        <?php endif ?>
                                            <a href="<?php echo (isset($brothel_data['tracking_number']) && !empty($brothel_data['tracking_number'])) ? '#' : 'tel:' . $brothel_data['phone'] ?>" data-action="Phone" data-category="<?php echo $category ?>" data-label_id="<?php echo $adv[$k]['current']['brothel_id'] ?>" data-category_id ="<?php echo get_the_id() ?>" data-position="<?php echo $k ?>" rel='nofollow' data-label="<?php echo $ga_name ?>" data-number=" <?php echo htmlentities($brothel_data['phone']) ?>" class="bd_brothel_tracking_number" style="text-decoration: none; color: black" onClick="<?php echo (isset($brothel_data['tracking_number']) && !empty($brothel_data['tracking_number'])) ? "javascript:makePhoneCall(" . $brothel_data['tracking_number'] . ", '" . $brothel_data['phone'] . "');return false;" : '' ?>"><span id="numdiv_<?php echo $brothel_data['tracking_number'] ?>_0" style="color: blue;"><?php echo $brothel_data['phone'] ?></span></a></div></p>
                                    <?php
                                    else:
                                        echo "</p>";
                                    endif;
                                    $desc_array = json_encode($brothel_data['description']);
                                    $hack = str_replace("& ", '&#038; ', $desc_array);
                                    $description = json_decode($hack, true);
                                    echo "
         <div class ='text-class'>";
                                    if (isset($description[get_the_ID()])) {
                                        echo reset($description[get_the_ID()]);
                                    } else if (isset($description[get_the_title()])) {
                                        echo reset($description[get_the_title()]);
                                    }
                                    echo "
       </div>";
                                }
                            }
                            $i = $k;
                            foreach ($brothels_in as $brothel) {
                                $brothel_data = get_post_meta($brothel, 'brothels', true);
                                $pretty_url = str_replace('http://', '', $brothel_data['website']);
                                $pretty_url = stripslashes(str_replace('https://', '', $pretty_url));
                                if (substr($pretty_url, -1) == '/') {
                                    $pretty_url = substr($pretty_url, 0, -1);
                                }
                                if (isset($brothel_data['website_readable']) && $brothel_data['website_readable'] != '') {
                                    $pretty_url = $brothel_data['website_readable'];
                                }
                                $desc_array = json_encode($brothel_data['description']);
                                $hack = str_replace("& ", '&#038; ', $desc_array);
                                $description = json_decode($hack, true);
                                echo "
       <p class ='bd_lower_listing'>
        <strong>" . $i . "." . strtoupper($brothel_data['name']) . "</strong><br>
        " . $brothel_data['address'] . "<br>";
                                if (isset($brothel_data['phone'])):
                                    echo $brothel_data['phone'] . "<br>";
                                endif;
                                if (isset($brothel_data['website']) && $brothel_data['website'] != '') {
                                    echo $pretty_url . '<br>';
                                }
                                if (isset($description[get_the_ID()])) {
                                    echo reset($description[get_the_ID()]);
                                } else if (isset($description[get_the_title()])) {
                                    echo reset($description[get_the_title()]);
                                }
                                echo "
     </p>";
                                $i++;
                            }
                            $bottom_content = get_post_meta($post->ID, 'bd_bottom_content', true);
                            if ($bottom_content) {
                                echo "<p class = 'bd_lower_listing'>" . $bottom_content . "</p>";
                            }
                            ?>
                <?php wp_link_pages(array('before' => '<div class="clear"></div><div class="page-link"><span>' . __('Pages:', 'wplook') . '</span>', 'after' => '</div>')); ?>
        <?php if (get_the_tag_list('', ', ')) { ?>
                                <div class="tag-i">
                                    <i class="icon-tags"></i> <a href="#" rel="tag"><?php echo get_the_tag_list('', ', ', ''); ?></a>
                                </div>
        <?php } ?>
        <?php wplook_prev_next(); ?>
                        </div>
                </article>
        <?php comments_template('', true); ?>
                <div class="clear"></div>
        <?php
    endwhile;
endif;
?>
    </div>
    <div id="secondary" class="grid_4 widget-area" role="complementary">
<?php dynamic_sidebar('in_city_sidebar'); ?>
    </div><!-- .grid_4 -->
    <div class="clear"></div>
</div>
<script type="text/javascript">
    var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>',
            ip_addr = '<?php echo $_SERVER['HTTP_X_FORWARDED_FOR']; ?>';

</script>

<?php get_footer(); ?>
