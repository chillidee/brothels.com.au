<?php
/**
 * The Template for the "brothels in" pages
 *
 * @package WordPress
 * @subpackage Brothels Dashboard Plugin
 * @author Che Jansen- Chillidee Marketing Group
 */
?>

<?php
get_header();
$brothels = get_post_meta($post->ID, 'brothels_in_page', true);
$banner_add = get_field('bottom_banner_ad');
?>

<style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1490665515515{background-image: url(<?php echo get_field('behind_slider_image') ?>) !important;background-position: 0 0 !important;background-repeat: no-repeat !important;}.vc_custom_1490595127697{padding-top: -30px !important;}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
<div class="container page-container">
    <div class="post-formatting">
        <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row row vc_custom_1490665515515 vc_row-has-fill vc_row-o-full-height vc_row-o-columns-middle vc_row-flex">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="vc_empty_space"  style="height: 30px" >
                            <span class="vc_empty_space_inner"></span>
                        </div>
                        <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                                <h1 style="text-align: center; color: #fff; font-size: 50px;"><?php echo $post->post_title ?></h1>
                            </div>
                        </div>
                        <div class="vc_empty_space"  style="height: 30px" >
                            <span class="vc_empty_space_inner"></span></div>
                        <div class="wpb_revslider_element wpb_content_element">
                            <?php putRevSlider(get_field('slider_alias')) ?>
                        </div>
                        <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                                <p><?php echo $post->post_content; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix">
        </div>
        <div class="vc_row row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="woocommerce columns-2">
                            <div class="products-container col-sm-12">
                                <div class="row">
                                    brothels
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row row vc_custom_1490595127697">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <style>
                            #el_149216312769078 .font-color { color: #ffffff; }
                            #el_149216312769078 .bg-color { background-color: #ffffff }
                            #el_149216312769078 .border-color { border-color: #ffffff }
                            #el_149216312769078 .border-color-top { border-top-color: #ffffff }
                            #el_149216312769078 .border-color-bottom { border-bottom-color: #ffffff }
                            #el_149216312769078 .border-color-left { border-left-color: #ffffff }
                            #el_149216312769078 .border-color-right { border-right-color: #ffffff }
                        </style>
                        <div class="lab_wpb_image_banner wpb_content_element banner-type-double-bordered-title text-position-bottom-right" id="el_149216312769078">
                            <a href="http://staging.brothels.com.au/" target="">
                                <img width="1024" height="192" src="<?php echo $banner_add['url'] ?>" class="banner-img attachment-large" alt="" srcset="<?php echo $banner_add['url'] ?> 1024w, <?php echo $banner_add['sizes']['medium'] ?> 300w, <?php echo $banner_add['sizes']['medium_large'] ?> 768w, <?php echo $banner_add['url'] ?> 1600w" sizes="(max-width: 1024px) 100vw, 1024px" />
                                <span class="ol" style="background-color: rgba(0,0,0,0.01);"></span>
                            </a>
                        </div>
                        <div class="vc_empty_space"  style="height: 25px" ><span class="vc_empty_space_inner"></span></div>
                    </div>
                </div>
            </div>
        </div>
        <div data-vc-full-width="true" data-vc-full-width-init="true" data-vc-parallax="1.5" data-vc-parallax-image="<?php the_field('seo_background_image'); ?>" class="vc_row row vc_row-has-fill vc_row-o-content-middle vc_row-flex vc_general vc_parallax vc_parallax-content-moving" style="position: relative; left: -48px; box-sizing: border-box; width: 1266px; padding-left: 48px; padding-right: 48px;">
            <div class="wpb_column vc_column_container vc_col-sm-2">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                    </div>
                </div>
            </div>
            <div class="wpb_column vc_column_container vc_col-sm-8">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                                <div style="padding: 25px 30px; border: solid 0.5px rgba(255, 255, 255, 0.19);">
                                    <h2 style="text-align: center;"><?php echo get_post_meta($post->ID, '_yoast_wpseo_title', true); ?></h2>
                                    <hr />
                                    <div style="height: 180px; width: 100%; border: 0px solid #ccc; overflow: auto;">
                                        <p style="text-align: center;"><?php echo get_post_meta($post->ID, '_yoast_wpseo_metadesc', true); ?></p>
                                    </div>
                                    <p>&nbsp;</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wpb_column vc_column_container vc_col-sm-2">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                    </div>
                </div>
            </div>
            <div class="vc_parallax-inner skrollable skrollable-between" data-bottom-top="top: -50%;" data-top-bottom="top: 0%;" style="height: 150%; background-image: url('<?php the_field('seo_background_image'); ?>')"></div>
        </div>
        <div class="vc_row-full-width vc_clearfix">
        </div>
    </div>
</div>
<script type='text/javascript' src='http://staging.brothels.com.au/wp-content/themes/aurum/assets/js/bootstrap.min.js'></script>
<script type='text/javascript' src='http://staging.brothels.com.au/wp-content/themes/aurum/assets/js/TweenMax.min.js'></script>
<script type='text/javascript' src='http://staging.brothels.com.au/wp-content/themes/aurum/assets/js/min/joinable.min.js'></script>
<script type='text/javascript' src='http://staging.brothels.com.au/wp-includes/js/wp-embed.min.js'></script>
<script type='text/javascript' src='http://staging.brothels.com.au/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js'></script>
<script type='text/javascript' src='http://staging.brothels.com.au/wp-content/plugins/js_composer/assets/lib/bower/skrollr/dist/skrollr.min.js'></script>
<script type='text/javascript' src='http://staging.brothels.com.au/wp-content/themes/aurum/assets/js/aurum-custom.js'></script>
<?php get_footer(); ?>
