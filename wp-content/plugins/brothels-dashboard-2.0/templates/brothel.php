
<?php
/*
 * Template for individual brothel page
 */
get_header();
wp_enqueue_script('bd_front', BD_PLUGIN_URL . '/assets/js/bd_front.js');
wp_enqueue_style('bd_front_css', BD_PLUGIN_URL . '/assets/css/bd-city.css');
$brothel_data = get_post_meta($post->ID, 'brothels', true);
$banner_image = get_field('brothel_page_banner_image');
$comments = get_comments(
        array(
            'status' => 'approve',
            'post_id' => $post->ID
        )
);
$features = get_field('services_checkboxes');
$realated_brothels = get_related_brothels(array('id' => array_values($brothel_data['in_page'])[0], 'this_id' => $post->ID));
$paid_customer = get_field('paid_customer');
$opening_times = get_field('opening_times');
?>
<input type="hidden" value="<?php echo $post->ID; ?>" name="post_id"/>
<div class="container-fluid">
    <div class="row">
        <div class="brothel-banner-image">
            <img id="brothel-page-banner-image" src="<?php echo $banner_image['url'] ? $banner_image['url'] : BD_PLUGIN_URL . 'assets/images/1800x1.png' ?>" alt="<?php echo $banner_image['alt'] ? $banner_image['alt'] : $brothel_data['name'] ?>"/>
            <h1 class="container"><?php
                echo $brothel_data['name'];
                if ($paid_customer):
                    echo "<span>Verified <img src='" . BD_PLUGIN_URL . "/assets/css/images/brothel_page/VerifiedShield.png' alt='verified' style='min-height:0'/></span>";
                endif;
                ?>
            </h1>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
            <div class="brothel-description">
                <h3>Description</h3>
                <p><?php the_field('city_page_long_description') ?></p>
                <?php if (count($features) > 0 && !empty($features)): ?>
                    <h3>Features</h3>
                    <div class="brothel-features-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?php foreach ($features as $feature): ?>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                <div class="brothel-feature-container">
                                    <img src="<?php echo BD_PLUGIN_URL ?>/assets/css/images/brothel_page/<?php echo $feature ?>.png" alt="brothel-feature-image"/>
                                    <p><?php echo get_brothel_feature_text($feature); ?></p>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
                <h3>Location</h3>
                <div class="brothel-location-container">
                    <iframe src='https://www.google.com/maps/embed/v1/place?key=<?php echo GOOGLE_MAPS_KEY ?>&q=<?php echo str_replace(" ", "+", $brothel_data['address']) ?>'></iframe>
                </div>
                <?php
                if (count($comments) > 0):
                    $star_rating = 0;
                    foreach ($comments as $comment):
                        $_star_rating = (int) get_post_meta($comment->comment_ID, 'star_rating', true);
                        if (!$_star_rating):
                            $_star_rating = 5;
                        endif;
                        $star_rating += $_star_rating;
                    endforeach;
                    $star_rating = (int) round($star_rating / count($comments));
                    ?>
                    <h3>Brothel Reviews (<?php echo count($comments) ?>)</h3>
                    <div class="stars total-stars">
                        <fieldset class="rating">
                            <input type="radio" <?php echo ($star_rating == 5) ? ' checked ' : '' ?> value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
                            <input type="radio" <?php echo ($star_rating == 4) ? ' checked ' : '' ?> value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                            <input type="radio" <?php echo ($star_rating == 3) ? ' checked ' : '' ?> value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
                            <input type="radio" <?php echo ($star_rating == 2) ? ' checked ' : '' ?> value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                            <input type="radio" <?php echo ($star_rating == 1) ? ' checked ' : '' ?> value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                        </fieldset>
                    </div>
                    <div class="brothel-reviews-container">
                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                        <span class="sr-only">Loading...</span>
                    </div>
                <?php endif; ?>
                <h3>Write a review</h3>
                <div class="brothel-write-review-container">
                    <?php
                    if (is_user_logged_in()):
                        ?>
                        <form id="brothel-review-form">
                            <p>Your Rating</p>
                            <div class="stars">
                                <fieldset class="rating">
                                    <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
                                    <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                    <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                    <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                    <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                                </fieldset>
                                <p class="review-star-error error hidden">Please select a star rating</p>
                            </div>
                            <p>Your Review</p>
                            <textarea id="review-text" name="review-text"></textarea>
                            <p class="hidden review_text_error error">Please fill out a review.</p>
                            <button class="btn" type="button" name="submit-review">SUBMIT</button>
                        </form>
                    <?php else: ?>
                        <p>You must be logged in to write a review, login or sign up <a href="/my-account">here</a>.</p>
                    <?php endif; ?>
                </div>
                <?php if (!$paid_customer): ?>
                    <h3>You may also like</h3>
                    <?php
                    foreach ($realated_brothels as $index => $realated_brothel):
                        echo $realated_brothel;
                        if ($index == 2):
                            break;
                        endif;
                    endforeach;
                endif;
                ?>
            </div>
        </div>
        <div class="col-lg-4">
            <!--            <div class="brothel-mobile-details">
                            <i class="fa fa-info" aria-hidden="true"></i>
                        </div>-->
            <div class="brothel-service-container mobile-hidden">
                <div class="brothel-service-address">
                    <img class="icon" src="<?php echo BD_PLUGIN_URL ?>/assets/css/images/brothel_page/Location.png" alt="brothel-service-address"/>
                    <p><?php echo $brothel_data['address'] ?></p>
                </div>
                <?php if (!empty($brothel_data['website'])): ?>
                    <div class="brothel-service-web">
                        <img class="icon" src="<?php echo BD_PLUGIN_URL ?>/assets/css/images/brothel_page/Computer.png" alt="brothel-service-web"/>
                        <p><a class='bd_action_link city-brothel-website' rel='nofollow' data-label = '<?php echo get_the_title(); ?>' data-action='Website Link' data-label_id='<?php echo get_the_id() ?>' data-category_id='-1' data-category = '<?php echo get_the_title(); ?> Detail Page' href='<?php echo $brothel_data['website'] ?>' target='_blank'><?php echo $brothel_data['website_readable'] ?></a></p>
                    </div>
                <?php endif; ?>
                <div class="brothel-service-phone">
                    <img class="icon" src="<?php echo BD_PLUGIN_URL ?>/assets/css/images/brothel_page/Mobile.png" alt="brothel-service-phone"/>
                    <?php
                    if (isset($brothel_data['phone']) && $brothel_data['phone'] != ''):
                        ?>
                        <span class="featured-phone-number" id="<?php echo get_the_id() ?>_featured-phone-number">
                            <?php if (isset($brothel_data['tracking_number']) && !empty($brothel_data['tracking_number'])): ?>
                                <input class="cid_number" value="<?php echo $brothel_data['tracking_number'] ?>"></input>
                            <?php endif ?>
                            <a href="<?php echo (isset($brothel_data['tracking_number']) && !empty($brothel_data['tracking_number'])) ? '#' : 'tel:' . str_replace(" ", "", $brothel_data['phone']) ?>" rel='nofollow' data-number=" <?php echo htmlentities($brothel_data['phone']) ?>" class="bd_brothel_tracking_number" style="text-decoration: none; color: black" onClick="<?php echo (isset($brothel_data['tracking_number']) && !empty($brothel_data['tracking_number'])) ? "javascript:makePhoneCall(" . $brothel_data['tracking_number'] . ", '" . $brothel_data['phone'] . "');return false;" : '' ?>"><span id="numdiv_<?php echo $brothel_data['tracking_number'] ?>_0" style=""><?php echo $brothel_data['phone'] ?></span></a></span></p>
                        <?php
                    else:
                        echo "</p>";
                    endif;
                    ?>
                </div>
                <?php if (count($opening_times) > 0): ?>
                    <div class="brothel-service-hours">
                        <h3>Opening Hours</h3>
                        <table>
                            <tbody>
                                <?php
                                foreach ($opening_times as $day):
                                    if (!empty($day['day'])):
                                        ?>
                                        <tr>
                                            <td><?php echo $day['day'] ?></td>
                                            <td><?php echo $day['am'] ?></td>
                                            <td>&dash;</td>
                                            <td><?php echo $day['pm'] ?></td>
                                        </tr>
                                        <?php
                                    endif;
                                endforeach;
                                ?>
                            </tbody>
                        </table>
                    </div>
                <?php endif; ?>
                <div class="brothel-service-social">
                    <h3>Share with your friends</h3>
                    <div>
                        <a style="color:#3b5998;" target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo get_site_url() ?>"><i class="fa fa-facebook" aria-hidden="true"></i> facebook</a>
                    </div>
                    <div>
                        <a style="color:#4099FF" target="_blank" href="https://twitter.com/share?url=<?php echo get_site_url() ?>&amp;text=Brothels.com.au&amp;hashtags=brothels.com.au"><i class="fa fa-twitter" aria-hidden="true"></i> twitter</a>
                    </div>
                    <div>
                        <a style="color: #DD4B39" target="_blank" href="https://www.instagram.com/brothelsau/"><i class="fa fa-instagram" aria-hidden="true"></i> Instagram</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type='text/javascript'> var ajaxurl = '<?php echo admin_url('admin-ajax.php') ?>', ip_addr = '<?php echo $_SERVER['HTTP_X_FORWARDED_FOR'] ?>', user_agent = '<?php echo $_SERVER['HTTP_USER_AGENT'] ?>', user_id = '<?php echo get_current_user_id(); ?>';
    window.pageVisit = true;
    window.pageDetails = {
        category_id: '-1',
        label_id: '<?php echo get_the_ID(); ?>',
        category: '<?php echo get_the_title(array_values($brothel_data['in_page'])[0]); ?>',
        label: '<?php echo $brothel_data['name']; ?>'
    }
</script>
<?php
get_footer();
?>