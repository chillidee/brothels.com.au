Brothels Dashboard- Created for Chillidee by Che' Jansen
---------------------------------------------------------------------------
-- Any questions please direct to che@chillidee.com.au or che@imaginatewebsites.com.au

Change log
---------------------------------------------------------------------------
1.1- Fixed bug in javascript on City page, also css issues in notifications
     Removed advanced fields in brothels

1.2- Changed loader svg to a gif, as svgs was being blocked by nginx
     Put advanced fields back into brothels, but hid them under a advanced button

1.3- Removed wordpress link for girls, hid the Sitemap meta box on city pages
     Rewrote the ga import function, to allow a whole city to be imported at once

1.4- Fixed bug in city page analytics that was stopping the bottom modifiers from working
     Fixed bug in helper function preventing brothels from dropping when expired
     Fixed bug in alt tags in city template

1.41- Fixed bug in city page analtyics, due to mis-named variable
      Removed brothel address as a required field in the brothe page- just commented just in case

1.42- Hide statistics that have 0 events
      Add date range to city statistics overview

1.43- Fixed bug in statistics caluculation for ads charge

1.44- Added Hide Brothel Name feature
      Changed text in empty position advertisement

1.45  Removed validation from transaction number in confirm brothels spot- location page

1.46  Removed validation from brothels

1.47  Added Eftpos to payment options

1.47  Added the ability to have line breaks and html in notes

1.50  Added Performance reports to city pages

1.51  Added SEO Hack for Yoast Premium

1.52 Added field to brothel details in advertising allowing for a tracking number to be added.
   Added to bd_front.js to show tracking number instead of real number if one exsits and delacon
   js to get the tracking phone number.
     Added to cites.php the a tag required by delacon for their js

1.53 removed tacking number for non advertising brothels

1.54 added dashboard tracking for delcon a tags, changed css on backend and id of page not title

1.65 added PHPExcel plugin for formated csv files

2.0 Updates for new theme: