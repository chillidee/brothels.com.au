<?php

/**
 * Class bd_brothel_cpt
 * This class creates our brothels custom post type, including all the meta boxes
 * It also redirects all traffic to the single page back to the home page, and issues a 301
 * Contains public static functions that can be called by actions and filters
 * @ Author- Che Jansen- Chillidee Marketing Group
 * Version 1.1.0
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly
//Remove the quick edit link from the city post type

function remove_quick_edit($actions) {
    global $post;
    if ($post->post_type == 'brothel') {
        unset($actions['inline hide-if-no-js']);
    }
    return $actions;
}

if (is_admin()) {
    add_filter('post_row_actions', 'remove_quick_edit', 10, 2);
}

class bd_brothel_cpt {

    //Create custom post type for our cities

    public static function create_brothels() {
        register_post_type('brothel', array(
            'labels' => array(
                'name' => 'Brothels',
                'singular_name' => 'Brothe',
                'add_new' => 'Add New Brothel',
                'add_new_item' => 'Add New Brothel',
                'edit' => 'Edit',
                'edit_item' => 'Edit Brothel',
                'new_item' => 'New Brothel',
                'view' => 'View',
                'view_item' => 'View Brothel',
                'search_items' => 'Search Brothels',
                'not_found' => 'No Brothel Found',
                'not_found_in_trash' => 'No Brothels found in Trash',
                'parent' => 'Parent Brothel'
            ),
            'public' => true,
            'menu_position' => 14.436,
            'supports' => array(
                'title'
            ),
            'taxonomies' => array(
                ''
            ),
            'menu_icon' => BD_PLUGIN_URL . 'assets/images/brothels.png',
            'has_archive' => true,
            'rewrite' => array(
                'slug' => '',
                'with_front' => false
            )
        ));
    }

    public static function prevent_post_change($data, $postarr) {
        if (!isset($postarr['ID']) || !$postarr['ID'])
            return $data;
        if ($postarr['post_type'] != 'brothel')
            return $data; // only for brothels
        $old = get_post($postarr['ID']); // the post before update
        if ($data['post_status'] == 'draft') {
            // force a post to be setted as incomplete before be published
            $data['post_status'] == 'publish';
        }
        return $data;
    }

    /**
     * Function remove_brothels_single_page
     * Redirects back to the home page, and issues a 301 if a single brothel is accessed
     */
    public static function remove_brothels_single_page() {
        $queried_post_type = get_query_var('brothels');

        // if (is_single() && 'sample_post_type' == $queried_post_type) {
        global $post;
        if (isset($post) && is_single() && $post->post_type == 'brothel') {
            load_template(BD_PLUGIN_PATH . '/templates/brothel.php');
//            wp_redirect(home_url(), 301);
//            $queried_post_type->wp_reset_postdata();
//            exit;
        }
    }

    /**
     * Function brothels_meta_1
     * Creates the meta boxes for the brothels custom post type
     */
    public static function brothels_meta_1() {
        add_meta_box('brothels_meta_main', 'Brothel details', array(
            'bd_brothel_cpt',
            'output_brothels_meta_main'
                ), 'brothel', 'normal', 'high');
        add_meta_box('brothels_contact', 'Brothel contact details', array(
            'bd_brothel_cpt',
            'output_brothels_meta_contact'
                ), 'brothel', 'normal', 'high');
        add_meta_box('brothels_meta_notes', 'Notes', array(
            'bd_brothel_cpt',
            'output_brothels_meta_notes'
                ), 'brothel', 'normal', 'high');
        add_meta_box('brothels_statistics', 'Statistics', array(
            'bd_brothel_cpt',
            'output_brothels_meta_statistics'
                ), 'brothel', 'side', 'high');
    }

    /**
     * Output for the main meta box on the brothels custom post type
     */
    public static function output_brothels_meta_main() {
        global $post;
        $brothel_data = get_post_meta($post->ID, 'brothels', true);
        if (!$brothel_data) {
            $brothel_data = array(
                'name' => '',
                'phone' => '',
                'tracking_number' => '',
                'address' => '',
                'description' => '',
                'image' => '',
                'image_150' => '',
                'website' => '',
                'website_readable' => '',
                'ga_name' => '',
                'extra_ga_name' => '',
                'extra_ga_name2' => '',
                'landing_page' => '',
                'long' => '',
                'lat' => ''
            );
        }
        $hidden = '';
        $brothel_long = get_post_meta($post->ID, 'brothel_long', true);
        $brothel_lat = get_post_meta($post->ID, 'brothel_lat', true);
        if (!isset($brothel_data['image']) || $brothel_data['image'] != '') {
            $hidden = " class ='hidden'";
        }
        $stats = get_post_meta($post->ID, 'bd_stat_settings', true);

        echo "<div id ='error_holder'></div>
      <input type ='hidden' name ='security' value ='" . wp_create_nonce('bd_add_brothel_nonce') . "'>
      <input type ='hidden' name ='bd_add_action' value ='bd_add_brothel'>
      <input type ='hidden' name = 'bd_post_id' value ='" . $post->ID . "'>
      <input type='hidden' name='brothel_lat' id='brothel_lat' value='" . $brothel_lat . "'/>
      <input type='hidden' name='brothel_long' id='brothel_long' value='" . $brothel_long . "'/>
       ";
        if (!isset($brothel_long) || !isset($brothel_lat)):
            echo "<p id='error_no_cord' style='background-color: red;padding: 10px;text-align: center;color: white;font-size: 20px;'>Brothel cordinates not set please set address</p>";
        endif;
        echo "<input type ='hidden' name ='in_page' value = '";
        if (isset($brothel_data['in_page'])) {
            echo json_encode($brothel_data['in_page']);
        }
        echo "'>
      <input type ='hidden' name ='bd_name'value = '" . get_the_title() . "'>
      <table class ='bd_table'>";
        echo "<tr>
         <td><label class ='bd_label'>Phone Number</label></td>
         <td><input placeholder = 'What is the brothels phone number?' type ='text' name ='bd_phone' value = '" . $brothel_data['phone'] . "'></td>
       </tr>
       <tr>
         <td><label class ='bd_label'>Tracking Number</label></td>
         <td><input placeholder = 'What is the brothels tracking number?' type ='text' name ='bd_track_phone' value = '" . $brothel_data['tracking_number'] . "'></td>
       </tr>
       <tr>
         <td><label class ='bd_label'>Address</label></td>
         <td><input id='address' placeholder = 'What is the brothels address?' type ='text' name ='bd_address' value = '" . $brothel_data['address'] . "'></td>
       </tr>
       <tr>
         <td><label class ='bd_label'>Map</label></td>
         <td>";
        echo (isset($brothel_long) && isset($brothel_lat)) ? "<iframe id='brothel_map' src='https://www.google.com/maps/embed/v1/place?key=" . GOOGLE_MAPS_KEY . "&q=" . str_replace(" ", "+", $brothel_data['address']) . "'></iframe>" : "<iframe id='brothel_map'></iframe>";
        echo "</td>
       </tr>
       <tr>
         <td><label class ='bd_label'>Website</label></td>
         <td><input type ='text' name ='bd_website' value = '" . $brothel_data['website'] . "' placeholder ='Go to brothel website, copy url, and paste here'>
         </td>
       </tr>
       <tr><td><span id = 'show_advanced'>Show advanced settings</span></td></tr>
       <tr class ='advanced'>
         <td><label class ='bd_label'>Website Readable</label></td>
         <td><input type ='text' name ='bd_website_readable' value = '" . $brothel_data['website_readable'] . "' placeholder ='Website to display to user'>
         </td>
       </tr>
       <tr class ='advanced'>
         <td><label class ='bd_label'>Landing Page</label></td>
         <td><input type ='text' name ='bd_landing_page' value = '" . $brothel_data['landing_page'] . "' placeholder ='Please ask supervisor how to create'>
         </td>
       </tr>
       <tr class ='advanced'>
         <td><label class ='bd_label'>GA Name</label></td>
         <td><input type ='text' name ='ga_name' value = '" . $brothel_data['ga_name'] . "' placeholder ='Enter the label for Google Analytics'>
         </td>
       </tr>
       <tr class ='advanced'>
         <td><label class ='bd_label'>Extra GA Name</label></td>
         <td><input type ='text' name ='extra_ga_name' value = '" . $brothel_data['extra_ga_name'] . "' placeholder ='Extra GA names for capturing'>
         </td>
       </tr>
       <tr class ='advanced'>
         <td><label class ='bd_label'>Extra GA Name 2</label></td>
         <td><input type ='text' name ='extra_ga_name2' value = '" . $brothel_data['extra_ga_name2'] . "' placeholder ='Extra GA names for capturing'>
         </td>
       </tr>
       <tr>
         <td><label class ='bd_label'>Descriptions</label>
          <button class = 'bd_button' id ='bd_add_description'>Add Description</button></td>";
        if (isset($brothel_data['description']) && is_array($brothel_data['description'])) {
            echo '<td>';
            foreach ($brothel_data['description'] as $status => $array) {
                foreach ($array as $key => $description) {
                    $city_name = ( get_the_title($status) ? get_the_title($status) : $status );

                    echo "<label class = 'brothel_description_name'>" . ucfirst($city_name) . "</label><br>";
                    if ($status == 'unassigned') {
                        echo "<div class = 'bd_remove_description' data-id ='" . $key . "'></div>";
                    }
                    echo "
                <textarea placeholder = 'Description is the text that will be displayed within the brothels ad' class ='bd_description_input' name ='bd_description[\"" . $status . "\"]'>" . $description . "</textarea><br>
                ";
                }
            }
            echo '</td>';
        } else {
            echo "
            <td><textarea placeholder = 'Description is the text that will be displayed within the brothels ad' class ='bd_description_input'name ='bd_description[\"unassigned\"]'></textarea><br>
            </td>";
        }
        echo "
        </tr>
        <tr>
            <td>
               <label class ='bd_label'>Listing Page Image</label>
            </td>
            <td>";
        if (!isset($brothel_data['image']) || $brothel_data['image'] == '') {
            $brothel_data['image'] = '';
            echo "<button class ='bd_button' " . $hidden . "id ='upload_new_brothel_image'>Upload Image</button>";
        }
        echo "
            <input type ='hidden' name = 'bd_image' id ='bd_image' value ='" . $brothel_data['image'] . "'>
            <div id ='bd_image_holder'>
             <img src = '" . $brothel_data['image'] . "' id ='bd_image_display'>
           </div>
           <button class ='bd_button hide_this' " . $hidden . "id ='upload_new_brothel_image'>Upload Image</button>
            </td>
        </tr>
        <tr>
         <td><label class ='bd_label'>Image<br>
          150px x 150px</label></td>
          <td>";
        if (!isset($brothel_data['image_150']) || $brothel_data['image_150'] == '') {
            $brothel_data['image_150'] = '';
            echo "<button class ='bd_button' " . $hidden . "id ='upload_new_brothel_image'>Upload Image</button>";
        }
        echo "
            <input type ='hidden' name = 'bd_image_150' id ='bd_image_150' value ='" . $brothel_data['image_150'] . "'>
            <div id ='bd_image_holder'>
             <img src = '" . $brothel_data['image_150'] . "' id ='bd_image_display'>
           </div>
           <button class ='bd_button hide_this' " . $hidden . "id ='upload_new_brothel_image'>Upload Image</button></td>
         </tr>
       </table>
       <span id ='old_versions'>See old versions</span>
       <style>a#bd_brothel_link {
         display:inline-block!important;
       }</style>";
        echo "<script>function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('address')),
            {types: ['geocode']});
        autocomplete.addListener('place_changed', fillInAddress);
      }
      function fillInAddress(){
            var details = autocomplete.getPlace();
            document.getElementById('brothel_lat').value = details.geometry.location.lat();
            document.getElementById('brothel_long').value = details.geometry.location.lng();
            document.getElementById('address').value = details.formatted_address;
            document.getElementById('brothel_map').src = 'https://www.google.com/maps/embed/v1/place?key=" . GOOGLE_MAPS_KEY . "&q=place_id:' + details.place_id;
            var error_message = document.getElementById('error_no_cord');
            error_message.parentNode.removeChild(error_message);
        }
        </script>";

        echo "<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=" . GOOGLE_MAPS_KEY . "&libraries=places&callback=initAutocomplete'></script>";
    }

    /**
     * Function output_brothels_meta_statistics
     * Outputs the meta box to hold our brothels statistics
     * To be populated with Ajax on page load
     */
    public static function output_brothels_meta_statistics() {
        global $post;
        $stats_data = get_post_meta($post->ID, 'bd_stat_settings', true);

        echo "<h2>Statistics</h2>
        <label class = 'bd_statistics_label'>Select dates</label>
        <input class = 'date_picker' id = 'from_datepicker' placeholder = 'From' name = 'bd_statistics_from'>
        <input class = 'date_picker' id = 'to_datepicker' placeholder = 'To' name = 'bd_statistics_to'>
        <div id = 'bd_set_dates'></div>
        <span id = 'statistics_date_range'>Showing Events from the last 7 days.</span>";
        $in_page = get_post_meta($post->ID, 'brothels', true);
        if (isset($in_page['description'])) {
            if (array_key_exists('unassigned', $in_page['description'])) {
                unset($in_page['description']['unassigned']);
            }
            if (intval(count($in_page['description'] > 1))) {
                echo "<select name = 'bd_stats_city_select'>
        <option value = 'all'>All pages</option>";
                foreach ($in_page['description'] as $city => $description) {
                    echo "<option value = '" . $city . "'>" . $city . "</option>";
                }
                echo "</select>";
            } else if (count($in_page['description'] > 0)) {
                $descript = reset($in_page['description']);
                echo "<div id = 'bd_in_city'>Showing statistics for " . key($in_page['description']) . "</div>";
            }
        }
        echo "
        <div id = 'statistics_holder'><img src = '" . BD_PLUGIN_URL . 'assets/images/reload.gif' . "' class = 'loader'></div>
        <div id = 'bd_stats_bottom_section'>
        <label class = 'bd_statistics_label bottom_label'>Unit Revenue</label><label class = 'dollar'>$</label><input id = 'bd_stats_unit-rev' class = 'bottom_input' name = 'bd_unit_revenue' value = '";
        if (isset($stats_data['unit_revenue'])) {
            echo $stats_data['unit_revenue'];
        } else {
            echo '340';
        }
        echo "'><br>
        <label class = 'bd_statistics_label bottom_label'>Conversion%</label><label class = 'dollar'>%</label><input id = 'bd_stats_conv-perc' class = 'bottom_input' name = 'bd_conv_percent' value = '";
        if (isset($stats_data['conv_percent'])) {
            echo $stats_data['conv_percent'];
        } else {
            echo '10';
        }
        echo "'><br>
        <label class = 'bd_statistics_label bottom_label'>Ads Charge</label><label class = 'dollar'>%</label><input id = 'bd_stats_ads-charge' class = 'bottom_input' name = 'bd_ads_charge' value = '";
        if (isset($stats_data['ads_charge'])) {
            echo $stats_data['ads_charge'];
        } else {
            echo '10';
        }
        echo "'>
        <div id = 'bd_set_stats_button'></div>
        </div>
        <script>
        var loader = \"<img src = '" . BD_PLUGIN_URL . "assets/images/reload.gif'  class = 'in-block-loader'></div>\";
      </script>";
        //$brothel_data = get_post_meta($post->ID, 'brothels', true);
        //$tracking_code = $brothel_data['tracking_number'];
        // $to_date = date("Y-m-d");
        //$from_date = strtotime("-7 day");
        //$from_date = date('Y-m-d', $from_date);
        // $url = "https://pla.delaconcorp.com/site/report/report.jsp?reportoption=xml&datefrom=" . $from_date . "&dateto=" . $to_date . "&cids=" . $tracking_code;
        //if(isset($tracking_code)):
        // $ch = curl_init();
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        // "Auth: 660_8w2PZ6VhpWExPlozKg5faV1nT9gCbegGe6/IKUkyc1BmoQJTcfoJkSyizOn0/Ps7"
        //  ));
        //curl_setopt($ch, CURLOPT_URL,$url);
        //curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //$server_output = curl_exec ($ch);
        //curl_close ($ch);
        //$xmlArray = new SimpleXMLElement($server_output);
    }

    /**
     * Function output_brothels_meta_contact
     * Outputs the meta box to hold our brothels contact details
     */
    public static function output_brothels_meta_contact() {
        global $post;
        global $wpdb;
        $contact_dump = $wpdb->get_results($wpdb->prepare("SELECT * FROM $wpdb->postmeta WHERE post_id = %d AND meta_key = %s", $post->ID, 'contact'), ARRAY_A);
        echo "<div class ='clear_helper'>";
        foreach ($contact_dump as $contact) {
            echo "<div class = 'contact_holder'>";
            $contact_array_parent = unserialize($contact['meta_value']);
            $i = 0;
            foreach ($contact_array_parent as $contact_array) {
                if ($i > 0) {
                    echo "<div class ='prev_contact_edits'>";
                }
                $j = 0;
                foreach ($contact_array as $key => $value) {
                    // strip out all whitespace and remove whitespace from key for a class
                    $class = preg_replace('/\s*/', '', $key);
                    $class = $contact['meta_id'] . '_' . strtolower($class);
                    //If this the first cycle, it's Name, so don't print it.  We'll use the contact's name as a header
                    if ($j != 0) {
                        echo "<div class = 'contact_header'>" . $key . " </div>";
                    }
                    $j++;
                    echo "<div class ='contact_value " . $class . "'>" . $value . "</div><br>";
                }
                echo "<input type ='hidden' value = '" . json_encode(unserialize(($contact['meta_value']))) . "' name = 'contact_array'>
          <button class ='edit_contact' data-id = '" . $contact['meta_id'] . "'></button>";
                $counted = count($contact_array_parent) - 1;
                if ($counted > 0) {
                    echo "<div class = 'bd_prev_contact_versions'></div>";
                }
                if ($i > 0) {
                    echo "</div>";
                }
                $i++;
            }
            echo "</div>";
        }
        echo "</div><button id ='add_contact'>";
    }

    /**
     * Function output_brothels_meta_notes
     * Outputs the meta box to hold our brothels Notes
     */
    public static function output_brothels_meta_notes() {
        global $post;
        $notes = array();
        $notes = get_post_meta($post->ID, 'notes');
        foreach ($notes as $note) {
            echo "<div class = 'note_header'>
    <span>Date</span>" . $note['date'] . "
    <span>Author</span>" . $note['author'] . "
  </div>
  <div class ='note_body'>" . $note['note'] . "</div>";
        }
        echo "<button id ='add_note'>";
    }

    /**
     * Function bd_add_note_form
     * Called by the footer filter
     * Outputs the form to add a note
     */
    public static function bd_add_note_form() {
        $post_type = get_post_type();
        global $current_user;
        $current_user = get_currentuserinfo();
        if ($post_type == 'brothel') {
            global $post;
            echo "
    <form id ='abd_add_new_note'>
      <div id ='brothel_add_note_div'>
       <span>New note</span>
       <div id ='close_notes'>
         <img src = '" . BD_PLUGIN_URL . 'assets/images/close.png' . "'>
       </div>
       <div id ='error_holder_notes'></div>
       <input type ='hidden' name ='security' value ='" . wp_create_nonce('bd_add_note_nonce') . "'>
       <input type ='hidden' name ='action' value ='bd_add_note'>
       <input type ='hidden' name = 'post_id' value ='" . $post->ID . "'>
       <table class ='bd_table'>
        <tr>
         <td><input type ='hidden' name ='bd_author' value = '" . $current_user->user_login . "'></td>
       </tr>
       <tr>
         <td><textarea name ='bd_note' placeholder = 'Type your note here'></textarea></td>
       </tr>
     </table>
   </form>
   <button class ='bd_button next-button' id ='bd_add_note_button'>Add note</button>
 </div>";
        }
    }

    /**
     * Function bd_add_contact_form
     * Called by the footer filter
     * Outputs the form to add a contact
     */
    public static function bd_add_contact_form() {
        $post_type = get_post_type();
        if ($post_type == 'brothel') {
            global $post;
            echo "
    <form id ='abd_add_new_contact'>
      <div id ='brothel_add_contact_div'>
       <span>New Contact</span>
       <div id ='close_contact'>
         <img src = '" . BD_PLUGIN_URL . 'assets/images/close.png' . "'>
       </div>
       <div id ='error_holder_contact'></div>
       <input type ='hidden' name ='security' value ='" . wp_create_nonce('bd_add_contact_nonce') . "'>
       <input type ='hidden' name ='action' value ='bd_add_contact'>
       <input type ='hidden' name = 'post_id' value ='" . $post->ID . "'>
       <table class ='bd_table'>
        <tr>
         <td><label class ='bd_label'>Name</label></td>
         <td><input type ='text' name ='bdc_name'></td>
       </tr>
       <tr>
         <td><label class ='bd_label'>Position</label></td>
         <td><select name ='bdc_position'>
           <option value = 'Owner'>Owner</option>
           <option value = 'Manager'>Manager</option>
           <option value = '2IC'>2IC</option>
           <option value = 'Receptionist'>Option</option>
           <option value = 'Secretary'>Secretary</option>
           <option value = 'Supervisor'>Supervisor</option>
           <option value = 'Operations Manager'>Operations Manager</option>
           <option value = 'Marketing Department'>Marketing Department</option>
           <option value = 'General Enquires'>General Enquiries</option>
           <option value = 'Executive Assistant'>Executive Assistant</option>
           <option value = 'Personal Assistant'>Personal Assistant</option>
           <option value = 'Other'>Other</option>
         </select>
       </td>
     </tr>
     <tr>
       <td><label class ='bd_label'>Phone</label></td>
       <td><input type ='text' name ='bdc_phone'></td>
     </tr>
     <tr>
       <td><label class ='bd_label'>Mobile</label></td>
       <td><input type ='text' name ='bdc_mobile'></td>
     </tr>            <tr>
     <td><label class ='bd_label'>Email 1</label></td>
     <td><input type ='text' name ='bdc_email1'></td>
   </tr>            <tr>
   <td><label class ='bd_label'>Email 2</label></td>
   <td><input type ='text' name ='bdc_email2'></td>
 </tr>            <tr>
 <td><label class ='bd_label'>Address</label></td>
 <td><input type ='text' name ='bdc_address'></td>
</tr>            <tr>
<td><label class ='bd_label'>Comments</label></td>
<td><input type ='text' name ='bdc_comments'></td>
</tr>
</table>
</form>
<button class ='bd_button nice_blue_button' id ='bd_add_contact_button'>Add contact</button>
</div>";
        }
    }

    /**
     * Function bd_edit_contact_form
     * Called by the footer filter
     * Outputs the form to edit a contact
     */
    public static function bd_edit_contact_form() {
        $post_type = get_post_type();
        if ($post_type == 'brothel') {
            global $post;
            echo "
    <form id ='abd_edit_contact'>
      <div id ='brothel_edit_contact_div'>
       <span>Edit Contact</span>
       <div id ='close_contact_edit'>
         <img src = '" . BD_PLUGIN_URL . 'assets/images/close.png' . "'>
       </div>
       <div id ='error_holder_edit_contact'></div>
       <input type ='hidden' name ='security' value ='" . wp_create_nonce('bd_edit_contact_nonce') . "'>
       <input type ='hidden' name ='action' value ='bd_edit_contact'>
       <input type ='hidden' name = 'post_id' value ='" . $post->ID . "'>
       <input type = 'hidden' name ='meta_id' value = ''>
       <input type = 'hidden' name = 'previous_value' value = ''>
       <table class ='bd_table'>
        <tr>
         <td><label class ='bd_label'>Name</label></td>
         <td><input type ='text' name ='bdc_edit_name'></td>
       </tr>
       <tr>
         <td><label class ='bd_label'>Position</label></td>
         <td><select name ='bdc_edit_position'>
           <option value = 'Owner'>Owner</option>
           <option value = 'Manager'>Manager</option>
           <option value = '2IC'>2IC</option>
           <option value = 'Receptionist'>Receptionist</option>
           <option value = 'Secretary'>Secretary</option>
           <option value = 'Supervisor'>Supervisor</option>
           <option value = 'Operations Manager'>Operations Manager</option>
           <option value = 'Marketing Department'>Marketing Department</option>
           <option value = 'General Enquires'>General Enquiries</option>
           <option value = 'Executive Assistant'>Executive Assistant</option>
           <option value = 'Personal Assistant'>Personal Assistant</option>
           <option value = 'Other'>Other</option>
         </select>
       </td>
     </tr>
     <tr>
       <td><label class ='bd_label'>Phone</label></td>
       <td><input type ='text' name ='bdc_edit_phone'></td>
     </tr>
     <tr>
       <td><label class ='bd_label'>Mobile</label></td>
       <td><input type ='text' name ='bdc_edit_mobile'></td>
     </tr>            <tr>
     <td><label class ='bd_label'>Email 1</label></td>
     <td><input type ='text' name ='bdc_edit_email1'></td>
   </tr>            <tr>
   <td><label class ='bd_label'>Email 2</label></td>
   <td><input type ='text' name ='bdc_edit_email2'></td>
 </tr>            <tr>
 <td><label class ='bd_label'>Address</label></td>
 <td><input type ='text' name ='bdc_edit_address'></td>
</tr>            <tr>
<td><label class ='bd_label'>Comments</label></td>
<td><textarea name ='bdc_edit_comments'></textarea></td>
</tr>
</table>
<button class ='bd_button nice_blue_button' id ='bd_edit_contact_button'>Edit contact</button>
</div>
</form>";
        }
    }

    /**
     * Function bd_add_extra_descriptions
     * Called by the footer filter
     * Outputs the form to add an extra description
     */
    public static function bd_add_extra_descriptions() {
        $post_type = get_post_type();
        if ($post_type == 'city' || $post_type == 'brothel') {
            global $post;
            echo "
    <script>var post_id = '" . $post->ID . "';</script>
    <form id ='abd_add_extra_description'>
      <div id ='brothel_add_extra_description_form' class ='popup'>
       <span>Add description</span>
       <div id ='close_extra_description' class ='close_popup'>
         <img src = '" . BD_PLUGIN_URL . 'assets/images/close.png' . "'>
       </div>
       <input type = 'hidden' name ='security' value ='" . wp_create_nonce('bd_add_description_nonce') . "'>
       <input type = 'hidden' name ='action' value ='bd_add_extra_description'>
       <input type = 'hidden' id = 'bd_add_desc_id' name = 'post_id' value =''>
       <table class ='bd_table'>
        <tr>
         <td><textarea placeholder = 'Description is the text that will be displayed within the brothels ad' name ='bdc_description' id = 'bd_add_desc_textarea'></textarea>
         </tr>
       </table>
       <button class ='bd_button nice_blue_button' id ='bd_add_extra_description_button'>Add description</button>
     </div>
   </form>";
        }
    }

    /**
     * Function bd_vc_form
     * Called by the footer filter
     * Outputs version control form
     */
    public static function bd_vc_form() {
        $post_type = get_post_type();
        if ($post_type == 'brothel') {
            global $post;
            global $wpdb;
            date_default_timezone_set('Australia/Sydney');
            $brothel = get_the_id();
            $table_name = $table_name = $wpdb->prefix . 'bd_brothels_vc';
            $versions = $wpdb->get_results("  SELECT *
      FROM $table_name
      WHERE brothel_id  = {$brothel}
      ORDER BY vc_id DESC
      ", ARRAY_A);
            echo "
    <div id = 'underlay_light'></div>
    <div id ='bd_vc_popup' class ='popup'>
     <span>Old versions</span>
     <div id ='close_vc' class ='close_popup'>
       <img src = '" . BD_PLUGIN_URL . 'assets/images/close.png' . "'>
     </div>";
            if (count($versions) > 1) {
                echo "<div id ='bd_vc_next'></div><br>";
            }
            $i = 0;
            if (count($versions) < 2) {
                echo "<div class ='notification'>There are no old versions to restore</div>
       <style>
  #old_versions { display:none; } </style>";
            } else {
                echo "
        <button id ='bc_vd_restore_button' data-vc_id = '0'>Restore</button>
        <input type = 'hidden' name ='action' value ='bd_save_old_vc'>
        <div id ='bd_vc_holder'>";
                foreach ($versions as $version) {
                    echo "
            <div class ='bd_version";
                    if ($i == 0) {
                        echo " bd_vc_current";
                    }
                    echo "' data-vc_id ='" . $i . "'>
            <form id = 'bd_revert_vc_form" . $i . "'>
              <input type ='hidden' name = 'bd_post_id' value ='" . $post->ID . "'>
              <table class ='bd_table'>
                <tbody>
                  <tr>
                   <td><label class ='bc_vc_info'>Created on</label></td>
                   <td><span class ='bd_bc_info_span'>" . date('d-m-Y', $version['date']) . "</span></td>
                 </tr>
                 <tr>
                   <td><label class ='bc_vc_info'>Created by</label></td>
                   <td><span class ='bd_bc_info_span'>";
                    $user = get_userdata($version['user_id']);
                    echo $user->display_name . "</span></td>
                  </tr>
                  <tr class ='bd_info-top'>
                   <td><label class ='bc_vc_info'>Phone</label></td>
                   <td><input type ='text' name ='bd_phone' value = '" . $version['phone'] . "' readonly></td>
                 </tr>
                 <tr>
                   <td><label class ='bc_vc_info'>Address</label></td>
                   <td><input type ='text' name ='bd_address' value = '" . $version['address'] . "' readonly></td>
                 </tr>
                 <tr>
                   <td><label class ='bc_vc_info'>Website</label></td>
                   <td><input type ='text' name ='bd_website' value = '" . $version['website'] . "' placeholder ='No website' readonly>
                   </td>
                 </tr>
                 <tr>
                  <td><label class ='bc_vc_info'>Landing Page</label></td>
                  <td><input type ='text' name ='bd_landing_page' value = '" . $version['landing_page'] . "' placeholder ='No landing page' readonly>
                  </td>
                </tr>
                <tr>
                  <td><label class ='bc_vc_info'>Descriptions</label></td>
                  <td>";
                    foreach (json_decode($version['description'], true) as $brothel => $description) {
                        echo "<span class = 'bd_desc_name'>" . $brothel . "</span><br><textarea class ='bd_vc_description' name ='bd_description[\"" . $brothel . "\"]' readonly>" . reset($description) . "</textarea>";
                    }
                    echo "  </td>
                  </tr>
                  <tr>                                                                       <tr>
                    <td><label class ='bc_vc_info'>Image</label></td>
                    <td> <img src ='" . $version['image'] . "' onerror=\"this.src = '" . BD_PLUGIN_URL . 'assets/images/no-image.png' . "';\">
                      <input type = 'hidden' name ='bd_image' value = '" . $version['image'] . "'></td>
                    </tr>
                  </tbody>
                </table>
              </form>
            </div>";
                    $i++;
                }
            }
        }
        echo "
    </div>
  </div>";
    }

    /**
     * add_sticky_column
     * @parameter $columns
     * Customises the columns for the brothels post type
     */
    public static function add_sticky_column($columns) {
        $columns = array(
            'cb' => '<input type="checkbox" />',
            'title' => __('Title'),
            'city_events' => __('Clicks last 7 days'),
            'in_city' => __('In City'),
            'date' => __('Date')
        );
        return $columns;
    }

    /**
     * display_brothels_columns
     * @parameter $column, $post_id
     * Customises the columns for the brothels post type
     */
    public static function display_brothels_columns($column, $post_id) {
        if ($column == 'in_city') {
            $cities = get_post_meta($post_id, 'brothels', false);
            if (is_array($cities)) {
                $city = reset($cities);
                if (isset($city['in_page'])) {
                    foreach (array_unique($city['in_page']) as $page) {
                        echo get_the_title($page) . '<br>';
                    }
                }
            }
        }
        if ($column == 'city_events') {
            echo "<div class = 'bd_ajax_holder'><img src = '" . BD_PLUGIN_URL . "assets/images/reload.gif' class ='bd_event_stats' data-id = '" . $post_id . "' onload='window.bd_get_stats(jQuery(this))'></div>";
        }
    }

    /**
     * Function my_wp_trash_post
     * @parameter $post_id
     * Prevents brothels from being deleted if they are currently in any cities
     */
    public static function my_wp_trash_post($post_id) {
        $post_type = get_post_type($post_id);
        $post_status = get_post_status($post_id);
        if ($post_type == 'brothel' && in_array($post_status, array(
                    'publish',
                    'draft',
                    'future'
                ))) {
            $data = get_post_meta($post_id, 'brothels', false);
            if (isset($data[0]['in_page']) && count($data[0]['in_page']) > 0) {
                wp_redirect(admin_url('/edit.php?post_type=brothel&bd_delete=false'));
                update_option('bd_error_msg', '1');
                exit;
            }
        }
    }

}

?>