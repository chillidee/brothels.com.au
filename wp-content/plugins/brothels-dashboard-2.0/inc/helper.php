<?php 
/**
* Helper class
* Contains functions needed by more than one class
*/
  
  class bd_helper {
/**
 * function calculate_advertisers
 * Cleans the advertisers of expired ads, moves queued advertisers up, and returns the array
 * Also checks for and send notifications of expired advertisers and advertisers expiring soon
 * Days and notification email can be set in the advertiser settings page
*/
    
    public static function calculate_advertisers( $j = 0, $top_6, $notifications, $days ) {
      for ($l = 1; $l <= $j; $l++) {
      if ( isset($top_6[$l]['current'] ) ) { 
        /**---------------------Generate notifications for brothels expiring soon.
        ---------------------------------------------*/ 
        
        //if the current ad has less than x days to expiry 
             if ( strtotime( $top_6[$l]['current']['date_to']) < strtotime ( date('d-m-Y') ) + 60 * 60 * 24 * $days) {
                  //If there is no ad queued after it, or if that ad does not start on the current ads expiry
                  if ( !isset( $top_6[$l]['queued'] ) || strtotime( $top_6[$l]['queued']['date_from']) > strtotime ( date('d-m-Y') ) + 60 * 60 * 24 * $days  ) {
                       //Set the notification
                       if ( !isset ( $notifications[get_the_id()][$top_6[$l]['current']['brothel_id']] ) ){
                            $notifications[get_the_id()][$top_6[$l]['current']['brothel_id']] = 1;
                            $message = get_the_title( $top_6[$l]['current']['brothel_id']) . ' on ' . get_the_title() . ' page is expiring soon, and has no brothel queued after it';
                            wp_mail( get_option( 'bd_notification_email' ), 'Brothel expiring soon', $message );
                        }
                   }
              }
       
        /**--------------------Remove expired ads, set the queued brothel to current, and send notification
        ----------------------------------------------*/
              if ( strtotime( $top_6[$l]['current']['date_to']) < strtotime ( date('d-m-Y') )) {
                //Remove the expiring notification
                unset ( $notifications[get_the_id()][$top_6[$l]['current']['brothel_id']] );
                 //Remove the current brothel
                
                 unset ($top_6[$l]['current']);
                 //If there is no queued brothel, issue the notification
                 if ( !isset( $top_6[$l]['queued'] ) || strtotime( $top_6['queued']['date_from']) > strtotime ( date('d-m-Y') )){
                        if (  !isset( $notifications[get_the_id()]['pos-' . $l] )) {
                           $notifications[get_the_id()]['pos-' . $l] = 0;
                           $message = 'Position ' . $l . ' on ' .  get_the_title() . ' is currently empty';
                           wp_mail( get_option( 'bd_notification_email' ), 'Empty advertising spot', $message );
                         }
                  }
             /**----------------There is a queued brothel, and it is current---------------------------------
             ---------------------------------------------------*/
             else {
                        $top_6[$l]['current'] = $top_6[$l]['queued'];
                        unset ($top_6[$l]['queued']);
                  }
             }
             if (isset( $notifications[get_the_id()]['pos-' . $l] )) {
                      unset ( $notifications[get_the_id()]['pos-' . $l]);
             }
             if ( strtotime( $top_6[$l]['current']['date_to']) > strtotime ( date('d-m-Y') ) + 60 * 60 * 24 * $days) {
                 if ( isset( $notifications[get_the_id()][$top_6[$l]['current']['brothel_id']] ) ){
                         unset ( $notifications[get_the_id()][$top_6[$l]['current']['brothel_id']] );
                 }
             }
             else  {
                 if ( isset( $top_6[$l]['queued'] ) && strtotime( $top_6[$l]['queued']['date_from']) <= strtotime ( date('d-m-Y') ) + 60 * 60 * 24 * $days  ) {
                   if ( isset( $notifications[get_the_id()][$top_6[$l]['current']['brothel_id']] ) )
                      unset ( $notifications[get_the_id()][$top_6[$l]['current']['brothel_id']] );
                 }
             }
    }
  //No brothel, check there is a notification, and if not, set one
  else if (  !isset( $notifications[get_the_id()]['pos-' . $l] )) {
                           $notifications[get_the_id()]['pos-' . $l] = 0;
                           $message = 'Position ' . $l . ' on ' .  get_the_title() . ' is currently empty';
                           wp_mail( get_option( 'bd_notification_email' ), 'Empty advertising spot', $message );
  }
 }
  return array(
       'top_6' => $top_6,
       'notifications' => $notifications
    );
}
    public static function create_menu() {
         echo "
           <div id ='bd_top_menu' style ='padding-top: 25px;padding-bottom: 0;height: 70px;'>
                <a title = 'Back to the main dash' class ='bd_tab_switcher' href ='/wp-admin/index.php?page=brothels-dashboard' data-tab ='main_dash' id ='bd_main_dash_link'>Main Dash</a>
                <a title ='View, edit, create or delete brothels' class ='bd_tab_switcher' href ='/wp-admin/edit.php?post_type=brothel' data-tab ='brothels' id ='bd_brothel_link'>Brothels</a>
                <a title ='View, edit, create or delete cities'class ='bd_tab_switcher' href ='/wp-admin/edit.php?post_type=city' id ='bd_city_by_link' data-tab ='city-by-city'>Locations</a>
                <a title = 'Reporting' class ='bd_tab_switcher' href ='/wp-admin/index.php?page=brothels-reporting' data-tab ='main_dash'>Reporting</a>
           </div>";
  }
}
