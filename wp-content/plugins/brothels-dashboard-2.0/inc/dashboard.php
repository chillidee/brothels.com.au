<?php
/**
 * Class bd_dashboard
 * Controls output for our custom brothels dashboard screen
 * Contains static functions that can be called by actions and filters
 * @ Author- Che Jansen- Chillidee Marketing Group
 * Version 1.1.0
*/

  if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$supports = array ('');

class bd_dashboard {

  public static function bd_register_menu() {
    $bd_adv_dash =  add_dashboard_page( 'Advertising', 'Advertising', 'manage_options', 'brothels-dashboard', array( 'bd_dashboard','bd_create_dashboard') );
    add_action( 'load-' . $bd_adv_dash, array('bd_dashboard','load_bd_custom_page_scripts_action'));
    $bd_adv_reporting =  add_dashboard_page( 'Reporting', 'Reporting', 'manage_options', 'brothels-reporting', array( 'bd_dashboard','bd_create_reporting') );
    add_action( 'load-' . $bd_adv_reporting, array('bd_dashboard','load_bd_reporting_page_scripts_action'));
}
  public static function load_bd_custom_page_scripts_action() {
             add_action( 'admin_enqueue_scripts', array ( 'bd_dashboard', 'enqueue_bd_dashboard_scripts'));
  }
  public static function load_bd_reporting_page_scripts_action() {
           add_action( 'admin_enqueue_scripts', array ( 'bd_dashboard', 'enqueue_bd_reporting_scripts'));
  }
  public static function enqueue_bd_dashboard_scripts() {
        wp_register_style( 'bd-dash', BD_PLUGIN_URL . 'assets/css/bd-dash.css', false );
        wp_enqueue_style( 'bd-dash' );
        wp_register_script( 'data-table', BD_PLUGIN_URL .  'assets/js/data-tables.js', array('jquery'));
        wp_register_script( 'bd-dash.js', BD_PLUGIN_URL .  'assets/js/bd-dash.js', array('jquery', 'data-table', 'jquery-ui-datepicker'));
        wp_enqueue_script( 'bd-dash.js' );
        wp_enqueue_style( 'advertising.css', BD_PLUGIN_URL .  'assets/css/advertising.css');
    }
  public static function enqueue_bd_reporting_scripts() {
      wp_register_style( 'selectize', BD_PLUGIN_URL . 'assets/css/selectize.css', false );
      wp_enqueue_style( 'selectize' );
      wp_register_style( 'bd-dash', BD_PLUGIN_URL . 'assets/css/bd-dash.css', false );
      wp_enqueue_style( 'bd-dash' );
      wp_register_script( 'selectize', BD_PLUGIN_URL .  'assets/js/selectize.js', array('jquery'));
      wp_register_script( 'data-table', BD_PLUGIN_URL .  'assets/js/data-tables.js', array('jquery'));
      wp_register_script( 'reporting.js', BD_PLUGIN_URL .  'assets/js/reporting.js', array('jquery', 'data-table', 'jquery-ui-datepicker','selectize'));
      wp_enqueue_script( 'reporting.js' );
      wp_enqueue_style( 'advertising.css', BD_PLUGIN_URL .  'assets/css/advertising.css');
  }


public static function bd_create_dashboard() {
    include_once( 'bd_dashboard.php'  );
}
public static function bd_create_reporting() {
    include_once( 'bd_reporting.php'  );
}
}
  ?>
