<?php

/**
 * This file contains all our ajax functions
 * Defined outside of class so parameters can be passed more easily
 * Author: Che Jansen - Chillidee Marketing Group
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

/**
 * Function bd_add_brothel
 * Creates an array to hold the information for each brothel, and saves it as post meta
 * Validation is performed by javascript before the function is called
 *
 */

function bd_add_brothel()
{

    if (wp_verify_nonce($_POST['security'], 'bd_add_brothel_nonce') == 1) {
        $description = array();
        if (!isset($_POST['bd_inpage'])) {
            $in_page = array();
        } else {
            $in_page = json_decode(stripslashes($_POST['bd_inpage']), true);
        }
        $ga_name = sanitize_text_field($_POST['bd_ga_name']);
        if ($ga_name == '') {
            $ga_name = get_the_title($_POST['bd_post_id']);
        }
        $description = json_decode(stripslashes($_POST['bd_description']), true);
        $brothel = array(
            'name' => get_the_title($_POST['bd_post_id']),
            'address' => sanitize_text_field($_POST['bd_address']),
            'phone' => sanitize_text_field($_POST['bd_phone']),
            'tracking_number' => sanitize_text_field($_POST['bd_track_phone']),
            'description' => $description,
            'in_page' => $in_page,
            'ga_name' => $ga_name,
            'extra_ga_name' => $_POST['bd_extra_ga_name'],
            'extra_ga_name2' => $_POST['bd_extra_ga_name2'],
            'image' => sanitize_text_field($_POST['bd_image']),
            'image_150' => sanitize_text_field($_POST['bd_image_150']),
            'website' => sanitize_text_field($_POST['bd_website']),
            'website_readable' => sanitize_text_field($_POST['bd_website_readable']),
            'landing_page' => sanitize_text_field($_POST['bd_landing_page'])
        );
        global $wpdb;
        update_post_meta($_POST['bd_post_id'], 'brothels', $brothel);
        update_post_meta($_POST['bd_post_id'], 'brothel_lat', $_POST['brothel_lat']);
        update_post_meta($_POST['bd_post_id'], 'brothel_long', $_POST['brothel_long']);
        update_brothel_location($_POST['bd_post_id'], $_POST['brothel_long'], $_POST['brothel_lat']);
        $table_name = $wpdb->prefix . 'bd_brothels_vc';
        $wpdb->insert(
            $table_name, array(
            'date' => strtotime(date('d-m-Y')),
            'user_id' => get_current_user_id(),
            'brothel_id' => sanitize_text_field($_POST['bd_post_id']),
            'name' => sanitize_text_field(get_the_title($_POST['bd_post_id'])),
            'address' => sanitize_text_field($_POST['bd_address']),
            'phone' => sanitize_text_field($_POST['bd_phone']),
            'tracking_number' => sanitize_text_field($_POST['bd_track_phone']),
            'description' => sanitize_text_field(stripslashes($_POST['bd_description'])),
            'image' => sanitize_text_field($_POST['bd_image']),
            'website' => sanitize_text_field($_POST['bd_website']),
            'website_readable' => sanitize_text_field($_POST['bd_website_readable']),
            'landing_page' => sanitize_text_field($_POST['bd_landing_page']),
        ), array(
                '%d',
                '%d',
                '%d',
                '%d',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
            )
        );
        die();
    }
}

/**
 * Function bd_add_note
 * Creates an array to hold notes about each brothel
 * Each note is contained in it's own post meta
 * Validation is performed by javascript before the function is called
 *
 */
function bd_add_note()
{
    if (wp_verify_nonce($_POST['security'], 'bd_add_note_nonce') == 1) {
        date_default_timezone_set('Australia/Sydney');
        $date = date('d-m-Y');
        $note = nl2br($_POST['bd_note']);
        $note = array(
            'date' => $date,
            'author' => sanitize_text_field($_POST['bd_author']),
            'note' => $note
        );
        add_post_meta($_POST['post_id'], 'notes', $note);
        echo "<div class = 'note_header'>
   <span>Date</span>" . $note['date'] . "
   <span>Author</span>" . $note['author'] . "
 </div>
 <div class ='note_body'>" . $note['note'] . "</div>                                               ";
        die();
    }
}

/**
 * Function bd_add_contact
 * Creates an array from the post data, then cleans it of empty values
 * Each contact is contained in it's own post meta
 * Validation is performed by javascript before the function is called
 *
 */
function bd_add_contact()
{
    //Security!
    if (wp_verify_nonce($_POST['security'], 'bd_add_contact_nonce') == 1) {
        global $current_user;
        get_currentuserinfo();
        date_default_timezone_set('Australia/Sydney');
        $date = date('d-m-Y');
        $contact = array(
            'Name' => sanitize_text_field($_POST['bdc_name']),
            'Position' => sanitize_text_field($_POST['bdc_position']),
            'Phone' => sanitize_text_field($_POST['bdc_phone']),
            'Mobile' => sanitize_text_field($_POST['bdc_mobile']),
            'Email 1' => sanitize_text_field($_POST['bdc_email1']),
            'Email 2' => sanitize_text_field($_POST['bdc_email2']),
            'Address' => sanitize_text_field($_POST['bdc_address']),
            'Comments' => sanitize_text_field($_POST['bdc_comments']),
            'Created by' => $current_user->display_name,
            'Created on' => $date
        );
        //Clean our array of empty values
        $clean_contact_array = array();
        foreach ($contact as $key => $value) {
            if ($value != '') {
                $clean_contact_array[$key] = $value;
            }
        }
        //Add our clean contact array to a holder array, so future edits will be in the same format
        $holder_array[0] = $clean_contact_array;
        $meta_id = add_post_meta($_POST['post_id'], 'contact', $holder_array);
        echo "<div class ='clear_helper'>";
        foreach ($holder_array as $contact_array) {
            echo "<div class = 'contact_holder'>";
            $i = 0;
            if ($i == 0) {
                $j = 0;
                foreach ($contact_array as $key => $value) {
                    // strip out all whitespace and remove whitespace from key for a class
                    $class = preg_replace('/\s*/', '', $key);
                    $class = $meta_id . '_' . strtolower($class);
                    //If this the first cycle, it's Name, so don't print it.  We'll use the contact's name as a header
                    if ($j != 0) {
                        echo "<div class = 'contact_header'>" . $key . " </div>";
                    }
                    $j++;
                    echo "<div class ='contact_value " . $class . "'>" . $value . "</div><br>";
                }
                echo "<input type ='hidden' value = '" . json_encode($contact_array) . "' name = 'contact_array'>
    <button class ='edit_contact' data-id = '" . $meta_id . "'></button>";
                $i++;
            } else {
                if ($i = 1) {
                    echo "<div class ='prev_contact_versions'></div>";
                    $i++;
                }
                echo "<div class = 'prev_contact_edits'>
 <div class ='contact_value " . $class . "'>" . $value . "</div><br>";
            }
        }
    }
    die();
}

/**
 * Function bd_edit_contact
 * Creates an array from the post data, then cleans it of empty values
 * Each contact is contained in it's own post meta
 * Edits are saved as an array of arrays for version control
 *
 */
function bd_edit_contact()
{
    if (wp_verify_nonce($_POST['security'], 'bd_edit_contact_nonce') == 1) {
        global $current_user;
        get_currentuserinfo();
        global $wpdb;
        date_default_timezone_set('Australia/Sydney');
        $date = date('d-m-Y');
        $contact = array(
            'Name' => sanitize_text_field($_POST['bdc_edit_name']),
            'Position' => sanitize_text_field($_POST['bdc_edit_position']),
            'Phone' => sanitize_text_field($_POST['bdc_edit_phone']),
            'Mobile' => sanitize_text_field($_POST['bdc_edit_mobile']),
            'Email 1' => sanitize_text_field($_POST['bdc_edit_email1']),
            'Email 2' => sanitize_text_field($_POST['bdc_edit_email2']),
            'Address' => sanitize_text_field($_POST['bdc_edit_address']),
            'Comments' => sanitize_text_field($_POST['bdc_edit_comments']),
            'Created by' => $current_user->display_name,
            'Created on' => $date
        );
        $clean_contact_array = array();
        foreach ($contact as $key => $value) {
            if ($value != '') {
                $clean_contact_array[$key] = $value;
            }
        }
        //Get our previous contact version, and turn it back into an array
        $current_array = stripslashes($_POST['previous_value']);
        $current_array = json_decode($current_array, true);
        // Add our new contact array at the start of the array
        $final_contact = array_unshift($current_array, $clean_contact_array);
        //Using $wpdb as it allows us to check the meta_id. Save the new contact meta
        $table_name = $wpdb->prefix . 'postmeta';
        $wpdb->update(
            $table_name, array(
            'meta_value' => maybe_serialize($current_array), //Serialize our array into a string
        ), array('meta_id' => $_POST['meta_id']), array(
                '%s', //Accept a string
            )
        );
        die();
    }
}

/**
 * Function bd_add_extra_description
 * Adds an extra description to the unassigned array inside $description
 */
function bd_add_extra_description()
{
    if (wp_verify_nonce($_POST['security'], 'bd_add_description_nonce') == 1) {
        $brothel_data = get_post_meta($_POST['post_id'], 'brothels', true);
        $descriptions = array();
        if (is_array($brothel_data['description'])) {
            $descriptions = $brothel_data['description'];
            $descriptions['unassigned'][] = $_POST['bdc_description'];
        } else {
            $descriptions['unassigned'][] = $brothel_data['description'];
            $descriptions['unassigned'][] = $_POST['bdc_description'];
        }
        $brothel_data['description'] = $descriptions;
        update_post_meta($_POST['post_id'], 'brothels', $brothel_data);
        print_r($_POST['post_id']);
        die();
    }
}

/**
 * Function add_brothels_to_page
 * Assigns brothels to a city
 * Adds the brothel to the 'in_page' post meta for that brothel page
 * Also adds the brothel to the 'brothels_in_page' post meta for the city page
 */
function add_brothels_to_page()
{
    if (wp_verify_nonce($_POST['security'], 'bd_add_brothel_nonce') == 1) {
        //Get the brothels currently in the page
        $brothels_in = get_post_meta($_POST['page_id'], 'brothels_in_page', true);
        foreach ($_POST['id'] as $id) {
            $brothel_data = get_post_meta($id, 'brothels', true);
            $brothel_name = get_the_title($id);
            if (array_key_exists('unassigned', $brothel_data['description'])) {
                if (count($brothel_data['description']['unassigned']) == 1) {
                    $brothel_data['description'][$_POST['page_id']][0] = $brothel_data['description']['unassigned'][0];
                    unset($brothel_data['description']['unassigned']);
                } else {
                    $selection = array_search($id, $_POST['description_choice']);
                    $brothel_data['description'][$_POST['page_id']][0] = $brothel_data['description']['unassigned'][$_POST['description_choice']['"' . $id . '"']];
                    unset($brothel_data['description']['unassigned'][$_POST['description_choice']['"' . $id . '"']]);
                    $brothel_data['description']['unassigned'] = array_values($brothel_data['description']['unassigned']);
                }
            }
            $in_page = array();
            if (array_key_exists('in_page', $brothel_data)) {
                $in_page = $brothel_data['in_page'];
            }
            $in_page[] = $_POST['page_id'];
            $brothel_data['in_page'] = $in_page;
            update_post_meta($id, 'brothels', $brothel_data);
            $brothels_in[] = $id;
        }
        update_post_meta($_POST['page_id'], 'brothels_in_page', $brothels_in);
        die();
    }
}

/*
 * Creates the content to populate the choose brothel screen
 * Gets all eligible brothels, formats, then echoes back to js
 */

function populate_description_selects()
{
    $brothel_data = get_post_meta($_POST['post_id'], 'brothels', true);
    foreach ($brothel_data['description']['unassigned'] as $key => $description) {
        echo "<input type ='radio' name ='description_number' value = '" . $key . "'>
    <textarea class = 'choose_description_textarea' disabled>" . $description . "</textarea><br>";
    }
    die();
}

/*
 * Function bd_remove_brothel_from_page
 * Gets all the brothels in a page, unsets from the array, rearranges the array, and then saves it
 * Also gets the description assigned to this page, and makes it available for other pages to use
 */

function bd_remove_brothel_from_page()
{
    // Get all the brothels currently in this page
    $brothels_in = get_post_meta($_POST['page_id'], 'brothels_in_page', true);
    //Get the position of this brothel in the array
    $pos = array_search($_POST['brothel_id'], $brothels_in);
    //Remove it, and resort the keys
    unset($brothels_in[$pos]);
    $brothels_in = array_values($brothels_in);
    //Get the brothel data for this brothel
    $brothels_data = get_post_meta($_POST['brothel_id'], 'brothels', true);
    //Get the in page data, remove this city, resort it, and assign it back to the array
    $in_page = $brothels_data['in_page'];
    $pos = array_search($_POST['page_id'], $in_page);
    unset($in_page[$pos]);
    $in_page = array_values($in_page);
    $brothels_data['in_page'] = $in_page;
    //Get the description assigned to this brothel, and reassign it to unassigned array
    $description = $brothels_data['description'][str_replace('#038;', '', get_the_title($_POST['page_id']))][0];
    if ($description == '') {
        $description = $brothels_data['description'][get_the_title($_POST['page_id'])][0];
    }
    $brothels_data['description']['unassigned'][] = $description;
    unset($brothels_data['description'][str_replace('#038;', '', get_the_title($_POST['page_id']))]);
    unset($brothels_data['description'][get_the_title($_POST['page_id'])]);
    //Update the post meta with the new values
    update_post_meta($_POST['page_id'], 'brothels_in_page', $brothels_in);
    update_post_meta($_POST['brothel_id'], 'brothels', $brothels_data);
    die();
}

/*
 * Rearranges the brothels inside the brothels_in_page post meta
 * For our drag and drop functionality
 */

function bd_rearrange_brothels()
{
    $current_array = stripslashes($_POST['order']);
    $current_array = json_decode($current_array, true);
    update_post_meta($_POST['page_id'], 'brothels_in_page', $current_array);
}

/*
 * Outputs content for our assign top spots screen
 * Gets all eligible brothels, formats and outputs back to js
 */

function populate_assign_top_spots()
{
    if (wp_verify_nonce($_POST['security'], 'bd_populate_eligible_brothels') == 1) {
        date_default_timezone_set('Australia/Sydney');
        $brothels_in = get_post_meta($_POST['post_id'], 'brothels_in_page', true);
        $i = -1;
        $days = '';
        $top_6 = get_post_meta($_POST['post_id'], 'top_6', true);
        $adv_array = array();
        if (!is_array($top_6)) {
            $top_6 = array();
        }
        foreach ($top_6 as $key => $adv) {
            if (isset($adv['current'])) {
                $adv_array[$key] = $adv['current']['brothel_id'];
            }
        }
        if (isset($_POST['days'])) {
            $days = "<input type ='hidden' value ='" . $_POST['days'] . "' name ='days' id ='bd_available_days'>";
        }
        if (isset($_POST['min_days'])) {
            $number = (strtotime($_POST['min_days']) - strtotime(date('d-m-Y'))) / 60 / 60 / 24;
            $days = "<input type ='hidden' value ='" . $number . "' name ='days' id ='bd_available_days_min'>";
        }
        if (count($brothels_in) == 0 || $brothels_in == '') {
            echo "
     <div class ='notification'>You must add brothels to the page first</div> ";
        } else {
            echo $days . "
     <input type ='hidden' name ='position_chosen' value ='" . $_POST['pos'] . "' id ='pre_confirm_assign_pos_chosen'>
     <input type ='hidden' name = 'choose_or_queue' value ='" . $_POST['top_6_action'] . "' id ='pre_confirm_choose_or'>
     <input type ='hidden' name ='post_id' value ='" . $_POST['post_id'] . "' id ='pre_confirm_post_id'>
     <table class ='bd_table drag_table' id = 'table-dnd1'>
      <tr class = 'nodrag nodrop'>
        <th class ='fifteen_percent'>Select</th>
        <th>Brothel</th>
      </tr>";
            foreach ($brothels_in as $id) {
                $class = '';
                if ($_POST['top_6_action'] != 'queued') {
                    if (in_array($id, $adv_array)) {
                        $class = ' class = "in_image_spot"';
                    }
                } else {
                    $class = '';
                    foreach ($top_6 as $adv) {
                        if (isset($adv['current']) && $adv['current']['brothel_id'] == $id && strtotime($_POST['min_days']) < strtotime($adv['current']['date_to'])) {
                            $class = ' class = "in_image_spot"';
                        } else if (isset($adv['queued']) && $adv['queued']['brothel_id'] == $id && strtotime($_POST['min_days']) < strtotime($adv['queued']['date_to'])) {
                            $class = ' class = "in_image_spot"';
                        }
                    }
                }
                $i++;
                $brothel_data = get_post_meta($id, 'brothels');
                //if ($brothel_data[0]['image'] && $brothel_data[0]['image'] != '') {
                echo "<tr" . $class . ">
         <td><input type ='radio' value = '" . $id . "' name = 'chosen_brothel'></td>
         <td>" . $brothel_data[0]['name'] . "</td>
       </tr>";
                //}
            }
            echo "</table>";
            die();
        }
    }
}

/**
 * Function add_image_city_screen
 * Gets the brothel array, and adds the image chosen on the city screen
 */
function add_image_city_screen()
{
    $brothel_data = get_post_meta($_POST['post_id'], 'brothels', true);
    $brothel_data['image'] = $_POST['image'];
    update_post_meta($_POST['post_id'], 'brothels', $brothel_data);
    die();
}

/**
 * *Function confirm_assign_top_spot
 * Saves a brothel into a top spot for advertising
 * Creates a new array with the brothel included, and saves it into post meta top_6
 * Also creates a column in the transactions table.
 */
function confirm_assign_top_spot()
{
    date_default_timezone_set('Australia/Sydney');
    $status = $_POST['choose_or_queue'];
    if (strtotime($_POST['date_from']) > strtotime(date('d-m-Y'))) {
        $status = 'queued';
    }
    global $wpdb;
    $table_name = $wpdb->prefix . 'bd_transaction_table';
    if ($_POST['payment_amount'] != 'PROMOTION') {
        $amount = str_replace(' ', '', str_replace('$', '', $_POST['payment_amount']));
    } else {
        $amount = 0;
    }
    $transaction_details = array(
        'date_from' => $_POST['date_from'],
        'date_to' => $_POST['date_to'],
        'payment_type' => $_POST['payment_type'],
        'trans_no' => $_POST['trans_no']
    );
    $wpdb->insert(
        $table_name, array(
        'user_id' => get_current_user_id(),
        'date' => strtotime($_POST['date_processed']),
        'transaction_type' => 'in_city_ad',
        'trans_amount' => $amount,
        'page_id' => $_POST['page_id'],
        'brothel_id' => $_POST['brothel_chosen'],
        'pos_no' => $_POST['position_chosen'],
        'transaction_details' => serialize($transaction_details)
    ), array(
            '%d',
            '%s',
            '%s',
            '%d',
            '%d',
            '%d',
            '%d',
            '%s'
        )
    );
    $top_6 = get_post_meta($_POST['page_id'], 'top_6', true);
    $brothel_array = array(
        'brothel_id' => $_POST['brothel_chosen'],
        'date_from' => $_POST['date_from'],
        'date_to' => $_POST['date_to'],
        'payment_amount' => $_POST['payment_amount'],
        'payment_type' => $_POST['payment_type'],
        'trans_no' => $_POST['trans_no'],
        'processed_by' => $_POST['user_id'],
        'processed_on' => $_POST['date_processed'],
        'trans_id' => $wpdb->insert_id
    );
    $top_6[$_POST['position_chosen']][$status] = $brothel_array;
    update_post_meta($_POST['page_id'], 'top_6', $top_6);
    //  print_r ( $top_6 );
    die();
}

/**
 * *Function output_transaction_details
 * Gets the transactions details for a current brothel, and returns it for veiwing
 */
function output_transaction_details()
{
    if (wp_verify_nonce($_POST['security'], 'bd_populate_eligible_brothels') == 1) {
        $top_6 = get_post_meta($_POST['page_id'], 'top_6', true);
        $name = get_user_by('id', $top_6[$_POST['pos']]['current']['processed_by']);
        echo "<div class = 'transactions_split_div'>
     <h3>Current</h3>
     <table class ='bd_table' id ='transaction_details_table'>
      <tr><td></td></tr>
      <tr>
       <td class = 'smaller_td'><label>Brothel Name</label</td>
       <td><span class ='info' id ='bd_trans_details_brothel_name'>" . get_the_title($top_6[$_POST['pos']]['current']['brothel_id']) . "</span></td>
     </tr>
     <tr>
       <td class = 'smaller_td'><label>Date From</label</td>
       <td><span class ='info'>" . $top_6[$_POST['pos']]['current']['date_from'] . "</span></td>
     </tr>
     <tr>
       <td class = 'smaller_td'><label>Date To</label</td>
       <td><span class ='info' id ='trans_details_date_to'>" . $top_6[$_POST['pos']]['current']['date_to'] . "</span></td>
     </tr>
     <tr>
       <td class = 'smaller_td'><label>Payment Amount</label</td>
       <td><span class ='info'>" . $top_6[$_POST['pos']]['current']['payment_amount'] . "</span></td>
     </tr>
     <tr>
       <td class = 'smaller_td'><label>Payment Type</label</td>
       <td><span class ='info'>" . $top_6[$_POST['pos']]['current']['payment_type'] . "</span></td>
     </tr>
     <tr>
       <td class = 'smaller_td'><label>Transaction Number</label</td>
       <td><span class ='info'>" . $top_6[$_POST['pos']]['current']['trans_no'] . "</span></td>
     </tr>
     <tr>
       <td class = 'smaller_td'><label>Date Processed</label</td>
       <td><span class ='info'>" . $top_6[$_POST['pos']]['current']['processed_on'] . "</span></td>
     </tr>
     <tr>
       <td class = 'smaller_td'><label>Processed by</label</td>
       <td><span class ='info'>" . $name->data->display_name . "</span></td>
     </tr>
     <tr><td></td></tr>
   </table>
   <div class ='bd_error' id ='remove_top_post'>Remove brothel from position " . $_POST['pos'] . "</div>
 </div>
 <div class = 'transactions_split_div'>
   <h3>Queued</h3>";
        if (isset($top_6[$_POST['pos']]['queued']) && $top_6[$_POST['pos']]['queued'] != '' && count($top_6[$_POST['pos']]['queued']) > 0) {
            echo "
    <table class ='bd_table' id ='transaction_details_table'>
      <tr><td></td></tr>
      <tr>
       <td class = 'smaller_td'><label>Brothel Name</label</td>
       <td><span class ='info'>" . get_the_title($top_6[$_POST['pos']]['queued']['brothel_id']) . "</span></td>
     </tr>
     <tr>
       <td class = 'smaller_td'><label>Date From</label</td>
       <td><span class ='info'>" . $top_6[$_POST['pos']]['queued']['date_from'] . "</span></td>
     </tr>
     <tr>
       <td class = 'smaller_td'><label>Date To</label</td>
       <td><span class ='info'>" . $top_6[$_POST['pos']]['queued']['date_to'] . "</span></td>
     </tr>
     <tr>
       <td class = 'smaller_td'><label>Payment Amount</label</td>
       <td><span class ='info'>" . $top_6[$_POST['pos']]['queued']['payment_amount'] . "</span></td>
     </tr>
     <tr>
       <td class = 'smaller_td'><label>Payment Type</label</td>
       <td><span class ='info'>" . $top_6[$_POST['pos']]['queued']['payment_type'] . "</span></td>
     </tr>
     <tr>
       <td class = 'smaller_td'><label>Transaction Number</label</td>
       <td><span class ='info'>" . $top_6[$_POST['pos']]['queued']['trans_no'] . "</span></td>
     </tr>
     <tr>
       <td class = 'smaller_td'><label>Date Processed</label</td>
       <td><span class ='info'>" . $top_6[$_POST['pos']]['queued']['processed_on'] . "</span></td>
     </tr>
     <tr>
       <td class = 'smaller_td'><label>Processed by</label</td>
       <td><span class ='info'>" . $name->data->display_name . "</span></td>
     </tr>
     <tr><td></td></tr>
   </table>
   <div class ='bd_error' id ='remove_queued_top_post'>Remove brothel from position " . $_POST['pos'] . "</div>";
        } else {
            echo "<button class ='bd_button next-button' id ='bd_queue_brothel'>Queue now</button>";
        }
        echo "   </div>";
        die();
    }
}

/**
 * *Function bd_remove_brothels_from_top
 * Removes a brothel from the advertising spots
 * Creates an entry in the transactions table to record details, and removes the brothel from the top_6 array
 */
function bd_remove_brothels_from_top()
{
    date_default_timezone_set('Australia/Sydney');
    global $wpdb;
    $table_name = $wpdb->prefix . 'bd_transaction_table';
    if ($_POST['bd_refund_offered'] > 0) {
        $amount = -$_POST[bd_refund_offered];
    } else {
        $amount = 0;
    }
    $transaction_details = array(
        'orig_trans_id' => $_POST['trans_id'],
        'removal_reason' => $_POST['bd_removal_reason'],
        'refund_amount' => $_POST['bd_refund_offered'],
        'notes' => $_POST['bd_removal_notes']
    );
    $wpdb->insert(
        $table_name, array(
        'user_id' => get_current_user_id(),
        'date' => strtotime(date('d-m-Y')),
        'transaction_type' => 'removal',
        'trans_amount' => $amount,
        'page_id' => $_POST['page_id'],
        'brothel_id' => $_POST['brothel_id'],
        'pos_no' => $_POST['pos_no'],
        'transaction_details' => serialize($transaction_details)
    ), array(
            '%d',
            '%s',
            '%s',
            '%d',
            '%d',
            '%d',
            '%d',
            '%s'
        )
    );
    $top_6 = get_post_meta($_POST['page_id'], 'top_6', true);
    $brothel_array = array();
    unset($top_6[$_POST['pos_no']][$_POST['bd_current_or_queued']]);
    update_post_meta($_POST['page_id'], 'top_6', $top_6);
    echo 'hi';
    die();
}

/**
 * *Function set_advertiser_count
 * Sets the advertiser count as post meta
 */
function set_advertiser_count()
{
    if (wp_verify_nonce($_POST['security'], 'set_advertiser_count_nonce') == 1) {
        $notifications = get_option('bd_notifications', array());
        if (isset($notifications[$_POST['post_id']])) {
            unset($notifications[$_POST['post_id']]);
        }
        $j = $_POST['count'] + 1;
        $adv = get_post_meta($_POST['post_id'], 'top_6', true);
        for ($j; $j <= 15; $j++) {
            if (is_array($adv)):
                unset($adv[$j]);
            endif;
        }
        update_post_meta($_POST['post_id'], 'top_6', $adv);
        update_option('bd_notifications', $notifications);
        update_post_meta($_POST['post_id'], 'advertiser_count', $_POST['count']);
    }
    die();
}

/**
 * Function bd_link_action
 * For recording clicks in the database
 */
function bd_link_action()
{
    date_default_timezone_set('Australia/Sydney');
    switch ($_POST['el_action']) {
        case 'Website Link':
            $action = '0';
            break;
        case 'Image Link':
            $action = '1';
            break;
        case 'Phone':
            $action = '2';
            break;
        case 'Page Visit':
            $action = '3';
            break;
    }
    global $wpdb;
    $table_name = $wpdb->prefix . 'bd_analytics';
    $return = $wpdb->insert(
        $table_name, array(
        'date' => strtotime(date('d-m-Y')),
        'category_id' => $_POST['el_category'],
        'action' => $action,
        'label_id' => $_POST['el_label'],
        'position' => $_POST['position']
    ), array(
            '%d',
            '%d',
            '%d',
            '%d',
            '%d',
        )
    );
    echo $return;
    die();
}

/**
 * Function bd_populate_statistics
 * Returns the statistics for brothel page on page load
 */
function bd_populate_statistics()
{
    global $wpdb;
    date_default_timezone_set('Australia/Sydney');
    $label = $_POST['post_id'];
    $table_name = $wpdb->prefix . 'bd_analytics';
    if ($_POST['date_from'] == 'default') {
        $date_from = strtotime(date('d-m-Y')) - (60 * 60 * 24 * 7);
        $date_to = strtotime(date('d-m-Y')) + (60 * 60 * 24 * 7);
    } else {
        $date_from = strtotime($_POST['date_from']) - 1000;
        $date_to = strtotime($_POST['date_to']) + 60 * 60 * 24;
    }
    if ($_POST['city'] == 'all') {
        $events = $wpdb->get_results(
            "
   SELECT *
   FROM $table_name
   WHERE label_id  = $label
   AND date > $date_from
   AND date < $date_to
   ", ARRAY_A
        );
    } else {
        $city = get_page_by_title($_POST['city'], ARRAY_A, 'city');
        $cat_id = $city['ID'];
        $events = $wpdb->get_results(
            "
   SELECT *
   FROM $table_name
   WHERE label_id  = $label
   AND date > $date_from
   AND date < $date_to
   AND category_id = $cat_id
   ", ARRAY_A
        );
    }
    $website_clicks = array();
    $image_clicks = array();
    $phone_clicks = array();
    $page_visits = array();
    foreach ($events as $event) {
        switch ($event['action']) {
            case '0':
                $website_clicks[] = $event;
                break;
            case '1':
                $image_clicks[] = $event;
                break;
            case '2':
                $phone_clicks[] = $event;
                break;
            case '3':
                $page_visits[] = $event;
                break;
        }
    }
    $stats = get_post_meta($_POST['post_id'], 'bd_stat_settings', true);
    if (!$stats) {
        $stats = array(
            'unit_revenue' => 340,
            'conv_percent' => 10,
            'ads_charge' => 10,
        );
    }
    if ($events == 0 || $events == '' || $events == NULL) {
        $rev = 0;
        $ads_rev = 0;
        $cost = 0;
        $exp_rev = 0;
    } else {
        $rev = round(count($events) * $stats['unit_revenue'], 2);
        $rev = number_format($rev, 0, '', ',');
        $ads_rev = round(((count($events) * $stats['unit_revenue']) * $stats['conv_percent'] / 100) * $stats['ads_charge'] / 100, 2);
        $ads_rev = number_format($ads_rev, 0, '', ',');
        $cost = round(((count($events) * $stats['unit_revenue']) / $stats['conv_percent']) * ($stats['ads_charge'] / 100) / count($events), 2);
        $exp_rev = round((count($events) * $stats['unit_revenue']) * ($stats['conv_percent'] / 100));
        $exp_rev = number_format($exp_rev, 0, '', ',');
    }

    echo "
<div class ='left_floater_30'>
 <div id ='bd_stats_total'>" . count($events) . "</div>
 <span id ='bd_stats_label'>Total Events</span>
</div>
<div class ='left_floater_70'>";
    if (count($website_clicks) != 0) {
        echo "
    <span class ='bd_ind_statistics'>
     <span class ='bd_ind_number' id ='bd_inv_website'>" . count($website_clicks) . "</span>
     <span class ='bd_ind_exp'>Website clicks</span>
   </span><br>";
    }
    if (count($image_clicks) != 0) {
        echo "
  <span class ='bd_ind_statistics'>
   <span class ='bd_ind_number' id ='bd_inv_image'>" . count($image_clicks) . "</span>
   <span class ='bd_ind_exp'>Image clicks</span>
 </span><br>";
    }
    if (count($phone_clicks) != 0) {
        echo "
  <span class ='bd_ind_statistics'>
   <span class ='bd_ind_number' id ='bd_inv_phone'>" . count($phone_clicks) . "</span>
   <span class ='bd_ind_exp'>Phone clicks</span>
 </span><br>";
    }
    if (count($page_visits) != 0) {
        echo "
  <span class ='bd_ind_statistics'>
   <span class ='bd_ind_number' id ='bd_inv_page_visits'>" . count($page_visits) . "</span>
   <span class ='bd_ind_exp'>Page Visits</span>
 </span><br>";
    }
    echo "
</div>
<div class ='bd_large_stats'>
  <label class ='bd_loud'>Revenue</label><label class ='bd_loud bd_green'>$" . $rev . "</label><br>
  <label class ='bd_loud'>Expected Ads Revenue</label><label class ='bd_loud bd_green'>$" . $ads_rev . "</label><br>
  <label class ='bd_loud'>Cost Per Acquistion</label><label class ='bd_loud bd_green'>$" . $cost . "</label>
  <label class ='bd_really_loud'>$" . $exp_rev . "</label>
  <label class ='bd_loud_under'>Expected Revenue</label>";
    die();
}

/**
 * Function bd_set_statistics
 * Triggered when the user changes the brothel statistic options
 * Sets the new values, recalulates and formats the statistics, then returns
 */
function bd_set_statistics()
{
    $stats = array(
        'unit_revenue' => $_POST['unit_revenue'],
        'conv_percent' => $_POST['conv_percent'],
        'ads_charge' => $_POST['ads_charge']
    );
    update_post_meta($_POST['post_id'], 'bd_stat_settings', $stats);
    global $wpdb;
    date_default_timezone_set('Australia/Sydney');
    $label = $_POST['post_id'];
    $table_name = $table_name = $wpdb->prefix . 'bd_analytics';
    if ($_POST['date_from'] == 'default') {
        $date_from = strtotime(date('d-m-Y')) - (60 * 60 * 24 * 7);
        $date_to = strtotime(date('d-m-Y')) + (60 * 60 * 24 * 7);
    } else {
        $date_from = strtotime($_POST['date_from']) - 60 * 60 * 24;
        $date_to = strtotime($_POST['date_to']) + 60 * 60 * 24;
    }
    if ($_POST['city'] == 'all') {
        $events = $wpdb->get_results(
            "
   SELECT *
   FROM $table_name
   WHERE label_id  = $label
   AND date > $date_from
   AND date < $date_to
   ", ARRAY_A
        );
    } else {
        $city = get_page_by_title($_POST['city'], ARRAY_A, 'city');
        $cat_id = $city['ID'];
        $events = $wpdb->get_results(
            "
   SELECT *
   FROM $table_name
   WHERE label_id  = $label
   AND date > $date_from
   AND date < $date_to
   AND category_id = $cat_id
   ", ARRAY_A
        );
    }
    $website_clicks = array();
    $image_clicks = array();
    $phone_clicks = array();
    $page_visits = array();
    foreach ($events as $event) {
        switch ($event['action']) {
            case '0':
                $website_clicks[] = $event;
                break;
            case '1':
                $image_clicks[] = $event;
                break;
            case '2':
                $phone_clicks[] = $event;
                break;
            case '3':
                $page_visits[] = $event;
                break;
        }
    }
    $statss = get_post_meta($_POST['post_id'], 'bd_stat_settings', true);
    if ($events == 0) {
        $rev = 0;
        $ads_rev = 0;
        $cost = 0;
        $exp_rev = 0;
    } else {
        $rev = round(count($events) * $stats['unit_revenue'], 2);
        $rev = number_format($rev, 0, '', ',');
        $ads_rev = round(((count($events) * $stats['unit_revenue']) * $stats['conv_percent'] / 100) * $stats['ads_charge'] / 100, 2);
        $ads_rev = number_format($ads_rev, 0, '', ',');
        $cost = round(((count($events) * $stats['unit_revenue']) / $stats['conv_percent']) * ($stats['ads_charge'] / 100) / count($events), 2);
        $exp_rev = round((count($events) * $stats['unit_revenue']) * ($stats['conv_percent'] / 100));
        $exp_rev = number_format($exp_rev, 0, '', ',');
    }
    echo "
<div class ='left_floater_30'>
 <div id ='bd_stats_total'>" . count($events) . "</div>
 <span id ='bd_stats_label'>Total Events</span>
</div>
<div class ='left_floater_70'>
  <span class ='bd_ind_statistics'>
   <span class ='bd_ind_number' id ='bd_inv_website'>" . count($website_clicks) . "</span>
   <span class ='bd_ind_exp'>Website clicks</span>
 </span><br>
 <span class ='bd_ind_statistics'>
   <span class ='bd_ind_number' id ='bd_inv_image'>" . count($image_clicks) . "</span>
   <span class ='bd_ind_exp'>Image clicks</span>
 </span><br>
 <span class ='bd_ind_statistics'>
   <span class ='bd_ind_number' id ='bd_inv_phone'>" . count($phone_clicks) . "</span>
   <span class ='bd_ind_exp'>Phone clicks</span>
 </span><br>
 <span class ='bd_ind_statistics'>
   <span class ='bd_ind_number' id ='bd_inv_page_visits'>" . count($page_visits) . "</span>
   <span class ='bd_ind_exp'>Page Visits</span>
 </span><br>
</div>
<div class ='bd_large_stats'>
  <label class ='bd_loud'>Revenue</label><label class ='bd_loud bd_green'>$" . $rev . "</label><br>
  <label class ='bd_loud'>Expected Ads Revenue</label><label class ='bd_loud bd_green'>$" . $ads_rev . "</label><br>
  <label class ='bd_loud'>Cost Per Acquistion</label><label class ='bd_loud bd_green'>$" . $cost . "</label>
  <label class ='bd_really_loud'>$" . $exp_rev . "</label>
  <label class ='bd_loud_under'>Expected Revenue</label>";

    die();
}

/**
 * Function bd_get_brothels_stats
 * Gets a plain count of events for each brothel
 */
function bd_get_brothels_stats()
{
    global $wpdb;
    date_default_timezone_set('Australia/Sydney');
    $label = $_POST['post_id'];
    $table_name = $table_name = $wpdb->prefix . 'bd_analytics';
    $date_from = strtotime(date('d-m-Y')) - (60 * 60 * 24 * 7);
    $date_to = strtotime(date('d-m-Y')) + (60 * 60 * 24 * 7);
    $events = $wpdb->get_results(
        "
   SELECT *
   FROM $table_name
   WHERE label_id  = $label
   AND date > $date_from
   AND date < $date_to
   ", ARRAY_A
    );
    echo count($events);
    die();
}

/**
 * Function bd_get_city_clicks
 * Gets a plain count of events for each city
 */
function bd_get_city_clicks()
{
    global $wpdb;
    date_default_timezone_set('Australia/Sydney');
    $category = $_POST['post_id'];
    $table_name = $table_name = $wpdb->prefix . 'bd_analytics';
    $date_from = strtotime(date('d-m-Y')) - (60 * 60 * 24 * 7);
    $date_to = strtotime(date('d-m-Y')) + (60 * 60 * 24 * 7);
    $events = $wpdb->get_results(
        "
   SELECT *
   FROM $table_name
   WHERE category_id  = $category
   AND date > $date_from
   AND date < $date_to
   ", ARRAY_A
    );
    echo count($events);
    die();
}

/**
 * Function  bd_get_city_revenue
 * Gets a total revenue for each city page
 */
function bd_get_city_revenue()
{
    $page_id = $_POST['post_id'];
    global $wpdb;
    $table_name = $wpdb->prefix . 'bd_transaction_table';
    $revenue = $wpdb->get_results(
        "
   SELECT *
   FROM $table_name
   WHERE page_id  = $page_id
   ", ARRAY_A
    );
    $total = 0;
    foreach ($revenue as $trans) {
        $total += $trans['trans_amount'];
    }
    echo '$' . $total;
    die();
}

/**
 * Function  bd_update_notification_settings
 * Updates the notification time after a user changes them
 */
function bd_update_notification_settings()
{
    update_option('notification_times', $_POST['notification_time']);
    die();
}

/**
 * Function  bd_email_settings
 * Updates the notification email after a user changes it
 */
function bd_email_settings()
{
    update_option('bd_notification_email', $_POST['email']);
    die();
}

/**
 * Function  bd_adjust_ga_stats
 * Requires a csv formatted string for each event
 * User must specify a brothel and event for the data
 * Formats into a serialized array, and then creates nessecary lines in the DB
 */
function bd_adjust_ga_stats()
{
    //Check security
    if (wp_verify_nonce($_POST['security'], 'ga_stats_nonce') == 1) {
        //Break the csv into an array by line break
        $data = explode("\n", $_POST['bd_ga_csv']);
        $formatted = array();
        //Create a new array with the data as key for each line
        $i = 0;
        $type = 'brothel';
        $args = array(
            'post_type' => $type,
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'orderby' => 'title',
            'order' => 'ASC'
        );
        $brothels = null;
        $brothel_array = array();
        $brothels = new WP_Query($args);
        $brothels_formatted = array();
        if ($brothels->have_posts()) {
            while ($brothels->have_posts()) : $brothels->the_post();
                $brothel_array[] = get_the_id();
            endwhile;
        };
        wp_reset_query();  // Restore global post data stomped by the_post().
        foreach ($brothel_array as $brothel) {
            $brothel_data = get_post_meta($brothel, 'brothels', true);
            if (isset($brothel_data['ga_name']) && $brothel_data['ga_name'] != '') {
                $name = str_replace("'", "", $brothel_data['ga_name']);
                $name = str_replace('&#8217;', '', $name);
            } else {
                $name = str_replace("'", "", $brothel_data['name']);
                $name = str_replace('&#8217;', '', $name);
            }
            $brothels_formatted[$name] = $brothel;
            if (isset($brothel_data['extra_ga_name'])) {
                $brothels_formatted[$brothel_data['extra_ga_name']] = $brothel;
            }
            if (isset($brothel_data['extra_ga_name2'])) {
                $brothels_formatted[$brothel_data['extra_ga_name2']] = $brothel;
            }
        }
        $events = array(
            'Website Link' => '0',
            'Image Link' => '1',
            'Phone' => '2'
        );
        foreach ($data as $line) {
            $parts = explode(',', $line);
            //print_r ( $parts );
            if ($parts[1] == 'Image Link' || $parts[1] == 'Website Link' || $parts[1] == 'Phone') {
                $formatted[$i]['pos'] = $parts[0][0];
                $brothel_name = str_replace('.', '', substr($parts[0], 2, -1)) . substr($parts[0], -1);
                if ($brothel_name[0] == ' ') {
                    $brothel_name = substr($brothel_name, 1, -1) . substr($brothel_name, -1);
                }
                if (!isset($brothels_formatted[$brothel_name]) && $brothel_name != 'Diamonds' && $brothel_name != 'Gypsyâ€™s Kittens & Cougars' && $brothel_name != 'Penny&#8217;s' && $brothel_name != 'Pickwood lodge') {
                    echo $brothel_name . ' not defined.  Please add as extra ga name in brothel page';
                    die();
                }
                if ($brothel_name == 'Myplaymate') {
                    if ($_POST['bd_ga_city_id'] == '2038') {
                        $formatted[$i]['brothel'] = '2506';
                    } else if (
                        $_POST['bd_ga_city_id'] == '1897'
                    ) {
                        $formatted[$i]['brothel'] = '2505';
                    } else if (
                        $_POST['bd_ga_city_id'] == '2376'
                    ) {
                        $formatted[$i]['brothel'] = '2504';
                    } else if (
                        $_POST['bd_ga_city_id'] == '2031'
                    ) {
                        $formatted[$i]['brothel'] = '2503';
                    } else if (
                        $_POST['bd_ga_city_id'] == '2397'
                    ) {
                        $formatted[$i]['brothel'] = '2502';
                    }
                } else {
                    $formatted[$i]['brothel'] = $brothels_formatted[$brothel_name];
                }
                $formatted[$i]['action'] = $events[$parts[1]];
                date_default_timezone_set('Australia/Sydney');
                $date = substr_replace($parts[2], '-', 4, 0);
                $formatted[$i]['date'] = strtotime(substr_replace($date, '-', 7, 0));
                $formatted[$i]['events'] = $parts[3];
                $i++;
            }
        }
        global $wpdb;
        $table_name = $wpdb->prefix . 'bd_analytics';
        //Run a loop for each date
        foreach ($formatted as $arr) {
            //Delete all lines for this date
            $wpdb->delete($table_name, array(
                'date' => $arr['date'],
                'category_id' => $_POST['bd_ga_city_id'],
                'label_id' => $arr['brothel'],
                'action' => $arr['action'],
                'position' => $arr['pos']
            ));
            //Then create the new lines.  Eg for date 02/31/15, create 22 lines
            for ($x = 0; $x < $arr['events']; $x++) {
                if ($arr['events'] != 0) {
                    $return = $wpdb->insert(
                        $table_name, array(
                        'date' => $arr['date'],
                        'category_id' => $_POST['bd_ga_city_id'],
                        'label_id' => $arr['brothel'],
                        'action' => $arr['action'],
                        'position' => $arr['pos']
                    ), array(
                            '%d',
                            '%d',
                            '%d',
                            '%d',
                            '%d',
                        )
                    );
                }
            }
        }
    }
    echo 'Import successful';
    // print_r ( $formatted );

    die();
}

function save_bottom_content()
{
    $yeah = update_post_meta($_POST['post_id'], 'bd_bottom_content', $_POST['content']);
    echo $yeah;
    die();
}

function save_mobile_banner()
{
    $yeah = update_post_meta($_POST['post_id'], 'brothels_meta_box_mobile_banner', $_POST['content']);
    die();
}

function bd_city_populate_statistics()
{
    global $wpdb;
    date_default_timezone_set('Australia/Sydney');
    $table_name = $table_name = $wpdb->prefix . 'bd_analytics';
    $date_from = strtotime($_POST['from']) - 1000;
    $date_to = strtotime($_POST['to']) + 60 * 60 * 24;
    $array = array();
    $pos_count = intval($_POST['stat_count']);
    $id = $_POST['page_id'];
    for ($x = 1; $x <= $pos_count; $x++) {
        $events = $wpdb->get_results(
            "
   SELECT *
   FROM $table_name
   WHERE date > $date_from
   AND date < $date_to
   AND category_id = $id
   AND position = $x
   ", ARRAY_A
        );
        $array[$x] = count($events);
    }
    echo json_encode($array);
    die();
}

function bd_city_populate_detailed_statistics()
{
    global $wpdb;
    date_default_timezone_set('Australia/Sydney');
    $table_name = $table_name = $wpdb->prefix . 'bd_analytics';
    if ($_POST['date_from'] == 'default') {
        $date_from = strtotime(date('d-m-Y')) - (60 * 60 * 24 * 7);
        $date_to = strtotime(date('d-m-Y')) + (60 * 60 * 24 * 7);
    } else {
        $date_from = strtotime($_POST['date_from']) - 1000;
        $date_to = strtotime($_POST['date_to']) + 60 * 60 * 24;
    }
    $id = $_POST['page_id'];
    $pos = $_POST['position'];
    $events = $wpdb->get_results(
        "
 SELECT *
 FROM $table_name
 WHERE position  = $pos
 AND date > $date_from
 AND date < $date_to
 AND category_id = $id
 ", ARRAY_A
    );

    $website_clicks = array();
    $image_clicks = array();
    $phone_clicks = array();
    $brothels = array();
    $event_count = 0;
    foreach ($events as $event) {
        $brothels[$event['label_id']] = 1;
        if (isset($_POST['brothels']) && $_POST['brothels'] != '') {
            if ($_POST['brothels'] == $event['label_id']) {
                $event_count++;
                switch ($event['action']) {
                    case '0':
                        $website_clicks[] = $event;
                        break;
                    case '1':
                        $image_clicks[] = $event;
                        break;
                    case '2':
                        $phone_clicks[] = $event;
                        break;
                    case '3':
                        $page_visits[] = $event;
                        break;
                }
            }
        } else {
            $event_count++;
            switch ($event['action']) {
                case '0':
                    $website_clicks[] = $event;
                    break;
                case '1':
                    $image_clicks[] = $event;
                    break;
                case '2':
                    $phone_clicks[] = $event;
                    break;
                case '3':
                    $page_visits[] = $event;
                    break;
            }
        }
    }
    if (count($brothels == '1')) {
        $stats = get_post_meta(key($brothels), 'bd_stat_settings', true);
    }
    $stats = array(
        'unit_revenue' => 340,
        'conv' => 10,
        'ads_charge' => 10,
    );
    if (isset($_POST['unit_rev']) && $_POST['unit_rev'] != '') {
        $stats['unit_revenue'] = $_POST['unit_rev'];
    }
    if (isset($_POST['conv']) && $_POST['conv'] != '') {
        $stats['conv'] = $_POST['conv'];
    }
    if (isset($_POST['ads_charge']) && $_POST['ads_charge'] != '') {
        $stats['ads_charge'] = $_POST['ads_charge'];
    }
    if ($events == 0 || $events == '' || $events == NULL) {
        $rev = 0;
        $ads_rev = 0;
        $cost = 0;
        $exp_rev = 0;
    } else {
        $rev = round($event_count * $stats['unit_revenue'], 2);
        $rev = number_format($rev, 0, '', ',');
        $ads_rev = round((($event_count * $stats['unit_revenue']) * $stats['conv'] / 100) * ($stats['ads_charge'] / 100), 2);
        $ads_rev = number_format($ads_rev, 0, '', ',');
        $cost = round((($event_count * $stats['unit_revenue']) / $stats['conv']) * ($stats['ads_charge'] / 100) / count($events), 2);
        $exp_rev = round(($event_count * $stats['unit_revenue']) * ($stats['conv'] / 100));
        $exp_rev = number_format($exp_rev, 0, '', ',');
    }
    if (count($brothels) > 1) {
        if (isset($_POST['brothels']) && $_POST['brothels'] != '') {
            $brothel_id = key($brothels);
            echo "<label id ='bd_stats_brothel_name'>Showing statistics for " . get_the_title($_POST['brothels']) . "</label>";
        }
        echo "<label class ='bd_statistics' id ='bd_stats_brothel_choice'>Brothels</label>
  <select id ='bd_stats_brothel_choice_select'>
    <option value =''>All brothels</option>";
        foreach ($brothels as $key => $value) {
            echo "<option value ='" . $key . "'>" . get_the_title($key) . "</option>";
        }
        echo "</select>";
    } else {
        $brothel_id = key($brothels);
        echo "<label id ='bd_stats_brothel_name'>Showing statistics for " . get_the_title($brothel_id) . "</label>";
        if (isset($_POST['brothels']) && $_POST['brothels'] != '') {
            echo "
    <select id ='bd_stats_brothel_choice_select'>
      <option value =''>All brothels</option>";
            foreach ($brothels as $key => $value) {
                echo "<option value ='" . $key . "'>" . get_the_title($key) . "</option>";
            }
            echo "</select>";
        }
    }
    echo "
  <div class ='left_floater_30'>
   <div id ='bd_stats_total'>" . $event_count . "</div>
   <span id ='bd_stats_label'>Total Events</span>
 </div>
 <div class ='left_floater_70'>";
    if (count($website_clicks) != 0) {
        echo "
    <span class ='bd_ind_statistics'>
     <span class ='bd_ind_number' id ='bd_inv_website'>" . count($website_clicks) . "</span>
     <span class ='bd_ind_exp'>Website clicks</span>
   </span><br>";
    }
    if (count($image_clicks) != 0) {
        echo "
  <span class ='bd_ind_statistics'>
   <span class ='bd_ind_number' id ='bd_inv_image'>" . count($image_clicks) . "</span>
   <span class ='bd_ind_exp'>Image clicks</span>
 </span><br>
 ";
    }
    if (count($phone_clicks) != 0) {
        echo "
 <span class ='bd_ind_statistics'>
   <span class ='bd_ind_number' id ='bd_inv_phone'>" . count($phone_clicks) . "</span>
   <span class ='bd_ind_exp'>Phone clicks</span>
 </span><br>";
    }
    if (count($page_visits) != 0) {
        echo "
 <span class ='bd_ind_statistics'>
   <span class ='bd_ind_number' id ='bd_inv_page_visits'>" . count($page_visits) . "</span>
   <span class ='bd_ind_exp'>Page Visits</span>
 </span><br>";
    }
    echo "
</div>
<div class ='bd_large_stats'>
  <label class ='bd_loud'>Revenue</label><label class ='bd_loud bd_green'>$" . $rev . "</label><br>
  <label class ='bd_loud'>Expected Ads Revenue</label><label class ='bd_loud bd_green'>$" . $ads_rev . "</label><br>
  <label class ='bd_loud'>Cost Per Acquistion</label><label class ='bd_loud bd_green'>$" . $cost . "</label>
  <label class ='bd_really_loud'>$" . $exp_rev . "</label>
  <label class ='bd_loud_under'>Expected Revenue</label>
  <div id ='bd_stats_bottom_section'>
    <label class ='bd_statistics_label bottom_label'>Unit Revenue</label><label class ='dollar'>$</label><input id = 'bd_stats_unit-rev' class ='bottom_input' name ='bd_unit_revenue' value = '";
    if (isset($stats['unit_revenue'])) {
        echo $stats['unit_revenue'];
    } else {
        echo '340';
    }
    echo "'><br>
    <label class ='bd_statistics_label bottom_label'>Conversion%</label><label class ='dollar'>%</label><input id ='bd_stats_conv-perc' class ='bottom_input' name ='bd_conv_percent' value = '";
    if (isset($stats['conv'])) {
        echo $stats['conv'];
    } else {
        echo '10';
    }
    echo "'><br>
    <label class ='bd_statistics_label bottom_label'>Ads Charge</label><label class ='dollar'>%</label><input id ='bd_stats_ads-charge' class ='bottom_input' name ='bd_ads_charge' value = '";
    if (isset($stats['ads_charge'])) {
        echo $stats['ads_charge'];
    } else {
        echo '10';
    }
    echo "'>
    <div id = 'bd_set_stats_button'></div>
  </div>";
    //  print_r ( $stats );
    die();
}

function bd_update_ga_category()
{
    update_post_meta($_POST['post_id'], 'bd_ga_category', $_POST['value']);
    die();
}

function update_post_views()
{
    update_post_meta($_POST['post_id'], 'post_views_count', $_POST['views']);
    die();
}

function change_city_statistics_date_range()
{
    print_r($_POST);
}

function bd_save_seo_hack()
{
    update_post_meta($_POST['post_id'], '_yoast_wpseo_metadesc', $_POST['description']);
    update_post_meta($_POST['post_id'], '_yoast_wpseo_title', $_POST['title']);
    die();
}

function getDelaconStats($from, $to)
{

    define('BR_delacon_API_key', '660_8w2PZ6VhpWExPlozKg5faV1nT9gCbegGe6/IKUkyc1BmoQJTcfoJkSyizOn0/Ps7');
    define('BR_delacon_userid', 'Angelica.Brenton@adultpress.com.au');
    define('BR_delacon_password', 'ABadp!2');
    define('BR_delacon_API_link', 'http://vxml5.delacon.com.au/site/report/report.jsp');

    $from_d = explode('-', $from);
    $from_date = $from_d[2] . '-' . $from_d[1] . '-' . $from_d[0];
    $to_d = explode('-', $to);
    $to_date = $to_d[2] . '-' . $to_d[1] . '-' . $to_d[0];

    $fields = array(
        'userid' => 'Angelica.Brenton@adultpress.com.au',
        'Auth' => '660_8w2PZ6VhpWExPlozKg5faV1nT9gCbegGe6/IKUkyc1BmoQJTcfoJkSyizOn0/Ps7',
        'password' => 'ABadp!2',
        'datefrom' => $from_date,
        'dateto' => $to_date,
        'showcid' => 1
    );

//url-ify the data for the POST
    foreach ($fields as $key => $value) {
        $fields_string .= $key . '=' . $value . '&';
    }
    rtrim($fields_string, '&');
    $fields_string = str_replace('"', '', $fields_string);
//open connection
    $options = array(
        CURLOPT_RETURNTRANSFER => true, // return web page
        CURLOPT_HEADER => false, // don't return headers
    );

    $ch = curl_init();

//set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, 'http://vxml5.delacon.com.au/site/report/report.jsp');
    curl_setopt($ch, CURLOPT_POST, count($fields));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt_array($ch, $options);
//execute post
    ob_start();
    $doc = curl_exec($ch);
//close connection
    curl_close($ch);
    $result = ob_end_clean();

    $delaconResults = array();
    $delaconStats = array();
    $delaconFinal = array();

    $dom = new DOMDocument();
    $dom->loadXML($doc);
    $dom->preserveWhiteSpace = false;
    $i = 0;
    while (is_object($callingFlows = $dom->getElementsByTagName("CallingFlow")->item($i))) {
        foreach ($callingFlows->childNodes as $nodename) {
            if ($nodename->childNodes)
                foreach ($nodename->childNodes as $subNodes) {
                    $delaconResults[$i][$nodename->nodeName] = $nodename->nodeValue;
                }
        }
        $i++;
    }

    foreach ($delaconResults as $item) {
        $dateParts = explode(" ", $item['Time']);
        $formatted = array(
            'Dial From' => $item['IncomingCallNumber'],
            'Transferred' => $item['TransferredNumber'],
            'Date' => $dateParts[0],
            'Time' => $dateParts[1],
            'Result' => $item['Result'],
            'Business Name' => $item['DealerName'],
            'Business Category' => 'brothels.com.au',
            'Time(sec)' => $item['Duration'],
            'Search Engine,' => $item['SearchEngine']
        );
        $formatted['Time'] = substr($formatted['Time'], 0, 8);
        $delaconStats[$item['CompanyID']]['csv'][] = $formatted;
        $delaconStats[$item['CompanyID']]['total_calls'] = (array_key_exists('total_calls', $delaconStats[$item['CompanyID']]) ? ++$delaconStats[$item['CompanyID']]['total_calls'] : 1);

        if ($item['Result'] == 'No answer')
            $delaconStats[$item['CompanyID']]['missed_calls'] = (array_key_exists('missed_calls', $delaconStats[$item['CompanyID']]) ? ++$delaconStats[$item['CompanyID']]['missed_calls'] : 1);
    }

    function formatRowAsCsv($row)
    {
        $csvRow = implode(',', $row);
        $csvRow .= ',';
        return $csvRow;
    }

    foreach ($delaconStats as $key => $row) {
        $first = reset($row);
        $csvHeaders = implode(',', array_keys($first[0])) . "\n";
        $delaconStats[$key]['csv'] = $csvHeaders . implode("\n", array_map('formatRowAsCsv', $row['csv']));
    }

    return $delaconStats;
}

function bd_test()
{
    $index = 1;

    try{

        $from = strtotime($_POST['date_from']) - (60 * 60 * 24);
        $to = strtotime($_POST['date_to']);
        $parts = implode("','", $_POST['cities']);
        $cities = "('" . $parts . "')";
        $cityCollector = [];
        global $wpdb;
        foreach ($_POST['cities'] as $city) {
            $query = "select label_id,
        category_id,
        position,
        sum(case when action = 0 then 1 else 0 end) as website_clicks,
        sum(case when action = 1 then 1 else 0 end) as image_clicks,
        sum(case when action = 2 then 1 else 0 end) as phone_clicks,
        sum(case when action = 3 then 1 else 0 end) as page_visits
        FROM bd_analytics
        WHERE bd_analytics.date >= '${from}'
        AND bd_analytics.date <= '${to}'
        AND bd_analytics.category_id = '$city'
        AND bd_analytics.category_id != bd_analytics.label_id
        group by label_id";

            $wpdb->show_errors();
            $response = $wpdb->get_results($query, ARRAY_A);
            $cityCollector[$city] = $response;
        }

        $collector = array();
        foreach ($cityCollector as $city_id => $city) {
            foreach ($city as $item)
                $collector[$city_id . '-' . $item['label_id']] = $item;
        }       

        $table_name = $wpdb->prefix . 'bd_email_templates';
        $templates = $wpdb->get_results(
            "
    SELECT *
    FROM $table_name
    ", ARRAY_A
        );
        $i = 0;

        unset($templates[0]);

        function getObjectId($obj)
        {
            return $obj[id];
        }

        $templateIds = array_map("getObjectId", $templates);

        $compiledStats = array();            

        foreach ($collector as $key => $stats) { 
            $postmeta = get_post_meta($stats['label_id'], 'brothels', true);            
            if (isset($postmeta[0]))
                $postmeta = $postmeta[0];            
            $contact = get_post_meta($stats['label_id'], 'contact', true);            
            if (isset($contact[0]))
                $contact[0]['email'] = $contact[0]['Email 1'];
            $postmeta['contact'] = $contact;            
            $postmeta['brothel_id'] = $stats['label_id'];
            $postmeta['category_id'] = $stats['category_id'];
            $postmeta['template'] = (!$postmeta['template'] || !in_array($postmeta['template'], $templateIds) ? '2' : $postmeta['template']);
            $postmeta['templates'] = $templates;
            $postmeta['website_clicks'] = $stats['website_clicks'];
            $postmeta['image_clicks'] = $stats['image_clicks'];
            $postmeta['website_referrals'] = ($stats['website_clicks'] + $stats['image_clicks']);
            $postmeta['page_visits'] = $stats['page_visits'];
            $postmeta['phone_clicks'] = $stats['phone_clicks'];            
            $postmeta['position'] = ( $stats['category_id'] === '-1' ? 'Details Page' : get_the_title($stats['category_id']));
            $compiledStats[$key] = $postmeta;   
            
            $index += 1;
        }       

        $delaconStats = getDelaconStats($_POST['date_from'], $_POST['date_to']);

        $finalResult = array();
        foreach ($compiledStats as $key => $value) {
            $finalResult[$key] = (object)$value;
            $finalResult[$key]->csv = $delaconStats[$value['tracking_number']]['csv'];
            $finalResult[$key]->total_calls = (array_key_exists($value['tracking_number'], $delaconStats) ? $delaconStats[$value['tracking_number']]['total_calls'] : 0);
            $finalResult[$key]->missed_calls = (array_key_exists($value['tracking_number'], $delaconStats) ? $delaconStats[$value['tracking_number']]['missed_calls'] : 0);
        }
        $numeric = array_values($finalResult);
        $returnStats = (object)[
            "iTotalRecords" => count($finalResult),
            "iTotalDisplayRecords" => count($finalResult),
            "aaData" => $numeric
        ];        

        echo json_encode($returnStats);
    
        die();
    } catch (Error $e) {
        die ('Caught exception: ' .  $e->getMessage() . ' | Index ' . $index . ' | Stack Trace: ' . $e->getTraceAsString());
    }
}

function bd_edit_reporting_contact()
{
    $contact = array(
        'Name' => $_POST['name'],
        'Email 1' => $_POST['email'],
        'Mobile' => $_POST['mobile']
    );
    $contact_array = get_post_meta($_POST['brothel_id'], 'contact', true);
    $contact_array[0]['Name'] = $_POST['Name'];
    $contact_array[0]['Email 1'] = $_POST['email'];
    $contact_array[0]['Mobile'] = $_POST['Mobile'];
    $contact_array[0]['Phone'] = $_POST['Phone'];
    // update_post_meta($_POST['brothel_id'], 'brothels',$brothel);
    update_post_meta($_POST['brothel_id'], 'contact', $contact_array);
    if ($_POST['email'])
        echo $_POST['brothel_id'];
    die();
}

function bd_load_email_template_editor()
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'bd_email_templates';
    $id = $_POST['id'];
    $template = $wpdb->get_results(
        "
   SELECT *
   FROM $table_name
   WHERE id = $id
   ", ARRAY_A
    );
    $settings = array(
        'teeny' => true,
        'textarea_rows' => 15,
        'tabindex' => 1,
        'editor_class' => 'mceEditor',
        'tinymce' => array(
            height => 600
        )
    );
    echo wp_editor(wp_specialchars_decode(stripslashes($template[0]['html'])), 'bd_template_editor', $settings);
    _WP_Editors::editor_js();
    die();
}

function bd_edit_email_template()
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'bd_email_templates';
    $wpdb->update(
        $table_name, array(
        'html' => $_POST['html'], //Serialize our array into a string
    ), array('id' => $_POST['id']), array(
            '%s', //Accept a string
        )
    );
    $wpdb->show_errors();
    die();
}

function bd_create_template()
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'bd_email_templates';
    $wpdb->insert(
        $table_name, array(
        'name' => $_POST['name']
    ), array(
            '%s'
        )
    );
    echo "<button id = 'template-button-{$wpdb->insert_id}' class = 'template_button' data-id = '" . $wpdb->insert_id . "' data-name = '{$_POST['name']}'>{$_POST['name']}</button>
  <i id = 'delete-button-{$wpdb->insert_id}' class='material-icons delete_template' data-id = '{$wpdb->insert_id}' data-name = '{$_POST['name']}'>&#xE5CD;</i>";
    die();
}

function bd_delete_template()
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'bd_email_templates';
    $wpdb->delete($table_name, array('id' => $_POST['id']));
    die();
}

function bd_assign_brothel_template()
{
    $postmeta = get_post_meta($_POST['brothel_id'], 'brothels', true);
    $postmeta['template'] = $_POST['template_id'];
    update_post_meta($_POST['brothel_id'], 'brothels', $postmeta);
    $ret = array(
        brothel_id => $_POST['brothel_id'],
        template_id => $_POST['template_id']
    );
    echo json_encode($ret);
    die();
}

function create_csv($csv)
{

    require_once BD_PLUGIN_PATH . 'assets/plugins/PHPExcel/PHPExcel.php';

    $phpExcel = new PHPExcel();

    $sheet = $phpExcel->getActiveSheet();

    $csvData = explode(',', $csv);

    //Number of columns in the csv file
    $numberOfColumns = 9;

    //Column index;
    $index = 0;
    $columnIndex = 1;
    $letterIndex = 'A';
    $timeColumn = 'D';
    $dateColumn = 'C';

    foreach ($csvData as $data):
        if ($data == 'Time'):
            $timeColumn = $letterIndex;
        endif;
        if ($data == 'Date'):
            $dateColumn = $letterIndex;
        endif;
        if ($index == $numberOfColumns):
            $columnIndex++;
            $letterIndex = 'A';
            $index = 0;
        endif;

        $sheet->setCellValue($letterIndex . $columnIndex, trim($data));
        if ($letterIndex == $timeColumn && $columnIndex != 1):
            $sheet->setCellValue($letterIndex . $columnIndex, htmlentities(substr($data, 0, 8)));
        endif;
        if ($letterIndex == $dateColumn && $columnIndex != 1):
            $dateArray = explode('-', $data);
            $cleanArray = array($dateArray[2], $dateArray[1], $dateArray[0]);
            $sheet->setCellValue($letterIndex . $columnIndex, implode('/', $cleanArray));
        endif;
        $index++;
        ++$letterIndex;
    endforeach;

    //Set the column widths and alignments

    $letterIndex = 'A';

    for ($i = 0; $i < $numberOfColumns; $i++):
        $sheet->getColumnDimension($letterIndex)->setAutoSize(true);
        ++$letterIndex;
    endfor;

    $phpExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $uploadDir = wp_upload_dir();
    $dir = $uploadDir['path'];

//    if (!file_exists($dir)) {
//        mkdir($dir, 0775, true);
//    }

    $filepath = $dir . '/call-report.xls';

    $objWriter = PHPExcel_IOFactory::createWriter($phpExcel, "Excel5");
    //$objWriter->save($filepath);
    //   $fd = fopen($filepath, 'w');
//    fwrite($fd, $csv);

    return $filepath;
}

function create_pdf($values)
{
    $uploadDir = wp_upload_dir();
    $dir = $uploadDir['path'] . 'bd_temp/';

    if (!file_exists($dir)) {
        mkdir($dir, 0775, true);
    }
    $filepath = $dir . 'report.pdf';
    $pdf = require('generate-report-pdf.php');
    $fp = fopen($filepath, 'w');
    fwrite($fp, $pdf);
    fclose($fp);
    return $filepath;
}

function bd_send_reports()
{
    /* ---
     * We use php mailer directly so we can reuse the smtp connection
     * We use the smtp settings created by SMTP plugin
     */
    require_once('phpmailer.php');
    require_once('fpdf.php');
    global $wpdb;
    $table_name = $wpdb->prefix . 'bd_email_templates';
    $template_id = $_POST['data'][0]['template'];
    $templates = $wpdb->get_results("SELECT * FROM $table_name", ARRAY_A);
    $templatesById = [];
    foreach ($templates as $id => $template) {
        $templatesById[$template['id']] = stripslashes($template['html']);
    }
    $mailOptions = get_option("wp_smtp_options");
    $settings = get_option('bd_reporting_settings');
    $reply_to_email = (isset($settings['reply_to']) ? $settings['reply_to'] : $mailOptions['from']);
    $reply_to_name = (isset($settings['reply_to_name']) ? $settings['reply_to_name'] : $mailOptions['fromname']);
    $subject = (isset($settings['subject']) ? $settings['subject'] : 'Advertising report from brothels.com.au');
    $mail = new PHPMailer();
    //$mail->IsSMTP(); // telling the class to use SMTP
    $mail->SMTPAuth = true; // enable SMTP authentication
    $mail->SMTPKeepAlive = true; // SMTP connection will not close after each email sent
    $mail->Host = gethostbyname($mailOptions['host']); // sets the SMTP server
    $mail->Port = $mailOptions['port'];                    // set the SMTP port for the server
    $mail->Username = $mailOptions['username'];  // SMTP account username
    $mail->Password = $mailOptions['password'];        // SMTP account password
    $mail->SetFrom($reply_to_email, $reply_to_name);
    $mail->IsHTML(true);
    $mail->AddReplyTo($reply_to_email, $reply_to_name);
    if (isset($settings['bcc']))
        $mail->AddBCC($settings['bcc'], "Brothels.com.au");
    $mail->Subject = $subject;
    foreach ($_POST['data'] as $brothel) {
        $variables = [];
        foreach ($brothel as $k => $v) {
            $variables['%%' . $k . '%%'] = $v;
        }
        $emailAddress = (isset($settings['test_mode']) ? $settings['test_email'] : $brothel['contact'][0]['email']);
        $message = strtr($templatesById[$brothel['template']], $variables);
        $message = str_replace(chr(194), "", $message);
        $footer = str_replace(chr(194), "", $templatesById['1']);
        $replace = array("'", "-", "-", "-");
        $search = array('&#8217;', '&ndash;', '&#x02013;', '&#8211;');
//          $headers = array('Content-Type: text/html; charset=UTF-8','From: Brothels.com.au <contact@brothels.com.au');
        if (isset($brothel['csv']) && !empty($brothel['csv'])):
            $csv = create_csv($brothel['csv']);
        endif;
        $values = array(
            brothel_name => htmlspecialchars_decode(html_entity_decode(str_replace($search, $replace, $brothel['name']), ENT_QUOTES | ENT_XML1, 'UTF-8')),
            date_range => 'From ' . $_POST['date_range'][0] . ' to ' . $_POST['date_range'][1],
            website_referrals => ( $brothel['website_clicks'] + $brothel['image_clicks']),
            page_visits => $brothel['page_visits'],
            total_calls => $brothel['total_calls'],
            missed_calls => $brothel['missed_calls'],
            position => ($brothel['position']) ? htmlspecialchars_decode(html_entity_decode($brothel['position'])) : ''
        );
        $pdf = create_pdf($values);
        echo $pdf;
        $attachments = $pdf;
        $uploads = wp_upload_dir();
        $file = $uploads['basedir'] . '/brothels.com.au.jpg'; //phpmailer will load this file
        $mail->AddEmbeddedImage($file, 'logo', 'brothels.com.au.jpg');
        $mail->AltBody = "To view the message, please use an HTML compatible email viewer. Contact Brothels.com.au for more information.";
        $mail->MsgHTML($message . $footer);
        $mail->AddAttachment($pdf);
        if (isset($brothel['csv']) && $brothel['csv'] != ''):
            $mail->AddAttachment($csv);
        endif;
        $mail->AddAddress($emailAddress);
        if (!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo 'Message has been sent to ' . $emailAddress;
        }
        $mail->ClearAllRecipients();
        $table_name = $wpdb->prefix . 'bd_reporting_stats';
        $wpdb->insert(
            $table_name, array(
            'date' => strtotime(date('d-m-Y')),
            'user_id' => get_current_user_id(),
            'template' => $brothel['template'],
            'brothel_id' => $brothel['brothel_id'],
            'position' => $brothel['category_id'],
            'website_referrals' => $brothel['website_referrals'],
            'missed_calls' => sanitize_text_field($brothel['missed_calls']),
            'total_calls' => sanitize_text_field($brothel['total_calls']),
            'page_visits' => sanitize_text_field($brothel['page_visits']),
            'email' => $emailAddress,
            'date_range' => sanitize_text_field($values['date_range'])
        ), array(
                '%d',
                '%d',
                '%d',
                '%d',
                '%s',
                '%d',
                '%d',
                '%d',
                '%d',
                '%s',
                '%s',
            )
        );
        $wpdb->show_errors();
    };
    $mail->SmtpClose();
    wp_die();
}

function bd_get_reporting_stats()
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'bd_reporting_stats';
    $brothel_id = $_POST['brothel_id'];
    $category_id = $_POST['category_id'];
//  echo $category_id;
    $stats = $wpdb->get_results(
        "
   SELECT *
   FROM $table_name
   WHERE brothel_id = '${brothel_id}'
   AND position = '${category_id}'
   ", ARRAY_A
    );
    $collector = array();
    foreach ($stats as $key => $stat) {
        $user = get_user_by('id', $stat['user_id']);
        $collector[$key] = $stat;
        $collector[$key]['page'] = get_the_title($stat['position']);
        $collector[$key]['date'] = date('d-m-Y', $stat['date']);
        $collector[$key]['user_id'] = $user->data->user_nicename;
    }
    echo json_encode($collector);
    die();
}

function bd_save_reporting_settings()
{
    unset($_POST['action']);
    update_option('bd_reporting_settings', $_POST);
    echo json_encode($_POST);
    die();
}

add_action('wp_ajax_bd_add_brothel', 'bd_add_brothel');
add_action('wp_ajax_bd_add_note', 'bd_add_note');
add_action('wp_ajax_bd_add_contact', 'bd_add_contact');
add_action('wp_ajax_bd_edit_contact', 'bd_edit_contact');
add_action('wp_ajax_add_brothels_to_page', 'add_brothels_to_page');
add_action('wp_ajax_bd_add_extra_description', 'bd_add_extra_description');
add_action('wp_ajax_populate_description_selects', 'populate_description_selects');
add_action('wp_ajax_bd_remove_brothel_from_page', 'bd_remove_brothel_from_page');
add_action('wp_ajax_bd_rearrange_brothels', 'bd_rearrange_brothels');
add_action('wp_ajax_populate_assign_top_spots', 'populate_assign_top_spots');
add_action('wp_ajax_populate_assign_top_6', 'populate_assign_top_6');
add_action('wp_ajax_add_image_city_screen', 'add_image_city_screen');
add_action('wp_ajax_confirm_assign_top_spot', 'confirm_assign_top_spot');
add_action('wp_ajax_output_transaction_details', 'output_transaction_details');
add_action('wp_ajax_bd_remove_brothels_from_top', 'bd_remove_brothels_from_top');
add_action('wp_ajax_set_advertiser_count', 'set_advertiser_count');
add_action('wp_ajax_nopriv_bd_link_action', 'bd_link_action');
add_action('wp_ajax_bd_link_action', 'bd_link_action');
add_action('wp_ajax_bd_populate_statistics', 'bd_populate_statistics');
add_action('wp_ajax_bd_set_statistics', 'bd_set_statistics');
add_action('wp_ajax_bd_get_brothels_stats', 'bd_get_brothels_stats');
add_action('wp_ajax_bd_get_city_clicks', 'bd_get_city_clicks');
add_action('wp_ajax_bd_get_city_revenue', 'bd_get_city_revenue');
add_action('wp_ajax_bd_update_notification_settings', 'bd_update_notification_settings');
add_action('wp_ajax_bd_email_settings', 'bd_email_settings');
add_action('wp_ajax_bd_adjust_ga_stats', 'bd_adjust_ga_stats');
add_action('wp_ajax_save_bottom_content', 'save_bottom_content');
add_action('wp_ajax_bd_city_populate_statistics', 'bd_city_populate_statistics');
add_action('wp_ajax_bd_city_populate_detailed_statistics', 'bd_city_populate_detailed_statistics');
add_action('wp_ajax_bd_update_ga_category', 'bd_update_ga_category');
add_action('wp_ajax_save_mobile_banner', 'save_mobile_banner');
add_action('wp_ajax_update_post_views', 'update_post_views');
add_action('wp_ajax_change_city_statistics_date_range', 'change_city_statistics_date_range');
add_action('wp_ajax_bd_save_seo_hack', 'bd_save_seo_hack');
add_action('wp_ajax_bd_get_reports_list', 'bd_get_reports_list');
add_action(' wp_ajax_reporting_edit_contact', 'reporting_edit_contact');
add_action('wp_ajax_bd_edit_reporting_contact', 'bd_edit_reporting_contact');
add_action('wp_ajax_bd_test', 'bd_test');
add_action('wp_ajax_bd_load_email_template_editor', 'bd_load_email_template_editor');
add_action('wp_ajax_bd_edit_email_template', 'bd_edit_email_template');
add_action('wp_ajax_bd_create_template', 'bd_create_template');
add_action('wp_ajax_bd_delete_template', 'bd_delete_template');
add_action('wp_ajax_bd_assign_brothel_template', 'bd_assign_brothel_template');
add_action('wp_ajax_bd_send_reports', 'bd_send_reports');
add_action('wp_ajax_bd_get_reporting_stats', 'bd_get_reporting_stats');
add_action('wp_ajax_bd_save_reporting_settings', 'bd_save_reporting_settings');
?>