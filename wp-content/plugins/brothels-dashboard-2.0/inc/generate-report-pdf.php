<?php
define('FPDF_FONTPATH','fonts');

$pdf = new FPDF('L','mm','A5');
$pdf->SetMargins(0,0,0,0);
$pdf->SetAutoPageBreak(false, 0 );
$pdf->SetFillColor(34,34,34);
$pdf->AddPage('L', 'A5' );
$pdf->Rect(0, 0, 10000, 10000, 'F');
$pdf->AddFont('Raleway','', 'Raleway-Medium.php');
$pdf->AddFont('Canon','', 'IMFeFCit28P.php');
$pdf->AddFont('Roboto','', 'RobotoSlab-Regular.php');
$pdf->SetFont('Raleway');
$pdf->SetFontSize(16);
$pdf->SetTextColor(255,255,255);
$pdf->Cell( 0, 10, '',0, 2, 'C', true );
$pdf->Cell( 0, 10, 'Performance Report',0, 2, 'C', true );
$pdf->SetFont('Canon');
$pdf->SetFontSize(26);
$pdf->Cell( 0, 15, $values['brothel_name'],0, 2, 'C', true );
$pdf->SetFillColor(132,18,18);
$pdf->Cell( 0, .5, '',0, 2, 'C', true );
$pdf->SetFillColor(34,34,34);
$pdf->Cell( 0, 2, '',0, 2, 'C', true );
$pdf->SetFont('Roboto');
$pdf->Cell( 0, 2, '',0, 2, 'C', true );
$pdf->SetFontSize(18);
$pdf->Cell( 0, 10, $values['position'], 0, 2, 'C', true );
$pdf->SetFontSize(14);
$pdf->SetTextColor(69,220,192);
$pdf->Cell( 0, 10, $values['date_range'], 0, 2, 'C', true );
$pdf->Cell( 0, 0, '',0, 2, 'C', true );
$pdf->SetFont('Roboto');
$pdf->SetTextColor(214,15,15);
$pdf->SetFontSize(18);
$pdf->Cell( 81, 15, $values['website_referrals'], 'L', 0, 'R', true );
$pdf->SetTextColor(255,255,255);
$pdf->Cell( 5, 13, '', 0, 0, 'L', true );
$pdf->Cell( 0, 13, 'Website Referrals', 0, 1, 'L', true );
if ( isset( $values['total_calls'] ) &&  $values['total_calls'] != 0 ){
  $pdf->SetTextColor(214,15,15);
  $pdf->Cell( 81, 13, $values['total_calls'], 'L', 0, 'R', true );
  $pdf->SetTextColor(255,255,255);
  $pdf->Cell( 5, 13, '', 0, 0, 'L', true );
  $pdf->Cell( 0, 13, 'Total Calls', 0, 1, 'L', true );
}
else {
    $height = 13;
}
if ( isset( $values['missed_calls'] ) && $values['missed_calls'] != 0 ){
  $pdf->SetTextColor(214,15,15);
  $pdf->Cell( 81, 13, $values['missed_calls'], 'L', 0, 'R', true );
  $pdf->SetTextColor(255,255,255);
  $pdf->Cell( 5, 13, '', 0, 0, 'L', true );
  $pdf->Cell( 0, 13, 'Missed Calls', 0, 1, 'L', true );
}
else {
    $height += 13;
}
if ( isset( $values['page_visits'] ) && $values['page_visits'] != 0 ){
    $pdf->SetTextColor(214,15,15);
    $pdf->Cell( 81, 13, $values['page_visits'], 'L', 0, 'R', true );
    $pdf->SetTextColor(255,255,255);
    $pdf->Cell( 5, 13, '', 0, 0, 'L', true );
    $pdf->Cell( 0, 13, 'Page Visits', 0, 1, 'L', true );
}
else {
    $height += 13;
}
    $pdf->SetTextColor(214,15,15);
    $pdf->Cell( 81, $height, '', 'L', 0, 'R', true );
    $pdf->SetTextColor(255,255,255);
    $pdf->Cell( 5, $height, '', 0, 0, 'L', true );
    $pdf->Cell( 0, $height, '', 0, 1, 'L', true );
$pdf->Cell( 0, 10, '',0, 1, 'C', true );
$pdf->SetFillColor(17,17,17);
$pdf->Cell( 0, 1000, '',0, 1, 'C', true );
$pdf->Image('http://brothels.com.au/wp-content/uploads/2014/03/LOGO.png',75,125,60,0,'PNG', 'http://brothels.com.au');
return $pdf->Output('S', $filepath, true );
?>
