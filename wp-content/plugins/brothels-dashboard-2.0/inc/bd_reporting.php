<?php
/**
 * Creates output for the 'Main dash page' in the brothels Dashboard
 * Required by the create function for this page
 * By Che' Jansen- Chillidee Marketing group
 */
/** WordPress Administration Bootstrap */
require_once( ABSPATH . 'wp-load.php' );
require_once( ABSPATH . 'wp-admin/admin.php' );
require_once( ABSPATH . 'wp-admin/admin-header.php' );

$settings = get_option('bd_reporting_settings');
if (isset($settings['test_mode']))
    echo '<div id = "test_mode_on">Test mode is currently on.  All emails will be sent to ' . $settings['test_email'] . '.
To turn test mode off click on the settings icon below</div>';
?>
<h1>Reporting</h1>
<?php
define('BR_PLUGIN_PATH', plugin_dir_path(__FILE__));
define('BR_PLUGIN_URL', plugin_dir_url(__FILE__));

add_action('activated_plugin', 'BR_save_error');

function BR_save_error() {
    file_put_contents(ABSPATH . 'wp-content/plugins/brothels-report/error.html', ob_get_contents());
}

function BR_date_to_mysql($date) {
    $arr = explode('/', $date);
    $MySQLdate = $arr[2] . '-' . $arr[1] . '-' . $arr[0];
    return $MySQLdate;
}
?>
<?php
$city_names = array();
$args = array(
    'posts_per_page' => 500,
    'orderby' => 'name',
    'order' => 'ASC',
    'post_type' => 'city',
    'post_status' => 'publish'
);
foreach (get_posts($args) as $value) {
    $city_names[$value->ID] = $value->post_title;
}
?>

<select id="bd_report_categories">
    <option value="-1">Details Page</option>
    <option value="-2">Front Page</option>
    <?php
    foreach ($city_names as $id => $name) {
        echo '<option value = "' . $id . '">' . $name . '</option>';
    }
    ?>
</select>

<div id = 'setting_section'>
    <i class="material-icons" id = 'close_settings_overlay'>&#xE5CD;</i>
    <h3>Settings</h3>
    <form id = 'bd_reporting_settings_form'>
        <input type = 'hidden' name = 'action' value = 'bd_save_reporting_settings'>
        <input type = 'checkbox' name = 'test_mode'<?php if (isset($settings['test_mode'])) echo 'checked'; ?>>
        <label id = 'test_mode'>Test Mode ON - all emails will be sent to testing email address if this is checked</label></br>
        <label>Test Email</label>
        <input type = 'text' name = 'test_email' value = '<?php if (isset($settings['test_email'])) echo $settings['test_email']; ?>'></br>
        <label>BCC email - if this is checked, all emails will be BCC'd to this email address</label>
        <input type = 'text' name = 'bcc' value = '<?php if (isset($settings['bcc'])) echo $settings['bcc']; ?>'></br>
        <label>Email Subject</label>
        <input type = 'text' name = 'subject' value = '<?php
        if (isset($settings['subject'])) {
            echo $settings['subject'];
        } else {
            echo 'Advertising report from brothels.com.au';
        }
        ?>'>
        <label>This will override the reply to email set in the SMTP plugin</label>
        <input type = 'text' name = 'reply_to' value = '<?php if (isset($settings['reply_to'])) echo $settings['reply_to']; ?>'></br>
        <label>This will override the reply to name set in the SMTP plugin</label>
        <input type = 'text' name = 'reply_to_name' value = '<?php if (isset($settings['reply_to_name'])) echo $settings['reply_to_name']; ?>'></br>
    </form>
    <button class="blue_button" id="save_settings">
        <div id="save_settings_text">Save Changes</div>
        <div class="loader white" id="saving_settings">
            <svg class="circular" viewBox="25 25 50 50">
            <circle class="white" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
            </svg>
        </div>
    </button>
    <h3>SMTP setting</h3>
    This plugin uses the setting created by the SMTP plugin on the brothels admin area.  To change the SMTP host, please edit the settings in this plugin.

</div>


<div id = 'template_edit_section'>
    <i class="material-icons" id = 'close_template_overlay'>&#xE5CD;</i>
    <div id = 'loading_overlay' class = 'template_loading'></div>
    <div id = 'create_template_overlay' class ='template_overlay'>
        <h3>Enter a name for your new template:</h3>
        <input type = 'text' id = 'new_template_name'>
        <button id = 'save_new_template' class = 'blue_button'>Save</button>
    </div>
    <div id = 'delete_template_overlay' class = 'template_overlay'>
        <h3>Are  you sure you want to delete <span id = 'delete_template_name'></span>?  Any brothel using this template will be reset to the default.</h3>
        <input type = 'hidden' id = 'delete_template_id'>
        <button id = 'cancel_delete_template' class = 'blue_button'>Cancel</button>
        <button id = 'delete_template' class = 'blue_button'><span id = 'deleting_template_text'>Delete</span>
            <div class='loader white' id ='deleting_template'>
                <svg class='circular' viewBox='25 25 50 50'>
                <circle class='white' cx='50' cy='50' r='20' fill='none' stroke-width='2' stroke-miterlimit='10'/>
                </svg>
            </div>
        </button>
    </div>
    <div class = 'perc_50'>
        <h3>Edit Templates</h4>
            <i class="material-icons" id = "create_template" title = 'Create New Template'>&#xE147;</i>
            <i class="material-icons" id = "bd_help" title = "help">&#xE887;</i>
            <div id = 'help_section'>
                <h3>Editing your templates</h3>
                You can edit a template by selecting it on the left, and using the text area on the right to edit.  The emails will be sent in HTML, so any styles applied here will work.<br><br>
                There are two templates that will be created on plugin installation- Default and footer.  These templates cannot be deleted.  The footer template will be added to the bottom of any template selected.
                The default template will be used whenever a brothel has no template selected.
                <h4>Available variables-</h4>
                %%address%% - Brothels address <br>
                %%website_referrals%% - Total Website Referrals<br>
                %%name%% - The brothels name<br>
                %%phone%% - Phone number we're sending calls to<br>
                %%position%% - Page on brothels.com.au this report refers to<br>
                %%total_calls%% - Total referred calls<br>
                %%missed_calls%% Total missed Calls<br>
                %%website_readable%% - Website we're sending traffic to<br><br>
                You can add the brothels logo to the footer like this:
                <span style="color: #444444; font-family: Monstserrat; font-size: 18px; line-height: 18.2px;">&lt;a href ="http://brothels.com.au"&gt;&lt;img alt="brothels.com.au" src="cid:logo"&gt;&lt;/a&gt;</span>
            </div>


            <div id = 'template_button_holder'>
                <?php
                global $wpdb;
                $table_name = $wpdb->prefix . 'bd_email_templates';
                $templates = $wpdb->get_results(
                        "
         SELECT *
         FROM $table_name
         ", ARRAY_A
                );
                $i = 0;
                foreach ($templates as $template) {
                    echo "<button id = 'template-button-{$template['id']}' class = 'template_button' data-id = '{$template['id']}' data-name = '{$template['name']}' data-html = '" . json_encode($template['html']) . "'>{$template['name']}</button>";
                    if ($i > 1)
                        echo "<i id = 'delete-button-{$template['id']}' class='material-icons delete_template' data-id = '{$template['id']}' data-name = '{$template['name']}'>&#xE5CD;</i>";
                    $i++;
                }
                ?>
            </div>
    </div>
    <div class = 'perc_50 editor_section'>
        <h3>Currently Editing <span id = 'current_template'><?php echo $templates[0]['name']; ?></span></h3>
        <input type = 'hidden' id = 'bd_current_template_id' value = '<?php echo $templates[0]['id']; ?>'>
        <button class = 'blue_button' id = 'save_template'>
            <div id = 'save_template_text'>Save Changes</div>
            <div class='loader white' id ='saving_template'>
                <svg class='circular' viewBox='25 25 50 50'>
                <circle class='white' cx='50' cy='50' r='20' fill='none' stroke-width='2' stroke-miterlimit='10'/>
                </svg>
            </div>
        </button>
        <div id = 'template_wp_edior'>
            <?php
            $settings = array(
                'teeny' => true,
                'textarea_rows' => 15,
                'tabindex' => 1,
                'editor_class' => 'mceEditor',
                'tinymce' => array(
                    'height' => 600
                )
            );
            wp_editor(wp_specialchars_decode(stripslashes($templates[0]['html'])), 'bd_template_editor', $settings);
            _WP_Editors::editor_js();
            ?>
        </div>
    </div>

</div>

<i class="material-icons" id ='bd_reporting_settings' title = 'Manage settings'>&#xE8B8;</i>
<button id = 'edit_templates'>Edit Templates</button>

<input type="text" name="bd_report_from" id="bd_report_from" placeholder="From">
<input type="text" name="bd_report_to" id="bd_report_to" placeholder="To">

<input hidden type="text" id="bd_report_table_from">
<input hidden type="text" id="bd_report_table_to">

<i id ='bd_load_report' class="material-icons">&#xE876;</i>

<hr>
<div id="br_report_list">
    <table id = 'bd_reporting_table' class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th id = 'rt_selected'><input id ='rt_checkall' type ='checkbox'></input></th>
                <th id = 'rt_title'>Title</th>
                <th id = 'rt_position'>Page</th>
                <th id = 'rt_clicks'>Clicks</th>
                <th id = 'rt_images'>Images</th>
                <th id = 'rt_phone'>Phone</th>
                <th id = 'rt_calls'>Total Calls</th>
                <th id = 'rt_missed'>Missed Calls</th>
                <th id = 'rt_page_visits'>Page Visits</th>
                <th id = 'rt_template'>Template</th>
                <th id = 'rt_download'>Download Csv</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th><input type ='checkbox'></input></th>
                <th>Title</th>
                <th>Clicks</th>
                <th>Images</th>
                <th>Phone</th>
                <th>Total Calls</th>
                <th>Missed Calls</th>
                <th>Page Visits</th>
                <th>Template</th>
                <th>Download Csv</th>
            </tr>
        </tfoot>
    </table>

</div>
