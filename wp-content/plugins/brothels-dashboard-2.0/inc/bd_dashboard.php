<?php
/**
 * Creates output for the 'Main dash page' in the brothels Dashboard
 * Required by the create function for this page
 * By Che' Jansen- Chillidee Marketing group
 */

/** WordPress Administration Bootstrap */
require_once( ABSPATH . 'wp-load.php' );
require_once( ABSPATH . 'wp-admin/admin.php' );
require_once( ABSPATH . 'wp-admin/admin-header.php' );
?>
<h1>Brothels Dashboard</h1>
<div id ='brothels_dashboard'>
    <div id ='bd_main-section'>
<?php function bd_admin_tabs( $current = 'transactions' ) {
          $tabs = array( 'transactions' => 'Transactions', 'settings' => 'Settings', 'ga-data' =>'Google data' );
          echo '<div id="icon-themes" class="icon32"><br></div>';
          echo '<h2 class="nav-tab-wrapper">';
          foreach( $tabs as $tab => $name ){
             $class = ( $tab == $current ) ? ' nav-tab-active' : '';
          echo "<a class='bd-nav nav-tab$class' data-tab='" . $tab . "'>$name</a>";

    }
    echo '</h2>';
}?>
<?php bd_admin_tabs();?>
      <div class ='tab active' id ='transactions'>
             <?php     
    global $wpdb;
    $table_name = $wpdb->prefix . 'bd_transaction_table';
    date_default_timezone_set("Australia/Sydney");
    $transactions = $wpdb->get_results( 
           "
              SELECT *
              FROM $table_name
      ", ARRAY_A
    );?><label class ='date-label'>Sort by date</label>
        <input id ='bd_date_from' class ='date_picker'placeholder ='Sort from date'>
        <input id ='bd_date_to' class ='date_picker' placeholder ='Sort to date'>
        <table class ='bd_table' id ='bd_trans_dash_table'>
            <thead>
               <tr>
                  <th data-sort = 'date'>Date</th>
                  <th data-sort = 'amount'>Amount</th>
                  <th data-sort = 'user'>Processed by</th>
                  <th data-sort = 'page'>In page</th>
                  <th data-sort = 'brothel'>Brothel</th>
                  <th data-sort = 'position'>Position</th>
                   <th data-sort = 'type'>Type</th>
                </tr>
          </thead>
          <tfoot>
               <tr>
                  <th data-sort = 'date'>Date</th>
                  <th data-sort = 'amount'>Amount</th>
                  <th data-sort = 'user'>Processed by</th>
                  <th data-sort = 'page'>In page</th>
                  <th data-sort = 'brothel'>Brothel</th>
                  <th data-sort = 'position'>Position</th>
                   <th data-sort = 'type'>Type</th>
                </tr>
          </tfoot>
            <tbody>
          <?php foreach ( $transactions as $transaction ) {
?>
               <tr>
                 <td><?php echo date('d-m-Y', $transaction['date']);?></td>
                <td>$<?php echo $transaction['trans_amount'];?></td>
                <td><?php $user_info = get_userdata($transaction['user_id']);
                          if ( $user_info ) echo $user_info->user_login;?></td>
                <td><?php echo get_the_title( $transaction['page_id']);?></td>
                <td><?php echo get_the_title( $transaction['brothel_id']);?></td>
                <td><?php echo $transaction['pos_no'];?></td>
                <td><?php echo $transaction['transaction_type'];?></td>
               </tr>
<?php }
?>
        </tbody>
        </table>
      </div>
      <div id ='settings' class ='tab'>
        <h3 class ='bd_dash_title'>Notifications settings</h3>
        Send notifications <input type ='text' value ='<?php echo get_option( 'notification_times' ); ?>' id ='bd_notification_times'> Days before brothel expiry
        <div id ='bd_set_notifications'></div><br>
        Send notifications to <input type ='text' value ='<?php echo get_option( 'bd_notification_email' ); ?>' id ='bd_notification_email'>
        <div id ='bd_set_notification_email'></div><br><br>
        What the different notification statuses mean.
        <div id ='bd_explain_notifications'></div>
      </div>
      <div id ='ga-data' class ='tab'>
        <h3 class ='bd_dash_title'>Import Google Analytics data</h3>
        <div id ='bd_ga_error_section'></div>
        <form id ='bd_import_ga_form'>
            <input type ='hidden' name ='action' value ='bd_adjust_ga_stats'>
            <input type ='hidden' name ='security' value ='<?php echo 
                 wp_create_nonce( 'ga_stats_nonce' );?>'>
          <?php /*
          $events = array(
                 '0' => 'Website Link',
                 '1' => 'Image Link',
                 '2' => 'Phone'
  );
          echo " <label class ='bd_ga_label'>Event</label>
                    <select name ='bd_ga_event_type' id ='ga_event'>
                    <option value ='null' selected>Please Select</option>";
             foreach ( $events as $event =>$name ) {
               echo "<option value = '" . $event . "'>" . $name . "</option>";
             }?>
          </select>
            <?php /*
             <label class ='bd_ga_label'>Position</label>
             <select name ='bd_ga_position' id ='ga_position'>
             <option value ='null' selected>Please Select</option>";
         <?php  for ($x = 1; $x <= 15; $x++) {
                  echo "<option value = '" . $x . "'>" . $x . "</option>";
         }   ?>        
  </select><br><br>
            <label class ='bd_ga_label'>Brothel</label>
            <select name ='bd_ga_brothel_id'>
            <option value ='null' selected>Please Select</option>
            <?php //Get all the brothels
            $type = 'brothel';
            $args=array(
              'post_type' => $type,
              'post_status' => 'publish',
              'posts_per_page' => -1,
              'orderby' => 'title',
              'order' => 'ASC'
            );
            $brothels = null;
            $brothel_array = array();
            $brothels = new WP_Query($args);
            if( $brothels ->have_posts() ) {
                while ($brothels->have_posts()) : $brothels->the_post(); 
                    $brothel_array[] = get_the_id();
                endwhile;
            };
            wp_reset_query();  // Restore global post data stomped by the_post().
            foreach ( $brothel_array as $brothel ) {
                  $brothel_data = get_post_meta ( $brothel, 'brothels', true );
              if ( isset( $brothel_data['ga_name']) && $brothel_data['ga_name'] != '' ) {
                      $name = $brothel_data['ga_name']; 
              }
              else { 
                   $name = $brothel_data['name'];
              }
              echo "<option value ='" . $brothel . "'>" . $name . "</option>";
            }?>
  </select>*/?>
           <label class ='bd_ga_label'>Page</label>
            <select name ='bd_ga_city_id'>
                <option value ='null' selected>Please Select</option>
            <?php //Get all the brothels
            $type = 'city';
            $args=array(
              'post_type' => $type,
              'post_status' => 'publish',
              'posts_per_page' => -1,
            );
            $brothels = null;
            $brothel_array = array();
            $brothels = new WP_Query($args);
            if( $brothels ->have_posts() ) {
                while ($brothels->have_posts()) : $brothels->the_post(); 
                    $city_array[] = get_the_id();
                endwhile;
            };
            wp_reset_query();  // Restore global post data stomped by the_post().
            foreach ( $city_array as $city ) {
              echo "<option value ='" . $city  . "'>" . get_the_title( $city ) . "</option>";
            }
           echo '</select>';?>
           <br><br>
            <label class ='bd_ga_label'>Enter Ga data here as Csv.  See Scotty or Che' for more information</label><br>
            <textarea name ='bd_ga_csv'></textarea><br>
            <button class="bd_button next-button" id="bd_ga_submit">Import now</button>
        </form>
        
      </div>
        
  </div>
  <div id ='bd_notifications_section'>
    <h2>Notifications</h2>
     <?php  $notifications =  get_option( 'bd_notifications', array() );
  if ( empty ( $notifications ) ) {
    echo '<h2>Congratulations, there are no notifications</h2>';
  } else {
  $sorted_notifications = array();
  foreach ( $notifications as $city => $arr ) {
        foreach ( $arr as $brothel => $state ) {
            if ( $state == 0 ) {
                  $sorted_notifications[$city][$brothel] = $state;
            }
     }
  }
    foreach ( $notifications as $city => $arr ) {
        foreach ( $arr as $brothel => $state ) {
            if ( $state == 1 ) {
                  $sorted_notifications[$city][$brothel] = $state;
            }
     } 
    }
  foreach ( $sorted_notifications as $city => $arr ) {
        foreach ( $arr as $brothel => $state ) {
              $class = ( $state == '1' ? 'bd_expiring' : 'bd_expired' );
          if ( $state == 1 ) {
          echo "<a href ='/wp-admin/post.php?post=" . $city . "&action=edit' class ='bd_not'>
                         <div class = '" . $class . "'>" . get_the_title( $city ) . '- ' . get_the_title( $brothel ) ." expiring soon.</div>
                </a>";
        }
          else {
            echo "<a href ='/wp-admin/post.php?post=" . $city . "&action=edit'>
                         <div class = '" . $class . "'>" . get_the_title( $city ) . '- ' . $brothel .  " is empty.</div>
                  </a>";
        }
        }
  }
  }?>
</div>

  </div>
