<?php

function getBrothelUrl($guid)
{
    $guid = str_replace("http://dev.brothels.com.au", "", $guid);
    $guid = str_replace("http://staging.brothels.com.au", "", $guid);
    return $guid;
}

function getPhoneNumber($brothel_data, $id, $k)
{
    $content = '';
    if (isset($brothel_data['phone']) && $brothel_data['phone'] != ''):

        if (isset($brothel_data['tracking_number']) && !empty($brothel_data['tracking_number'])):
            $content .= "<input type='hidden' class='cid_number' value='{$brothel_data['tracking_number']}'></input>";
        endif;
        $content .= "<a href='";
        $content .= (isset($brothel_data['tracking_number']) && !empty($brothel_data['tracking_number'])) ? '#' : 'tel:' . str_replace(" ", "", $brothel_data['phone']);
        $content .= "' data-action='Phone' rel='nofollow' data-number='" . htmlentities($brothel_data['phone']) . "' class='bd_brothel_tracking_number' style='text-decoration: none'";
        $content .= ' onClick="';
        $content .= (isset($brothel_data['tracking_number']) && !empty($brothel_data['tracking_number'])) ? "javascript:makePhoneCall('{$brothel_data['tracking_number']}','{$brothel_data['phone']}');return false;" : "";
        $content .= '"';
        $content .= "><span class = 'bd_phone_span' id='numdiv_{$brothel_data['tracking_number']}_0' style='color: #a21515;;'>{$brothel_data['phone']}</span></a>";

    endif;
    return $content;
}

function getWebsite($brothel, $id, $brothel_data, $position ){
    $category = get_the_title($id);
    $label = $position . '.' . get_the_title($brothel->ID);
    return "<a class='bd_action_link' rel='nofollow' data-action='Website Link' data-position = '{$position}' data-label_id='{$brothel->ID}' data-label = '{$label}' data-category_id='{$id}' data-category = '{$category}' data-action='Website Link' href='{$brothel_data['website']}' target='_blank'>{$brothel_data['website_readable']}</a>";
}

function getWrapperClass($image, $description ){
    $thumbnailClass = ( $image != '' ? 'has-post-thumbnail ' : '');
    $shopClass = ( $description != '' ? 'product-card ' : 'no-desc ');
    $content = "<div class= '" . $thumbnailClass . $shopClass . "shop-item hover-effect-1 product type-product status-publish product_cat-sydney  instock taxable shipping-taxable purchasable product-type-simple has-images'>";
    return $content;
}

function backup($image, $description ){
    $thumbnailClass = ( $image != '' ? 'has-post-thumbnail ' : '');
    $shopClass = ( $description != '' ? 'shop-item ' : 'no-desc ');
    $content = "<div class= '" . $thumbnailClass . $shopClass . "hover-effect-1 product type-product status-publish product-card instock taxable shipping-taxable purchasable product-type-simple has-images'>";
    return $content;
}

function generateAdvertisingSpot($brothel, $brothel_data, $description, $id, $k, $advertising_spot, $brothel_image = '', $brothel_image_mobile = '')
{
    $content = "<div class='item-column image-ad col-lg-4 col-md-4 col-sm-6 col-xs-12'>";
    $content .= "<input type='hidden' name='post_id' value='" . $brothel->ID . "'/>";
    $content .= getWrapperClass($brothel_image, $description);
    $content .= "<div class='item-image'>";
    if ( $brothel_image != '' ):
    $content .= "<a href='$brothel->guid' class='woocommerce-LoopProduct-link bd_action_link'>";
    $content .= "<img width='100%' class='shop-image lazy-load-shop-image attachment-shop-thumb size-shop-thumb city-desktop-brothel-image' alt='{$brothel_image['alt']}' src='{$brothel_image['url']}'>";
    $content .= "<img width='100%' class='shop-image lazy-load-shop-image attachment-shop-thumb size-shop-thumb city-mobile-brothel-image' alt='{$brothel_image_mobile['alt']}' src='{$brothel_image_mobile['url']}'>";
    $content .= "</a>";
    endif;
    $content .= "</div>";
    $content .= "<div class='item-info'>";
    $content .= "<p class='item-info-title'><a href='$brothel->guid'>$brothel->post_title</a></p>";
    $content .= getPhoneNumber($brothel_data, $id, $k);
    $content .= "<p class='city-brothel-address'>{$brothel_data['address']}</p>";
    $content .= "<div class='city-brothel-website'>";
    if (!empty($brothel_data['website'])):
        $content .= getWebsite($brothel, $id, $brothel_data, $k);
    endif;
    $content .= "</div>";
    if ( $description != '' )
      $content .= "<div class='city-brothel-desc'>" . $description . "</div>";
    $content .= "</div>";
    $content .= "<div class='city-brothel-bottom'>";
    $content .= "<div class='city-brothel-bottom-left'>";
    $content .= "<p>Distance: <span id='";
    $content .= ($advertising_spot) ? "" : "$brothel->ID";
    $content .= "_distance_container'>";
    $content .= ($advertising_spot) ? "0km" : "<i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i></span>";
    $content .= "</p>";
    $content .= "</div>";
    $content .= "<a class='city-brothel-bottom-right' href='$brothel->guid'>View &gt;</a>";
    $content .= "</div>";
    $content .= "</div>";
    $content .= "</div>";
    return $content;
}

function get_brothels_for_page($atts)
{
    $brothels = get_post_meta($atts['id'], 'brothels_in_page', true);
    $content = "";
    foreach ($brothels as $index => $brothel):
        $brothel_image = get_field('brothel_city_image', $brothel);
        $advertising_spot = get_field('advertising_spot', $brothel);
        if ($brothel_image):
            $brothel = get_post($brothel);
            $brothel_data = get_post_meta($brothel->ID, 'brothels', true);
            $brothel_image_mobile = get_field('brothel_city_image_(mobile_ad)', $brothel);
            $brothel->guid = getBrothelUrl($brothel->guid);
            if ($advertising_spot):
                $brothel->guid = "/advertise/";
            endif;
            $k = $index + 1;
            $content .= generateAdvertisingSpot($brothel, $brothel_data, array_values($brothel_data['description'][$atts['id']])[0], $atts['id'], $k, $advertising_spot, $brothel_image, $brothel_image_mobile );
        endif;
    endforeach;
    $content .= "<script type='text/javascript'> var ajaxurl='" . admin_url('admin-ajax.php') . "',ip_addr='{$_SERVER['HTTP_X_FORWARDED_FOR']}'</script>";
    return $content;
}

function get_text_ads_for_page($atts)
{
    $brothels = get_post_meta($atts['id'], 'brothels_in_page', true);
    $content = "";
    foreach ($brothels as $index => $brothel):
        $advertising_spot = get_field('advertising_spot', $brothel);
        $brothel_image = get_field('brothel_city_image', $brothel);
        if (!$brothel_image):
            $brothel = get_post($brothel);
            if (!empty($brothel->post_title)):
                $brothel_data = get_post_meta($brothel->ID, 'brothels', true);
                $brothel->guid = getBrothelUrl($brothel->guid);
                $k = $index + 1;
                $content .= generateAdvertisingSpot($brothel, $brothel_data, array_values($brothel_data['description'][$atts['id']])[0], $atts['id'], $k, $advertising_spot);
            endif;
        endif;
    endforeach;
    $content .= "<script type='text/javascript'> var ajaxurl='" . admin_url('admin-ajax.php') . "',ip_addr='{$_SERVER['HTTP_X_FORWARDED_FOR']}'</script>";
    return $content;
}

function get_related_brothels($atts)
{
    $brothels = get_post_meta($atts['id'], 'brothels_in_page', true);
    $advertising_spot = array_search(4758, $brothels);
    if ($advertising_spot) {
        unset($brothels[$advertising_spot]);
    }
    $content_array = array();
    foreach ($brothels as $index => $brothel):
        $brothel_image = get_field('brothel_city_image', $brothel);
        if ((int)$atts['this_id'] != (int)$brothel && $brothel_image):
            $content = "";
            $advertising_spot = get_field('advertising_spot', $brothel);
            $brothel = get_post($brothel);
            $brothel_data = get_post_meta($brothel->ID, 'brothels', true);
            $brothel_image_mobile = get_field('brothel_city_image_(mobile_ad)', $brothel);
            $brothel->guid = getBrothelUrl($brothel->guid);
            $k = $index + 1;
            $content .= generateAdvertisingSpot($brothel, $brothel_data, array_values($brothel_data['description'][$atts['id']])[0], $atts['id'], $k, $advertising_spot, $brothel_image, $brothel_image_mobile );
            array_push($content_array, $content);
        endif;
    endforeach;
    while (count($content_array) <= 1):
        array_push($content_array, get_advertising_brothel(4758));
    endwhile;
    return $content_array;
}

function get_advertising_brothel($id)
{
    $content = "";
    $brothel = get_post($id);
    $advertising_spot = get_field('advertising_spot', $brothel->ID);
    $brothel_image = get_field('brothel_city_image', $brothel->ID);
    $brothel->guid = getBrothelUrl($brothel->guid);
    if ($brothel_image):
        $brothel = get_post($brothel);
        $brothel_data = get_post_meta($brothel->ID, 'brothels', true);
        $brothel_image_mobile = get_field('brothel_city_image_(mobile_ad)', $brothel);
        if ($advertising_spot):
            $brothel->guid = "/advertise/";
        endif;
        $k = $index + 1;
        $content .= generateAdvertisingSpot($brothel, $brothel_data, array_values($brothel_data['description'][$id])[0], $id, $k, $advertising_spot, $brothel_image, $brothel_image_mobile );
    endif;
    return $content;
}

function calc_dist($latitude1, $longitude1, $latitude2, $longitude2)
{
    $thet = $longitude1 - $longitude2;
    $dist = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($thet)));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $kmperlat = 111.325;
    $dist = $dist * $kmperlat;
    return ($dist < 1.0) ? number_format($dist, 2) : round($dist);
}

function get_brothels_for_front_page($brothel_array, $user_array)
{
    $content = "";
    $_index = 0;
    foreach ($brothel_array as $index => $brothel):
        $brothel = $brothel['post_id'];
        $brothel_image = get_field('brothel_city_image', $brothel);
        $advertising_spot = get_field('advertising_spot', $brothel);
        $paid_customer = get_field('paid_customer', $brothel);
        if ($brothel_image && $paid_customer):
            $brothel = get_post($brothel);
            $brothel_data = get_post_meta($brothel->ID, 'brothels', true);
            $brothel_image_mobile = get_field('brothel_city_image_(mobile_ad)', $brothel);
            $brothel->guid = str_replace("http://dev.brothels.com.au", "", $brothel->guid);
            $brothel->guid = str_replace("http://staging.brothels.com.au", "", $brothel->guid);
            if ($advertising_spot):
                $brothel->guid = "/advertise/";
            else:

            endif;
            $k = $index + 1;
            $content .= generateAdvertisingSpot($brothel, $brothel_data, '', '4057', $k, $advertising_spot, $brothel_image, $brothel_image_mobile );
            $_index++;
        endif;
        if ($_index >= 3):
            break;
        endif;
    endforeach;
    $content .= "<script type='text/javascript'> var ajaxurl='" . admin_url('admin-ajax.php') . "',ip_addr='{$_SERVER['HTTP_X_FORWARDED_FOR']}'</script>";
    return $content;
}

add_shortcode('brothels', 'get_brothels_for_page');
add_shortcode('brothels_text_ads', 'get_text_ads_for_page');
