<?php
/**
 * Inititialize functions for our Brothels dashboard plugin
 * Mostly dealing with our custom tables
 * @ Author- Che Jansen- Chillidee marketing Group
*/

  if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Create custom tables for use with the brothels dashboard
 * Runs on plugin activation
*/

function bd_create_our_tables() {
    global $wpdb;
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    $table_name = $wpdb->prefix . 'bd_transaction_table';
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
        global $charset_collate;
        // Call this manually as we may have missed the init hook
        $table_name = $wpdb->prefix . 'bd_transaction_table';
        $sql_create_table = "CREATE TABLE {$table_name} (
              transaction_id bigint(20) unsigned NOT NULL auto_increment,
              user_id bigint(20) unsigned NOT NULL,
              date bigint(20) unsigned NOT NULL,
              transaction_type varchar(10) NOT NULL,
              trans_amount int(7) NOT NULL,
              page_id int(5) unsigned NOT NULL,
              brothel_id int(5),
              pos_no tinyint(2),
              transaction_details varchar(250),
              PRIMARY KEY (transaction_id),
              KEY user_id (user_id)
         ) $charset_collate; ";
        dbDelta( $sql_create_table );
    }
    $table_name = $wpdb->prefix . 'bd_analytics';
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
          $table_name = $wpdb->prefix . 'bd_analytics';
          global $charset_collate;
          // Call this manually as we may have missed the init hook
          $sql_create_table2 = "CREATE TABLE {$table_name} (
                event_id bigint(20) unsigned NOT NULL auto_increment,
                date bigint(20) unsigned NOT NULL,
                category_id int(7) NOT NULL,
                action tinyint(2) NOT NULL,
                position tinyint(2) NOT NULL,
                label_id int(7) unsigned NOT NULL,
                PRIMARY KEY (event_id),
           ) $charset_collate; ";
        dbDelta( $sql_create_table2 );
    }
    $table_name = $wpdb->prefix . 'bd_brothels_vc';
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
       $table_name = $wpdb->prefix . 'bd_brothels_vc';
        global $charset_collate;
        // Call this manually as we may have missed the init hook
        $sql_create_table3 = "CREATE TABLE {$table_name} (
              vc_id bigint(20) unsigned NOT NULL auto_increment,
              date bigint(20) unsigned NOT NULL,
              user_id int(5) unsigned NOT NULL,
              brothel_id int(7) NOT NULL,
              name varchar(60) NOT NULL,
              address varchar(150) NOT NULL,
              phone varchar(15) NOT NULL,
              description varchar(300),
              image varchar(100),
              website varchar(100),
              landing_page varchar(100),
              PRIMARY KEY (vc_id),
         ) $charset_collate; ";

    dbDelta( $sql_create_table3 );
    }
    $table_name = $wpdb->prefix . 'bd_email_templates';
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
       $table_name = $wpdb->prefix . 'bd_email_templates';
        global $charset_collate;
        // Call this manually as we may have missed the init hook
        $sql_create_table4 = "CREATE TABLE {$table_name} (
              id bigint(20) unsigned NOT NULL auto_increment,
              name varchar(255) NOT NULL,
              html text,
              PRIMARY KEY  (id)
         ) {$charset_collate}; ";

    dbDelta( $sql_create_table4 );
    $wpdb->insert(
     $table_name,
     array(
       'name' => 'Footer',
       'html' => 'This footer will be added to at the bottom of all templates',
       ),
     array(
       '%s',
       '%s'
       )
     );
     $wpdb->insert(
      $table_name,
      array(
        'name' => 'Default',
        'html' => 'This is the default template, which will be used for all brothels unless otherwise selected',
        ),
      array(
        '%s',
        '%s'
        )
      );
    }
    $table_name = $wpdb->prefix . 'bd_reporting_stats';
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
       $table_name = $wpdb->prefix . 'bd_reporting_stats';
        global $charset_collate;
        // Call this manually as we may have missed the init hook
        $sql_create_table5 = "CREATE TABLE {$table_name} (
              id bigint(20) unsigned NOT NULL auto_increment,
              date bigint(20) unsigned NOT NULL,
              user_id int(5) unsigned NOT NULL,
              template int(5) unsigned NOT NULL,
              brothel_id int(7) unsigned NOT NULL,
              position int(7),
              website_referrals int(7),
              missed_calls int(7),
              total_calls int(7),
              email varchar(100),
              date_range varchar(100),
              PRIMARY KEY  (id)
         ) {$charset_collate}; ";

    dbDelta( $sql_create_table5 );
    }
}
class bd_init {

   public static function bd_register_transaction_table() {
       global $wpdb;
       $wpdb->bd_transaction_table = "{$wpdb->prefix}bd_transaction_table";
       $wpdb->bd_analytics = "{$wpdb->prefix}bd_analytics";
       $wpdb->bd_brothels_vc = "{$wpdb->prefix}bd_brothels_vc";
       $wpdb->bd_email_templates = "{$wpdb->prefix}bd_email_templates";
       $wpdb->bd_reporting_stats = "{$wpdb->prefix}bd_reporting_stats";
   }
}

class initApi {

    /**
         * A Unique Identifier
         */
        protected $plugin_slug;

        /**
         * A reference to an instance of this class.
         */
        private static $instance;

        /**
         * The array of templates that this plugin tracks.
         */
        protected $templates;


        /**
         * Returns an instance of this class.
         */
        public static function get_instance() {

                if( null == self::$instance ) {
                        self::$instance = new initApi();
                }

                return self::$instance;

        }

        /**
         * Initializes the plugin by setting filters and administration functions.
         */
        private function __construct() {

                $this->templates = array();


            //     Add a filter to the attributes metabox to inject template into the cache.
                add_filter(
          'page_attributes_dropdown_pages_args',
           array( $this, 'register_project_templates' )
        );


          //       Add a filter to the save post to inject out template into the page cache
                add_filter(
          'wp_insert_post_data',
          array( $this, 'register_project_templates' )
        );


                // Add a filter to the template include to determine if the page has our
        // template assigned and return it's path
       //         add_filter(
       //   'template_include',
      //    array( $this, 'view_project_template')
     //   );


                // Add your templates to this array.
                $this->templates = array(
                        'brothels-api.php'     => 'Brothels Api',
                );

        }


        /**
         * Adds our template to the pages cache in order to trick WordPress
         * into thinking the template file exists where it doens't really exist.
         *
         */

        public function register_project_templates( $atts ) {

                // Create the key used for the themes cache
                $cache_key = 'page_templates-' . md5( get_theme_root() . '/' . get_stylesheet() );

                // Retrieve the cache list.
        // If it doesn't exist, or it's empty prepare an array
                $templates = wp_get_theme()->get_page_templates();
                if ( empty( $templates ) ) {
                        $templates = array();
                }

                // New cache, therefore remove the old one
                wp_cache_delete( $cache_key , 'themes');

                // Now add our template to the list of templates by merging our templates
                // with the existing templates array from the cache.
                $templates = array_merge( $templates, $this->templates );

                // Add the modified cache to allow WordPress to pick it up for listing
                // available templates
                wp_cache_add( $cache_key, $templates, 'themes', 1800 );

                return $atts;

        }

        /**
         * Checks if the template is assigned to the page
         */
        public function view_project_template( $template ) {

                global $post;

                if ( $post ){

                if (!isset($this->templates[get_post_meta(
          $post->ID, '_wp_page_template', true
        )] ) ) {

               return $template;

                }

               return plugin_dir_path(__FILE__).'brothels-api.php';
             }
       }
}

add_action( 'plugins_loaded', array( 'initApi', 'get_instance' ) );
?>
